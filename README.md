# driverMacOs
## _WebDriver MacOs_

macosDriver is a web driver for macos, it is based on the <a href="https://www.w3.org/TR/webdriver/" target="_blank">W3C WebDriver</a> project

## Features

## Tech
macosDriver uses a number of open source projects to work properly:
- <a href="https://crowcpp.org/master/" target="_blank">crowCpp</a> Crow is a C++ framework for creating HTTP or Websocket web services
- <a href="https://think-async.com/Asio/" target="_blank">AsioCpp</a> libraries asio cpp dependence de CrowCpp
- <a href="https://pugixml.org/" target="_blank">pugixml</a> pugixml is a light-weight C++ XML processing library
- <a href="https://github.com/catchorg/Catch2" target="_blank"> Catch2 </a> Catch2 is a multi-paradigm test framework for C++
- <a href="https://github.com/nlohmann/json" target="_blank"> nlohmann/json </a> JSON for Modern C++
- <a href="https://github.com/yhirose/cpp-httplib" target="_blank"> cpp-httplib </a> A C++ header-only HTTP/HTTPS server and client library
- <a href="https://github.com/glennrp/libpng" target="_blank"> libpng </a> libpng is the official PNG reference library

## Development
### - clone src macosDriver
```sh
git clone --recurse-submodules https://gitlab.com/actiontestscript/macosdriver.git
cd linuxdriver
./clean.sh        # clean build directory
./configure.sh    # configure build directory
./build.sh        # build macosDriver
```

## Settings
### directory structure
```sh
.actiontestscript
├── drivers/
│   └── macosdriver
├── libs/
├── .atsProperties
```

### Default network settings
#### Allowed ip
default allowed ip is localhost
#### Bind address
default bind address is localhost 127.0.0.1
#### Port
the autoconfiguration port range is 9700 to 9800
- default port is 9700 for macos driver http
- default port is 9800 for macos driver websocket
- default port is 9710 for chrome driver
- default port is 9710 for firefox gecko driver
- default port is 9710 for opera driver
- default port is 9710 for ms-edge driver
- default port is 9710 for safari driver and 9711 for safari proxy nghttp2


### For remote access to macosdriver
#### Settings on the remote machine
- install macosdriver on the remote machine
- configure firewall on the remote machine to allow access to the macosdriver
- for remote access to the macosdriver, you must configure the remote machine to allow access to the macosdriver
- in command line of macosdriver :
- - execute the command : macosdriver --allowedips=\<LIST OF IP SEPARATED BY COMMA> --globalip=\<IP OF THE REMOTE MACHINE>

### Safari settings for testing on remote machine
#### Settings safari in development mode
- in safari, go to preferences
- in advanced tab, check "show develop menu in menu bar"
- in develop menu, check "allow remote automation"


#### Settings safari for remote access
- install nghttp2 on the remote machine :
  - brew install nghttp2

## Status
in development

## Support

## Roadmap

## Contributing

## Authors and acknowledgment
<a href="https://fr.agilitest.com/" target="_blank">Agilitest</a>


## License
OpenSource apache

[crowCpp]:<https://crowcpp.org/master/>
[AsioCpp]:<https://think-async.com/Asio/>
[W3C WebDriver]:<https://www.w3.org/TR/webdriver/>
[Agilitest]:<https://fr.agilitest.com/>


