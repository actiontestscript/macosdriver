#! /bin/sh
if [  -d "$HOME/.actiontestscript/drivers/" ]; then
cd $HOME/.actiontestscript/drivers/
./macosdriver
else
  if [ -d "out/build" ]; then
    cd out/build
    ./macosdriver
  fi
fi

