#include "main1.hpp"
/*
std::string Logger::filename = "log.txt";
bool Logger::loggingToFile = false;
LogLevel Logger::currentLogLevel = LogLevel::NONE;
*/
int main(int argc, char** argv) {
    Logger::setLoggingToFile(false);
    Logger::setCapture(true);
    std::string optionLogLevel="INFO";
    Logger::setLogLevel(LogLevel::INFO);
    LD_LOG_ALWAYS << "un message qui dois toujours etre afficher dans la console" << std::endl;

    LD_LOG_INFO << "un message d'info" << " avec un autre message" << " et encore un autre"<<std::endl;
    std::cout << "test de cout manuel"<<std::endl;
    LD_LOG_WARNING << "un warning";
    std::cerr << "test de cerr manuel"<<std::endl;
    LD_LOG_ERROR << "une erreur";
    LD_LOG_DEBUG << "message de debug";

    LD_LOG_DEBUG << "une autre ligne";

    LD_LOG_ERROR << "une erreur";

    Logger::setLogLevel(optionLogLevel);
    LD_LOG_INFO << "un message d'info 2";
    LD_LOG_WARNING << "un warning 2 ";

    LD_LOG_ERROR << "une erreur 2";
    LD_LOG_DEBUG << "message de debug 2";

    LD_LOG_DEBUG << "une autre ligne 2";

    LD_LOG_ERROR << "une erreur 2";

    crow::logger::setLogLevel(crow::LogLevel::DEBUG);
    crow::SimpleApp app;

    // Define the route and bind it to the handler function
    CROW_ROUTE(app, "/")
            .methods("GET"_method)
                    ([](const crow::request& req) {
                        crow::response res;
                        res.code = 201;
                        res.write("MacOs driver is started");
                        return res;
                    });

    // Start the Crow server
    app.port(8080).multithreaded().run();

//Errors errors;
//    OptionsData options;
//    OptionLineCmd optionLineCmd(argc, argv, options.getOptions(), errors);
//    Session *session = Session::getInstance();
//    XmlSettings xmlSettings;
//    ld::Config::currentPath = ld::Utils::getCurrentPath();
//    ld::Config::tmpPath = ld::Utils::getTmpPath();
//    ld::Config::argv0 = argv[0];
/*
    bool logOut{false};
    bool logErr{false};
    if (optionLineCmd.getOptionsError()) {
        int numErrors = optionLineCmd.getNumError();
        if (numErrors == 0) return EXIT_SUCCESS;
        else {
            errors.displayError(numErrors);
            return EXIT_FAILURE;
        }
    }
    LD_
    */
}