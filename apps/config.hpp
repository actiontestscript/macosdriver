#ifndef MACOSDRIVER_CONFIG_HPP
#define MACOSDRIVER_CONFIG_HPP

#define DIRSEPARATOR '/'
//#include "crow.h"
//#include "crow/logging.h"
#include "../include/utils/logger.hpp"
#include <string>
/*
#define LD_LOG_DEBUG                                                  \
    if (crow::logger::get_current_log_level() <= crow::LogLevel::Debug) \
    crow::logger(crow::LogLevel::Debug,true)

#define LD_LOG_INFO                                                  \
    if (crow::logger::get_current_log_level() <= crow::LogLevel::Info) \
    crow::logger(crow::LogLevel::Info,true)

#define LD_LOG_WARNING                                                  \
    if (crow::logger::get_current_log_level() <= crow::LogLevel::Warning) \
    crow::logger(crow::LogLevel::Warning,true)
*/
namespace ld{
/*!
 * @class Config
 * @brief The class Config
 * @details This class is used to store the configuration of the application.
 */
    class Config{

        public:
        [[maybe_unused]] static char* currentPath;                                                                      /* application Path  */
        [[maybe_unused]] static char* tmpPath;                                                                          /*> tmp path         */
        [[maybe_unused]] static char* argv0;                                                                            /*> path application */
        [[maybe_unused]] static std::string logLevelAll;                                                                /*> log level        */
        [[maybe_unused]] static std::string logType;                                                                    /*> log type                   */
        [[maybe_unused]] static std::string logWebDriverLevel;                                                          /*> log WebDriver level        */
        [[maybe_unused]] static std::string logCrowCppLevel;                                                            /*> log CrowCpp level          */

    };
}
#endif //MACOSDRIVER_CONFIG_HPP
