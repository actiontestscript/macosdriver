#include "main.hpp"

 int main(int argc, char** argv) {
     if (getuid() == 0) {
         std::cerr << "WARNING - macosdriver started in root user" << std::endl;
     }

     Errors errors;
     OptionsData options;
     OptionLineCmd optionLineCmd(argc, argv, options.getOptions(), errors);
     Session *session = Session::getInstance();
     Session::setCpuArchitecture();
     XmlSettings xmlSettings;
     ld::Config::currentPath = ld::Utils::getCurrentPath();
     ld::Config::tmpPath = ld::Utils::getTmpPath();
     ld::Config::argv0 = argv[0];
     int wsAgentPort = Session::getWsAgentPort();
     string atsLogPath= ld::Config::tmpPath;
     atsLogPath += DIRSEPARATOR;
     atsLogPath += "ats_log";
     if (!Utils::directoryExist(atsLogPath)) {
         std::string result{};
         std::string cmd = "mkdir -p " + atsLogPath;
         ld::Utils::exec(cmd.c_str(), result);
     }
     atsLogPath += DIRSEPARATOR;

     Logger::setFilename(atsLogPath + "macosdriver");
     Logger::setLoggingToFile(true);
     Logger::setCapture(true);
     Logger::setLogLevel("all");
     Logger::setLoggingToFile(false);

     if (optionLineCmd.getOptionsError()) {
         int numErrors = optionLineCmd.getNumError();
         if (numErrors == 0) return EXIT_SUCCESS;
         else {
             errors.displayError(numErrors);
             return EXIT_FAILURE;
         }
     }

     //check if macosdriver is running and kill it
     Utils::killMacOsdriver();

     if (xmlSettings.isExist()) {
         xmlSettings.loadParameters();
         optionLineCmd.setOptionsValue("bind-address", xmlSettings.getLocalIpsBinding());
         Session::setAllowedIps(xmlSettings.getAllowedIps());
         session->setRemoteHostname(xmlSettings.getRemoteHostname());
     }

     /* AllowedIps and GlobalIps */
     if (!optionLineCmd.getOptionValue("allowedips").empty()) {
         Session::setAllowedIps(optionLineCmd.getOptionValue("allowedips"));
     }
     if (!optionLineCmd.getOptionValue("globalip").empty()) {
         Session::setGlobalIps(optionLineCmd.getOptionValue("globalip"));
     }

     bool isLocal = false;
     if(Utils::isBool(optionLineCmd.getOptionValue("local"))){
         isLocal = Utils::strToBool(optionLineCmd.getOptionValue("local"));
     };
     if(!isLocal && Session::isLocal()) isLocal=true;
     Session::setIsLocal(isLocal);

     bool isSilent = false;
     if(Utils::isBool(optionLineCmd.getOptionValue("silent"))){
         isSilent = Utils::strToBool(optionLineCmd.getOptionValue("silent"));
     };
     if(isSilent) Logger::setLogLevel("silent");

    /* AllowedIps and GlobalIps */
    if (!optionLineCmd.getOptionValue("allowedips").empty()) {
        Session::setAllowedIps(optionLineCmd.getOptionValue("allowedips"));
    }
    if (!optionLineCmd.getOptionValue("globalip").empty()) {
        Session::setGlobalIps(optionLineCmd.getOptionValue("globalip"));
    }

     if( (optionLineCmd.getOptionValue("allwebdriver") == "true") ){
         //download WebDriver
         Logger::setLogLevel("all");
         isLocal=true;
         const vector<string> listDriver = {"opera","chrome", "firefox", "msedge", "brave", "opera"};
         for(const auto& driverName : listDriver){
             auto driver = DriversFactory().create<Driver>(driverName);
             driver->makePath(driver->getType("browser"));
             driver->downloadWebdriver();
         }
         return EXIT_SUCCESS;
     };

     CrowSettings crowSettings(&errors, &optionLineCmd);
     int crowPort = crowPort = crowSettings.getPort();
     string version =  to_string(MACOSDRIVER_VERSION_MAJOR) + "." + to_string(MACOSDRIVER_VERSION_MINOR) + "." + to_string(MACOSDRIVER_VERSION_PATCH);

     //check port is available
     if (Network::isPortInUse(crowSettings.getPort())) {
         //check other port available
         crowPort = Network::getAvailablePort(crowSettings.getPort());
         if (crowPort == -1) {
             errors.displayError(10);
             return EXIT_FAILURE;
         }
     }

     //wsAgent get Automatic port
     if(Network::isPortInUse(wsAgentPort) || wsAgentPort == crowPort){
         wsAgentPort += 1;
         wsAgentPort = Network::getAvailablePort(wsAgentPort);
         if( wsAgentPort == crowPort){
             wsAgentPort += 1;
             wsAgentPort = Network::getAvailablePort(wsAgentPort);
         }
         if (wsAgentPort == -1) {
             errors.displayError(10);
             return EXIT_FAILURE;
         }
         Session::setWsAgentPort(wsAgentPort);
     }


     std::vector<std::thread> crowThreads;

     std::vector<std::string> crowAddresses = crowSettings.getIps();
     session->setBindIpAddresses(crowAddresses);

     std::string logPathOut;

     if (ld::Config::logType == "CONSOLE" || ld::Config::logType == "FILE") {
         //log default on Info
         if (!ld::Config::logCrowCppLevel.empty() || ld::Config::logCrowCppLevel =="NONE" ) {
             if (ld::Config::logCrowCppLevel == "DEBUG") { crow::logger::setLogLevel(crow::LogLevel::DEBUG); }
             else if (ld::Config::logCrowCppLevel == "INFO") { crow::logger::setLogLevel(crow::LogLevel::INFO); }
             else if (ld::Config::logCrowCppLevel == "WARNING") { crow::logger::setLogLevel(crow::LogLevel::WARNING); }
             else if (ld::Config::logCrowCppLevel == "ERROR") { crow::logger::setLogLevel(crow::LogLevel::ERROR); }
             else if (ld::Config::logCrowCppLevel == "CRITICAL") {
                 crow::logger::setLogLevel(crow::LogLevel::CRITICAL);
             }
         } else if (!ld::Config::logLevelAll.empty()) {
             if (ld::Config::logLevelAll == "DEBUG") crow::logger::setLogLevel(crow::LogLevel::DEBUG);
             else if (ld::Config::logLevelAll == "INFO") crow::logger::setLogLevel(crow::LogLevel::INFO);
             else if (ld::Config::logLevelAll == "WARNING") crow::logger::setLogLevel(crow::LogLevel::WARNING);
             else if (ld::Config::logLevelAll == "ERROR") crow::logger::setLogLevel(crow::LogLevel::ERROR);
             else if (ld::Config::logLevelAll == "CRITICAL") crow::logger::setLogLevel(crow::LogLevel::CRITICAL);
         } else crow::logger::setLogLevel(crow::LogLevel::INFO);

         if(ld::Config::logType == "FILE") { Logger::setLoggingToFile(true); }
     }
     else {
         //disable log
         crow::logger::setLogLevel(crow::LogLevel::INFO);
     }

     crow::logger::setLogLevel(crow::LogLevel::INFO);

//     WebDriverProxy::Start(9705, 9720);

    int numberInstance = crowSettings.getNbrInstance();
    crowThreads.reserve(numberInstance);
for (int i = 0; i < numberInstance; i++) {
        crowThreads.emplace_back([i, &crowPort, &crowAddresses, &session]()
        {
            crow::SimpleApp app;

            //ROUTE ats-core for start
             CROW_ROUTE(app, "/isMacosDriver")
                 .methods("GET"_method)
                     ([](const crow::request& req) {
                         crow::response res;
                         ld::CrowJsonResponse::getJsonNullResponse(res);
                         return res;
                 });

            //ROUTE for test password ats-test techLinux
            CROW_ROUTE(app, "/passwd")
                .methods("POST"_method)
                        ([](const crow::request& req) {
                            crow::response res;
                            Passwd passwd(req);
                            passwd.printCapabilities(res);
                            return res;
                });

             CROW_ROUTE(app, "/")
                 .methods("GET"_method)
                     ([](const crow::request& req) {
                         crow::response res;
                         res.code = 201;
                         res.write("macos driver is started");
                         return res;
                 });


            CROW_ROUTE(app, "/start")
            .methods("GET"_method)
                ([](const crow::request& req) {
                    LD_LOG_DEBUG << "Start>request : " + req.raw_url << std::endl;
                    crow::response res;
                    AtsStartPage atsStartPage(req);
                    res.set_header("Content-Type", "text/html; charset=utf-8");
                    res.body = atsStartPage.getHtmlPage();
                   return res;
                });

             //ROUTE GET for shutdown
             CROW_ROUTE(app, "/shutdown")
             .methods("GET"_method)
                     ([]() {
                         crow::response res;
//                         SessionDelete sessionDelete;
//                         sessionDelete.delAllSession();
//                         ld::CrowJsonResponse::getJsonNullResponse(res);
                         LD_LOG_DEBUG << "Shutdown>response : " + res.body << std::endl;
                         return res;
                 });

             //ROUTE GET for shutdown
             CROW_ROUTE(app, "/stop")
             .methods("GET"_method)
                 ([]() {
                     crow::response res;
                     //ld::CrowJsonResponse::getJsonNullResponse(res);
                     CrowJsonResponse::getJsonHeaderResponse(res);
                     json j;
                     j["value"] = "ok";
                     res.code = 200;
                     res.write(j.dump());
                     LD_LOG_DEBUG << "Stop>response :" + res.body << std::endl;
                     return res;
                 });


            //ROUTE for create session
            CROW_ROUTE(app, "/session")
            .methods("POST"_method)
            ([](const crow::request& req) {
                crow::response res;
                LD_LOG_DEBUG << "Create session>json : " + req.body << std::endl;

                ld::SessionCreate sessionCreate;
                if(CrowJsonResponse::isJson(req.body)) {
                    std::string webDriver{};
                    if (sessionCreate.isOsDriverLaunch(const_cast<crow::request &>(req), webDriver)) {
                        sessionCreate.createOsDriver(const_cast<crow::request &>(req), res, webDriver);
                        return res;
                    }
                }
                sessionCreate.create(const_cast<crow::request &>(req), res);

                LD_LOG_DEBUG << "Create session>response : " + res.body << std::endl;
                return res;
            });

            //ROUTE for delete session
            CROW_ROUTE(app, "/session/<string>")
            .methods("DELETE"_method)
            ([](const crow::request& req, const std::string& sessionId) {
                crow::response res;
                LD_LOG_DEBUG << "Delete session> sessionId :" + sessionId << std::endl;
                SessionDelete sessionDelete;
                sessionDelete.delSession(sessionId, res);
                LD_LOG_DEBUG << "Delete session>response : " + res.body << std::endl ;
               return res;
            });

            //ROUTE for GET session
            CROW_ROUTE(app, "/session")
            .methods("GET"_method)
                ([](const crow::request& req) {
                    crow::response res;
                    SessionGet sessionGet;
                    sessionGet.printCapabilities(res);
                    LD_LOG_DEBUG << "session->response : " + res.body << std::endl;
                    return res;
                });

            //ROUTE for get driverStatus
            CROW_ROUTE(app, "/status")
            .methods("GET"_method)
                ([](const crow::request& req) {
                    crow::response res;
                    ld::DriverStatus driverStatus;
                    driverStatus.setRemoteAddress(req);
                    driverStatus.printCapabilities(res);
                    LD_LOG_DEBUG << "status->response : " + res.body << std::endl;
                    return res;
                });

            //ROUTE for get Profile
            CROW_ROUTE(app, "/profile")
            .methods("POST"_method)
                ([](const crow::request& req) {
                    LD_LOG_DEBUG << "Profile->request : " << req.raw_url << std::endl;
                    crow::response res;
                    AtsProfile profile;
                    profile.printProfile(req, res);
                    LD_LOG_DEBUG  << "Profile->response : " << res.body << std::endl;
                    return res;
                });

             //ROUTE POST for find Window by title or by  name
             CROW_ROUTE(app, "/window/title")
             .methods("POST"_method)
                 ([](const crow::request& req) {
                     LD_LOG_DEBUG << "window>title> "<< req.body << std::endl;
                     crow::response res;
                     WindowTitle windowTitle;
                     windowTitle.execute(req,res);
                     LD_LOG_DEBUG  << "window>title->response : " << res.body << std::endl;
                     return res;
                 });

            CROW_ROUTE(app, "/session/<string>/ats/driver/<string>")
                .methods("POST"_method)
                    ([](const crow::request& req, const string& sessionId,const string& type) {
                        crow::response res;
                        std::string driverType = type;
                        std::transform(driverType.begin(), driverType.end(), driverType.begin(), ::tolower);
                        auto driver = AtsDriverFactory::create(driverType);
                        if(driver == nullptr){
                            ld::CrowJsonResponse::getJsonNullResponse(res);
                            return res;
                        }
                        driver->execute(req, res);
                        return res;
                    });
            //ROUTE for ats recorder
            CROW_ROUTE(app, "/session/<string>/ats/recorder/<string>")
            .methods("POST"_method, "GET"_method)
                ([&session](const crow::request& req, const std::string& sessionId, const std::string& rType) {
                    LD_LOG_DEBUG << "Recorder_" << rType << "->json : " + req.body << std::endl;
                    crow::response res;
                    ld::CrowJsonResponse::getJsonHeaderResponse(res);
                    std::string sessionIdUpdate = "0162d5-f5b9-7945-568a-3504a8df43bd3e";
                    std::string recorderType = rType;
                    std::transform(recorderType.begin(), recorderType.end(), recorderType.begin(), ::tolower);
                    auto recorder = RecorderFactory().create<Recorder>(recorderType);
                    if(recorder == nullptr){
                        ld::CrowJsonResponse::getJsonNullResponse(res);
                        return res;
                    }
                    else {
                        recorder->setSessionId(sessionIdUpdate);
                        recorder->execute(req, res);
                    }
/*
                    if(recorderType != "download") {
                        LD_LOG_DEBUG << "Recorder->response : "  + res.body << std::endl;
                    }
*/
                    return res;
                });
            //ats winwdow
            CROW_ROUTE(app, "/session/<string>/ats/window/<string>")
                .methods("POST"_method)
                    ([](const crow::request& req, const string& sessionId,const string& type) {
                        crow::response res;
                        std::string windowType = type;
                        std::transform(windowType.begin(), windowType.end(), windowType.begin(), ::tolower);
                        auto windows = WindowsFactory().create<Windows>(windowType);
                        if(windows == nullptr){
                            ld::CrowJsonResponse::getJsonNullResponse(res);
                            return res;
                        }
                        windows->execute(req, res);
                        return res;
                    });

            //ats elements
            CROW_ROUTE(app, "/session/<string>/ats/element/<string>")
                    .methods("POST"_method)
                            ([](const crow::request& req, const string& sessionId,const string& type) {
                                crow::response res;
                                std::string elementType = type;
                                std::transform(elementType.begin(), elementType.end(), elementType.begin(), ::tolower);
                                std::transform(elementType.begin(), elementType.end(), elementType.begin(), ::tolower);
                                auto elements = ElementFactory().create<Element>(elementType);
                                if(elements == nullptr){
                                    ld::CrowJsonResponse::getJsonNullResponse(res);
                                    return res;
                                }
                                elements->execute(req, res);
                                return res;
                            });
        CROW_ROUTE(app, "/session/<string>/ats/keyboard/<string>")
                .methods("POST"_method)
                    ([](const crow::request& req, const string& sessionId,const string& type) {
                        crow::response res;
                        std::string keyType = type;
                        std::transform(keyType.begin(), keyType.end(), keyType.begin(), ::tolower);
                        auto keys = KeyFactory().create<Key>(keyType);
                        if(keys == nullptr){
                            ld::CrowJsonResponse::getJsonNullResponse(res);
                            return res;
                        }
                        keys->execute(req, res);
                        return res;
                    });

            CROW_ROUTE(app, "/session/<string>/ats/mouse/<string>")
                .methods("POST"_method)
                    ([](const crow::request& req, const string& sessionId,const string& type) {
                        crow::response res;
                        std::string mouseType = type;
                        std::transform(mouseType.begin(), mouseType.end(), mouseType.begin(), ::tolower);
                        auto mouse = MouseFactory().create<Mouse>(mouseType);
                        if(mouse == nullptr){
                            ld::CrowJsonResponse::getJsonNullResponse(res);
                            return res;
                        }
                        mouse->execute(req, res);
                        return res;
                    });

            //ROUTE for ats recorder
            CROW_ROUTE(app, "/session/<string>/ats/recorder/<string>/<string>")
             .methods("POST"_method)
                 ([](const crow::request& req, const std::string& sessionId, const std::string& rType , const std::string& rType2) {
                     crow::response res;
                     std::string sessionIdUpdate = "0162d5-f5b9-7945-568a-3504a8df43bd3e";
                     ld::CrowJsonResponse::getJsonHeaderResponse(res);
                     std::string recorderType = rType + rType2;
                     std::transform(recorderType.begin(), recorderType.end(), recorderType.begin(), ::tolower);
                     auto recorder = RecorderFactory().create<Recorder>(recorderType);
                     if(recorder == nullptr){
                         ld::CrowJsonResponse::getJsonNullResponse(res);
                         return res;
                     }
                     recorder->setSessionId(sessionIdUpdate);
                     recorder->execute(req, res);
                     return res;
                 });

            //ROUTE for get session timeouts
            CROW_ROUTE(app, "/session/<string>/timeouts")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    Timeouts timeouts;
                    timeouts.getTimeouts(sessionId, res);
                    return res;
                });

            //ROUTE for POST Set timeouts
            CROW_ROUTE(app, "/session/<string>/timeouts")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    Timeouts timeouts;
                    timeouts.setTimeouts(sessionId, const_cast<crow::request &>(req), res);
                    return res;
                });

            //ROUTE for POST Navigate to
            CROW_ROUTE(app, "/session/<string>/url")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonHeaderResponse(res);
                    Navigate navigate;
                    navigate.navigateTo(sessionId, const_cast<crow::request &>(req), res);
                    return res;
                });

            //ROUTE GET current url
            CROW_ROUTE(app, "/session/<string>/url")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonHeaderResponse(res);
                    Navigate navigate;
                    navigate.getCurrentUrl(sessionId, res);
                    return res;
                });

            //ROUTE for POST back
            CROW_ROUTE(app, "/session/<string>/back")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonHeaderResponse(res);
                    Navigate navigate;
                    navigate.navigateBack(sessionId, res);
                    return res;
                });

            //ROUTE POST for Forward
            CROW_ROUTE(app, "/session/<string>/forward")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonHeaderResponse(res);
                    Navigate navigate;
                    navigate.navigateForward(sessionId, res);
                    return res;
                });

            //ROUTE POST for Refresh
            CROW_ROUTE(app, "/session/<string>/refresh")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get title
            CROW_ROUTE(app, "/session/<string>/title")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Window Handle
            CROW_ROUTE(app, "/session/<string>/window")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE DELETE for Close Window
            CROW_ROUTE(app, "/session/<string>/window")
            .methods("DELETE"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Switch to Window
            CROW_ROUTE(app, "/session/<string>/window")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Window Handles
            CROW_ROUTE(app, "/session/<string>/window/handles")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for New Window
            CROW_ROUTE(app, "/session/<string>/window/new")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Switch to Frame
            CROW_ROUTE(app, "/session/<string>/frame")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Switch to Parent Frame
            CROW_ROUTE(app, "/session/<string>/frame/parent")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for get Window Rect
            CROW_ROUTE(app, "/session/<string>/window/rect")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Set Window Rect
            CROW_ROUTE(app, "/session/<string>/window/rect")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Maximize Window
            CROW_ROUTE(app, "/session/<string>/window/maximize")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Minimize Window
            CROW_ROUTE(app, "/session/<string>/window/minimize")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Fullscreen Window
            CROW_ROUTE(app, "/session/<string>/window/fullscreen")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Active Element
            CROW_ROUTE(app, "/session/<string>/element/active")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element Shadow Root
            CROW_ROUTE(app, "/session/<string>/element/<string>/shadow")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Find Element
            CROW_ROUTE(app, "/session/<string>/element")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Find Elements
            CROW_ROUTE(app, "/session/<string>/elements")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Find Element From Element
            CROW_ROUTE(app, "/session/<string>/element/<string>/element")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Find Elements From Element
            CROW_ROUTE(app, "/session/<string>/element/<string>/elements")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Find Element From Shadow Root
            CROW_ROUTE(app, "/session/<string>/element/<string>/shadow/element")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Find Elements From Shadow Root
            CROW_ROUTE(app, "/session/<string>/element/<string>/shadow/elements")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Is Element Selected
            CROW_ROUTE(app, "/session/<string>/element/<string>/selected")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element Attribute
            CROW_ROUTE(app, "/session/<string>/element/<string>/attribute/<string>")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId, const std::string& name) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element Property
            CROW_ROUTE(app, "/session/<string>/element/<string>/property/<string>")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId, const std::string& name) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element CSS Value
            CROW_ROUTE(app, "/session/<string>/element/<string>/css/<string>")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId, const std::string& propertyName) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element Text
            CROW_ROUTE(app, "/session/<string>/element/<string>/text")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element Tag Name
            CROW_ROUTE(app, "/session/<string>/element/<string>/name")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Element Rect
            CROW_ROUTE(app, "/session/<string>/element/<string>/rect")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Is Element Enabled
            CROW_ROUTE(app, "/session/<string>/element/<string>/enabled")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Computed Role
            CROW_ROUTE(app, "/session/<string>/element/<string>/computedrole")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Computed Label
            CROW_ROUTE(app, "/session/<string>/element/<string>/computedlabel")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Element Click
            CROW_ROUTE(app, "/session/<string>/element/<string>/click")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Element Clear
            CROW_ROUTE(app, "/session/<string>/element/<string>/clear")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Element Send Keys
            CROW_ROUTE(app, "/session/<string>/element/<string>/value")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Page Source
            CROW_ROUTE(app, "/session/<string>/source")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Execute Script
            CROW_ROUTE(app, "/session/<string>/execute/sync")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Execute Async Script
            CROW_ROUTE(app, "/session/<string>/execute/async")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get All Cookies
            CROW_ROUTE(app, "/session/<string>/cookie")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Named Cookie
            CROW_ROUTE(app, "/session/<string>/cookie/<string>")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& name) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Add Cookie
            CROW_ROUTE(app, "/session/<string>/cookie")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE DELETE for Delete Cookie
            CROW_ROUTE(app, "/session/<string>/cookie/<string>")
            .methods("DELETE"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& name) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE DELETE for Delete All Cookies
            CROW_ROUTE(app, "/session/<string>/cookie")
            .methods("DELETE"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Perform Actions
            CROW_ROUTE(app, "/session/<string>/actions")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE DELETE for Release Actions
            CROW_ROUTE(app, "/session/<string>/actions")
            .methods("DELETE"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Dismiss Alert
            CROW_ROUTE(app, "/session/<string>/alert/dismiss")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Accept Alert
            CROW_ROUTE(app, "/session/<string>/alert/accept")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Get Alert Text
            CROW_ROUTE(app, "/session/<string>/alert/text")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Send Alert Text
            CROW_ROUTE(app, "/session/<string>/alert/text")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Take Screenshot
            CROW_ROUTE(app, "/session/<string>/screenshot")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE GET for Take Element Screenshot
            CROW_ROUTE(app, "/session/<string>/element/<string>/screenshot")
            .methods("GET"_method)
                ([](const crow::request& req, const std::string& sessionId, const std::string& elementId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });

            //ROUTE POST for Print Page
            CROW_ROUTE(app, "/session/<string>/print")
            .methods("POST"_method)
                ([](const crow::request& req, const std::string& sessionId) {
                    crow::response res;
                    ld::CrowJsonResponse::getJsonNullResponse(res);
                    return res;
                });


            string address = crowAddresses[i];
            app.port(crowPort).bindaddr(address).multithreaded().run();
        });
    }
/*
     for (int i = 0; i < numberInstance; i++) {
         crowThreads.emplace_back([i, &crowAddresses]() {
             ld::WebDriverProxy::Start(9705, 9721, crowAddresses[i]);
         });
     }
     */
/*
     for (int i = 0; i < numberInstance; i++) {
         crowThreads.emplace_back([i, &crowAddresses]() {
             ld::WebDriverProxy::Start(9706, 9721, crowAddresses[i]);
         });
     }
*/
    //switch in console mode
//    dup2(console_stdout, STDOUT_FILENO);
//    dup2(console_stderr, STDERR_FILENO);


    Session::addMsgStartup("Starting ATS MacOs Driver " + version + " on port " + std::to_string(crowPort));
    if (xmlSettings.getAllowedIps().empty() && isLocal) {
        Session::addMsgStartup("Only local connections are allowed");
    }
     Session::addMsgStartup("macosdriver was started sucessfully");



     Logger::setMsgStartupCallback([]() {
         for(auto &msg : Session::getMsgStartup()){
             LD_LOG_ALWAYS << msg << endl;
         }
     });

    /******************* Start Websocket Agent ********************/
    WsAgent wsAgent(wsAgentPort);
    Logger::setLogCallback([&wsAgent](const std::string& message) {
     if (ld::WsAgent::IsWebSocketConnected()) {
         wsAgent.SendLog(message);
     }
    });

    wsAgent.Start();
    wsAgent.SetData(version, crowPort, wsAgentPort,isLocal);

    for(auto &msg : Session::getMsgStartup()){
        LD_LOG_ALWAYS << msg << endl;
    }

    for (auto &t : crowThreads) {
        t.join();
    }

    wsAgent.Stop();
    Logger::setLoggingToFile(false,false);
    return EXIT_SUCCESS;
}
