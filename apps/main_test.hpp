#ifndef MACOSDRIVER_MAIN_HPP
#define MACOSDRIVER_MAIN_HPP


#include "config.hpp"
#include <ixwebsocket/IXWebSocketServer.h>
#include <ixwebsocket/IXConnectionState.h>

#include "../include/crowSettings/crowSettings.hpp"
#include "../include/crowSettings/crowJsonResponse.hpp"
#include "../include/lineCmdOption/optionLineCmd.hpp"
#include "../include/lineCmdOption/options.hpp"
#include "../include/lineCmdOption/errors.hpp"
#include "../../include/W3C/driverStatus/driverStatus.hpp"
#include "../../include/W3C/session/sessionCreate.hpp"
#include "../../include/W3C/session/sessionDelete.hpp"
#include "../../include/W3C/session/sessionGet.hpp"
#include "../../include/W3C/timeouts/timeouts.hpp"
#include "../../include/W3C/window/title.hpp"
#include "../include/W3C/navigate/navigate.hpp"
#include "../include/drivers/driverFactory.hpp"
#include "../include/utils/xmlSettings.hpp"
#include "../include/utils/atsStartPage.hpp"
#include "../include/utils/atsProfile.hpp"
#include "../include/utils/network.hpp"
#include "../include/utils/utils.hpp"
#include "../include/utils/passwd.hpp"
#include "../include/utils/httpRequest.hpp"
#include "../include/utils/wsAgent.hpp"
#include "../include/ats/driver/driverFactory.hpp"
#include "../include/ats/element/elementFactory.hpp"
#include "../include/ats/key/keyFactory.hpp"
#include "../include/ats/mouse/mouseFactory.hpp"
#include "../include/ats/recorder/recorderFactory.hpp"
#include "../include/ats/windows/windowsFactory.hpp"


#include <fstream>
#include <sstream>

using namespace ld;
using json = nlohmann::json;
void stopWebSocketServer();
#endif //MACOSDRIVER_MAIN_HPP
