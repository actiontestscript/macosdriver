#! /bin/sh
PARAM=$1
CURRENT_PATH=$(pwd)
num_core=$(sysctl -n hw.ncpu)

cd external/amf3-cpp || exit
if [ ! -d "lib" ]; then
  mkdir  lib
fi
cd lib || exit

if [ "$PARAM" = "debug" ]; then
  echo "Debug mode for amf3-cpp"
  cmake -S ../ -B ./ -DCMAKE_BUILD_TYPE=Debug
else
  cmake -S ../ -B ./ -DCMAKE_BUILD_TYPE=Release
fi
make -j$num_core


cd "$CURRENT_PATH" || exit
if [ ! -d "external/libpng/build" ]; then
  mkdir -p external/libpng/build
  cd external/libpng || exit
  ./configure
  cd "$CURRENT_PATH" || exit
fi

if [ ! -d "external/libpng/lib" ]; then
  mkdir -p external/libpng/lib
fi

if [ -d "external/libpng/build" ]; then
  cd external/libpng/build || exit
if [ "$PARAM" = "debug" ]; then
  echo "Debug mode for macosdriver"
  cmake ../ -DPNG_STATIC=ON -DPNG_TESTS=OFF -DPNG_SHARED=OFF -DPNG_DEBUG=ON -DCMAKE_INSTALL_PREFIX=../lib
  #cmake ../ -DPNG_STATIC=ON -DPNG_TESTS=OFF -DPNG_SHARED=OFF -DPNG_DEBUG=ON -DCMAKE_CXX_FLAGS_DEBUG="-g -O0" -DCMAKE_INSTALL_PREFIX=../lib

else
  cmake ../ -DPNG_STATIC=ON -DPNG_TESTS=OFF -DPNG_SHARED=OFF -DPNG_DEBUG=OFF -DCMAKE_INSTALL_PREFIX=../lib
  #cmake ../ -DPNG_STATIC=ON -DPNG_TESTS=OFF -DPNG_SHARED=OFF -DPNG_DEBUG=OFF -DCMAKE_CXX_FLAGS_RELEASE="-O2" -DCMAKE_INSTALL_PREFIX=../lib
fi
make -j$num_core
make install
fi


cd "$CURRENT_PATH" || exit
if [ ! -d "out" ]; then
  mkdir -p out/build
fi
cd out/build || exit
pwd
if [ "$PARAM" = "debug" ]; then
  echo "Debug mode for macosqdriver"
  cmake -DCMAKE_BUILD_TYPE=Debug -S ../../ -B ./
else
  cmake  -S ../../ -B ./ -DCMAKE_BUILD_TYPE=Release
fi

