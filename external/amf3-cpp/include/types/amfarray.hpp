#pragma once
#ifndef AMFARRAY_HPP
#define AMFARRAY_HPP

#include <map>
#include <string>

#include "types/amfitem.hpp"
#include "utils/amfitemptr.hpp"
#include "amfbytearray.hpp"
#include <typeinfo>

namespace amf {

class SerializationContext;

class AmfArray : public AmfItem {
public:
	AmfArray() { }

	template<class V>
	AmfArray(std::vector<V> densePart) {
		for (const V& it : densePart)
			push_back(it);
	}

	template<class V, class A>
	AmfArray(std::vector<V> densePart, std::map<std::string, A> associativePart) {
		for (const V& it : densePart)
			push_back(it);

		for (const auto& it : associativePart)
			insert(it.first, it.second);
	}
/*
    template <class T>
    typename std::enable_if<std::is_base_of<AmfItem, T>::value, void>::type
    push_back(const T &item) {
        if constexpr (IsAmfBytearray<T>::value) {
            dense.emplace_back(new AmfByteArray(item, item.duplicateValue));
        } else {
            dense.emplace_back(new T(item));
        }
    }
*/
    /*
    template<class T>
    typename std::enable_if<IsAmfBytearray<T>::value, void>::type
    push_back(const T& item) {
//        push_back(const T& item, const bool& duplicateValue) {
        static_assert(std::is_base_of<AmfItem, T>::value, "Elements must extend AmfItem");
        dense.emplace_back(new AmfByteArray(item, item.duplicateValue));
    }
*/

	template<class T>
	void push_back(const T& item) {
		static_assert(std::is_base_of<AmfItem, T>::value, "Elements must extend AmfItem");
		dense.emplace_back(new T(item));
	}


    void push_back(const AmfByteArray& item) {
        static_assert(std::is_base_of<AmfItem, AmfByteArray>::value, "Elements must extend AmfItem");
        auto* byteArray = new AmfByteArray(item);
        byteArray->duplicateValue = item.duplicateValue;
        dense.emplace_back(byteArray);
    }

	template<class T>
	void insert(const std::string key, const T& item) {
		static_assert(std::is_base_of<AmfItem, T>::value, "Elements must extend AmfItem");

		associative[key] = AmfItemPtr(new T(item));
	}

	template<class T>
	T& at(int index) {
		return dense.at(index).as<T>();
	}

	template<class T>
	const T& at(int index) const {
		return dense.at(index).as<T>();
	}

	template<class T>
	T& at(std::string key) {
		return associative.at(key).as<T>();
	}

	template<class T>
	const T& at(std::string key) const {
		return associative.at(key).as<T>();
	}

	bool operator==(const AmfItem& other) const override;
	std::vector<u8> serialize(SerializationContext& ctx) const override;
	static AmfItemPtr deserializePtr(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);
	static AmfArray deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

	std::vector<AmfItemPtr> dense;
	std::map<std::string, AmfItemPtr> associative;
};

} // namespace amf

#endif
