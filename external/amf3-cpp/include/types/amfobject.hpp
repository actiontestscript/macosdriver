#pragma once
#ifndef AMFOBJECT_HPP
#define AMFOBJECT_HPP

#include <functional>
#include <map>
#include <unordered_map>
#include <utility>

#include "types/amfitem.hpp"
#include "utils/amfitemptr.hpp"
#include "utils/amfobjecttraits.hpp"

namespace amf {

class SerializationContext;

class AmfObject : public AmfItem {
public:
	AmfObject() : traits("", false, false) { }
	AmfObject(std::string className, bool dynamic, bool externalizable) :
		traits(std::move(className), dynamic, externalizable) { }

	bool operator==(const AmfItem& other) const override;
	std::vector<u8> serialize(SerializationContext& ctx) const override;

	template<class T>
	void addSealedProperty(const std::string& name, const T& value) {
		traits.addAttribute(name);
        //sealedProperties.emplace_back(name, AmfItemPtr(new T(value)));
        sealedProperties[name] = AmfItemPtr(new T(value));  //map
	}

	template<class T>
	void addDynamicProperty(const std::string& name, const T& value) {
		dynamicProperties[name] = AmfItemPtr(new T(value));
	}

	template<class T>
	T& getSealedProperty(const std::string& name) {
		if (!traits.hasAttribute(name))
			throw std::out_of_range("AmfObject::getSealedProperty");
        /*
        auto it = std::find_if(sealedProperties.begin(), sealedProperties.end(),
                               [&](const std::pair<std::string, AmfItemPtr>& pair) {
                                   return pair.first == name;
                               });
        if (it != sealedProperties.end()) {
            return it->second.template as<T>();
        }
        throw std::out_of_range("AmfObject::getSealedProperty");
        */
		return sealedProperties.at(name).as<T>();
	}

	template<class T>
	T& getDynamicProperty(const std::string& name) {
		return dynamicProperties.at(name).as<T>();
	}

	static AmfItemPtr deserializePtr(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);
	static AmfObject deserialize(v8::const_iterator& it, v8::const_iterator end, SerializationContext& ctx);

	[[nodiscard]] const AmfObjectTraits& objectTraits() const {
		return traits;
	}

	//std::map<std::string, AmfItemPtr> sealedProperties;
	//std::unordered_map<std::string, AmfItemPtr> sealedProperties;
    std::map<std::string, AmfItemPtr> sealedProperties;
//    std::vector<std::pair<std::string, AmfItemPtr>> sealedProperties;
	std::map<std::string, AmfItemPtr> dynamicProperties;
//	std::unordered_map<std::string, AmfItemPtr> dynamicProperties;

	typedef std::function<v8(const AmfObject*, SerializationContext& ctx)> Externalizer;
	Externalizer externalizer;

//    void copySealedProperties(const AmfObject& source);

    int getSealedPropertyIndex(const std::string& name) const {
        int index = 0;
        for (auto& [key, value] : sealedProperties) {
            if (key == name) {
                return index;
            }
            index++;
        }
        return -1;
    }

private:
	AmfObject(AmfObjectTraits traits) : traits(std::move(traits)) { }

	AmfObjectTraits traits;
};

} // namespace amf

#endif
