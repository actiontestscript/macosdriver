#! /bin/sh
CURRENT_PATH=$(pwd)

if [ -d "external/libpng" ]; then
cd external/libpng
#  ./configure
#  make clean
fi
cd "$CURRENT_PATH"
if [ -d "external/libpng/build" ]; then
  cd external/libpng/build
  find . -delete
fi
cd "$CURRENT_PATH"
if [ -d "external/libpng/lib" ]; then
  cd external/libpng/lib
  find . -delete
fi


cd "$CURRENT_PATH"
if [ -d "out/build" ]; then
  cd out/build
  find . -delete
fi

cd "$CURRENT_PATH"

if [ -d "external/amf3-cpp/lib" ]; then
  cd external/amf3-cpp/lib
  find . -delete
fi

