#! /bin/sh
PARAM=$1
CURRENT_PATH=$(pwd)
num_core=$(sysctl -n hw.ncpu)

if [ ! -d "out" ]; then
  mkdir -p out/build
fi

cd out/build


if [ "$PARAM" = "install" ]; then

  if [ ! -d "$HOME/.actiontestscript/drivers" ]; then
    mkdir -p $HOME/.actiontestscript/drivers
  fi
  cp ./macosdriver ~/.actiontestscript/drivers
  cp ../../.atsProperties ~/.actiontestscript/.atsProperties


elif [ "$PARAM" = "install_zip" ]; then
    if [ ! -d "$HOME/.actiontestscript/drivers" ]; then
      mkdir -p $HOME/.actiontestscript/drivers
    fi
    cp ./macosdriver ~/.actiontestscript/drivers
    cp ../../.atsProperties ~/.actiontestscript/.atsProperties
    cd ~
   zip -r actiontestscript.zip .actiontestscript/
    mv actiontestscript.zip ~/git/macosdriver/out/build/actiontestscript.zip

elif [ "$PARAM" = "tests" ]; then
  cd "$CURRENT_PATH" || exit
      if [ ! -d "out/build" ]; then
        echo "Build directory not found, please run build.sh first"
        exit 1
      else
        if [ ! -d "out/build/tests"  ]; then
          mkdir -p out/build/tests
        else
          cd out/build/tests || exit
          cmake -S ../../../tests -B ./
          make -j$num_core
        fi
      fi
else
  make -j$num_core
  cp ../../.atsProperties ../.atsProperties
fi




