#ifndef MACOSDRIVER_DRIVERSFACTORY_HPP
#define MACOSDRIVER_DRIVERSFACTORY_HPP
#include "driver.hpp"
#include "driverBrave.hpp"
#include "driverChrome.hpp"
#include "driverEdge.hpp"
#include "driverGecko.hpp"
#include "driverOpera.hpp"
#include "driverSafari.hpp"


namespace ld{
    class DriversFactory{
    public:
        /*!
         * @brief Create a Driver object
         * @details Create a Driver object
         * @param T The type of the Driver object
         * @param type The type of the Driver object
         * @return A unique pointer to the Driver object
         */
        template <typename T>
        std::unique_ptr<Driver> createDrivers() {
            std::unique_ptr<Driver> driver = std::make_unique<T>();
            return driver;
        }

        template <typename T>
        std::unique_ptr<Driver> create(const std::string& browserName) {
            if(browserName == "chrome" || browserName == "chromedriver" || browserName == "ChromeDriver"){
                auto driver = std::make_unique<Chrome>();
                return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
                //return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
            }
            else if (browserName == "firefox" || browserName == "geckodriver"|| browserName == "gecko" || browserName == "GeckoDriver"){
                auto driver = std::make_unique<Gecko>();
                return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
            }

            else if (browserName == "opera" || browserName == "operadriver"|| browserName == "OperaDriver"){
                auto driver = std::make_unique<Opera>();
                return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
            }
            else if (browserName == "edge" || browserName == "msedge" || browserName == "Microsoft Edge WebDriver"|| browserName == "msedgedriver" || browserName == "MicrosoftEdgeDriver" || browserName == "MsEdgeDriver" || browserName == "MsEdgeDriver"){
                auto driver = std::make_unique<Edge>();
                return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
            }
            else if (browserName == "brave" || browserName == "bravedriver" ){
                auto driver = std::make_unique<Brave>();
                return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
            }
            else if (browserName == "safari" || browserName == "safaridriver" ){
                auto driver = std::make_unique<Safari>();
                return std::unique_ptr<Driver>(dynamic_cast<Driver*>(driver.release()));
            }
            else {
                return nullptr;
            }
        }
    };
}

#endif //MACOSDRIVER_DRIVERSFACTORY_HPP
