#ifndef MACOSDRIVER_DRIVESAFARI_HPP
#define MACOSDRIVER_DRIVESAFARI_HPP
#include "driver.hpp"


namespace ld{
    class Safari : public Driver{
    private:
        std::string m_proxyHttp2Name="nghttpx";                                                                         /*!< The name of the http2 proxy */
        std::string m_proxyHttp2Path{};                                                                                 /*!< The path of the http2 proxy */

    public:
        /*!
         * @brief Constructor
         * @details Constructor of the CvChrome class
         */
        Safari();
        /*!
         * @brief Destructor
         * @details Destructor of the CvChrome class
         */
        ~Safari() override = default;

        /*!
         * @brief execute method override from Driver
         * @details This method is used to execute the browser. is execute parent method
         * @param <crow::response>& res
         * @return <bool> True if the browser is executed, false otherwise
         */
        bool execute(crow::response& res) override;

        /*!
         * @brief deleteDriver method override from Driver
         * @details This method is used to delete the browser. is execute parent method
         * @param <crow::response>& res
         * @return <bool> True if the browser is deleted, false otherwise
         */
        void deleteDriver(crow::response& res, std::string& port) override  { Driver::deleteDriver(res, port); };

        /*!
         * @brief checkTools method override from Driver
         * @details This method is used to check if the macOs tools are installed. is execute parent method
         * @param <crow::response>& res
         */
        bool checkTools(crow::response& res) override  { return Driver::checkTools(res); };

        /*!
         * @brief isInProgress method override from Driver
         * @details This method is used to check if the browser is in progress.
         * @return <bool> True if the browser is in progress, false otherwise
         */
        bool isInProgress() override {return Driver::isInProgress(); };

        /*!
         * @brief isInstalled method virtual from Driver
         * @details This method is used to check if the browser is installed.
         * @return <bool> True if the browser is installed, false otherwise
         */
        bool isInstalled() override {return Driver::isInstalled(); };

        /*!
         * @brief getBrowserVersion method virtual from Driver
         * @details This method is used to get the browser version.
         */
        std::string getBrowserVersion(const bool& fullVersion) override;

        /*!
         * @brief getWebDriverVersion method virtual from Driver
         * @details This method is used to get version of the web driver.
         */
        std::string getWebDriverVersion(const bool& fullVersion) override;

        /*
         * @brief getIconBrowserBase64 method virtual from Driver
         * @details This method is used to get the icon of the browser.
         * @return <std::string> The icon of the browser in base64
         */
        std::string getIconBrowserBase64() override {return Driver::getIconBrowserBase64(); };


        /*!
         * @brief downloadWebdriver method virtual from Driver
         * @details This method is used to download the web driver.
         */
        bool downloadWebdriver() override {return Driver::downloadWebdriver(); };

        /*!
         * @brief getLocalListIpsBind method virtual from Driver
         * @details This method is used to get the list of local ips bind. is execute parent method
         */
        std::string getLocalListIpsBind() override {return Driver::getLocalListIpsBind();  };

        /*!
         * @brief setLocalListIpsBind method virtual from Driver
         * @details This method is used to set the list of local ips bind.
         */
        bool setLocalListIpsBind(const std::vector<std::string>& localListIpsBind) override {return Driver::setLocalListIpsBind(localListIpsBind); }

        /*!
         * @brief getWhiteListIps method virtual from Driver
         * @details This method is used to get the list of white ips bind. is execute parent method
         */
        std::string getWhiteListIps() override  {return Driver::getWhiteListIps(); };

        /*!
         * @brief setWhiteListIps method virtual from Driver
         * @details This method is used to set the list of white ips bind.
         */
        bool setWhiteListIps(const std::vector<std::string>& whiteListIps) override {return Driver::setWhiteListIps(whiteListIps); };

        /*!
         * @brief makeUrl method virtual from Driver
         * @details This method is used to make the url to execute the browser. is execute parent method
         */
        void makeUrl() override  {Driver::makeUrl(); };

        /*!
         * @brief startDriver method virtual from Driver
         * @details This method is used to start the browser. is execute parent method
         */
        bool startDriver() override ;

        /*!
        * @brief stopDriver method virtual from Driver
        * @details This method is used to stop the browser. is execute parent method
        */
        bool stopDriver() override {return Driver::stopDriver(); };

        /*!
         * @brief printCapabilities method virtual from Driver
         * @details This method is used to send the url response for the browser. is execute parent method
         */
        void printCapabilities(crow::response& res) override { Driver::printCapabilities(res);};

        /*!
         * @brief makeDriverArgslineCmd method virtual from Driver
         * @details This method is used to make the args line command for the browser.
         */
        void makeDriverArgslineCmd() override { Driver::makeDriverArgslineCmd(); };

        /*!
          * @brief makePath method virtual from Driver
          * @details This method is used to make the browser path or driver. is execute parent method
          * @param const <type>& t
          */
        void makePath(const type& t)override;

        /*!
         * @brief setSessionId method virtual from Drivers
         * @details This method is used to set the session id. is execute parent method
         * @param const <std::string>& sessionId
         */
        void setSessionId(const std::string& sessionId) override {Driver::setSessionId(sessionId);};

        /*!
         * @brief getSessionId method virtual from Drivers
         * @details This method is used to get the session id. is execute parent method
         * @return <std::string> session id
         */
        std::string getSessionId() override {return Driver::getSessionId();};

        /*!
         * @brief getDriverPort method virtual from Drivers
         * @details This method is used to get the driver port. is execute parent method
         * @return <int> driver port
         */
        int getDriverPort() override {return Driver::getDriverPort();};

        /*!
         * @brief getDriverName method virtual from Drivers
         * @details This method is used to get the driver name. is execute parent method
         * @return <std::string> driver name
         */
        std::string getDriverName() override {return Driver::getDriverName();};

        /*!
         * @brief getDriverProcessName method virtual from Drivers
         * @details This method is used to get the driver name. is execute parent method
         * @return <std::string> driver name
         */
        std::string getDriverProcessName() override {return Driver::getDriverProcessName();};

        /*!
         * @brief getDriverPath method virtual from Drivers
         * @details This method is used to get the driver path. is execute parent method
         * @return <std::string> driver path
         */
        std::string getDriverPath() override {return Driver::getDriverPath();};

        /*!
         * @brief getDriverUrl method virtual from Drivers
         * @details This method is used to get the url. is execute parent method
         * @return <std::string> url
         */
        std::string getDriverUrl() override {return Driver::getDriverUrl();};

        /*!
         * @brief getBrowserName method virtual from Drivers
         * @details This method is used to get the browser name. is execute parent method
         * @return <std::string> browser name
         */
        std::string getBrowserName() override {return Driver::getBrowserName();};

        /*!
         * @brief getBrowserPath method virtual from Driver
         * @details This method is used to get the path of the browser.
         * @return <std::string> The Path of the browser
         */
        std::string getBrowserPath() override {return Driver::getBrowserPath();};

        /*!
         * @brief getRemoteIp method virtual from Drivers
         * @details This method is used to get the remote ip. is execute parent method
         * @return <std::string> remote ip
         */
        std::string getRemoteIp() override {return Driver::getRemoteIp();};

        /*!
         * @brief getWebDriverOffsetX method virtual from Drivers
         * @details This method is used to get the offset x. is execute parent method
         * @return <double> offset x
         */
        double getWebDriverOffsetX() override {return Driver::getWebDriverOffsetX();};

        /*!
         * @brief getWebDriverOffsetY method virtual from Drivers
         * @details This method is used to get the offset x. is execute parent method
         * @return <double> offset y
         */
        double getWebDriverOffsetY() override {return Driver::getWebDriverOffsetY();};

        /*!
         * @brief getWebDriverHeadlessOffsetX method virtual from Drivers
         * @details This method is used to get the offset x. is execute parent method
         * @return <double> offset x
         */
        double getWebDriverHeadlessOffsetX() override {return Driver::getWebDriverHeadlessOffsetX();};

        /*!
         * @brief getWebDriverHeadlessOffsetY method virtual from Drivers
         * @details This method is used to get the offset x. is execute parent method
         * @return <double> offset y
         */
        double getWebDriverHeadlessOffsetY() override {return Driver::getWebDriverHeadlessOffsetY();};

        /*!
         * @brief setRemoteIp method virtual from Drivers
         * @details This method is used to set the remote ip. is execute parent method
         * @param const <std::string>& remoteIp
         */
        void setRemoteIp(const std::string& remoteIp) override {Driver::setRemoteIp(remoteIp);};

        /*!
         * @brief setDriverRemoteHeaderUrl method virtual from Drivers
         * @details This method is used to set the url. is execute parent method
         * @param const <std::string>& driverUrl
         */
        void setDriverRemoteHeaderUrl(const std::string& headerHostUrl) override { Driver::setDriverRemoteHeaderUrl(headerHostUrl);};

        /*!
         * @brief getJsonOptionsApplicationPath method virtual from Drivers
         * @details This method is used to get the json options application path. is execute parent method
         * @return <std::string> json options application path
         */
        bool getJsonOptionsApplicationPath() override {return Driver::getJsonOptionsApplicationPath();};

        /*!
         * @brief setJsonOptionsApplicationPath method virtual from Drivers
         * @details This method is used to set the json options application path. is execute parent method
         * @param const <bool>& jsonOptionsApplicationPath
         */
        void setJsonOptionsApplicationPath(const bool& jsonOptionsApplicationPath) override {Driver::setJsonOptionsApplicationPath(jsonOptionsApplicationPath);};

        /*!
         * @brief getAlternativeApplicationPath method virtual from Drivers
         * @details This method is used to get the alternative application path. is execute parent method
         * @return <std::string> alternative application path
         */
        std::string getAlternativeApplicationPath() override {return Driver::getAlternativeApplicationPath();};

        /*!
         * @brief setAlternativeApplicationPath method virtual from Drivers
         * @details This method is used to set the alternative application path. is execute parent method
         * @param const <std::string>& alternativeApplicationPath
         */
        void setAlternativeApplicationPath(const std::string& alternativeApplicationPath) override {Driver::setAlternativeApplicationPath(alternativeApplicationPath);};

        /*!
         * @brief isBrowserWebDriverVersionMatch method
         * @details This method is used to check if the browser and web driver version match.
         * @return <bool> True if the browser and web driver version match, false otherwise
         */
        bool isBrowserWebDriverVersionMatch(const std::string& browserVersion,const std::string& webDriverVersion) override {return Driver::isBrowserWebDriverVersionMatch(browserVersion, webDriverVersion);};

        bool proxyHttp2IsInstalled();

        bool proxyHttp2IsRunning();

    };
}
#endif //MACOSDRIVER_DRIVESAFARI_HPP
