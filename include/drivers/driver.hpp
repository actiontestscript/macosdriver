#ifndef MACOSDRIVER_DRIVER_HPP
#define MACOSDRIVER_DRIVER_HPP
#include "../../apps/config.hpp"
#include "../crowSettings/crowJsonResponse.hpp"
#include "../crowSettings/crowSettings.hpp"
#include "../W3C/session/session.hpp"
#include "../utils/utils.hpp"
#include <set>
#include "crow.h"

using json = nlohmann::json;

namespace ld{
    class Driver{

    protected:
        Session* m_session{};
        std::string m_sessionId{};
        int m_defaultPort{};                                                                                        /*!< The default port of the driver */
        int m_PortRangeStart{9700};                                                                                 /*!< The port range start */
        int m_PortRangeEnd{10000};                                                                                  /*!< The port range end */
        int m_port{};                                                                                               /*!< The port of the driver */
        std::string m_driverName{};                                                                                 /*!< The name of the driver */
        std::string m_driverPath{};                                                                                 /*!< The path of the driver */
        std::string m_driverProcessName{};                                                                          /*!< The name of the driver process */
        std::string m_driverArgslineCmd{};                                                                          /*!< The args line cmd of the driver */
        std::string m_browserPath{};                                                                                /*!< The path of the browser */
        std::string m_browserName{};                                                                                /*!< The name of the browser */
        std::string m_browserVersion{};                                                                             /*!< The version of the browser */
        std::string m_browserBundleIdentifier{};
        std::string m_url{};                                                                                        /*!< The url to access to the driver */
        std::string m_whiteListIps{};                                                                               /*!< The list of white ips separate , */
        std::string m_localListIpsBind{};                                                                           /*!< The list of local ips bind separate , */
        std::vector<std::string> m_localListIps{};                                                                  /*!< The list of local ips */
        std::string m_response{};                                                                                   /*!< The response of exec cmd */
        std::string m_downloadUrl{"https://actiontestscript.com/releases/ats-drivers/"};                   /*!< The url to download the driver */
        std::string m_remoteIp{};                                                                                   /*!< The remote ip of the client */
        std::string m_remoteHeaderHostUrl{};                                                                        /*!< The remote header host url of the client */
        bool m_jsonOptionsApplicationPath{false};                                                                   /*!< The flag to check if the json options has the application path */
        std::string m_alternativeApplicationPath{};                                                                 /*!< The alternative application path */
        bool m_testDownload{false};                                                                                 /*!< The flag to check if the driver is downloaded */
        double m_webDriverOffsetX{0.0};                                                                             /*!< The offset x of the web driver */
        double m_webDriverOffsetY{0.0};                                                                             /*!< The offset y of the web driver */
        double m_webDriverHeadlessOffsetX{0.0};                                                                     /*!< The offset x of the web driver */
        double m_webDriverHeadlessOffsetY{0.0};                                                                     /*!< The offset y of the web driver */
        std::string m_iconBrowserBase64{};                                                                                /*!< The icon of the browser in base64*/

    public:
        enum class type{BROWSER , DRIVER };                                                                          /*!< The type of the driver */

        static type getType(const std::string& strType){
            if(strType == "browser") return type::BROWSER;
            return type::DRIVER;
        }
        const std::string MAC_OS_PATH_X86_64 {"macos/"};                                                                      /*!< The path of the mac os x86_64 */
        const std::string MAC_OS_PATH_ARM64 {"macos_arm/"};                                                                        /*!< The path of the mac os arm64 */
        bool isResponseExecuted{false};                                                                              /*!< The flag to check if the res is executed */

        Driver();
        /*!
         * @brief Destructor
         * @details Destructor of the Driver class
         */
        virtual ~Driver() = default;

        /*!
         * @brief execute method virtual from Driver
         * @details This method is used to execute the driver.
         * @param <crow::response&> res
         * @return <bool> True if the driver is executed, false otherwise
         */
        virtual bool execute(crow::response& res)=0;

        /*!
         * @brief deleteDriver method virtual from Driver
         * @details This method is used to delete the driver.
         */
        virtual void deleteDriver(crow::response& res,std::string& port )=0;

        /*!
         * @brief checkTools method virtual from Driver
         * @details This method is used to check if the macOS tools are installed.
         */
        virtual bool checkTools(crow::response& res)=0;

        /*!
         * @brief isInProgress method virtual from Driver
         * @details This method is used to check if the driver is in progress.
         * @return <bool> True if the driver is in progress, false otherwise
         */
        virtual bool isInProgress()=0;

        /*!
         * @brief isInstalled method virtual from Driver
         * @details This method is used to check if the driver is installed.
         * @return <bool> True if the driver is installed, false otherwise
         */
        virtual bool isInstalled()=0;


        /*!
         * @brief getBrowserVersion method virtual from Driver
         * @details This method is used to get version of the browser.
         */
        virtual std::string getBrowserVersion(const bool& fullVersion)=0;

        /*!
         * @brief getWebDriverVersion method virtual from Driver
         * @details This method is used to get version of the web driver.
         */
        virtual std::string getWebDriverVersion(const bool& fullVersion)=0;

        /*!
         * @brief downloadWebdriver method virtual from Driver
         * @details This method is used to download the web driver.
         */
        virtual bool downloadWebdriver()=0;

        /*!
         * @brief getLocalListIpsBind method virtual from Driver
         * @details This method is used to get the list of local ips bind.
         */
        virtual std::string getLocalListIpsBind()=0;

        /*!
         * @brief setLocalListIpsBind method virtual from Driver
         * @details This method is used to set the list of local ips bind.
         */
        virtual bool setLocalListIpsBind(const std::vector<std::string>& localListIpsBind)=0;

        /*!
         * @brief getWhiteListIps method virtual from Driver
         * @details This method is used to get the list of white ips.
         */
        virtual std::string getWhiteListIps()=0;

        /*!
         * @brief setWhiteListIps method virtual from Driver
         * @details This method is used to set the list of white ips.
         */
        virtual bool setWhiteListIps(const std::vector<std::string>& whiteListIps)=0;

        /*!
         * @brief makeUrl method virtual from Driver
         * @details This method is used to make the url of the driver.
         */
        virtual void makeUrl()=0;

        /*!
         * @brief startDriver method virtual from Driverr
         * @details This method is used to start the driver.
         */
        virtual bool startDriver() = 0;

        /*!
         * @brief stoptDriver method virtual from Driverr
         * @details This method is used to stop the driver.
         */
        virtual bool stopDriver() = 0;


        /*!
         * @brief printCapabilities method virtual from Driver
         * @details This method is used to send the url of the driver.
         */
        virtual void printCapabilities(crow::response& res)=0;

        /*!
         * @brief makeDriverArgslineCmd method virtual from Driver
         * @details This method is used to make the args line cmd of the driver.
         */
        virtual void makeDriverArgslineCmd()=0;

        /*!
         * @brief makePath method virtual from Driver
         * @details This method is used to make the path of the browser.
         */
        virtual void makePath(const type& t)=0;

        /*!
         * @brief setSessionId method virtual from Driver
         * @details This method is used to set the session id of the driver.
         * @param const <std::string>& sessionId
         */
        virtual void setSessionId(const std::string& sessionId)=0;

        /*!
         * @brief getSessionId method virtual from Driver
         * @details This method is used to get the session id of the driver.
         * @return <std::string> The session id of the driver
         */
        virtual std::string getSessionId()=0;

        /*!
         * @breif getDriverPort method virtual from Driver
         * @details This method is used to get the port of the driver.
         * @return <int> The port of the driver
         */
        virtual int getDriverPort()=0 ;

/*!
         * @brief getDriverName method virtual from Driver
         * @details This method is used to get the name of the driver.
         * @return <std::string> The name of the driver
         */
        virtual std::string getDriverName()=0;

        /*!
         * @brief getDriverProcessName method virtual from Driver
         * @details This method is used to get the Process name of the driver.
         * @return <std::string> The name of the driver
         */
        virtual std::string getDriverProcessName()=0;



        /*!
         * @brief getDriverPath method virtual from Driver
         * @details This method is used to get the path of the driver.
         * @return <std::string> The path of the driver
         */
        virtual std::string getDriverPath()=0;

        /*!
         * @brief getDriverUrl method virtual from Driver
         * @details This method is used to get the url of the driver.
         * @return <std::string> The url of the driver
         */
        virtual std::string getDriverUrl()=0;

        /*!
         * @brief getBrowserName method virtual from Driver
         * @details This method is used to get the name of the browser.
         * @return <std::string> The name of the browser
         */
        virtual std::string getBrowserName()=0;

        /*!
         * @brief getBrowserPath method virtual from Driver
         * @details This method is used to get the path of the browser.
         * @return <std::string> The Path of the browser
         */
        virtual std::string getBrowserPath()=0;


        /*!
         *  @brief getRemoteIp method virtual from Driver
         *  @details This method is used to get the remote ip of the client.
         *  @return <std::string> The remote ip of the client
         */
        virtual std::string getRemoteIp()=0;

        /*!
         * @brief getWebDriverOffsetX method virtual from Driver
         * @details This method is used to get the offset x of the web driver.
         * @return <double> The offset x of the web driver
         */
        virtual double getWebDriverOffsetX()=0;

        /*!
         * @brief getWebDriverOffsetY method virtual from Driver
         * @details This method is used to get the offset y of the web driver.
         * @return <double> The offset y of the web driver
         */
        virtual double getWebDriverOffsetY()=0;

        /*!
         * @brief getWebDriverOffsetX method virtual from Driver
         * @details This method is used to get the offset x of the web driver.
         * @return <double> The offset x of the web driver
         */
        virtual double getWebDriverHeadlessOffsetX()=0;

        /*!
         * @brief getWebDriverOffsetY method virtual from Driver
         * @details This method is used to get the offset y of the web driver.
         * @return <double> The offset y of the web driver
         */
        virtual double getWebDriverHeadlessOffsetY()=0;

        /*!
         * @brief getIconBrowserBase64 method virtual from Driver
         * @details This method is used to get the icon of the browser.
         * @return <std::string> The icon of the browser in base64
         */
        virtual std::string getIconBrowserBase64()=0;

        /*!
         * @brief setRemoteIp method virtual from Driver
         * @details This method is used to set the remote ip of the client.
         * @param const <std::string>& remoteIp
         */
        virtual void setRemoteIp(const std::string& remoteIp)=0;

        /*!
         * @brief setDriverRemoteHeaderUrl method virtual from Driver
         * @details This method is used to set the url of the driver.
         * @param const <std::string>& driverUrl
         */
        virtual void setDriverRemoteHeaderUrl(const std::string& headerHostUrl)=0;

        /*!
         * @brief getJsonOptionsApplicationPath method virtual from Driver
         * @details This method is used to get the json options application path.
         * @return <bool> True if the json options application path is set, false otherwise
         */
        virtual bool getJsonOptionsApplicationPath()=0;

        /*!
         * @brief setJsonOptionsApplicationPath method virtual from Driver
         * @details This method is used to set the json options application path.
         * @param const <bool>& jsonOptionsApplicationPath
         */
        virtual void setJsonOptionsApplicationPath(const bool& jsonOptionsApplicationPath)=0;

        /*!
         * @brief getAlternativeApplicationPath method
         * @details This method is used to get the alternative application path.
         * @return <std::string> The alternative application path
         */
        virtual std::string getAlternativeApplicationPath()=0;

        /*!
         * @brief setAlternativeApplicationPath method
         * @details This method is used to set the alternative application path.
         * @param const <std::string>& alternativeApplicationPath
         */
        virtual void setAlternativeApplicationPath(const std::string& alternativeApplicationPath)=0;

        /*!
         * @brief isBrowserWebDriverVersionMatch method
         * @details This method is used to check if the browser and web driver version match.
         * @return <bool> True if the browser and web driver version match, false otherwise
         */
        virtual bool isBrowserWebDriverVersionMatch(const std::string& browserVersion,const std::string& webDriverVersion)=0;

        /*!
         * @brief execDriver method
         * @details This method is used to execute a command
         * @param const <char*> cmd  -- The command to be executed
         * @param <std::string>& result -- The output of the command
         * @param <std::string>& browserName -- The name of the browser
         * @param <int>& port  -- The port of the browser
         * @param <std::string>& url -- The url of the browser
         * @return <bool> True if the command is executed, false otherwise
         */
        static bool execDriver(const char* cmd, std::string& result, const std::string& browserName , int& port, std::string url, std::atomic<bool>& isRunning, bool logFile = false, const std::string& logFileStd = "" )  {
            std::regex STARTED_REGEXP(".*was started successfully(.*)");
            std::regex GECKO_STARTED_REGEXP(".*Listening on (.*):(.*)");
            std::regex START_ERROR_REGEXP(".*port not available. Exiting.*");
            std::regex GECKO_START_ERROR_REGEXP(".*geckodriver.exe: error:(.*)");

            std::array<char, 128> buffer{};

            std::string command = cmd;
            command += " 2>&1";                             // redirect stderr to stdout for fgets while loop

            LD_LOG_DEBUG << "cmd exec driver: " << command << std::endl;

            std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(command.c_str(), "r"), pclose);

            if (pipe == nullptr) {
                LD_LOG_ERROR << "error -1 popen() failed!" << std::endl;
                result = "error -1 popen() failed!";
                return false;
            }

           Session* session = Session::getInstance();
           Session::ProcessInfo info;

           //PID of the process
           info.command = cmd;
           info.url = std::move(url);
           info.port = std::to_string(port);

           //store process info in vector
            std::unique_lock<std::mutex> lock(ld::Session::m_mutex);
           //std::lock_guard<std::mutex> lock(ld::Session::m_mutex);
                session->m_processes[browserName] = info;
            lock.unlock();

            //file log driver
            std::ofstream outFile;
            std::regex FILE_REGEX("(.*\\/)(.*\\.log)$");
            std::string replacement = "$1ld_$2$3";
            std::string bufferContent;
            bool isStarted = false;
            if ( logFile && !logFileStd.empty()){
                std::string logFileStd_ld;
                logFileStd_ld = std::regex_replace(logFileStd, FILE_REGEX, replacement);

                //delete file if exist
                try {
                    std::remove(logFileStd_ld.c_str());
                }
                catch (const std::exception& e) {
                    LD_LOG_ERROR << "error remove file: " << e.what() << std::endl;
                }
                if(browserName =="safari"){
                    isRunning.store(true);
                    isStarted = true;
                }

                while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
                    if(browserName =="safari"){
                        isRunning.store(true);
                        isStarted = true;
                    }

                    outFile.open(logFileStd_ld, std::ofstream::app);
                    if(!isStarted) {
                        bufferContent = buffer.data();

                        if (std::regex_search(bufferContent, STARTED_REGEXP) ||
                            std::regex_search(bufferContent, GECKO_STARTED_REGEXP)) {
                            isRunning.store(true);
                            isStarted = true;
                           // LD_LOG_DEBUG << "startDriver :  " << bufferContent << std::endl;
   //                         LD_LOG_DEBUG << "startDrivernif :  " << browserName << " started successfully" << std::endl;
                        }
                        else if (std::regex_search(bufferContent, START_ERROR_REGEXP) ||
                                   std::regex_search(bufferContent, GECKO_START_ERROR_REGEXP)){
     //                       LD_LOG_DEBUG << "startDriver elseif :  " << bufferContent << std::endl;
                        return false;
                        }
                    }
                    outFile << buffer.data();
                    result.append(buffer.data());
                    if(outFile.is_open()) outFile.close();
                }
            }
            else {
                if(browserName =="safari"){
                    isRunning.store(true);
                    isStarted = true;
                }

                while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
                    bufferContent = buffer.data();

                    if (std::regex_search(bufferContent, STARTED_REGEXP) ||
                        std::regex_search(bufferContent, GECKO_STARTED_REGEXP)) {
                        isRunning.store(true);
                        isStarted = true;
//                        LD_LOG_DEBUG << "startDriver :  " << bufferContent << std::endl;
//                        LD_LOG_DEBUG << "startDriver :  " << browserName << " started successfully" << std::endl;
                    }
                    else if (std::regex_search(bufferContent, START_ERROR_REGEXP) ||
                             std::regex_search(bufferContent, GECKO_START_ERROR_REGEXP)){
//                        LD_LOG_INFO << "startDriver :  " << bufferContent << std::endl;
                        return false;
                    }
                }
                    result.append(buffer.data());
            }
            return true;
        }
    };
}
#endif //MACOSDRIVER_DRIVER_HPP
