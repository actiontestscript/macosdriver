#ifndef MACOSDRIVER_CROWJSONRESPONSE_HPP
#define MACOSDRIVER_CROWJSONRESPONSE_HPP

#include "../../apps/config.hpp"
#include <nlohmann/json.hpp>
#include "crow.h"
#include "crow/logging.h"
#include "../utils/utils.hpp"
#include <map>


using json = nlohmann::json;


namespace ld{
    class CrowJsonResponseDataErrorCode{
    public:
        struct ErrorCode{
            int httpCode;
            std::string jsonErrorCode;
            std::string messageErrorCode;
        };
        CrowJsonResponseDataErrorCode();
        std::map<std::string, ErrorCode> getErrorMap(){return m_errorMap;};

    private:
        std::map<std::string, ErrorCode> m_errorMap;
    };

    /*!
     * @class CrowJsonResponse
     * @brief class CrowJsonResponse
     * @details  CrowJsonResponse is used to manage json response to client.
     */
    class CrowJsonResponse {


        private:
        /*!
         * @brief CrowJsonResponseDataErrorCode
         * @details
         * @see https://www.w3.org/TR/webdriver/#handling-errors
         */
        CrowJsonResponseDataErrorCode m_lisErrorCodes;


    protected:
        /*!
         * @brief _jsonMsgError
         * @details
         * This method is used to build json error return.
         * @return json error return.
         */
        json _jsonMsgError();

        /*!
         * @brief _setMsgErrorCode
         * @details
         * This method is used to set error code.
         * @param const <string>& errorStrCode
         * @param const <string>& errorMessage
         * @param const <string>& stackTrace
         */
        void _setMsgErrorCode(const std::string& errorStrCode,const std::string& errorMessage, const std::string& stackTrace);

        /*!
         * @brief _isValidJson
         * @details
         * This method is used to check if the json input is valid.
         * @param const <string>& body The json body.
         * @return true if the json is valid, false otherwise and set error code.
         */
        bool _isValidJson(const std::string& body);

    public:
        enum class ResponseType{
            JSON,
            AMF
        };

        struct MsgError{
            std::string error;
            std::string message;
            std::string stacktrace;
            void clear(){
                error.clear();
                message.clear();
                stacktrace.clear();
            }
        };
        /*!
         * @@brief getErrorCode
         * @details This method is used to get error code.
         * @param const <string>& errorCode
         * @return <CrowJsonResponseDataErrorCode::ErrorCode> error code
         */
        CrowJsonResponseDataErrorCode::ErrorCode getErrorCode (const std::string& errorCode);

        /*!
         * @brief setMsgErrorCode
         * @details This method is used to set error code.
         * @param const <string>& errorStrCode
         * @param const <string>& errorMessage
         * @param const <string>& stackTrace
         */
        void setMsgErrorCode(const std::string& errorStrCode,const std::string& errorMessage, const std::string& stackTrace){ _setMsgErrorCode(errorStrCode,errorMessage,stackTrace);};

        /*!
         * @brief jsonMsgError
         * @details This method is used to build json error return.
         * @return json error return.
         */
        json jsonMsgError(){return _jsonMsgError();};

        /*!
         * @brief isValidJson
         * @details This method is used to check if the json input is valid with MsrErrorCode set.
         * @param const <string>& body The json body.
         * @return true if the json is valid, false otherwise and set error code.
         */
        bool isValidJson(const std::string& body){return _isValidJson(body);};

        bool m_error {false};                       /*!< error flag */
        std::string m_errorMessage;                 /*!< error message */
        std::string m_stackTrace;                   /*!< stack trace */
        int m_errorCode{0};                        /*!< error code */

        /*!
         * @brief isJson
         * @details This method is used to check if the string is a json.
         * @param const <string>& body The json body.
         * @return true if the string is a json, false otherwise.
         */
        static bool isJson(const std::string& body);

        /*!
         * @brief getJsonHeaderResponse
         * @details This method is used to get json header response.
         * @param crow::response& res passed by reference
         */
        static void getJsonHeaderResponse(crow::response& res){
            res.set_header("Content-Type", "application/json; charset=utf-8");
            res.set_header("Cache-Control", "no-cache, no-store, must-revalidate");
            res.set_header("Pragma", "no-cache");
            res.set_header("Expires", "0");
        }

        /*!
         * @brief getJsonNullResponse
         * @details This method is used to get json null response.
         * @param crow::response& res passed by reference
         */
        static void getJsonNullResponse(crow::response& res){
            CrowJsonResponse::getJsonHeaderResponse(res);
            json j;
            j["value"] = nullptr;
            res.code = 200;
            res.write(j.dump());
        }

        /*!
         * @brief setDataFromString
         * @details This method is used to set data from string.
         * @param const <string>& dataStr The string data.
         * @param T& data The data passed by reference.
         * @tparam T The type of data.
         * @return true if the string is a json, false otherwise.
         */
        template<typename T>
        static void setDataFromString(const std::string& dataStr, T& data){
            if constexpr(std::is_same_v<T, std::string>){ data = ( (!dataStr.empty()) ? dataStr : data  ); }
            else if constexpr(std::is_same_v<T, int>){ data = ( (ld::Utils::isInt(dataStr)) ? std::stoi(dataStr) : data  ); }
            else if constexpr(std::is_same_v<T, long>){ data = ( (ld::Utils::isLong(dataStr)) ? std::stol(dataStr) : data  ); }
            else if constexpr(std::is_same_v<T, double>){ data = ( (ld::Utils::isDouble(dataStr)) ? std::stod(dataStr) : data  ); }
            else if constexpr(std::is_same_v<T, float>){ data = ( (ld::Utils::isFloat(dataStr)) ? std::stof(dataStr) : data  ); }
            else if constexpr(std::is_same_v<T, bool>){
                if( ld::Utils::isBool(dataStr)){
                    std::string lowerDataStr = dataStr;
                    transform(lowerDataStr.begin(), lowerDataStr.end(), lowerDataStr.begin(), ::tolower);
                    if (lowerDataStr == "true" || lowerDataStr == "1" ) data = true;
                    else if (lowerDataStr == "false" || lowerDataStr == "0") data = false;
                }
            }
        }

        /*!
         * @brief addDataElement Add data element to vector
         * @details Add data element to vector
         * @param const <json>& element The json element
         * @param T& data The data passed by reference
         * @tparam T The type of data.
         */
        template<typename T>
        static void addDataElement(const json& element, T& data) {
            if constexpr (std::is_same_v<T, std::vector<std::string>> ||
                          std::is_same_v<T, std::vector<int>> ||
                          std::is_same_v<T, std::vector<long>> ||
                          std::is_same_v<T, std::vector<double>> ||
                          std::is_same_v<T, std::vector<float>> ||
                          std::is_same_v<T, std::vector<bool>>) {
                if(element.is_string()) {
                    std::string elementStr = element.get<std::string>();
                    typename T::value_type dataElement;
                    setDataFromString(elementStr, dataElement);
                    data.push_back(dataElement);
                }
                else if (element.is_number() || element.is_boolean()) {
                    auto dataElement = element.get<typename T::value_type>();
                    data.emplace_back(dataElement);
                }
            }
          }

        /*!
         * @brief jsonParse Parse the string to get a data
         * @details Parse the string to get a data
         * @param const <json>& value The json value
         * @param const <string>& key The key
         * @param T& data The data passed by reference
        */
        template<typename T>
        [[maybe_unused]]static void jsonParse(const json &value, const std::string &key, T& data){
            if(value.find(key) != value.end() ) {

                if (value[key].is_string()) {
                    const std::string& dataStr = value[key];
                    setDataFromString(dataStr, data);
/*
                    if(std::is_same_v<T, std::string>){
                        data = value[key].get<std::string>();
                        //data = value[key];
                    }
                    else{
                        const std::string& dataStr = value[key];
                        setDataFromString(dataStr, data);
                    }
                    */
                }
                else if ( value[key].is_number() || value[key].is_boolean() ) {
                    data = value[key].get<T>();
                    //data = value[key];
                }

                else if ( value[key].is_array() ) {

                    if constexpr (std::is_same_v<T, std::vector<std::string>> ||
                                  std::is_same_v<T, std::vector<int>> ||
                                  std::is_same_v<T, std::vector<long>> ||
                                  std::is_same_v<T, std::vector<double>> ||
                                  std::is_same_v<T, std::vector<float>> ||
                                  std::is_same_v<T, std::vector<bool>>) {
                        data.clear();
                    }
                    for( const auto& element : value[key] ) {
                        addDataElement(element, data);
                    }
                }

            }
        }


        /*!
         * @brief strParse Parse the string to get a data
         * @details Parse the string to get a data
         * @param std::string& strResearch The string passed by reference
         * @param const <char>& delimiter The delimiter
         * @param const <T>& defaultValue The default value
         * @param T& data The data passed by reference
        */
        template <typename T>
        [[maybe_unused]] static void strParse(std::string &strResearch, const char& delimiter, const T& defaultValue, T &data ){
            size_t pos =  strResearch.find(delimiter);
            std::string dataStr = strResearch.substr(0, pos);
//            setDataFromString(dataStr, data);

            if constexpr(std::is_same_v<T, int>){ data = ( (ld::Utils::isInt(dataStr)) ? std::stoi(dataStr) : defaultValue  ); }
            else if constexpr(std::is_same_v<T, long>){ data = ( (ld::Utils::isLong(dataStr)) ? std::stol(dataStr) : defaultValue  ); }
            else if constexpr(std::is_same_v<T, double>){ data = ( (ld::Utils::isDouble(dataStr)) ? std::stod(dataStr) : defaultValue  ); }
            else if constexpr(std::is_same_v<T, std::string>){ data = ( (!dataStr.empty()) ? dataStr : defaultValue  ); }
            else if constexpr(std::is_same_v<T, float>){ data = ( (ld::Utils::isFloat(dataStr)) ? std::stof(dataStr) : defaultValue  ); }
            else if constexpr(std::is_same_v<T, bool>){
                if (ld::Utils::isBool(dataStr)){
                    transform(dataStr.begin(), dataStr.end(), dataStr.begin(), ::tolower);
                    if(dataStr =="true" || dataStr =="1" ) data = true;
                    else if(dataStr =="false" || dataStr =="0" ) data = false;
                    else data=defaultValue;
                }
                else data = defaultValue;
            }

            strResearch.erase(0, pos + 1);
        }

    };

}

#endif //MACOSDRIVER_CROWJSONRESPONSE_HPP
