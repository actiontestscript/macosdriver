#ifndef MACOSDRIVER_CROWSETTINGS_HPP
#define MACOSDRIVER_CROWSETTINGS_HPP
#include "../../apps/config.hpp"
#include "../utils/network.hpp"
#include "../lineCmdOption/optionLineCmd.hpp"
#include "../lineCmdOption/errors.hpp"
#include "../lineCmdOption/options.hpp"



using namespace ld;
//using std::string;

namespace ld {

    /*!
*\brief Class CrowSettings
* Class CrowSetting set value for app crow and validate parameters
*/
    class CrowSettings {
    public:
        /*!
         * \brief Constructor
         * @param errors
         * @param optionsData
         */
        CrowSettings(Errors* errors, OptionLineCmd* optionsData);

        /*!
         * \brief getIps
         * Get list ips
         * \return <vector <string>> list ips
         */
        vector<string> getIps() { return m_ips; }

        /*!
         * \brief getPort
         * Get port
         * \return <int> port
         */
        [[nodiscard]] int getPort() const { return m_port; }

        /*!
         * \brief getNbrInstance
         * Get number of instance
         * \return <int> number of instance
         */
        int getNbrInstance(){return (int) m_ips.size();}
//        static int checkPort(const int &portStart,const int &PortEnd, const string &ip){return _checkPort(portStart,PortEnd,ip);};

    private:
        Errors* m_errors;                                               //!< Errors class
        OptionLineCmd* m_optionsData;                                   //!< OptionsData class
//        bool m_isPortSetup;                                             //!< true if port is setup  in command line parameters
        int m_port;                                                     //!< port value
        vector<string> m_ips;                                           //!< List hostname, ips value
        vector<string> m_ListsIpsLocal;                                 //!< List ips local

        /*!
         * \brief isPortSetup
         * Check if port is setup in command line parameters
         * \return true if port is setup
         */
        bool _isPortSetup();

        /*!
         * \brief _checkPort
         * Check if port is available
         * \param portStart port start
         * \param PortEnd port end
         * \return <int> numberPort available, 0 if no port available
         */
//        static int _checkPort(const int &portStart,const int &PortEnd, const string &ip);

        /*!
         * \brief _setPort
         * Set port for app crow
         */
        void _setPort();

        /*!
         * \brief _setListIps
         * Set list ips for app crow
         */
        void _setListIps();

        /*!
         * \brief _checkHostname
         * Check if hostname is available
         * \param name hostname
         * \param errors
         * \return <string> return ip of hostname if hostname is available, empty string if hostname is not
         */
        string _checkHostname(const string& name);

        /*!
         * \brief _isIpList
         * Check if ip is available
         * \param name hostname
         * \return <string> return ip if ip is available, empty string if ip is not
         */
        bool _isIpList(string name);

        /*!
         * \brief _ipV4ToTab
         * Convert ip v4 to tab
         * \param name ip v4
         * \param tab tab
         * \param arrayLength array length
         */
        static void _ipV4ToTab(string name, int tab[], const int &arrayLength);

        /*!
         * \brief _setListIpsLocal
         * Set list ips local
         */
//        void _setListIpsLocal();
    };

}


#endif //MACOSDRIVER_CROWSETTINGS_HPP
