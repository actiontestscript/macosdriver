#ifndef MACOSDRIVER_UTILS_HPP
#define MACOSDRIVER_UTILS_HPP
#include "../../apps/config.hpp"
#include <array>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <dirent.h>
#include <fstream>
#include <libgen.h>
#include "mach-o/dyld.h"
#include <memory>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <utility>
#include <vector>
#include <sys/sysctl.h>



namespace ld{


class Utils {
public:
        /*!
         * @brief exec method
         * @details This method is used to execute a command
         * @param <const char*> cmd The command to be executed
         * @return <std::string> The output of the command
         */
        static bool exec(const char* cmd,std::string& result) {
          //  LD_LOG_INFO << "cmd: " << cmd << std::endl;
            std::array<char, 128> buffer{};
            std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
            if (pipe == nullptr) { result = "error -1 popen() failed!"; return false;}

            while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
                result.append(buffer.data());
            }
          return true;
          }


       /*!
        * @brief applicationExist method
        * @details This method is used to check if an application installed exist
        * @param <const std::string&> appName The name of the application
        * @return <bool> True if the application exist, false otherwise
        */
        static bool applicationExist(const std::string& appName){
            std::string result{};
            std::string cmd = "which "+appName;
            Utils::exec(cmd.c_str(), result);
            if(result.empty()) return false;
            else return true;
        }

        /*!
         * @brief fileExist method
         * @details This method is used to check if a file exist
         * @param <const std::string&> filename The name of the file
         * @return <bool> True if the file exist, false otherwise
         */
        static bool fileExist(const std::string& filename) {
            std::ifstream f(filename.c_str());
            return f.good();
        }

        /*!
         * @brief directoryExist method
         * @details This method is used to check if a directory exist
         * @param <const std::string&> pathDirectory The path of the directory
         * @return <bool> True if the directory exist, false otherwise
        */
        static bool directoryExist(const std::string& pathDirectory){
            struct stat info{};
            if(stat(pathDirectory.c_str(), &info) !=0) return false;
            else if(info.st_mode & S_IFDIR) return true;
            else return false;
            /*DIR* dir = opendir(pathDirectory.c_str());
            if (dir) {closedir(dir); return true;}
            else return false;
             */
        }

    /*!
       * @brief escapePath method
       * @details This method is used to escape a path
       * @param <std::string&> path The path to be escaped
       * @return <void>
       */
    static void escapePath(std::string& path){
        std::regex patternEspace(" ");
        path = std::regex_replace(path, patternEspace, "\\ ");
    }

    /*!
         * @brief directoryIsWritable method
         * @details This method is used to check if a directory is writable
         * @param <const std::string&> pathDirectory The path of the directory
         * @return <bool> True if the directory is writable, false otherwise
         */
        static bool directoryIsWritable(const std::string& pathDirectory){ return (access(pathDirectory.c_str(), W_OK) == 0); }

        /*!
         * @brief directoryAsFilesOrDirectory method
         * @details This method is used to check if a directory contains files or directories
         * @param <const std::string&> pathDirectory The path of the directory
         * @return <bool> True if the directory contains files or directories, false otherwise
         */
        static bool directoryAsFilesOrDirectory(const std::string& pathDirectory){
            DIR* dir = opendir(pathDirectory.c_str());
            if (dir) {
                struct dirent* ent;
                while ((ent = readdir(dir)) != nullptr) {
                    if(strcmp(ent->d_name, ".") != 0 && strcmp(ent->d_name,"..")!=0 ) {
                        if (ent->d_type == DT_DIR || ent->d_type == DT_REG) {
                            closedir(dir);
                            return true;
                        }
                    }
                }
                closedir(dir);
            }
            return false;
        }

        /*!
         * @brief isFloat method
         * @details This method is used to check if a string is a float
         * @param <const std::string&> str The string to check
         * @return <bool> True if the string is a float, false otherwise
         */
        static bool isFloat(const std::string& str) {
            std::regex pattern("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$");
            return  std::regex_match(str, pattern);
        }

        /*!
         * @brief isInt method
         * @details This method is used to check if a string is an integer
         * @param <const std::string&> str The string to check
         * @return <bool> True if the string is an integer, false otherwise
         */
        static bool isInt(const std::string& str) {
            std::regex pattern("^[-+]?[0-9]+$");
            return std::regex_match(str, pattern);
        }

        /*!
         * @brief isBool method
         * @details This method is used to check if a string is a boolean
         * @param <const std::string&> str The string to check
         * @return <bool> True if the string is a boolean, false otherwise
         */
        static bool isBool(const std::string& str) {
            std::regex pattern("^(true|false|0|1)$");
            return std::regex_match(str, pattern);
        }

        /*!
         * @brief isLong method
         * @details This method is used to check if a string is a long
         * @param <const std::string&> str The string to check
         * @return <bool> True if the string is a long, false otherwise
         */
        static bool isLong(const std::string& str) {
            std::regex pattern("^[-+]?[0-9]+$");
            return std::regex_match(str, pattern);
        }

        /*!
         * @brief isDouble method
         * @details This method is used to check if a string is a double
         * @param <const std::string&> str The string to check
         * @return <bool> True if the string is a long, false otherwise
         */
        static bool isDouble(const std::string& str)
        {
            std::regex regex_double("[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?");
            return std::regex_match(str, regex_double);
        }


        /*!
         * @brief getCurrentPath method
         * @details This method is used to get the current path of the application
         * @return <char*> The current path of the application
         */
        static char* getCurrentPath(){
            char pathBuffer[PATH_MAX];
            uint32_t sizePath = sizeof(pathBuffer);
            if(_NSGetExecutablePath(pathBuffer,&sizePath) == 0){
                char *realPath = realpath(pathBuffer,NULL);
                if(realPath){
                    return dirname(realPath);
                }
            }
            return strdup("./");
        }

        /*!
         * @brief getTmpPath method
         * @details This method is used to get the tmp path of the application
         * @return <char*> The tmp path of the application
         */
        static char* getTmpPath(){
           return strdup("/tmp");
        }

        enum class FileSizeUnit { KB, MB, GB };                                                     /*> The unit of the size */
        enum class FileSizeType { Total, Used, Available, UsedPercentage, MountPoint };                                 /*> The type of the size */
        /*!
         * @brief getDriveSize method
         * @details This method is used to get the size of a drive
         * @param <const std::string&> path The path of the drive
         * @param <const FileSizeUnit&> fsUnit The unit of the size
         * @param <const FileSizeType&> fsType The type of the size
         * @return <std::string> The size of the drive or the mount point
         */
        static std::string getDriveSize(const std::string& path,const FileSizeUnit& fsUnit, const FileSizeType& fsType){
            std::string  result{};
            std::string cmd = "df -P ";
            switch (fsUnit) {
                case FileSizeUnit::KB: cmd += "-k"; break;
                case FileSizeUnit::MB: cmd += "-m"; break;
                case FileSizeUnit::GB: cmd += "-g"; break;
            }
            cmd += " " + path;
            cmd += " | awk 'NR==2{print $";
            switch (fsType) {
                case FileSizeType::Total : cmd += "2"; break;
                case FileSizeType::Used : cmd += "3"; break;
                case FileSizeType::Available : cmd += "4"; break;
                case FileSizeType::UsedPercentage : cmd += "5"; break;
                case FileSizeType::MountPoint : cmd += "6"; break;

            }
            cmd += "}'";
            LD_LOG_DEBUG << "getDriveSize->cmd : " << cmd << std::endl;

            if(ld::Utils::exec(cmd.c_str(), result)) {
                std::regex pattern("[\\n]");
                std::string res = std::regex_replace(result, pattern, "");
                return res;
            }
            return "";
        };


    /*!
     * @brief base64_decode method static
     * @details This method is used to decode a base64 string
     * @param <const std::string&> input The base64 string to decode
     * @return <std::vector<uint8_t>> The decoded string
     */
    static std::vector<uint8_t> base64_decode(const std::string &input) {
        static constexpr char kBase64Alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

        static std::array<uint8_t, 256> kBase64Lookup = [] {
            std::array<uint8_t, 256> table{};
            for (size_t i = 0; i < sizeof(kBase64Alphabet) - 1; i++) {
                table[static_cast<unsigned char>(kBase64Alphabet[i])] = i;
            }
            return table;
        }();

        std::vector<uint8_t> output;
        output.reserve(input.size() * 3 / 4);
        uint32_t buffer = 0;
        int bits = 0;
        for (const char c : input) {
            const auto index = kBase64Lookup[static_cast<unsigned char>(c)];
            if (index == 0xFF) continue;
            buffer = (buffer << 6) | index;
            bits += 6;

            if (bits >= 8) {
                output.push_back(static_cast<uint8_t>((buffer >> (bits - 8)) & 0xFF));
                bits -= 8;
            }
        }

        return output;
    }

    /*!
     * @brief getPngSize method
     * @details This method is used to get the size of a png image
     * @param <const std::string&> imgBase64 The image data
     * @param <double&> height The height of the image
     * @param <double&> width The width of the image
     */
    static void getPngSize(const std::string& imgBase64, double& height,double& width ){
        if(imgBase64.empty()) return;
        std::vector<uint8_t> imgData = base64_decode(imgBase64);
        getPngSize(imgData,height,width);
    }

    /*!
     * @brief getPngSize method
     * @details This method is used to get the size of a png image
     * @param <std::vector<uint8_t>&> imgData The image data
     * @param <double&> w The width of the image
     * @param <double&> h The height of the image
     */
    static void getPngSize(std::vector<uint8_t>& imgData, double& w,double& h){
        const std::array<uint8_t, 8> pngHeader = {137, 80, 78, 71, 13, 10, 26, 10};
        if (std::equal(imgData.begin(), imgData.begin() + 8, pngHeader.begin())) {
            uint32_t width = (imgData[16] << 24) | (imgData[17] << 16) | (imgData[18] << 8) | imgData[19];
            uint32_t height = (imgData[20] << 24) | (imgData[21] << 16) | (imgData[22] << 8) | imgData[23];
            w = static_cast<double>(width);
            h = static_cast<double>(height);
        }
    }

    /*!
     * @brief getIteration method
     * @details This method is used to get the number of iteration of a string
     * @param <std::string> data The string to check
     * @param <const std::string&> delimiter The delimiter
     * @return <int> The number of iteration
     */
    static int getIteration(std::string data, const std::string& delimiter){
        int nbrElements = 0;
        size_t pos = std::string::npos;
        do {
            pos = data.find(delimiter);
            if (pos != std::string::npos) {
                data.erase(0, pos + 1);
            }
            nbrElements++;
        }
        while (pos != std::string::npos) ;
        return nbrElements;
    }

    /*!
     * @brief strToBool method
     * @details This method is used to convert a string to a boolean
     * @param str The string to convert
     * @return <bool> The boolean
     */
    static bool strToBool(const std::string& str){
        if(str == "true" || str == "1") return true;
        else return false;
    }

    /*!
     * @brief split method
     * @details This method is used to split a string
     * @param <const std::string&> str The string to split
     * @param <const std::string&> delimiter The delimiter
     * @return <std::vector<std::string>> The vector of string
     */
    static std::vector<std::string> split(const std::string& str, const std::string& delimiter){
        std::vector<std::string> result;
        std::string s = str;
        size_t pos = std::string::npos;
        do {
            pos = s.find(delimiter);
            if (pos != std::string::npos) {
                result.push_back(s.substr(0, pos));
                s.erase(0, pos + delimiter.length());
            }
        }
        while (pos != std::string::npos) ;
        result.push_back(s);
        return result;
    }

    /*!
     * @brief toLower method
     * @details This method is used to convert a string to lower case
     * @param <const std::string&> str The string to convert
     * @return <std::string> The string in lower case
     */
    static std::string toLower(const std::string& str){
        std::string result = str;
        std::transform(result.begin(), result.end(), result.begin(), [](unsigned char c){ return std::tolower(c); });
        return result;
    }

    /*!
     * @brief getBrowserLaunchWithWebdriver method
     * @details This method is used to get the browser launched with webdriver
     * @return <std::vector<std::string>> The vector of string
     */
    static std::vector<std::string> getBrowserLaunchWithWebdriver(){
        std::vector<std::string> pids;
        //chrome, brave, msedge
        std::string cmd = "ps -ef | grep -i webdriver | grep -v grep | awk '{print $2}'";
        std::string result;
        if(ld::Utils::exec(cmd.c_str(), result)){
            std::vector<std::string> lines = ld::Utils::split(result,"\n");
            for(const auto& line : lines){
                if(!line.empty()) pids.push_back(line);
            }
        }
        //gecko
        cmd = "ps -ef | grep -i -- \"--marionette\" | grep -v grep | awk '{print $2}'";
        result = "";
        if(ld::Utils::exec(cmd.c_str(), result)){
            std::vector<std::string> lines = ld::Utils::split(result,"\n");
            for(const auto& line : lines){
                if(!line.empty()) pids.push_back(line);
            }
        }
        return pids;
    }

    /*!
     * @brief killBrowserLaunchWithWebdriver method
     * @details This method is used to kill the browser launched with webdriver
     */
    static void killBrowserLaunchWithWebdriver(){
        std::vector<std::string> pids = getBrowserLaunchWithWebdriver();
        std::string cmd = "";
        std::string output;
        for(const auto& pid : pids){
            if(Utils::isInt(pid) && !pid.empty()){
                cmd = "kill -9 "+pid;
                ld::Utils::exec(cmd.c_str(), output);
            }
        }
    }

    static void killMacOsdriver(){
        pid_t currentPid = getpid();
        std::ostringstream cmdStream;
        std::vector<std::string> listOfDriver = {"macosdriver","chromedriver","geckodriver","msedgedriver","bravedriver","operadriver","safaridriver"};
        std::string grepRegEx = "";
        for(const auto& driver : listOfDriver){
            if(!grepRegEx.empty()) grepRegEx += "|";
            grepRegEx += driver;
        }
//        cmdStream << "ps -ef | grep -i driver | grep -v grep | awk '{print $2}' | grep -v " << currentPid << "";
        cmdStream << "ps -ef | grep -E '("+grepRegEx+")' | grep -v grep | awk '{print $2}' | grep -v " << currentPid << "";
        std::string cmd = cmdStream.str();
        std::string output;
        ld::Utils::exec(cmd.c_str(), output);
        std::vector<std::string> lines = ld::Utils::split(output,"\n");
        for(const auto& line : lines){
            if(Utils::isInt(line) && !line.empty()){
                cmd = "kill -9 "+line;
                ld::Utils::exec(cmd.c_str(), output);
            }
        }
    }

    template <typename T>
    static T getSysCtl(const std::string& name);

}; //end class Utils

    template<>
    std::string Utils::getSysCtl<std::string>(const std::string& name);

   }
#endif //MACOSDRIVER_UTILS_HPP
