#ifndef MACOSDRIVER_XMLSETTINGS_HPP
#define MACOSDRIVER_XMLSETTINGS_HPP
#include "../../apps/config.hpp"
#include "../utils/network.hpp"
#include "../utils/utils.hpp"
#include "../W3C/session/session.hpp"
#include <algorithm>
#include "pugixml.hpp"
#include <set>

namespace ld{
    /*! @brief XmlSettings class
     *  @details This class is used to load the settings from the xml file
     */
    class XmlSettings {
    private:
        static std::string m_fileName;                                                                  /*!< The name of the xml file */
        static std::string m_fileFullPath;                                                              /*!< The full path of the xml file */
        std::string m_allowedIps {};                                                                    /*!< The allowed ips */
        std::string m_localIpsBinding {"127.0.0.1"};                                                 /*!< The local ips binding */
        std::string m_remoteHostname {};                                                                /*!< The remote hostnames */


    public:
        /*! @brief XmlSettings constructor
         *  @details This constructor is used to create a XmlSettings object
         */
        XmlSettings() = default;

        /*! @brief getLastOctet function static
         *  @details This function is used to get the last Byte of an address ip
         *  @param const <std::string>& ip The ip
         *  @return <int> The last octet of the ip
         */
        static int getLastOctet(const std::string& ip);

        /*! @brief getRange function static
         *  @details This function is used to get the range of an ip
         *  @param const <std::string>& startIp The start ip
         *  @param const <std::string>& endIp The end ip
         *  @return <int> The number of the ip between the start and the end
         */
        static int getRange(const std::string& startIp,const std::string& endIp);

        /*! @brief getIpNetwork function static
         *  @details This function is used to get the address network of an ip first 3 Bytes
         *  @param const <std::string>& ip The ip
         *  @return <std::string> The network of the ip
         */
        static std::string getIpNetwork(const std::string& ip);

        /*! @brief isExist function
         *  @details This function is used to check if the xml file exist
         *  @return <bool> True if the file exist, false otherwise
         */
        bool isExist();

        /*! @brief loadParameters function
         *  @details This function is used to load the parameters from the xml file
         *  @return <bool> True if the parameters are loaded, false otherwise
         */
        bool loadParameters();

        /*! @brief getRemoteHostnames function
         *  @details This function is used to get the remote hostnames
         *  @return <std::string> The remote hostnames
         */
        [[nodiscard]] std::string getRemoteHostname() const {return m_remoteHostname;}

        /*! @brief getAllowedIps function
         *  @details This function is used to get the allowed ips
         *  @return <std::string> The allowed ips
         */
        [[nodiscard]] std::string getAllowedIps() const {return m_allowedIps;}

        /*! @brief getLocalIpsBinding function
         *  @details This function is used to get the local ips binding
         *  @return <std::string> The local ips binding
         */
        [[nodiscard]] std::string getLocalIpsBinding() const {return m_localIpsBinding;}

        /*! @brief setPath function
         *  @details This function is used to set the path of the xml file
         */
        void setPath();

        /*! @brief findAtsProperties function static
         *  @details This function is used to find the ats properties
         *  @return <bool> True if the ats properties are found, false otherwise
         */
        static bool findAtsProperties();
    };
}
#endif //MACOSDRIVER_XMLSETTINGS_HPP
