#ifndef MACOSDRIVER_LOGGER_HPP
#define MACOSDRIVER_LOGGER_HPP
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <streambuf>
#include <memory>
#include <algorithm>
#include <utility>
#include <functional>

namespace ld{
    /*!
     * @enum LogLevel
     * @brief The enum LogLevel
     * @details This enum is used to define the log level.
     * @var LogLevel::NONE
     * @var LogLevel::ERROR
     * @var LogLevel::INFO
     * @var LogLevel::WARNING
     * @var LogLevel::ALL
     */
    enum class LogLevel
    {
        NONE = 0,
        ERROR,
        INFO,
        WARNING,
        ALL
    };

    /*!
     * @class ldBuff
     * @brief The class ldBuff
     * @details This class extend std::streambuf to redirect the output to a file and console.
     */
    class LdBuff : public std::streambuf{
    public:
        /*!
         * @brief LdBuff
         * @details Constructor of LdBuff
         * @param <std::streambuf*> sb1 : the first streambuf
         * @param <std::streambuf*> sb2 : the second streambuf
         * @return void
         */
        LdBuff(std::streambuf* sb1, std::streambuf* sb2) : sb1(sb1), sb2(sb2), logInterne(false), logFile(false) {}
        ~LdBuff() override {
            sync();
        }

        /*!
         * @brief setLogInterne
         * @details set the log interne
         * @param <bool> logInterne : the log interne
         * @return void
         */
        void setLogInterne(bool bLogIntern){ this->logInterne = bLogIntern; }

        /*!
         * @brief setLogFile
         * @details set the log file
         * @param <bool> logFile : the log file
         * @return void
         */
        void setLogFile(bool bLogFile){ this->logFile = bLogFile; }

    protected:

        /*!
         * @brief overflow
         * @details this mehtode override the overflow of streambuf to the file and console
         * @param <int> c : the char
         * @return int : the char
         */
        int overflow(int c) override {
            if (c == EOF) { return EOF; }
            else {
                if(!logInterne) {
                    if (logFile && sb2 && sb2->sputc(c) == EOF) { return EOF; }                                                    /*> all write to file if existe  */
                } else {
                    if (sb1->sputc(c) == EOF) { return EOF; }
                    return logFile && sb2 && sb2->sputc(c) == EOF ? EOF : c;
                }
            }
            return c;
        }

        /*!
         * @brief sync
         * @details this mehtode override the sync of streambuf to the file and console
         * @return int : the char
         */
        int sync() override {
            if (sb1->pubsync() == -1) return -1;
            return logFile && sb2 && sb2->pubsync() == -1 ? -1 : 0;
        }

    private:
        std::streambuf* sb1;                                                                                            /*> streambuf 1     */
        std::streambuf* sb2;                                                                                            /*> streambuf 2     */
        bool logInterne {false};                                                                                        /*> log intern     */
        bool logFile{false};                                                                                            /*> log file        */
    };

    /*!
     * @class Logger
     * @brief The class Logger
     * @details This class is used to log the application.
     */
    class Logger {
    public:
        static const std::string ATS_DRIVER_VERSION;                                                                    /*> the driver version                  */
        static const std::string ATS_DRIVER_PORT;                                                                       /*> the driver port                     */
        static const std::string ATS_AGENT_PORT;                                                                        /*> the agent port                      */
        static const std::string ATS_AGENT_OUTPUT;                                                                      /*> the agent log output - Debug        */
        static const std::string ATS_AGENT_WARNING;                                                                     /*> the agent log warning               */
        static const std::string ATS_AGENT_ERROR;                                                                       /*> the agent log error                 */
        static const std::string ATS_AGENT_INFO;                                                                        /*> the agent log info                  */
        static const std::string ATS_AGENT_CONNECTED;                                                                   /*> the agent connected                 */

        /*!
         * @brief instance
         * @details get the instance of the logger
         * @param std::ostream& os : the output stream
         * @return Logger& : the logger
         */
        static Logger& instance(std::ostream& os) {
            static Logger loggerCout(std::cout,"cout");
            static Logger loggerCerr(std::cerr,"cerr");
            return (os.rdbuf() == std::cout.rdbuf()) ? loggerCout : loggerCerr;
        }

        /*!
         * @brief operator<<
         * @details operator << for the logger
         * @param const T& message : the message
         * @return Logger& : the logger
         */
        template <typename T>
        Logger& operator<<(const T& message) {
            if(logLevel <= currentLogLevel) {
                currentMessage << message;
            }
            return *this;
        }

        /*!
         * @brief operator<<
         * @details operator << for the logger
         * @param std::ostream& (*func)(std::ostream&) : the function
         * @return Logger& : the logger
         */
        Logger& operator<<(std::ostream& (*func)(std::ostream&)) {
            if( (logLevel <= currentLogLevel) && func == static_cast<std::ostream& (*)(std::ostream&)>(std::endl)) {
                std::ostringstream oss;
                oss << msgLogLevel;
                oss << currentMessage.str();
                oss << std::endl;
                dynamic_cast<LdBuff *>(ldBuff2.rdbuf())->setLogInterne(true);
                ldBuff2 << oss.str();
                dynamic_cast<LdBuff *>(ldBuff2.rdbuf())->setLogInterne(false);
                currentMessage.str(""); // Reset the current message
                currentMessage.clear(); // Clear any error flags
                SendLogToWebSocket(oss.str());
            }
            return *this;
        }

        Logger(Logger const&) = delete;                                                                                 /*> delete copy constructor - singleton    */
        void operator=(Logger const&)  = delete;                                                                        /*> delete assignment operator -singleton  */

        /*!
         * @brief setFilename
         * @details set the filename of the log file
         * @param const <std::string&> fn : the filename
         * @return void
         */
        static void setFilename(const std::string& fn) { filename = fn; }

        /*!
         * @brief setLoggingToFile
         * @details set the logging to file
         * @param bool logging : true if logging to file, false otherwise
         * @return void
         */
        static void setLoggingToFile(bool logging, bool closeFile = false , bool eraseFile = false) {
            loggingToFile = logging;
            dynamic_cast<LdBuff *>(instance(std::cout).ldBuff2.rdbuf())->setLogFile(logging);
            dynamic_cast<LdBuff *>(instance(std::cerr).ldBuff2.rdbuf())->setLogFile(logging);

            if (loggingToFile) {
                if (!instance(std::cout).filebuf) {
                    instance(std::cout).filebuf = std::make_unique<std::filebuf>();
                    if(eraseFile){
                        instance(std::cout).filebuf->open((filename + "_cout.log").c_str(), std::ios::out | std::ios::trunc);
                    }
                    else{
                        instance(std::cout).filebuf->open((filename + "_cout.log").c_str(),std::ios::out | std::ios::app);
                    }
                    dynamic_cast<LdBuff *>(instance(std::cout).ldBuff2.rdbuf())->setLogInterne(true);
                }

                if (!instance(std::cerr).filebuf) {
                    instance(std::cerr).filebuf = std::make_unique<std::filebuf>();
                    if(eraseFile){
                        instance(std::cerr).filebuf->open((filename + "_cerr.log").c_str(), std::ios::out | std::ios::trunc);
                    }
                    else{
                        instance(std::cerr).filebuf->open((filename + "_cerr.log").c_str(), std::ios::out | std::ios::app);
                    }
                    dynamic_cast<LdBuff *>(instance(std::cerr).ldBuff2.rdbuf())->setLogInterne(true);
                }
            }
            else if(closeFile){
                if (instance(std::cout).filebuf) {
                    instance(std::cout).filebuf->close();
                    instance(std::cout).filebuf.reset();
                }
                if (instance(std::cerr).filebuf) {
                    instance(std::cerr).filebuf->close();
                    instance(std::cerr).filebuf.reset();
                }
            }
        }

        /*!
         * @brief setLogLevel
         * @details set the log level
         * @param LogLevel level : the log level
         * @return void
         */
        static void setLogLevel(LogLevel level) { currentLogLevel = level; }
        static void setLogLevel(std::string level) {
            transform(level.begin(), level.end(), level.begin(), ::tolower);
            if(level == "silent") Logger::setLogLevel(LogLevel::NONE);
            else if(level == "info") Logger::setLogLevel(LogLevel::INFO);
            else if(level == "warning") Logger::setLogLevel(LogLevel::WARNING);
            else if(level == "error") Logger::setLogLevel(LogLevel::ERROR);
            else if(level == "all") Logger::setLogLevel(LogLevel::ALL);
        }


        /*!
         * @brief debug
         * @details set the log level to debug
         */
        [[maybe_unused]] Logger& debug() { msgLogLevel = Logger::ATS_AGENT_OUTPUT + '|' ; logLevel = LogLevel::ALL; return *this; }                             /*> set the log level to debug  */

        /*!
         * @brief info
         * @details set the log level to info
         */
        [[maybe_unused]] Logger& info() {msgLogLevel = Logger::ATS_AGENT_INFO + '|' ; logLevel = LogLevel::INFO ; return *this; }                              /*> set the log level to info   */

        /*!
         * @brief warning
         * @details set the log level to warning
         */
        [[maybe_unused]] Logger& warning() {msgLogLevel = Logger::ATS_AGENT_WARNING + '|' ;logLevel = LogLevel::WARNING;return *this; }                        /*> set the log level to warning*/

        /*!
         * @brief error
         * @details set the log level to error
         */
        [[maybe_unused]] Logger& error() {msgLogLevel = Logger::ATS_AGENT_ERROR + '|' ;logLevel = LogLevel::ERROR ;return *this;}                              /*> set the log level to error  */

        /*!
         * @brief silent
         * @details set the log level to silent
         */
        [[maybe_unused]] Logger& silent() {msgLogLevel = "";logLevel = LogLevel::NONE ;return *this;}                                    /*> set the log level to None -> Always printing console  */


        /*!
         * @brief setCapture
         * @details set the capture of cout and cerr
         * @param bool capture : true if capture, false otherwise
         */
        static void setCapture(bool capture) {
            if(capture) {
                std::cout.rdbuf(Logger::instance(std::cout).getStreamBuffer());
                std::cerr.rdbuf(Logger::instance(std::cerr).getStreamBuffer());
            }
            else {
                std::cout.rdbuf(originalCoutBuffer);
                std::cerr.rdbuf(originalCerrBuffer);
            }
        }

        /*!
         * @brief getStreamBuffer
         * @details get the stream buffer
         * @param void
         */
        std::streambuf* getStreamBuffer() {
            dynamic_cast<LdBuff *>(ldBuff2.rdbuf())->setLogInterne(false);
            return ldBuff2.rdbuf();
        }

        /*!
         * @brief Type definition for a logging callback function.
         *
         * LogCallback is a function pointer type that represents a callback function
         * used for logging purposes. The callback function takes a single argument,
         * a constant reference to a string, which contains the log message. The
         * function does not return a value (void).
         *
         * This type is typically used to define a callback that can be registered
         * with a logging system, where the callback will be invoked with a log
         * message each time a new message needs to be logged.
         *
         */
        using LogCallback = std::function<void(const std::string&)>;

        /*!
         * @brief setLogCallback
         * @details set the log callback
         * @param LogCallback callback : the log callback
         */
        static void setLogCallback(LogCallback callback) {
            logCallback = std::move(callback);
        }

        /*!
         * @brief Type definition for a essage startup callback function.
         *
         * MsgStartupCallback is a function pointer type that represents a callback function
         * used for startup purposes. The callback function takes no argument,
         * and does not return a value (void).
         *
         * This type is typically used to define a callback that can be registered
         * with a logging system, where the callback will be invoked at startup.
         *
         */
        using MsgStartupCallback = std::function<void()>;

        /*!
         * @brief setMsgStartupCallback
         * @details set the message startup callback
         * @param MsgStartupCallback callback : the message startup callback
         */
        static void setMsgStartupCallback(MsgStartupCallback callback) {
            msgStartupCallback = std::move(callback);
        }

        /*!
         * @brief SendLogToWebSocket
         * @details send the log to websocket use the callback defined by setLogCallback in Main
         * @param const std::string& log : the log
         */
        static void SendLogToWebSocket(const std::string& log) {
            if (logCallback) {
                logCallback(log);
            }
        }

        /*!
         * @brief clearConsoleAndFile
         * @details clear the console and the file
         */
        static void clearConsoleAndFile(){
/*
            const char* term = std::getenv("TERM");

            if(loggingToFile) {
                Logger::setLoggingToFile(false, true);
                Logger::setLoggingToFile(true,false,true);
            }


            if (!term) {
                setenv("TERM", "xterm", 1);
                std::system("clear");
            } else {
                std::system("clear");
            }
            if (msgStartupCallback) {
                msgStartupCallback();
            }
            */
        }


    private:
        /*!
         * @brief Logger
         * @details the constructor of the logger - private constructor for singleton
         * @param std::ostream& os : the output stream
         * @param const std::string& streamType : the type stream for filename
         */
        explicit Logger(std::ostream& os, const std::string& streamType) : filebuf(loggingToFile ? new std::filebuf() : nullptr), ldBuff1(os.rdbuf(), filebuf.get()), ldBuff2(&ldBuff1), logLevel(LogLevel::NONE) {
            if (loggingToFile && filebuf) {
                //filebuf->open(filename.c_str(), std::ios::out | std::ios::app);
                filebuf->open((filename + "_"+ streamType + ".log").c_str(), std::ios::out | std::ios::app);
            }
        }

        std::unique_ptr<std::filebuf> filebuf;                                                                          /*> the file buffer for logging to file  */
        std::string msgLogLevel;                                                                                        /*> the log level message                */
        LdBuff ldBuff1;                                                                                                 /*> the buffer for logging to file       */
        std::ostream ldBuff2;                                                                                           /*> the buffer for logging to console    */
        LogLevel logLevel;                                                                                              /*> the log level                        */
        static std::string filename;                                                                                    /*> the filename for logging to file     */
        static bool loggingToFile;                                                                                      /*> true if logging to file, false otherwise  */
        static LogLevel currentLogLevel;                                                                                /*> the current log level                */
        static std::streambuf* originalCoutBuffer;                                                                      /*> the original cout buffer             */
        static std::streambuf* originalCerrBuffer;                                                                      /*> the original cerr buffer             */
        std::ostringstream currentMessage;                                                                              /*> the current message                  */
        static LogCallback logCallback;                                                                                 /*> the log callback                     */
        static MsgStartupCallback msgStartupCallback;                                                                   /*> the msg startup callback             */
    };

//define macros for logging
    #define LD_LOG(level, os) (Logger::instance(os).level())
    #define LD_LOG_DEBUG LD_LOG(debug, std::cout)
    #define LD_LOG_INFO LD_LOG(info, std::cout)
    #define LD_LOG_WARNING LD_LOG(warning, std::cout)
    #define LD_LOG_ERROR LD_LOG(error, std::cerr)
    #define LD_LOG_ALWAYS LD_LOG(silent, std::cout)
}
#endif //MACOSDRIVER_LOGGER_HPP
