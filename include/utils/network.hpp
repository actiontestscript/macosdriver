#ifndef MACOSDRIVER_NETWORK_HPP
#define MACOSDRIVER_NETWORK_HPP

#include "../../apps/config.hpp"
#include "../../include/utils/utils.hpp"
#include <arpa/inet.h>
#include <cerrno>
#include <cstring>
#include <ifaddrs.h>
#include <iostream>
#include <net/if.h>
#include <netdb.h>
#include <netinet/in.h>
#include <regex>
#include <string>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <utility>
#include <vector>

namespace ld{
    class Network{
    public:
        /*!
         * @brief checkPort method
         * @details This method is used to check if a port is available.
         * @return <std::string> The ip of the machine
         */
        static int getAvailablePort(const int &portStart,const int &PortEnd = 65535, const std::string &ip = "127.0.0.1");

        /*!
         * @brief setListIpsLocal method
         * @details This method is used to get list ips local and store in vector.
         * @return <std::vector<std::string>> The list ips local
         */
        static std::vector<std::string> getListIpsLocal(const bool& withSubnetMask = false);

        /*!
         * @brief isPortInUse method
         * @details This method is used to check if a port is in use.
         */
        static bool isPortInUse(const int &port){
            if(Network::getAvailablePort(port, port, "127.0.0.1") == -1) return true;
            return false;
        }

        /*!
         * @brief isValidIpv4 method
         * @details This method is used to check if a ip is valid formatting.
         * @param const <std::string>& ipAddress
         * @return <bool> True if the ip is valid, false otherwise
         */
        static bool isValidIpv4(const std::string &ipAddress);

        /*!
         * @brief getIpMask method
         * @details This method is used to get the mask of an interface.
         * @param const <std::string>& interface
         * @param <std::string>& mask
         * @return <bool> True if the mask is found, false otherwise
         */
        static bool getIpMask(const std::string &interface, std::string &mask);

        /*!
         * @brief getIpNetwork method
         * @details This method is used to get the network of an ip.
         * @param const <std::string>& ip
         * @param const <std::string>& mask
         * @return <std::string> The network of the ip
         */
        static std::string getIpNetwork(const std::string &ip, const std::string &mask);

        /*!
         * @brief getIpHost method
         * @details This method is used to get the host of an ip.
         * @param const <std::string>& ip
         * @param const <std::string>& mask
         * @return <std::string> The host of the ip
         */
        static std::string getIpHost(const std::string &ip, const std::string &mask);

        /*!
         * @brief getDefaultGatewayIp method
         * @details This method is used to get the default gateway ip.
         * @return <std::string> The default gateway ip
         */
        static std::string getDefaultGatewayIp();

    };
}
#endif //MACOSDRIVER_NETWORK_HPP
