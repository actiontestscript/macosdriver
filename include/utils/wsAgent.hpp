#ifndef MACOSDRIVER_WSAGENT_HPP
#define MACOSDRIVER_WSAGENT_HPP
#include "../../apps/config.hpp"
#include <ixwebsocket/IXWebSocketServer.h>
#include <ixwebsocket/IXConnectionState.h>
#include "../W3C/session/session.hpp"
#include "../W3C/session/sessionDelete.hpp"
#include <thread>
#include <atomic>
#include <mutex>
#include <memory>
#include <condition_variable>

namespace ld{
    class WsAgent {
    private:
        ix::WebSocketServer server;                                                                                     /*< Websocket server */
        std::thread serverThread;                                                                                       /*< Thread for websocket server */
        std::atomic<bool> isRunning;                                                                                    /*< Is websocket server running */
        std::mutex connectionMutex;                                                                                     /*< Mutex for connection */
        std::weak_ptr<ix::ConnectionState> activeConnection;                                                            /*< Active connection */
        std::weak_ptr<ix::WebSocket> activeWebSocket;                                                                   /*< Active websocket */
        std::condition_variable cv;                                                                                     /*< Condition variable */
        std::mutex cvMutex;                                                                                             /*< Mutex for condition variable */
        std::string activeConnectionId;                                                                                 /*< Active connection id */
        static bool m_isConnected;                                                                                      /*< Is websocket connected */
        bool m_isLocal{true};                                                                                           /*< Is websocket local */
        SessionDelete m_sessionDelete;                                                                                  /*< Session delete */

        /*!
         * @brief Run websocket server
         */
        void Run();

    public:

        /*!
         * @brief Constructor
         * @param port Port for websocket server
         */
        explicit WsAgent(int port);

        /*!
         * @brief Destructor
         */
        ~WsAgent();

        /*!
         * @brief Connect to websocket server
         * @param webSocket Websocket
         * @param connectionState Connection state
         */
        void Connect(const std::weak_ptr<ix::WebSocket>& webSocket, const std::shared_ptr<ix::ConnectionState>& connectionState);

        /*!
         * @brief Disconnect from websocket server
         * @param webSocket Websocket
         * @param connectionState Connection state
         */
        void DisConnect(const std::weak_ptr<ix::WebSocket>& webSocket, const std::shared_ptr<ix::ConnectionState>& connectionState);

        /*!
         * @brief Update websocket server
         * @param webSocket Websocket
         * @param connectionState Connection state
         * @param string String
         */
        void Update(const std::weak_ptr<ix::WebSocket> &webSocket,const std::shared_ptr<ix::ConnectionState> &connectionState,const std::string& string);

        /*!
         * @brief Set data
         * @param driverVersion Driver version
         * @param webServerPort Web server port
         * @param wsPort Websocket port
         * @param local Is local
         */
        void SetData(const std::string& driverVersion, int webServerPort, int wsPort, bool local);

        /*!
         * @brief Start websocket server
         */
        void Start();

        /*!
         * @brief Stop websocket server
         */
        void Stop();

        /*!
         * @brief Send log
         * @param log Log
         */
        void SendLog(const std::string& log);

        /*!
         * @brief Print log
         * @param type Type
         * @param data Data
         * @param silent Is silent
         */
        void PrintLog(const std::string& type, const std::string& data, bool silent);

        /*!
         * @brief Print log
         * @param type Type
         * @param data Data
         * @param outputError Output error
         * @param silent Is silent
         */
        void PrintLog(const std::string& type, const std::string& data, bool outputError, bool silent);

        /*!
         * @brief Log info
         * @param data Data
         * @param silent Is silent
         */
        void LogInfo(const std::string& data, bool silent);

        /*!
         * @brief Log warning
         * @param data Data
         * @param silent Is silent
         */
        void LogWarning(const std::string& data, bool silent);

        /*!
         * @brief Log error
         * @param data Data
         * @param silent Is silent
         */
        void LogError(const std::string& data, bool silent);

        /*!
         * @brief Log output
         * @param data Data
         * @param silent Is silent
         */
        void LogOutput(const std::string& data, bool silent);

        /*!
         * @brief Set websocket connected
         * @param isConnected Is connected
         */
        static void SetWebSocketConnected(bool isConnected);

        /*!
         * @brief Is websocket connected
         * @return Is connected
         */
        static bool IsWebSocketConnected(){return m_isConnected;};

        /*!
         * @brief Clear terminal
         */
        static void ClearTerminal();

        std::string address;                                                                                            /* Agent Address */
        std::string usedBy;                                                                                             /* Agent Used by */
        int port;                                                                                                       /* Agent Port */
        std::string driverVersion;                                                                                      /* Driver version */
    };
}
#endif //MACOSDRIVER_WSAGENT_HPP
