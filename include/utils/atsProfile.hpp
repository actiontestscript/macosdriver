#ifndef MACOSDRIVER_ATSPROFILE_HPP
#define MACOSDRIVER_ATSPROFILE_HPP
#include "../crowSettings/crowJsonResponse.hpp"
#include "utils.hpp"
#include <pwd.h>
namespace ld{
    /*!
     * @brief Class AtsProfile is the class to get the profile of the user.
     * @details This class is used to get the profile of the user, herited from CrowJsonResponse.
     */
    class AtsProfile : CrowJsonResponse{
    public:
        struct ProfileFirefox{
            std::string name;                                                                                           /*!< The name of profile. */
            bool isRelative{false};                                                                                     /*!< True if the profile is relative path. */
            std::string path;                                                                                           /*!< The path of profile. */
            bool isDefault{false};                                                                                      /*!< True if the profile is default. */
        };

        AtsProfile()= default;
        /*!
         * @brief checkProfile check the profile of the user.
         * @details This method check the profile of the user. create the profile if userProfile as name, if userProfile si absolue path, check if exist and if not create it.
         * @param const <crow::request>& req
         * @param <crow::response>& res
         * @return <bool> true if profile exist, false if not.
         */
        bool checkProfile(const crow::request& req, crow::response& res);

        /*!
         * *@brief printProfile print the profile of the user.
         * @details This method print the userDataPath of the user or error if not exist.
         * @param const <crow::request>& req
         * @param <crow::response> res
         */
        void printProfile(const crow::request& req, crow::response &res);

        /*!
         * @brief getProfileFirefox get the profile of firefox.
         * @details This method get the profile of firefox.
         * @param const <std::string>& userProfile
         * @return <std::vector<ProfileFirefox>> the profile of firefox.
         */
        [[nodiscard]] const std::string &getHomePath() const { return m_homePath; };

        /*!
         * @brief getUserDataPath get the home path of the user.
         * @details This method get the home path of the user.
         * @return <std::string> the home path of the user.
         */
        [[nodiscard]] const std::string &getUserDataPath() const { return m_userDataPath; };

        /*!
         * @brief getUserProfile get the user profile.
         * @details This method get the user profile.
         * @return <std::string> the user profile.
         */
        [[nodiscard]] const std::string &getUserProfile() const { return m_userProfile; };

        /*!
         * @brief getBrowserName get the browser name.
         * @details This method get the browser name.
         * @return <std::string> the browser name.
         */
        [[nodiscard]] const std::string &getBrowserName() const { return m_browserName; };

        /*!
         * @brief getAtsFolder get the ats folder.
         * @details This method get the ats folder.
         * @return <std::string> the ats folder.
         */
        [[nodiscard]] const std::string &getAtsFolder() const { return m_atsFolder; };

        /*!
         * @brief getUserProfileIsAbsolutePath get if the user profile is absolute path.
         * @details This method get if the user profile is absolute path.
         * @return <bool> true if the user profile is absolute path, false if not.
         */
        [[nodiscard]] const bool &getUserProfileIsAbsolutePath() const { return m_userProfileIsAbsolutePath; }

        /*!
         * @brief getProfileAutoName get if the user profile is auto name.
         * @details This method get if the user profile is auto name.
         * @return <std::string>& autoName the auto name of the profile.
         */
        [[nodiscard]] const std::string &getProfileAutoName() const { return m_profileAutoName; }

        /*!
         * @brief setHomePath set the home path.
         * @details This method set the home path.
         * @param const <std::string>& HomePath
         */
        bool setHomePath();

        /*!
         * @brief setUserProfile set the user profile.
         * @details This method set the user profile.
         * @param const <std::string>& UserProfile
         */
        void setUserProfile(const std::string &UserProfile) { m_userProfile = UserProfile; };

        /*!
         * @brief setBrowserName set the browser name.
         * @details This method set the browser name.
         * @param const <std::string>& BrowserName
         */
        void setBrowserName(const std::string &BrowserName) { m_browserName = BrowserName; };

        /*!
         * @brief setUserDataPath set the home path.
         * @details This method set the home path.
         * @param const <std::string>& HomePath
         */
        void setUserDataPath(const std::string &HomePath) { m_userDataPath = HomePath; };

        /*!
         * @brief createProfileFirefox create the profile of firefox.
         * @details This method create the profile of firefox.
         * @param <crow::response>& res
         * @return <bool> true if the profile is created, false otherwise.
         */
        bool createProfileFirefox(crow::response &res);

        /*!
         * @brief getPathFirefoxProfile get the path of profile of firefox.
         * @details This method get the profile of firefox.
         * @return <std::string> the path of the profile of firefox.
         */
        [[nodiscard]] const std::string &getPathFirefoxProfile() const { return m_pathFirefoxProfile; };

        /*!
         * @brief setPathFirefoxProfile set the path of profile of firefox.
         * @details This method set the path of profile of firefox.
         * @param const <std::string>& PathFirefoxProfile
         */
        void setPathFirefoxProfile(const std::string &PathFirefoxProfile) { m_pathFirefoxProfile = PathFirefoxProfile; };

        /*!
         * @brief getListProfilesFirefox get the list of profiles of firefox.
         * @details This method get the list of profiles of firefox.
         * @return <std::vector<ProfileFirefox>> the list of profiles of firefox.
         */
        [[nodiscard]] const std::vector<ProfileFirefox> &getListProfilesFirefox() const { return m_listProfilesFirefox; };

        /*!
         * @brief setUserProfileAbsolutePath set if the user profile is absolute path.
         * @details This method set if the user profile is absolute path.
         * @param const <bool>& userProfileIsAbsolutePath
         */
        void setUserProfileAbsolutePath(const bool &userProfileIsAbsolutePath) { m_userProfileIsAbsolutePath = userProfileIsAbsolutePath; }

        /*!
         * @brief setProfileAutoName set the profile auto name.
         * @details This method set the profile auto name.
         * @param const <std::string>& profileAutoName
         */
        void setProfileAutoName(const std::string &profileAutoName) { m_profileAutoName = profileAutoName; }

        /*!
         * @brief createListProfilesFirefox create the list of profiles of firefox.
         * @details This method create the list of profiles of firefox.
         * @return <bool> true if the list is created, false otherwise.
         */
        bool createListProfilesFirefox();

        /*!
         * @brief getProfileDirectoryIsCreated get if the profile directory is created.
         * @details This method get if the profile directory is created.
         * @return <bool> true if the profile directory is created, false otherwise.
         */
        [[nodiscard]] const bool &getProfileDirectoryIsCreated() const { return m_profileDirectoryIsCreated; }

        /*!
         * @brief setProfileDirectoryIsCreated set if the profile directory is created.
         * @details This method set if the profile directory is created.
         * @param const <bool>& profileDirectoryIsCreated
         */
        void setProfileDirectoryIsCreated(const std::string& directoryPath);

        /*!
         * @brief getProfileDirectoryIsWritable get if the profile directory is writable.
         * @details This method get if the profile directory is writable.
         * @return <bool> true if the profile directory is writable, false otherwise.
         */
        [[nodiscard]] const bool &getProfileDirectoryIsWritable() const { return m_profileDirectoryIsWritable; }

        /*!
         * @brief setProfileDirectoryIsWritable set if the profile directory is writable.
         * @details This method set if the profile directory is writable.
         * @param const <bool>& profileDirectoryIsWritable
         */
        void setProfileDirectoryIsWritable(const bool &profileDirectoryIsWritable) { m_profileDirectoryIsWritable = profileDirectoryIsWritable; }

        /*!
         * @brief createProfileDirectory create the profile directory.
         * @details This method create the profile directory.
         * @param const <std::string>& directoryPath
         * @return <bool> true if the profile directory is created, false otherwise.
         */
        static bool createProfileDirectory(const std::string& directoryPath);

        /*!
         * @brief directoryIsWritable check if the directory is writable.
         * @details This method check if the directory is writable.
         * @param const <std::string>& directoryPath
         * @return <bool> true if the directory is writable, false otherwise.
         */
        static bool directoryIsWritable(const std::string &directoryPath);

    private:
        std::string m_homePath{};                                                                                       /*!< The home path. */
        std::string m_atsFolder{"ats/profiles"};                                                                     /*!< The ats folder. */
        std::string m_userDataPath{};                                                                                   /*!< The user data path. */
        std::string m_userProfile{};                                                                                    /*!< The user profile. */
        std::string m_browserName{};                                                                                    /*!< The browser name. */
        std::string m_pathFirefoxProfile{".mozilla/firefox/profiles.ini"};                                           /*!< The path of the firefox profile. */
        std::vector<ProfileFirefox> m_listProfilesFirefox{};                                                            /*!< The list of profiles of firefox. */
        bool m_userProfileIsAbsolutePath{false};                                                                        /*!< The user profile is an absolute path. */
        std::string m_profileAutoName{"ats"};                                                                        /*!< The profile auto name. */
        bool m_profileDirectoryIsCreated{false};                                                                        /*!< The profile directory is created. */
        bool m_profileDirectoryIsWritable{false};                                                                       /*!< The profile directory is writable. */

        /*!
         * @brief _getJsonUSerProfile get the user profile from the json.
         * @details This method get the user profile from the json.
         * @param const <json>& body
         * @return <bool> true if the user profile is found, false otherwise.
         */
        bool _getJsonUSerProfile(const json& body);

        /*!
         * @brief _getJsonBrowserName get the browser name from the json.
         * @details This method get the browser name from the json.
         * @param const <json>& body
         * @return <bool> true if the browser name is found, false otherwise.
         */
        bool _getJsonBrowserName(const json& body);

    };
}
#endif //MACOSDRIVER_ATSPROFILE_HPP
