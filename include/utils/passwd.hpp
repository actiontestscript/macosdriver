#ifndef MACOSDRIVER_PASSWD_HPP
#define MACOSDRIVER_PASSWD_HPP
#include "../W3C/capabilities/cap_os.hpp"
#include "../crowSettings/crowJsonResponse.hpp"
#include <functional>

namespace ld {
    class PasswdData {
    public:
        explicit PasswdData();
        std::vector<std::vector<std::string>> m_passwdData{};
    };

    class Passwd {
        public:
            explicit Passwd(const crow::request& req);
            ~Passwd()=default;
            void printCapabilities(crow::response &res);

        private:
        PasswdData m_passwdData;
        std::string m_login{};
        std::string m_password{};
        std::string m_passwordHash{"5085901176080100765"};
        bool m_check{false};
        void _parseJson(const crow::request &req);
        bool checkLoginPassword();
    };
}

#endif //MACOSDRIVER_PASSWD_HPP
