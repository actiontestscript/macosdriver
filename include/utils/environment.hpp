#ifndef MACOSDRIVER_ENVIRONMENT_HPP
#define MACOSDRIVER_ENVIRONMENT_HPP
#include "../../apps/config.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <string>
#include <algorithm>

namespace ld{
    /*!@class Environment
     * @brief The class Environment
     * @details This class is used to get the environment of the application.
     */
    class Environment {
    private:
        std::string m_distributionName{"unknown"};                                                                   /*!<  distribution name */
        std::string m_sessionType{"none"};                                                                           /*!<  session type */
        bool m_graphicalEnvironment{false};                                                                             /*!<  graphical environment */
        bool m_isDockerContainers{false};                                                                                /*!<  docker containers */

    public:
        /*!
         * @brief The constructor of the class Environment
         * @details This constructor is used to get the environment of the application.
         */
        Environment();

        /*!
         * @brief getLinuxDistribution
         * @details This function is used to get the linux distribution in file /etc/os-release.
         * @return true if the distribution is found, false otherwise.
         */
        bool getLinuxDistribution();

        /*!
         * @brief checkEnvironmentVariable
         * @details This function is used to check the environment variable, set the session type and the graphical environment.
         * @return true if the environment variable is found, false otherwise.
         */
        bool checkEnvironmentVariable();

        /*!
         * @brief getDistributionName
         * @details This function is used to get the distribution name.
         * @return the distribution name.
         */
        [[nodiscard]] std::string getDistributionName() const { return m_distributionName; }

        /*!
         * @brief getSessionType
         * @details This function is used to get the session type.
         * @return the session type.
         */
        [[nodiscard]] std::string getSessionType() const { return m_sessionType; }

        /*!
         * @brief getGraphicalEnvironment
         * @details This function is used to get the graphical environment.
         * @return true if the graphical environment is found, false otherwise.
         */
        [[nodiscard]] bool getGraphicalEnvironment() const { return m_graphicalEnvironment; }

        [[nodiscard]] bool getDockerContainers();

        [[nodiscard]] bool isDockerContainers() const { return m_isDockerContainers; }
    };


}
#endif //MACOSDRIVER_ENVIRONMENT_HPP
