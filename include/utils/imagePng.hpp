#ifndef MACOSDRIVER_IMAGEPNG_HPP
#define MACOSDRIVER_IMAGEPNG_HPP
#include "../../../apps/config.hpp"
#include <png.h>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <vector>

namespace ld{
    class ImagePng {
    private:
        int m_colorType{PNG_FORMAT_RGB};
    public:
        struct [[maybe_unused]] MemoryBuffer {
            uint8_t *data;                                                                                              /*> pointer to the data */
            size_t size;                                                                                                /*> size of the data */
            size_t current_position;                                                                                    /*> current position in the data */
        };

        static void memoryWrite(png_structp pngPtr, png_bytep datas, png_size_t length) {
            auto *buffer = static_cast<MemoryBuffer *>(png_get_io_ptr(pngPtr));
            memcpy(buffer->data + buffer->current_position, datas, length);
            buffer->current_position += length;
        }

        static void memoryFlush(png_structp pngPtr) {
            auto *buffer = static_cast<MemoryBuffer *>(png_get_io_ptr(pngPtr));
            buffer->current_position = 0;
        }

        ImagePng();


        std::vector<png_byte> data{0};
        int width{0};
        int height{0};

        static void createEmptyPNGImage(ImagePng& img, int w, int h) {
            img.width = w;
            img.height = h;
            img.data.resize(w * h * 4, 0);
        }
        std::vector<unsigned char> pngImageToBytes(const ImagePng& img);
        std::vector<unsigned char> pngImageToBytes(ImagePng& img,const int& w, const int& h);
        void setAllPixelColor(const int& red = 0, const int& green = 0, const int& blue = 0,const int& alpha = 255);
        void setPixelColor(const int& x,const int& y, const int& red = 0, const int& green = 0, const int& blue = 0,const int& alpha = 255);

        static std::vector<uint8_t> vectorToPng(const std::vector<uint8_t> &image_data, const int& width, const int& height, const int& bytes_per_line) {
            png_structp pngPtr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
            if (!pngPtr) {
                if(ld::Config::logType != "NONE")
                    LD_LOG_ERROR << "imgPng vectorToPng Error : Unable to create PNG writing structure." << std::endl;
                return std::vector<uint8_t> {};
            }

            png_infop infoPtr = png_create_info_struct(pngPtr);
            if (!infoPtr) {
                if(ld::Config::logType != "NONE")
                    LD_LOG_ERROR << "imgPng vectorToPng Error : Unable to create Information structure PNG." << std::endl;

                png_destroy_write_struct(&pngPtr, NULL);
                return std::vector<uint8_t> {};
            }

            if (setjmp(png_jmpbuf(pngPtr))) {
                if(ld::Config::logType != "NONE")
                    LD_LOG_ERROR << "Erreur : Problem while converting to PNG." << std::endl;
                png_destroy_write_struct(&pngPtr, &infoPtr);
                return std::vector<uint8_t> {};
            }

            MemoryBuffer memoryBuffer{};
            memoryBuffer.size = width * height * 4;
            memoryBuffer.data = new uint8_t[memoryBuffer.size];
            memoryBuffer.current_position = 0;

            png_set_write_fn(pngPtr, &memoryBuffer, memoryWrite, memoryFlush);

            png_set_IHDR(pngPtr, infoPtr, width, height, 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

            png_write_info(pngPtr, infoPtr);

            std::vector<png_bytep> row_pointers(height);
            for (int i = 0; i < height; ++i) {
                row_pointers[i] = const_cast<png_bytep>(&image_data[i * bytes_per_line]);
            }

            png_write_image(pngPtr, row_pointers.data());
            png_write_end(pngPtr, NULL);

            std::vector<uint8_t> image(memoryBuffer.data, memoryBuffer.data + memoryBuffer.current_position);
//            delete[] memoryBuffer.data;

            png_destroy_write_struct(&pngPtr, &infoPtr);

            return image;
        }
        void setColorType(int colorType);
        int getColorType() const{return m_colorType;};
    };

}
#endif //MACOSDRIVER_IMAGEPNG_HPP
