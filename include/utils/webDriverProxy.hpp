#ifndef MACOSDRIVER_WEBDRIVERPROXY_HPP
#define MACOSDRIVER_WEBDRIVERPROXY_HPP
#include "../../apps/config.hpp"
#include "../crowSettings/crowJsonResponse.hpp"
#include "../crowSettings/crowSettings.hpp"
#include <httplib.h>
namespace ld{
    class WebDriverProxy{
    public:
        explicit WebDriverProxy();
        static void Start(int portIn, int portOut, const std::string& bindAddr);
    };
}

#endif //MACOSDRIVER_WEBDRIVERPROXY_HPP
