#ifndef MACOSDRIVER_OPTIONLINECMD_HPP
#define MACOSDRIVER_OPTIONLINECMD_HPP
#include "../../apps/config.hpp"
#include "errors.hpp"
#include "../../macosdriverConfig.h"
#include "../utils/utils.hpp"
#include <map>
#include <regex>
#include <utility>

using std::pair;
using std::map;
using namespace ld;

namespace ld {
/*!
* \brief Class OptionsStruct
* Class set structur option
*
*/
    class OptionsStruct {
    public:
        struct strOptions {
            string key;                        /*!< key option */
            string value;                    /*!< value option */
            string detailsHelp;                /*!< details help option */
            string regValidate;                /*!< regular expression validate option */
            int errorNumber = 0;                /*!< error number */
            bool visibility = true;            /*!< visibility option */
            bool isCmdLineSet = false;                /*!< is command line set option */
        };
    };

/*!
* \brief Class OptionLineCmd
* Class set and manage command line options
*/
    class OptionLineCmd {

    public:
        /*!
        * \brief Constructor
        * Constructor of class OptionLineCmd
        * \param argc : number of arguments
        * \param argv : list of arguments
        */
        OptionLineCmd(int argc, char **argv);

        /*!
        * \brief Constructor
        * Constructor of class OptionLineCmd
        * \param argc : number of arguments
        * \param argv : list of arguments
        * \param listOptions : list of options
        * \param errors : list of errors
        */
        OptionLineCmd(int argc, char **argv, map<string, OptionsStruct::strOptions> listOptions, Errors errors);

//    OptionLineCmd(int i, char **pString, map<string, OptionsStruct::strOptions> map1, Errors errors);

/*!
    *\brief SetOptions
    * Set options
    * \param key : key option
    * \param value : value option
    * \param detailHelp : details help option
    * \param regValidate : value regExp to validate parametre
    */
        void setOptions(const string &key, const string &value, const string &detailHelp, const string &regValidate);

        /*!
        * \brief  setOptionsValue
        * Set value option
        * \param key : key option
        * \param value : value option
        * \return bool false if errors
        */
        bool setOptionsValue(const string &key, const string &value);

        /*!
        * \brief  setOptionsIsCmdSet
        * Set isCmdSet value option
        * \param key : key option
        * \param value : value option of isCmdSet
        * \return bool false if errors
        */
        bool setOptionsIsCmdSet(const string &key, const bool &value);

        /*!
        *\brief getOption
        * Get lists of options
        * \return list of options
        */
        map<string, OptionsStruct::strOptions> getOptions() { return m_listArgv; }

        /*!
        * \brief getOptionValue
        * Get value option
        * \param key : key option
        * \return string of value options
        */
        string getOptionValue(const string &key);

        /*!
        * \brief getOptionIsCmdSet
        * Get value IsCmdSet option
        * \param key : key option
        * \return bool of value isCmdSet options
        */
        bool getOptionIsCmdSet(const string &key);


        /*!
        * \brief getOptionsError
        * Get error option
        * \return return a boolean at true if an error is detected when entering a parameter
        */
        [[nodiscard]] const bool getOptionsError() const { return m_optionsError; };

        /*!
        * \brief readOptions
        * read the options enter in parameters
        */
        bool readOptions();

        /*!
        * \brief _checkInput
        * check input options
        * \return bool false if errors
        */
        bool _checkInput() { return __checkInput(); };  //pour test

        /*!
         * \brief getNumError
         * get number of errors
         * \return number of errors
         */
        [[nodiscard]] int getNumError() const { return m_numError; }

        /*!
         * \brief setNumError
         * set number of errors
         * \param numError : number of errors
         */
        void setNumError(int numError) { m_numError = numError; }

        /*!
         * \brief getDetailsErrors
         * get details errors
         * \return details errors
         */
        string getDetailsErrors() { return m_detailsErrors; }

        /*!
         * \brief setDetailsErrors
         * set details errors
         * \param detailsErrors : details errors
         */
        void setDetailsErrors(string detailsErrors) { m_detailsErrors = std::move(detailsErrors); }

        /*!
         * @brief getPortArgument : get port argument
         * @return <bool> : true if port argument
         */
        [[nodiscard]] bool getPortArgument() const { return m_PortArgument; }

        /*!
         * @brief setPortArgument : set port argument
         * @param portArgument : true if port argument
         */
        void setPortArgument(const bool& portArgument) { m_PortArgument = portArgument; }


    private:
        int m_argc;                                                /*!< number of arguments */
        bool m_options;                                            /*!< options */
        map<string, OptionsStruct::strOptions> m_listArgv;        /*!< list of options */
        vector<string> m_argv;                                    /*!< list of arguments */
        bool m_optionsError;                                    /*!< options error */
        Errors m_errors;                                        /*!< message errors */
        int m_numError;                                            /*!< number errors */
        string m_detailsErrors;                                    /*!< details errors */
        bool m_PortArgument{false};                                /*!< is port argument */
        /*!
        * \brief _checkInput
        * check input options
        * \return bool false if errors
        */
        bool __checkInput();

    };
}
#endif //MACOSDRIVER_OPTIONLINECMD_HPP
