#ifndef MACOSDRIVER_OPTIONS_HPP
#define MACOSDRIVER_OPTIONS_HPP
#include "optionLineCmd.hpp"
using namespace ld;
namespace ld {
/*!
*\brief Class OptionsData
* Class OptionsData is a data class that contains all the options in program input parameters
*/

    class OptionsData {
        std::map<string, OptionsStruct::strOptions> m_listOptions;            //!< map of all options

    public:
        /*!
        *\brief Constructor
        * Construcor of OptionsData
        */
        OptionsData();

        /*!
        * \brief getOptions
        * Get options from map
        * \return map for all options
        */
        std::map<string, OptionsStruct::strOptions> getOptions() { return m_listOptions; }
        bool setOptionsIsCmdSet(const string &key, const bool &value);
    };
}
#endif //MACOSDRIVER_OPTIONS_HPP
