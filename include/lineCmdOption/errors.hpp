#ifndef MACOSDRIVER_ERRORS_HPP
#define MACOSDRIVER_ERRORS_HPP
#include "../../apps/config.hpp"
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::to_string;

/*! @namespace ld
 * @brief Namespace for MacOsDriver
 * @details Namespace for MacOsDriver
 */
namespace ld {
/*!
* \class ErrorsData
* \brief set data for error message
*/
    class ErrorsData {
    public:
        struct ErrorInfo {
            int errorNumber = 0;        /*!< Error Number type int */
            string errorMessage;    /*!< Error Message type string */
            int gravity;            /*!< Error Gravity type int */
        };
    };

/*!
* \class Errors
* \brief manage error in software
*/

    class Errors {

    private :
        vector<ErrorsData::ErrorInfo> m_errors;    /*!< vector of ErrorsData */

    public:
        enum {
            INFO, WARNING, ERROR, CRITICAL
        };    /*!< enum of gravity */
        /*!
        * \brief Constrcutor
        *
        * Constructor of Errors inititialize m_errors
        *
        */
        Errors();

        /*!
        * \brief getError
        *
        * get a structur of ErrorsData
        *
        * \param <int> errorNumber : number of error
        * \return a struct of ErrorData
        */
        const ErrorsData::ErrorInfo getError(int errorNumber);

        /*!
        * \brief display Error
        *
        * Display error message in console
        *
        * \param errorNumber : number of error
        */

        void displayError(int errorNumber);
    };
}

using namespace ld;

#endif //MACOSDRIVER_ERRORS_HPP
