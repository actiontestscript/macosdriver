#ifndef MACOSDRIVER_GI_X11_HPP
#define MACOSDRIVER_GI_X11_HPP
#include "graphicInterface.hpp"
/*
#define Q_SIGNALS
#include <QtGui/QGuiApplication>
#include <QtGui/QScreen>
#include <QtGui/QPixmap>
#include <QtCore/QBuffer>
#undef Q_SIGNALS
*/

/*
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include "../utils/imagePng.hpp"
*/
namespace ld{
    class GI_X11 : public GraphicInterface{
    public:
        bool createWindow() override {return true;};
        std::vector<uint8_t> screenShot(const int& x, const int& y, const int& width, const int& height) override ;
    };
}
#endif //MACOSDRIVER_GI_X11_HPP
