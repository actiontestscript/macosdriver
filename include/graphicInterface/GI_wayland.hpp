#ifndef MACOSDRIVER_GI_WAYLAND_HPP
#define MACOSDRIVER_GI_WAYLAND_HPP
#include "graphicInterface.hpp"
#include <cstring>
#include <wayland-client.h>
#include <wayland-egl.h>
#include <wayland-cursor.h>
#include <wayland-client-protocol.h>

namespace ld{
    class GI_Wayland : public GraphicInterface{
    private:
        static uint32_t m_outputId;                                                                /*!< Output id */
        static uint32_t m_screenWidth;                                                                /*!< Output id */
        static uint32_t m_screenHeight;                                                                /*!< Output id */
    public:
        /*!
         * @brief GI_Wayland
         * @details Constructor of the class.
         */
        GI_Wayland();
        /*!
         * @brief ~GI_Wayland
         * @details Destructor of the class.
         */
        ~GI_Wayland();

        static void output_handle_geometry(void *data, struct wl_output *wl_output,
                                           int32_t x, int32_t y,
                                           int32_t physical_width, int32_t physical_height,
                                           int32_t subpixel, const char *make,
                                           const char *model, int32_t transform)
        {
            // Store the output size
            m_screenWidth = physical_width;
            m_screenHeight = physical_height;
        }

        constexpr static const struct wl_output_listener output_listener = {
                output_handle_geometry,
                NULL,
                NULL,
                NULL
        };

        static void registry_handle_global(void *data, struct wl_registry *registry,
                                           uint32_t id, const char *interface, uint32_t version)
        {
            if (std::strcmp(interface, "wl_output") == 0) {
                m_outputId = id;
            }
        }

        static void registry_handle_global_remove(void *data, struct wl_registry *registry,uint32_t name){ }

        constexpr static const struct wl_registry_listener registry_listener = {
                registry_handle_global,
                registry_handle_global_remove
        };
        static void getScreenSize();
    };
}
#endif //MACOSDRIVER_GI_WAYLAND_HPP
