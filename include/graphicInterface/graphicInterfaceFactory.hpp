#ifndef MACOSDRIVER_GRAPHICINTERFACEFACTORY_HPP
#define MACOSDRIVER_GRAPHICINTERFACEFACTORY_HPP
#include <memory>
#include "GI_X11.hpp"

namespace ld{
    class GraphicInterfaceFactory{
    public:
        template <typename T>
        std::unique_ptr<T> createGraphicInterface(const std::string& typeInterface) {
            if (typeInterface == "x11") {
                auto ig = std::make_unique<GI_X11>();
                return std::unique_ptr<GraphicInterface>(dynamic_cast<GraphicInterface *>(ig.release()));
            } else {
                return nullptr;
            }
        }
    };
}//namespace ld
#endif //MACOSDRIVER_GRAPHICINTERFACEFACTORY_HPP
