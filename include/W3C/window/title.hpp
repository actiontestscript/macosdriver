#ifndef MACOSDRIVER_TITLE_HPP
#define MACOSDRIVER_TITLE_HPP
#include "../../apps/config.hpp"
#include "../../W3C/session/session.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../../drivers/driverFactory.hpp"

namespace ld{
    class WindowTitleData{
        public:
        std::string id{};
        int pid{-1};
        int handle{-1};
        std::string tag {"Window"};
        double width {0.0};
        double height {0.0};
        double x {0.0};
        double y {0.0};

        std::string appBuildVersion{};
        std::string appIcon{};
        std::string appName{};
        std::string appPath{};
        std::string appVersion{};
    };

    class WindowTitle : CrowJsonResponse {
    private:
        Session* m_session{};                                                                                           /*< Session object */
        WindowTitleData m_windowTitleData{};                                                                                      /*!< The data of the window title. */
        std::string m_titleUid{};                                                                                       /*!< The title uid. */
        std::string m_driverName{};                                                                                       /*!< The driver name. */
        char m_postDataDelimiter{'\n'};                                                                                 /*< Post data delimiter */
        /*!
         * @brief _jsonIsValid Check if the json is valid
         * @details Check if the json is valid
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the json is valid, false otherwise
         */
        bool _jsonIsValid(const crow::request &req, crow::response &res);

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req);

        /*!
         * @brief _parseJson Parse the post data json
         * @details Parse the post data json
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parseJson(const crow::request &req);

        /*!
         * @brief _strParseString Parse the string to get a string
         * @details Parse the string to get a string
         * @param std::string& strResearch The string
         * @param const char& delimiter The delimiter
         * @param const std::string& defaultValue The default value
         * @param std::string& data The data
         */
        [[maybe_unused]] void _strParseString(std::string &strResearch, const char& delimiter, const std::string& defaultValue, std::string &data);

        void _setWindowTitleData();
    public:
        //WindowTitle();
        bool execute(const crow::request &req, crow::response &res);
        void printCapabilities(crow::response &res);

    };
}
#endif //MACOSDRIVER_TITLE_HPP
