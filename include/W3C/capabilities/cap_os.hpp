#ifndef MACOSDRIVER_CAP_OS_HPP
#define MACOSDRIVER_CAP_OS_HPP
#include "../../apps/config.hpp"
#include "../../macosdriverConfig.h"
#include "../../utils/utils.hpp"
#include "capabilities.hpp"
#include <sys/stat.h>
#include <sys/sysctl.h>
#include <sys/utsname.h>

namespace ld{
    /*!
     * @class OsData
     * @brief Class for OsData extends Capabilities
     * @details Class for OsData extends Capabilities
     */
    class OsData {
    public:
        enum class GraphicsInterface {
            X11,
            Wayland,
            None,
            Aqua
        };
        std::string machineName{};                                                     /*!< The name of machine. */
        std::string version{};                                                  /*!< The version of the operating system. */
        std::string processorArch{};                                                     /*!< The architecture of the operating system. */
        std::string platformName{};                                             /*!< The name of the platform. */
        std::string platformVersion{};                                          /*!< The version of the platform. */
        std::string driverVersion{};                                            /*!< The version of the driver. */
        std::string countryCode{"033"};                                      /*!< The country code. */
        std::string screenWidth{"0"};                                        /*!< The screen width. */
        std::string screenHeight{"0"};                                       /*!< The screen height. */
        std::string virtualWidth{"0"};                                       /*!< The virtual screen width. */
        std::string virtualHeight{"0"};                                       /*!< The virtual screen height. */
        std::string systemDriveMountPoint{};                                  /*!< The system drive mount point. */
        std::string systemDriveTotalSize{0};                                   /*!< The system drive total size. */
        std::string systemDriveFreeSpace{0};                                   /*!< The system drive free space. */
        std::string buildNumber{};                                              /*!< The build number. */
        std::string processorName{};                                            /*!< The name of the processor. */
        std::string processorSocket{};                                          /*!< The socket of the processor. */
        std::string processorSpeed{};                                           /*!< The speed of the processor. */
        long processorSpeedGhz{0};                                           /*!< The speed of the processor. */
        std::string processorCoreNumber{};                                      /*!< The number of core of the processor. */
        GraphicsInterface graphicsInterface{GraphicsInterface::None};        /*!< The graphics interface. */
        bool withGraphicsInterface{false};                                        /*!< The graphics interface. */
        bool headlessMode{false};                                                   /*!< The headless interface. */
        bool interactive{true};                                                    /*!< The interactive interface. */

        //setting timeouts
        int script{30000};                                                      /*!< The timeout to use for asynchronous scripts. if null script infinite execution*/
        int pageLoad{300000};                                                   /*!< The timeout to use for page loads. */
        float implicit{0.0f};                                                   /*!< The timeout to use for implicit waits. */
        //navigation
        int currentUrl{-1};                                                        /*!< The url index to navigate to. */
        std::vector<std::string> urls{};                                           /*!< The url vector to navigate to. */
    };

    /*!
     * @class Os
     * @brief Class for Os extends Capabilities
     * @details Class for CapabilitiesFactory for Os
     */
    class Os : public Capabilities{
    private:
        /*!
         * @brief _setOsBase update the os data
         * @details This method is used to update the os data wtih the system data.
         * @return true if the os data are correct, false otherwise and set the error message.
         */
        bool _setOsBase(CrowJsonResponse::MsgError& msgError);
// @TODO recuperation du countrycode avec la variable env : LANG
// @TODO screenWidth  screenHeight en fonction du driver ecran
// @TODO processorName, processeur speed, processor count processorSocket via proc/cpuinfo

    public:
        static Type type;                                                                       /*!< The type of capabilities. */
        Os();
        OsData m_data;                                                                          /*!< The data of the Os. */
        /*!
         * @brief executeCapabilities overide method from Capabilities
         * @details This method is used to execute the capabilities check and settings.
         * @param <crow::request>& req The request.
         * @param <crow::response>& res The response.
         * @return true if the capabilities are correct return json capabilities, false otherwise and set the error message.
         */
        bool executeCapabilities(const crow::request& req, crow::response& res) override;

        /*!
         * @brief printCapabilities overide method from Capabilities
         * @details This method is used to print the capabilities in json format.
         * @param <json>& j The json body.
         */
        void printCapabilities(json &j) override;

        /*!
         * @brief getTimeouts overide method from Capabilities
         * @details This method is used to get the timeouts.
         * @param const <std::string&> sessionId
         * @param <json&> j
         * @return true if the timeouts are correct, false otherwise and set the error message.
         */
        bool getTimeouts(const std::string& sessionId,json& j) override;

        /*!
         * @brief setTimeouts overide method from Capabilities
         * @details This method is used to set the timeouts.
         * @param const <std::string&> sessionId
         * @param const <crow::request&> req
         * @return true if the timeouts are correct, false otherwise and set the error message.
         */
        bool setTimeouts(const std::string& sessionId,const crow::request& req) override;

        /*!
         * @brief navigateTo overide method from Capabilities
         * @details This method is used to navigate to the url.
         * @param const <std::string&> sessionId
         * @param const <crow::request&> req
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool navigateTo(const std::string& sessionId,const crow::request& req) override;

        /*!
         * @brief getCurrentUrl overide method from Capabilities
         * @details This method is used to get the current url.
         * @param const <std::string&> sessionId
         * @param <json&> j The json return data.
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool getCurrentUrl(const std::string& sessionId,json& j) override;

        /*!
         * @brief navigateBack overide method from Capabilities
         * @details This method is used to navigate back.
         * @param const <std::string&> sessionId
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool navigateBack(const std::string& sessionId) override;

        /*!
         * @brief navigateForward overide method from Capabilities
         * @details This method is used to navigate forward.
         * @param const <std::string&> sessionId
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool navigateForward(const std::string& sessionId) override;

        [[nodiscard]] OsData getData() const{return m_data;};

        void setProcessorInfo();

        void setDiskInfo();

        void setScreenInfo();

        template <typename T>
        T getSysCtl(const std::string& name){
            T value;
            size_t  bufferSizes = sizeof(value);
            sysctlbyname(name.c_str(), &value, &bufferSizes, nullptr, 0);
            return value;
        }
        template <>
        std::string getSysCtl(const std::string& name){
            char buffer[256];
            size_t  bufferSizes = sizeof(buffer);
            sysctlbyname(name.c_str(), &buffer, &bufferSizes, nullptr, 0);
            return {buffer};
        }


    };
}
#endif //MACOSDRIVER_CAP_OS_HPP
