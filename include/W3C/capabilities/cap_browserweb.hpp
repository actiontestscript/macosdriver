#ifndef MACOSDRIVER_CAP_BROWSERWEB_HPP
#define MACOSDRIVER_CAP_BROWSERWEB_HPP
#include "../../apps/config.hpp"
#include "capabilities.hpp"
#include "../session/session.hpp"


namespace ld{
    /*!
     * @class BrowserWebData
     * @brief Class for BrowserWeb Data
     * @details content information capabilities od browser agent
     */
    class BrowserWebData{
        /*!
         * @enum proxyType
         * @brief enum for proxy type
         * @details enum for proxy type
         */
        enum class proxyType{pac,direct, autodetect, system, manual};           /*!< The type of proxy configuration. autorized value*/
    public:
        bool isSetAcceptInsecureCerts{false};                                   /*!< isSet by firstmatc */
        bool isSetSetWindowRect{false};                                         /*!< isSet by firstmatc */
        bool isSetStrictFileInteractability{false};                             /*!< isSet by firstmatch */
        bool isSetPageLoadStrategy{false};                                      /*!< isSet by firstmatch */
        bool isSetSocksVersion{false};                                          /*!< isSet by firstmatch */
        bool isNoProxy{false};                                                  /*!< isSet by firstmatch */
        bool isFirstMatchBrowserName{false};                                    /*!< isSet by firstmatch */
        bool isAlwaysMatchBrowserName{false};                                   /*!< isSet by alwaysmatch */

        std::string browserName;                                                /*!< The name of the browser being used. */
        std::string browserVersion;                                             /*!< The version of the browser being used. */
        std::string platformName;                                               /*!< The name of the platform on which the browser is being run. (linux, windows, mac ...)*/
        bool acceptInsecureCerts{false};                                        /*!< Whether untrusted and self-signed SSL certificates are trusted implicitly on
                                                                                    navigations. When true, the capability takes precedence over the
                                                                                    acceptInsecureCerts setting in the [W3C WebDriver] section of the
                                                                                    [WebDriver Protocol]. */
        std::string pageLoadStrategy{"normal"};                              /*!< The page load strategy to use. */
        bool setWindowRect{false};                                              /*!< Whether the Set Window Rect command is supported. */

        std::string unhandledPromptBehavior;                                    /*!< The default action to take when an unhandled prompt is encountered. */
        bool strictFileInteractability{false};                                  /*!< Whether file uploads should be restricted to the same origin as the
                                                                                     current browsing context. When true, file uploads are restricted to
                                                                                     the same origin as the current browsing context. When false, file
                                                                                     uploads are allowed from any origin. */

        //setting Proxy
        std::string proxyType;                                                  /*!< The type of proxy configuration. */
        std::string proxyAutoconfigUrl;                                         /*!< The URL for the proxy autoconfiguration file. */
        std::string ftpProxy;                                                   /*!< The proxy to use for FTP requests. */
        std::string httpProxy;                                                  /*!< The proxy to use for HTTP requests. */
        std::vector<std::string> noProxy;                                       /*!< A vector list of hosts for which no proxy should be used. */
        std::string sslProxy;                                                   /*!< The proxy to use for SSL requests. */
        std::string socksProxy;                                                 /*!< The proxy to use for SOCKS requests. */
        uint8_t socksVersion;                                                   /*!< The version of the SOCKS proxy protocol to use. between  and 255. */

        //setting timeouts
        int script{30000};                                                      /*!< The timeout to use for asynchronous scripts. if null script infinite execution*/
        int pageLoad{300000};                                                   /*!< The timeout to use for page loads. */
        float implicit{0.0f};                                                   /*!< The timeout to use for implicit waits. */

        //navigation
        int currentUrl{-1};                                                        /*!< The url index to navigate to. */
        std::vector<std::string> urls{};                                           /*!< The url vector to navigate to. */
    };

/*!
 * @class BrowserWeb extends Capabilities  class Abstract for design factory pattern
 * @brief Class for BrowserWeb
 * @details Class for CapabilitiesFactory for BrowserWeb
 */
    class BrowserWeb : public Capabilities{
    protected:
        /*!
         * @brief _checkIsValidElements
         * @details
         * This method is used to check if the elements are valid.
         * @param <json>& j The json body.
         * @return true if the elements are valid, false otherwise and set the error message.
         */
        static bool _checkIsValidElements(json &j,CrowJsonResponse::MsgError& msgError);
        int m_dataSelected{0};                                                                  /*!< The data selected. */


    public:
        static Type type;                                                                       /*!< The type of capabilities. */
        //Type type{Capabilities::Type::BrowserWeb};                                            /*!< The type of capabilities. */
        BrowserWebData m_data;                                                                  /*!< The data of the BrowserWeb. */
        std::vector<BrowserWebData> m_dataVector;                                               /*!< The data vector of the BrowserWeb. */

        /*!
         * @brief printCapabilities overide method from Capabilities
         * @details This method is used to print the capabilities in json format.
         * @param <json>& j The json body.
         */
        void printCapabilities(json &j) override;

        /*!
         * @brief executeCapabilities overide method from Capabilities
         * @details This method is used to execute the capabilities check and settings.
         * @param <crow::request>& req The request.
         * @param <crow::response>& res The response.
         * @return true if the capabilities are correct return json capabilities, false otherwise and set the error message.
         */
        bool executeCapabilities(const crow::request& req, crow::response& res) override;

        /*!
         * @brief getTimeouts overide method from Capabilities
         * @details This method is used to get the timeouts.
         * @param const <std::string&> sessionId
         * @param <json&> j
         * @return true if the timeouts are correct, false otherwise and set the error message.
         */
        bool getTimeouts(const std::string& sessionId,json& j) override;

        /*!
         * @brief setTimeouts overide method from Capabilities
         * @details This method is used to set the timeouts.
         * @param const <std::string&> sessionId
         * @param <crow::request>& req
         * @return true if the timeouts are correct, false otherwise and set the error message.
         */
        bool setTimeouts(const std::string& sessionId,const crow::request& req) override;

        /*!
         * @brief navigateTo overide method from Capabilities
         * @details This method is used to navigate to the url.
         * @param const <std::string&> sessionId
         * @param const <crow::request&> req
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool navigateTo(const std::string& sessionId,const crow::request& req) override;

        /*!
         * @brief getCurrentUrl overide method from Capabilities
         * @details This method is used to get the current url.
         * @param const <std::string&> sessionId
         * @param <json&> j The json return data.
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool getCurrentUrl(const std::string& sessionId,json& j) override;

        /*!
         * @brief navigateBack overide method from Capabilities
         * @details This method is used to navigate back.
         * @param const <std::string&> sessionId
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        bool navigateBack(const std::string& sessionId) override;

        /*!
          * @brief navigateForward overide method from Capabilities
          * @details This method is used to navigate forward.
          * @param const <std::string&> sessionId
          * @return true if the navigate is correct, false otherwise and set the error message.
          */
        bool navigateForward(const std::string& sessionId) override;

        /*!
         * @brief mandatoryFields
         * @details
         * This method is used to check if the mandatory fields are present in the json.
         * @param <json>& j The json body.
         * @return true if the mandatory fields are present, false otherwise and set the error message.
         */
        static bool mandatoryFields(json& j);

        /*!
        * @brief checkCapabilities
        * @details
        * This method is used to check if the capabilities are correct.
        * @param <json>& j The json body.
        * @return true if the capabilities are correct, false otherwise and set the error message.
        */
        static bool checkCapabilities(json& j, CrowJsonResponse::MsgError& msgError );

        /*!
         * @brief mergeCapabilities
         * @details
         * This method is used to merge the capabilities firstMatch and AlwayMAtch.
         * @param <json>& j The json body.
         */
        void mergeCapabilities(json &j);

        /*!
         * @brief setDataValue
         * @details
         * This method is used to set the data value merge element update data.
         * @param <json>& j The json body.
         * @param <BrowserWebData&> data The data.
         */
        static void setDataValue(const json &j, BrowserWebData& data);

        /*!
         * @brief getDataSelected const
         * @details
         * This method is used to get the data selected.
         * @param <json>& j The json body.
         * @param <BrowserWebData&> data The data.
         * @return <int> The data selected value.
         */
        [[nodiscard]] int getDataSelected() const{return m_dataSelected;};

        /*!
         * @brief setDataSelected
         * @details
         * This method is used to set the data selected.
         * @param <int> dataSelected The data selected value.
         */
        void setDataSelected(int dataSelected){m_dataSelected = dataSelected;};

        /*!
         * @brief selectElement
         * @details
         * This method is used to select the element in the vector.
         * the element is selected by the last browsername present in both firstmatch and always match.
         * if the browsername is not present in both firstmatch and alwaysmatch
         * the element is selected by the last browsername present in firstmatch or if not present in firstmatch in alwaysmatch.
         */
        void selectElement();



    };


};
#endif //MACOSDRIVER_CAP_BROWSERWEB_HPP
