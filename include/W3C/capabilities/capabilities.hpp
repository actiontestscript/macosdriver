#ifndef MACOSDRIVER_CAPABILITIES_HPP
#define MACOSDRIVER_CAPABILITIES_HPP
#include "../../apps/config.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "crow.h"
#include <string>
#include <regex>
//#include "../session/session.hpp"

using json = nlohmann::json;

namespace ld{
    /*!
     * @class Capabilities
     * @brief Interface for Capabilities
     * @details Interface for Capabilities
     */
    class Capabilities {
    public:
        /*!
         * @brief Type enum for Capabilities
         * @details Type enum for Capabilities
         */
        enum Type{
            BrowserWeb,
            BrowserMobile,
            Os
        };

        /*!
         * @brief Destructor
        * @details Destructor of the Capabilities class
        */
        virtual ~Capabilities() = default;

        /*!
         * @brief executeCapabilities method virtual from Capabilities
         * @details This method is used to execute the capabilities check and settings.
         * @param <crow::request>& req The request.
         * @param <crow::response>& res The response.
         * @return true if the capabilities are correct return json capabilities, false otherwise and set the error message.
         */
        virtual bool executeCapabilities(const crow::request& req, crow::response& res)=0;

        /*!
         * @brief printCapabilities method virtual from Capabilities
         * @details This method is used to print the capabilities in json format.
         * @param <json&> j
         */
        virtual void printCapabilities(json &j)=0;

        /*!
         * @brief getTimeouts method virtual from Capabilities
         * @details This method is used to get the timeouts.
         * @param <json&> j
         */
        virtual bool getTimeouts(const std::string& sessionId,json& j)=0;

        /*!
         * @brief setTimeouts method virtual from Capabilities
         * @details This method is used to set the timeouts.
         * @param const <crow::request&> req
         */
        virtual bool setTimeouts(const std::string& sessionId,const crow::request& req)=0;

        /*!
         * @brief navigateTo method virtual from Capabilities
         * @details This method is used to navigate to the url.
         * @param const <crow::request&> req
         */
        virtual bool navigateTo(const std::string& sessionId,const crow::request& req)=0;

        /*!
         * @brief getCurrentUrl method virtual from Capabilities
         * @details This method is used to get the current url.
         * @param const <std::string&> sessionId
         * @param <json&> j The json return data.
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        virtual bool getCurrentUrl(const std::string& sessionId,json& j)=0;

        /*!
         * @brief navigateBack method virtual from Capabilities
         * @details This method is used to navigate back.
         * @param const <std::string&> sessionId
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        virtual bool navigateBack(const std::string& sessionId)=0;

        /*!
         * @brief navigateForward method virtual from Capabilities
         * @details This method is used to navigate forward.
         * @param const <std::string&> sessionId
         * @return true if the navigate is correct, false otherwise and set the error message.
         */
        virtual bool navigateForward(const std::string& sessionId)=0;

        static Type type;   /*!< The type of capabilities. */
    };
}


#endif //MACOSDRIVER_CAPABILITIES_HPP
