#ifndef MACOSDRIVER_CAPABILITIESFACTORY_HPP
#define MACOSDRIVER_CAPABILITIESFACTORY_HPP
#include "../../apps/config.hpp"
#include <memory>
#include "capabilities.hpp"
#include "cap_browserweb.hpp"
#include "cap_os.hpp"


namespace ld{
    /*!
 * @class CapabilitiesFactory
 * @brief Factory for Capabilities
 * @details Factory for Capabilities
 */
    class CapabilitiesFactory {
        /*!
         * @brief Create a Capabilities object
         * @details Create a Capabilities object
         * @param T The type of the Capabilities object
         * @param type The type of the Capabilities object
         * @return A unique pointer to the Capabilities object
         */
    public:
/*
        template <typename T>
        std::unique_ptr<Capabilities> createCapabilities() {
            std::unique_ptr<Capabilities> caps = std::make_unique<T>();
            return caps;
        }

        std::unique_ptr<Capabilities> create(Capabilities::Type type) {
            if(type == Capabilities::BrowserWeb){
                auto caps = createCapabilities<BrowserWeb>();
                caps->type = type;
                return std::unique_ptr<Capabilities>(dynamic_cast<Capabilities*>(caps.release()));
            }
        }
*/
/*
        template <typename T>
        std::unique_ptr<T> createCapabilities(Capabilities::Type type) {
            std::unique_ptr<T> caps;
            if (type == Capabilities::Type::BrowserWeb ) {
                caps = std::make_unique<BrowserWeb>();
            } else if (type == Capabilities::Type::BrowserMobile) {
                //caps = std::make_unique<BrowserMobile>();
            } else if (type ==  Capabilities::Type::Os) {
                //caps = std::make_unique<Os>();
            }
            return std::unique_ptr<T>(std::move(caps));
        }
*/
        /*!
         * @brief Create a Capabilities object for update data
         * @details Create a Capabilities object for update data with dynamic_cast
         * @tparam T The type of the Capabilities object
         * @return A unique pointer to the Capabilities object
         */

        template <typename T>
        std::unique_ptr<T> create(std::string name) {
            if(name == "BrowserWeb"){
                //auto caps = createCapabilities<BrowserWeb>(Capabilities::Type::BrowserWeb);
                auto caps =  std::make_unique<BrowserWeb>();
                return std::unique_ptr<T>(dynamic_cast<T*>(caps.release()));
            }
            else if(name == "Os"){
                auto caps =  std::make_unique<Os>();
                return std::unique_ptr<T>(dynamic_cast<T*>(caps.release()));
            }
            else {
             //   auto caps = createCapabilities<T>(Capabilities::Type::BrowserWeb);
               // return std::unique_ptr<T>(dynamic_cast<T *>(caps.release()));
               return nullptr;
            }
        }

    };



}
#endif //MACOSDRIVER_CAPABILITIESFACTORY_HPP