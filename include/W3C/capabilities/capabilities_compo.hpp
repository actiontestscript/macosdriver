#ifndef MACOSDRIVER_CAPABILITIES_H
#define MACOSDRIVER_CAPABILITIES_H
#include "../../apps/config.hpp"
#include <iostream>
#include <string>
#include <memory>
#include <vector>

namespace ld{
    /*!
     * @brief Capabilities Abstract class
     * @details This class is the base class for all capabilities (DP Composite : class component)
     * @note This class is abstract
     *
     */
    class Capabilities{
    public:
        enum class Type {KString, KBool, KInt, KDouble, KNull,KObject,Karray};
        Capabilities(const std::string& name,Type type): m_name(name),m_type(type) {};
        virtual ~Capabilities() = default;
        virtual void add(std::unique_ptr<Capabilities> capability) {};
        virtual void print() const {
            std::cout << "Name: " << m_name << " type: " << toascii(static_cast<int>(m_type)) << std::endl;
        };
        void setLevel(int level){m_level=level;};
    private:

    protected:
        std::string m_name;
        Type m_type;
        int m_level{0};
    };

    /*!
     * @brief ObjectCapabilities Object class
     * @details This class is the base class for all capabilities - (DP Composite : class composite)
     *
     */
    class ObjectCapabilities : public Capabilities{
    public: ObjectCapabilities(const std::string& name, Type type ): Capabilities(name,type) {};
        void add(std::unique_ptr<Capabilities> capability) override {
            capability->setLevel(this->m_level+1);
            //            this->m_level=this->m_level+1;
            m_capabilities.push_back(std::move(capability));
        };
        void print() const override {
            std::string tab="";
            for(int i=0;i<m_level;i++){
                tab+="\t";
            }
            std::cout<< tab << "Nom:1 " << m_name << std::endl;

            for(auto& capability : m_capabilities){
                capability->print();
            }
        };
    private:
        std::vector<std::unique_ptr<Capabilities>> m_capabilities;
    };

    /*!
     * @brief StringCapabilities String class
     * @details This class is the base class for all capabilities - (DP Composite : class leaf)
     *
     */
    class StringCapabilities : public Capabilities{
    public: StringCapabilities(const std::string& name, Type type, const std::string& value): Capabilities(name,type), m_value(value) {};
        void print() const override {
            std::string tab="";
            for(int i=0;i<m_level;i++){
                tab+="    ";
            }
            //Capabilities::print();
            std::cout<< tab << "Value: " << m_value << std::endl;
        };
    private:
        std::string m_value;
    };




}

#endif //MACOSDRIVER_CAPABILITIES_H
