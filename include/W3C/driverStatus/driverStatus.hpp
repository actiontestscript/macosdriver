#ifndef MACOSDRIVER_DRIVERSTATUS_HPP
#define MACOSDRIVER_DRIVERSTATUS_HPP
#include "../../apps/config.hpp"
#include "../capabilities/cap_os.hpp"
#include "../../utils/network.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../session/session.hpp"



namespace ld{
    /*!
     * @brief Class DriverStatus is the class print state of the macosdriver.
     * @details This class is used to print the state of the macosdriver, herited from CrowJsonResponse.
     */
    class DriverStatus : CrowJsonResponse{
    public:
        /*!
         * @brief printCapabilities print the state of the macosdriver.
         * @details This method print the state of the macosdriver.
         * @param <crow::response> res
         */
        void printCapabilities(crow::response &res);

        /*!
         * @brief setRemoteAddress
         * @details This method is used to set the remote address.
         * @param const <crow::request&> req
         */
        void setRemoteAddress(const crow::request &req);

        /*!
         * @brief getRemoteAddress
         * @details This method is used to get the remote address.
         * @return <std::string> The remote address.
         */
        std::string getRemoteAddress(){return m_remoteAddress;};

    private:
        Os m_osData;                                                                                /*!< The data of the Os. */
        Session* m_session{};                                                                       /*!< The session. */
        std::string m_remoteAddress{};                                                              /*!< The remote address. */
    };

}
#endif //MACOSDRIVER_DRIVERSTATUS_HPP
