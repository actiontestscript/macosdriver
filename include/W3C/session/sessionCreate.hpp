#ifndef MACOSDRIVER_SESSIONCREATE_HPP
#define MACOSDRIVER_SESSIONCREATE_HPP
#include "../../apps/config.hpp"
#include "session.hpp"
#include "crow.h"
#include "../capabilities/capabilitiesFactory.hpp"
#include "../capabilities/cap_browserweb.hpp"
#include "../../drivers/driverFactory.hpp"

//#include <utility>



namespace ld{
    class SessionCreate : public CrowJsonResponse{

    private:
        Session* m_session;                                                                                             /*!< The session. */
        std::string m_sessionId{};                                                                                      /*!< The session id. */
        std::string m_browserName{};                                                                                    /*!< The browser name. */
        std::string m_applicationPath{};                                                                                  /*!< The application path. */

        /*!
         * @brief _getCapName
         * @details This method is used to get the name of the capability define in capabilitiesFactory.
         * @param <std::string&>name
         */
        void _getCapName(std::string& name);

    public:

        explicit SessionCreate();
//        void setSessionId(const std::string &sessionId){m_sessionId = sessionId;}
//        std::string getSessionId(){return m_sessionId;}

        /*!
        * @brief createSession
        * @details
        * This method is used create a new session.
        * @param pointer of structure sessionInfo The information of the session. null if not defined.
        * @return The id of the new session.
        */
        void create(crow::request& req,  crow::response& res);

        /*!
         * @brief getBrowserName
         * @details This method is used to get the browser name from the request.
         * @param <crow::request&>req
         * @param <crow::request&>browserName
         */
        static void getBrowserName(crow::request& req,  std::string &browserName);

        /*!
         * @brief isBrowserWebDriverLaunch
         * @details Tis method is used to check if the session is launch with a browser web driver.
         * @param const <crow::request>& req The request.
         * @param <std::string>& webDriverName The name of the web driver.
         * @return <bool> True if the session is launch with a browser web driver.
         */
        bool isOsDriverLaunch(const crow::request& req, std::string& webDriverName);

        /*!
         * @brief createOsDriver
         * @details This method is used to create a new session with a os driver.
         * @param const <crow::request>& req The request.
         * @param <crow::response>& res The response.
         * @param const <std::string>& webDriverName The name of the web driver.
         */
        void createOsDriver(const crow::request& req,  crow::response& res, const std::string& webDriverName);

        /*!
         * @brief getApplicationPath
         * @details This method is used to get the application path.
         * @return <std::string> The application path.
         */
        [[maybe_unused]] std::string getApplicationPath(){return m_applicationPath;};

        /*!
         * @brief setApplicationPath
         * @details This method is used to set the application path.
         * @param const <std::string>& webDriverName The name of the web driver.
         */
        void setApplicationPath(const std::string& webDriverName){m_applicationPath = webDriverName;};
    };
}


#endif //MACOSDRIVER_SESSIONCREATE_HPP
