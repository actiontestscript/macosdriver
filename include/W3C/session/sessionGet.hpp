#ifndef MACOSDRIVER_SESSIONGET_HPP
#define MACOSDRIVER_SESSIONGET_HPP
#include "../../apps/config.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../capabilities/cap_os.hpp"

namespace ld{
    class SessionGet : CrowJsonResponse{
    private:
        Os m_osData;                                                                              /*!< The data of the Os. */
        std::string sessionId{"root"};                                                        /*!< The session id. */

    public:
        /*!
         * @brief printCapabilities
         * @details Print the capabilities of the session.
         * @param <crow::res> res
         */
        void printCapabilities(crow::response &res);
    };

}
#endif //MACOSDRIVER_SESSIONGET_HPP
