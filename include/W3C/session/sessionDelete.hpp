#ifndef MACOSDRIVER_SESSIONDELETE_HPP
#define MACOSDRIVER_SESSIONDELETE_HPP

#include "../../apps/config.hpp"
#include "session.hpp"
#include "crow.h"
#include "../../include/drivers/driverFactory.hpp"


namespace ld {

    /*! @class SessionDelete extends CrowJsonResponse
     *  @brief Class to delete a session
     *  @details This class is used to delete a session
     */
    class SessionDelete : public CrowJsonResponse{
    public:
        /*!
         * @brief SessionDelete
         * @details
         * Constructor of SessionDelete class
         */
        SessionDelete();
        /*!
         * @brief delSession
         * @details
         * This method is used to delete a session
         * @param sessionId string uuid of session
         * @param res crow::response object
         */
        void delSession(const std::string &sessionId, crow::response &res);

        /*!
         * @brief deleteAllSession
         * @details delete all Session WebDriver
         * This method is used to delete all session WebDriver
         */
        void deleteAllSession();

    private:
        std::string m_sessionId;                            /*!< string uuid of session */
        Session* m_session;                                 /*!< Session object */
    };
}
#endif //MACOSDRIVER_SESSIONDELETE_HPP
