#ifndef MACOSDRIVER_SESSION_HPP
#define MACOSDRIVER_SESSION_HPP
#include "../../apps/config.hpp"
#include <atomic>
#include <mutex>
#include <vector>
#include <string>
#include <iostream>
#include <unordered_map>
#include "guid.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../../W3C/capabilities/capabilities.hpp"
#include "../../ats/recorder/visualAction.hpp"
#include "../../ats/recorder/testSummary.hpp"
#include "../../ats/recorder/visualReport.hpp"
#include "../../utils/environment.hpp"
#include "../../utils/utils.hpp"



namespace ld{
    /*!
     * @class SessionData
     * @brief The SessionData class
     * @details
     * This class is used to store the data of the session.
     */
    class SessionData {
    public:
        /*!
         * @brief The SessionInfo struct
         * @details
         * This struct is used to store the information of a session.
         */
        struct SessionInfo {
            SessionInfo() = default;

            SessionInfo(SessionInfo &&other) noexcept {
                id = std::move(other.id);
                agentName = std::move(other.agentName);
                startTime = other.startTime;
                maxDuration = other.maxDuration;
                timeout = other.timeout;
                isTimeout = other.isTimeout;
                isEnded = other.isEnded;
                isInfinite = other.isInfinite;
                capabilities = std::move(other.capabilities);
                nameCapabilities = std::move(other.nameCapabilities);
                isWebDriver = other.isWebDriver;
                webdriverUrl = std::move(other.webdriverUrl);
                webdriverPort = std::move(other.webdriverPort);
                webdriverPath = std::move(other.webdriverPath);
                webdriverProcessName = std::move(other.webdriverProcessName);
                webdriverName = std::move(other.webdriverName);
                browserName = std::move(other.browserName);
                applicationName = std::move(other.applicationName);
                applicationVersion = std::move(other.applicationVersion);
                applicationPath = std::move(other.applicationPath);
                webDriverOffsetX = other.webDriverOffsetX;
                webDriverOffsetY = other.webDriverOffsetY;
                webDriverHeadlessOffsetX = other.webDriverHeadlessOffsetX;
                webDriverHeadlessOffsetY = other.webDriverHeadlessOffsetY;

            }

            // Move assignment operator
            SessionInfo &operator=(SessionInfo &&other) noexcept {
                id = std::move(other.id);
                agentName = std::move(other.agentName);
                startTime = other.startTime;
                maxDuration = other.maxDuration;
                timeout = other.timeout;
                isTimeout = other.isTimeout;
                isEnded = other.isEnded;
                isInfinite = other.isInfinite;
                capabilities = std::move(other.capabilities);
                nameCapabilities = std::move(other.nameCapabilities);
                isWebDriver = other.isWebDriver;
                webdriverUrl = std::move(other.webdriverUrl);
                webdriverPort = std::move(other.webdriverPort);
                webdriverPath = std::move(other.webdriverPath);
                webdriverProcessName = std::move(other.webdriverProcessName);
                webdriverName = std::move(other.webdriverName);
                browserName = std::move(other.browserName);
                applicationName = std::move(other.applicationName);
                applicationVersion = std::move(other.applicationVersion);
                applicationPath = std::move(other.applicationPath);
                webDriverOffsetX = other.webDriverOffsetX;
                webDriverOffsetY = other.webDriverOffsetY;
                webDriverHeadlessOffsetX = other.webDriverHeadlessOffsetX;
                webDriverHeadlessOffsetY = other.webDriverHeadlessOffsetY;

                return *this;
            }

            std::string id;                                                             /*!< The id of the session */
            std::string agentName{"macosDriver"};                                    /*!< The name of the agent */
            unsigned long long startTime{0};                                            /*!< The start time of the session */
            unsigned long long maxDuration{0};                                          /*!< The max duration of the session */
            unsigned long long timeout{0};                                              /*!< The timeout of the session */
            bool isTimeout{false};                                                      /*!< True if the session is timeout */
            bool isEnded{false};                                                        /*!< True if the session is ended */
            bool isInfinite{false};                                                     /*!< True if the session is infinite */
            std::unique_ptr<Capabilities> capabilities{nullptr};                        /*!< The capabilities of the session */
            std::string nameCapabilities;                                               /*!< The name of the capabilities */
            bool isWebDriver{false};                                                    /*!< True if the session is a WebDriver session */
            std::string webdriverUrl{};                                                 /*!< The url of the session */
            std::string webdriverPort{};                                                /*!< The port of the session */
            std::string webdriverPath{};                                                /*!< The webdriver path of the session */
            std::string webdriverName{};                                                /*!< The webdriver name of the session */
            std::string webdriverProcessName{};                                                /*!< The webdriver name of the session */
            std::string browserName{};                                                  /*!< The browser name of the session */
            std::string applicationName{};                                              /*!< The application name of the session */
            std::string applicationVersion{};                                           /*!< The application version of the session */
            std::string applicationPath{};                                              /*!< The application path of the session */
            double webDriverOffsetX{0.0};                                                /*!< The offset x of the web driver */
            double webDriverOffsetY{0.0};                                                /*!< The offset y of the web driver */
            double webDriverHeadlessOffsetX{0.0};                                        /*!< The offset Headless x of the web driver */
            double webDriverHeadlessOffsetY{0.0};                                        /*!< The offset Headless y of the web driver */

        };
    };

    /*!
     * @class Session
     * @brief The Session class
     * @details
     * This class is a singleton class. Is used to manage the session.
     */
    class Session {
        /*!
         * @brief ProcessInfo struct
         * @details This struct is used to store the process information
         * @param <int> pid The process id
         * @param <int> ppid The parent process id
         * @param <std::string> command The command of the process
         */
    public:

        /*!
         * @brief The ProcessInfo struct
         * @details
         * This struct is used to store the information of a process.
         */
        struct ProcessInfo {
            std::string command{};                                                                /*!< The command of the process */
            std::string url{};                                                                    /*!< The url of the process */
            std::string port{"0"};                                                                   /*!< The port of the process */
            bool isFirstStart{true};                                                            /*!< True if the process is started for the first time */
        };
        static std::mutex m_mutex;                                                              /*!< The mutex of the singleton */
        std::map<std::string, ProcessInfo> m_processes{};                                       /*!< The map of processes */
        std::map<std::string, std::thread> m_threads{};                                         /*!< The map of threads */
//        std::vector<std::pair<std::string,std::string>> m_nameWebDriverProcessAndPids{};
//        std::unordered_map<std::string, std::vector<std::string>> m_nameBrowserPids{};

        struct RecorderInfo {
            std::string id{};                                                                   /*!< The id of the recorder */
            VisualAction currentAction{};                                                       /*!< The current action */
            VisualReport currentReport{};                                                       /*!< The current report */
            TestSummary currentSummary{};                                                     /*!< The current summary */
            std::string currentActionName{};                                                    /*!< The current action name */
            int currentIndex{0};                                                                /*!< The current index */
            std::string amfFullPathFileName{};                                                  /*!< The path of the recorder */
            std::string imageType{"png"};                                                   /*!< The image type */
            int offsetPositionX{0};                                                             /*!< The offset position x */
            int offsetPositionY{0};                                                             /*!< The offset position y */
        };
        std::vector<RecorderInfo> m_recorders{};                                                /*!< The vector of recorders */
        std::string m_recorderSessionId{};                                           /*!< The recorder session id of the session */
    private:
        static std::atomic<Session *> m_instance;                                                /*!< The instance of the singleton */
        uint8_t m_maxSession{
                10};                                                               /*!< The maximum number of session */
        std::vector<std::string> m_bindIpAddresses{};                                           /*!< The vector of bind ip addresses */
        std::string m_allowedIps{};                                                             /*!< The allowed ip addresses */
        std::string m_globalIps{};                                                             /*!< The allowed ip addresses */
        std::string m_remoteHostname{};                                                         /*!< The remote hostname */
        Environment m_environment{};                                                            /*!< The environment */
        bool m_isLocal{true};                                                                   /*!< True if the session is local */
        std::vector<std::string> m_msgStartup{};                                                 /*!< The vector of startup messages */
        //webSocket Server Agent
        std::string m_wsListenIps{"0.0.0.0"};                                               /*!< The ip of the webSocket server */
        int m_wsPort{9800};                                                                 /*!< The port of the webSocket server */
        std::string m_usedBy{};                                                             /*!< The used by of the webSocket server */
        bool m_isConnected{false};                                                          /*!< True if the webSocket server is connected */
        std::string m_cpuArchitecture;                                                      /*!< The cpu architecture of the server */
        std::map <std::string, std::string> m_mapBrotherAtsPropertiesPath{};                /*!< The map of brother ats properties path */

        /*!
         * @brief Session
         * @details
         * This constructor is used to create a new instance of the class.
         */
        Session() = default;

    public:

        /*!
         * @brief getInstance
         * @details
         * This method is used to get the instance of the singleton, in multithreaded environment.
         * @return The instance of the singleton.
         */
        std::vector<SessionData::SessionInfo> m_sessions;                    // The vector of sessions
        static Session *getInstance() {
            Session *instance = m_instance.load(std::memory_order_acquire);
            if (!instance) {
                std::lock_guard<std::mutex> lock(m_mutex);
                instance = m_instance.load(std::memory_order_relaxed);
                if (!instance) {
                    instance = new Session;
                    m_instance.store(instance, std::memory_order_release);
                }
            }
            return instance;
        }


        /*!
         * @brief deleteInstance
         * @details
         * This method is used to delete the copy constructor of the instance.
         */
        Session(Session const &) = delete;

        /*!
         * @brief addSession
         * @details
         * This method is used to add session to the vector of sessions.
         */
        void addSession(SessionData::SessionInfo &&sessionInfo);    // Add a session

        /*!
         * @brief deleteSession
         * @details
         * This method is used to delete a session from the vector of sessions.
         * @param string uuid The id of the session to delete.
         * @return True if the session is deleted, false otherwise.
         */
        bool deleteSession(const std::string &uuid);                                      // Delete a session

        /*!
         * @brief getSession
         * @details
         * This method is used to get a session from the vector of sessions.
         */
//        SessionData::SessionInfo* getSession(const std::string& id);                                      // Get a session

//        [[nodiscard]] uint8_t getMaximumSession() const {return m_maxSession;}                                                  // Get the maximum number of session

        /*!
         * @brief getIsMaxSessionReached
         * @details This method is used to get if the maximum number of session is reached.
         * @return True if the maximum number of session is reached, false otherwise.
         */
        [[nodiscard]] bool getIsMaxSessionReached() const;

        /*!
         * @brief isEndedSession
         * @param const <std::string>& uuid The uuid of the session to check.
         * @return True if the session is ended, false otherwise.
         */
        bool isEndedSession(const std::string &uuid);

        /*!
         * @brief isActiveSession
         * @details
         * This method is used to get if the session is active.
         * @param const <std::string>& uuid The uuid of the session to check.
         * @return True if the session is active, false otherwise.
         */
        bool isActiveSession(const std::string &uuid);


        /*!
         * @brief getSessionInfo
         * @details
         * This method is used to get a session info data from the vector of sessions.
         * @param <string> uuid The uuid of the session to get.
         * @return The session info data.
         */
        SessionData::SessionInfo *getSessionInfo(const std::string &uuidSession);

        /*!
         * @brief getBindIpAddresses
         * @details This method is used to get the bind ip addresses of the server.
         * @return The vector of bind ip addresses.
         */
        [[nodiscard]] const std::vector<std::string> &getBindIpAddresses() const;

        /*!
         * @brief setBindIpAddresses
         * @details This method is used to set the bind ip addresses of the server.
         * @param <std::vector<std::string>> bindIpAddresses The vector of bind ip addresses.
         */
        void setBindIpAddresses(const std::vector<std::string> &bindIpAddresses);

        /*!
         * @brief setAllowedIps
         * @details This method is used to set the allowed ip addresses of the server.
         * @param <std::string> allowedIps The allowed ip addresses.
         */
        static void setAllowedIps(const std::string &allowedIps) {getInstance()->m_allowedIps = allowedIps; }

        /*!
         * @brief getAllowedIps
         * @details This method is used to get the allowed ip addresses of the server.
         * @return The allowed ip addresses.
         */
        static std::string getAllowedIps() {
            return getInstance()->m_allowedIps;
        }

        /*!
         * @brief setRemoteHostname
         * @details This method is used to set the remote hostname of the server.
         * @param const <std::string>& remoteHostname The remote hostname.
         */
        void setRemoteHostname(const std::string &remoteHostname) { m_remoteHostname = remoteHostname; }

        /*!
         * @brief getRemoteHostname
         * @details This method is used to get the remote hostname of the server.
         * @return The remote hostname.
         */
        [[nodiscard]] std::string getRemoteHostname() const { return m_remoteHostname; }

        /*!
         * @brief getProcessPort
         * @details This method is used to get the port of a process.
         * @param const <std::string>& driverName The name of the driver.
         * @return <std::string> The port of the process.
         */
        std::string getProcessPort(const std::string &driverName);

        /*!
         * @brief getRecorder
         * @details This method is used to get a recorder from the vector of recorders.
         * @param const <std::string>& uuidRecorder The uuid of the recorder to get.
         * @return The recorder info.
         */
        RecorderInfo *getRecorder(const std::string &uuidRecorder);

        /*!
         * @brief setRecorder
         * @details This method is used to set a recorder in the vector of recorders.
         * @param const <std::string>& uuidRecorder The uuid of the recorder to set.
         * @param const <RecorderInfo>& recorderInfo The recorder info to set.
         */
        void setRecorder(const std::string &uuidRecorder, const RecorderInfo &recorderInfo);

        /*!
         * @brief deleteRecorder
         * @details This method is used to delete a recorder from the vector of recorders.
         * @param const <std::string>& uuidRecorder The uuid of the recorder to delete.
         * @return True if the recorder is deleted, false otherwise.
         */
        bool deleteRecorder(const std::string &uuidRecorder);

        /*!
         * @brief getEnvironment
         * @details This method is used to get the environment of the server.
         * @return The environment of the server.
         */
        [[nodiscard]] Environment getEnvironment() const {
            std::lock_guard<std::mutex> lock(m_mutex);
            return m_environment;
        };

        void setRecorderSessionId(const std::string &sessionId) {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_recorderSessionId = sessionId;
        };

        [[nodiscard]] std::string getRecorderSessionId() const {
            std::lock_guard<std::mutex> lock(m_mutex);
            return m_recorderSessionId;
        };

        [[nodiscard]] std::string getCurrentDriverName(const std::string& sessionId) const {
            std::lock_guard<std::mutex> lock(m_mutex);
            for(auto const& session : m_sessions){
                if(session.id == sessionId){
                    return session.webdriverName;
                }
                else return "";
            }return "";
        }


        [[nodiscard]] std::string getCurrentDriverProcessName(const std::string& sessionId) const {
            std::lock_guard<std::mutex> lock(m_mutex);
            for(auto const& session : m_sessions){
                if(session.id == sessionId){
                    return session.webdriverProcessName;
                }
                else return "";
            }
            return "";
        }

        void setRecorderOffsetPositionXY(const std::string& idRecorders, const int& offsetX, const int& offsetY){
            std::lock_guard<std::mutex> lock(m_mutex);
            for(auto recorder : m_recorders){
                if(recorder.id == idRecorders){
                    recorder.offsetPositionX = offsetX;
                    recorder.offsetPositionY = offsetY;
                }
            }
        }

        void setRecorderOffsetPositionX(const std::string& idRecorders, const int& offsetX){
            std::lock_guard<std::mutex> lock(m_mutex);
            for(auto recorder : m_recorders){
                if(recorder.id == idRecorders){
                    recorder.offsetPositionX = offsetX;
                }
            }
        }


        void setRecorderOffsetPositionY(const std::string& idRecorders, const int& offsetY){
            std::lock_guard<std::mutex> lock(m_mutex);
            for(auto recorder : m_recorders){
                if(recorder.id == idRecorders){
                    recorder.offsetPositionY = offsetY;
                }
            }
        }

        int getRecorderOffsetPositionX(const std::string& idRecorders){
            std::lock_guard<std::mutex> lock(m_mutex);
            for(const auto& recorder : m_recorders){
                if(recorder.id == idRecorders){
                    return recorder.offsetPositionX;
                }
            }
            return -1;
        }

        int getRecorderOffsetPositionY(const std::string& idRecorders){
            std::lock_guard<std::mutex> lock(m_mutex);
            for(const auto& recorder : m_recorders){
                if(recorder.id == idRecorders){
                    return recorder.offsetPositionY;
                }
            }
            return -1;
        }

        /*
         * @brief getProcessIsFirstStart
         * @details This method is used to get the isFirstStart value of a process.
         * @param const <std::string>& browserName The name of the browser.
         * @return <bool> The isFirstStart value of the process.
         * @return <false> if the process is not found.
         */
        bool getProcessIsFirstStart(const std::string& browserName ){
            std::lock_guard<std::mutex> lock(m_mutex);
            auto it = m_processes.find(browserName);
            if(it != m_processes.end()){
                return it->second.isFirstStart;
            }
            return false;
        }

        /*
         * @brief setProcessIsFirstStart
         * @details This method is used to set the isFirstStart value of a process.
         * @param const <std::string>& browserName The name of the browser.
         * @param <bool> isFirstStartValue The value to set.
         * @return <void>
         */
        void setProcessIsFirstStart(const std::string& browserName, bool isFirstStartValue ){
            std::lock_guard<std::mutex> lock(m_mutex);
            auto it = m_processes.find(browserName);
            if(it != m_processes.end()){
                it->second.isFirstStart = isFirstStartValue;
            }
        }

        bool getIsDockerContainer() const {
            std::lock_guard<std::mutex> lock(m_mutex);
            return m_environment.isDockerContainers();
        }

        ProcessInfo getProcessInfo(const std::string& browserName){
            std::lock_guard<std::mutex> lock(m_mutex);
            auto it = m_processes.find(browserName);
            if(it != m_processes.end()){
                return it->second;
            }
            return ProcessInfo{};
        }

        static std::string getProcessPid(const ProcessInfo& processInfo){
            std::lock_guard<std::mutex> lock(m_mutex);
            std::string pid{};
            std::string resultExec{};
            std::string cmd = "lsof -i:" + processInfo.port + " | awk 'NR==2 {print $2}'";
            //std::string cmd = "ss -tnlp | grep " + processInfo.port;
            std::regex pattern("pid=([0-9]*),");
            std::smatch pidMatches;
            if (ld::Utils::exec(cmd.c_str(), resultExec)) {
                //pid = std::regex_replace(pid, pattern, "");
                if (std::regex_search(resultExec, pidMatches, pattern)) {
                    pid = pidMatches[1].str();
                }
            }
            return pid;
        }

        static std::vector<std::string> getBrowserPid(const std::string& webDroverPid){
            std::lock_guard<std::mutex> lock(m_mutex);
            if(webDroverPid.empty()) return {};
            std::string cmd = "pstree -p " + webDroverPid + " | awk -F\"---\" '{print $2}' | awk -F\"(\" '{print $2}' | awk -F\")\" '{print $1}'";
            std::vector<std::string> browserPids{};
            std::string result{};
            std::regex patternPid(R"(\d+)");
            std::smatch match;
            if(Utils::exec(cmd.c_str(),result)){
                while(std::regex_search(result, match, patternPid)){
//                    if( !match.empty() ) m_nameBrowserPids.emplace_back(process.first, match[0]);
                    if( !match.empty() ) browserPids.push_back(match[0]);
                    result = match.suffix();
                }
            }
            return browserPids;
        }

        /*!
         * @brief getWsAgentPort
         * @details This method is used to get the port of the webSocket server.
         * @return <int> The port of the webSocket server.
         * @return <9800> if the port is not set.
         */
        static int getWsAgentPort(){
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_wsPort;
        }

        /*!
         * @brief setWsAgentPort
         * @details This method is used to set the port of the webSocket server.
         * @param <int> wsPort The port of the webSocket server.
         * @return <void>
         */
        static void setWsAgentPort(int wsPort){
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_wsPort = wsPort;
        }

        /*!
         * @brief getWsAgentUsedBy
         * @details This method is used to get the usedBy of the webSocket server.
         * @return <std::string> The usedBy of the webSocket server.
         * @return <macosDriver> if the usedBy is not set.
         */
        static std::string getWsAgentUsedBy(){
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_usedBy;
        }

        /*!
         * @brief setWsAgentUsedBy
         * @details This method is used to set the usedBy of the webSocket server.
         * @param <std::string> usedBy The usedBy of the webSocket server.
         * @return <void>
         */
        static void setWsAgentUsedBy(const std::string& usedBy){
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_usedBy = usedBy;
        }

        /*!
         * @brief getWsAgentIsConnected
         * @details This method is used to get the isConnected of the webSocket server.
         * @return <bool> The isConnected of the webSocket server.
         * @return <false> if the isConnected is not set.
         */
        static bool getWsAgentIsConnected(){
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_isConnected;
        }

        /*!
         * @brief setWsAgentIsConnected
         * @details This method is used to set the isConnected of the webSocket server.
         * @param <bool> isConnected The isConnected of the webSocket server.
         * @return <void>
         */
        static void setWsAgentIsConnected(bool isConnected){
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_isConnected = isConnected;
        }

        /*!
         * @brief setIsLocal
         * @details This method is used to set the isLocal of the session.
         * @param <bool> isLocal The isLocal of the session.
         * @return <void>
         */
        static void setIsLocal(bool isLocal){
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_isLocal = isLocal;
        }

        /*!
         * @brief getIsLocal
         * @details This method is used to get the isLocal of the session.
         * @return <bool> The isLocal of the session.
         * @return <true> if the isLocal is not set.
         */
        static bool getIsLocal(){
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_isLocal;
        }

        /*!
         * @brief getGlobalIps
         * @details This method is used to get the global ip addresses of the server.
         * @return The global ip addresses.
         */
        static std::string getGlobalIps() {
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_globalIps;
        }

        /*!
         * @brief setGlobalIps
         * @details This method is used to set the global ip addresses of the server.
         * @param <std::string> globalIps The global ip addresses.
         */
        static void setGlobalIps(const std::string &globalIps) {
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_globalIps = globalIps;
        }

        /*!
         * @brief getMsgStartup
         * @details This method is used to get the vector of startup messages.
         * @return The vector of startup messages.
         */
        static std::vector<std::string> getMsgStartup() {
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_msgStartup;
        }

        /*!
         * @brief setMsgStartup
         * @details This method is used to set the vector of startup messages.
         * @param <std::vector<std::string>> msgStartup The vector of startup messages.
         */
        static void setMsgStartup(const std::vector<std::string> &msgStartup) {
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_msgStartup = msgStartup;
        }

        /*!
         * @brief addMsgStartup
         * @details This method is used to add a startup message to the vector of startup messages.
         * @param <std::string> msgStartup The startup message to add.
         */
        static void addMsgStartup(const std::string &msgStartup) {
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_msgStartup.emplace_back(msgStartup);
        }

        /*!
         * @brief isLocal method
         * @details This method is used to check if the server is local.
         * @return <bool> True if the server is local, false otherwise.
         */
        static bool isLocal(){
//            std::lock_guard<std::mutex> lock(m_mutex);
            return (getGlobalIps() == "127.0.0.1" && getAllowedIps() == "127.0.0.1");
        }

        /*!
         * @brief getCpuArchitecture methode
         * @details This method is used to get the cpu architecture of the server.
         * @return <std::string> The cpu architecture of the server.
         */
        static std::string getCpuArchitecture() {
            std::lock_guard<std::mutex> lock(m_mutex);
            return getInstance()->m_cpuArchitecture;
        }

        /*!
         * @brief setCpuArchitecture methode
         * @details This method is used to set the cpu architecture of the server.
         * @return <void>
         */
        static void setCpuArchitecture(){
            std::lock_guard<std::mutex> lock(m_mutex);
            std::string name = "hw.machine";
            char buffer[256];
            size_t bufferSizes = sizeof(buffer);
            sysctlbyname(name.c_str(), &buffer, &bufferSizes, nullptr, 0);
            getInstance()->m_cpuArchitecture = {buffer, bufferSizes};
        }

        /*!
         * @brief addBrowserAtsPropertiesPath
         * @details This method is used to add a brother ats properties path to the map of brother ats properties path.
         * @param <std::string> name The name of the brother ats properties path.
         * @param <std::string> path The path of the brother ats properties path.
         * @return <void>
         */
        static void addBrowserAtsPropertiesPath(const std::string& name, const std::string& path){
            std::lock_guard<std::mutex> lock(m_mutex);
            getInstance()->m_mapBrotherAtsPropertiesPath.emplace(name, path);
        }

        /*!
         * @brief getBrowserAtsPropertiesPath
         * @details This method is used to get a brother ats properties path from the map of brother ats properties path.
         * @param <std::string> name The name of the brother ats properties path.
         * @return <std::string> The path of the brother ats properties path.
         */
        static std::string getBrowserAtsPropertiesPath(const std::string& name){
            std::lock_guard<std::mutex> lock(m_mutex);
            auto it = getInstance()->m_mapBrotherAtsPropertiesPath.find(name);
            if(it != getInstance()->m_mapBrotherAtsPropertiesPath.end()){
                return it->second;
            }
            return "";
        }
    };
}
#endif //MACOSDRIVER_SESSION_HPP
