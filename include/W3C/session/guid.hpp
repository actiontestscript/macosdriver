#ifndef MACOSDRIVER_GUID_HPP
#define MACOSDRIVER_GUID_HPP
#include "../../apps/config.hpp"
#include <ctime>
#include <cstring>
#include <random>
#include <string>
#include <sstream>
#include <iomanip>

namespace ld{
    /*!
     * @brief class Guid
     * @details generate a guid conform to RFC 4122 version 1
     * https://www.rfc-editor.org/rfc/rfc4122
     */
    class Guid{
    private:
        /*! @brief generate a guid conform to RFC 4122 version 1
         * @details https://www.rfc-editor.org/rfc/rfc4122
         * @return a guid string
         */
        static std::string _generateV1();

        /*! @brief generate a guid conform to RFC 4122 version 4
         * @details https://www.rfc-editor.org/rfc/rfc4122
         * @return a guid string
         */
        static std::string _generateV4();
    public:
        enum class GuidVersion{V4 = 4, V1 = 1};

        /*! @brief generate a guid
         * @details generate a guid conform to RFC 4122 version 1 or 4
         * @param version 1 or 4
         * @return a guid string
         */
        static std::string generate(int version = static_cast<int>(Guid::GuidVersion::V4));
    };

}

#endif //MACOSDRIVER_GUID_HPP
