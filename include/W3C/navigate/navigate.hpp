#ifndef MACOSDRIVER_NAVIGATE_HPP
#define MACOSDRIVER_NAVIGATE_HPP
#include "../../apps/config.hpp"
#include "crow.h"
#include "../session/session.hpp"
#include "../capabilities/capabilitiesFactory.hpp"
#include "../capabilities/cap_browserweb.hpp"

namespace ld {
    class Navigate : public CrowJsonResponse {
        private:
        Session* m_session;                               /*!< The session. */
        public:
        /*!
         * @brief Constructor
         * @details Constructor navigate set Session* m_session
         */
        Navigate();

        /*!
         * @brief navigateTo method
         * @details go navigate to the url and set the response
         * @param sessionId The session id
         * @param const <crow::request&>req The request
         * @param crow::response& res The response
         */
        void navigateTo(const std::string& sessionId,const crow::request &req, crow::response& res);

        /*!
         * @brief getCurrentUrl method
         * @details get the current url and set the response
         * @param sessionId The session id
         * @param crow::response& res The response
         */
        void getCurrentUrl(const std::string& sessionId,crow::response& res);

/*!
         * @brief navigateBack method
         * @details go back and set the response
         * @param sessionId The session id
         * @param crow::response& res The response
         */
        void navigateBack(const std::string& sessionId,crow::response& res);

        /*!
         * @brief navigateForward method
         * @details go forward and set the response
         * @param sessionId The session id
         * @param crow::response& res The response
         */
        void navigateForward(const std::string& sessionId,crow::response& res);

    };
}

#endif //MACOSDRIVER_NAVIGATE_HPP
