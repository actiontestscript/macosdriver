#ifndef MACOSDRIVER_TIMEOUTS_HPP
#define MACOSDRIVER_TIMEOUTS_HPP
#include "../../apps/config.hpp"
#include "crow.h"
#include "../session/session.hpp"
#include "../capabilities/capabilitiesFactory.hpp"
#include "../capabilities/cap_browserweb.hpp"

/*!
 * namespace ld
 * @brief The namespace of the MacosDriver project
 * @details The namespace of the MacosDriver project
 */
namespace ld{
    /*!
     * @class Timeouts
     * @brief Class for Timeouts extends CrowJsonResponse
     * @details Class for Timeouts extends CrowJsonResponse
     */
    class Timeouts : public CrowJsonResponse{
    private:
        Session* m_session;                               /*!< The session. */
    public:
        /*!
         * @brief Constructor
         * @details Constructor timeouts set Session* m_session
         */
        Timeouts();

        /*!
         * @brief getTimeouts
         * @details getTimeouts get the timeouts of the current session  and set the response
         * @param sessionId The session id
         * @param res The response
         */
        void getTimeouts(const std::string& sessionId,crow::response& res);

        /*!
         * @brief setTimeouts
         * @details setTimeouts set the timeouts of the current session  and set the response
         * @param const <string&> sessionId The session id
         * @param const <crow::request&>req The request
         * @param wcrow::response> res The response
         */
        void setTimeouts(const std::string& sessionId,const crow::request &req, crow::response& res);
    };
}
#endif //MACOSDRIVER_TIMEOUTS_HPP
