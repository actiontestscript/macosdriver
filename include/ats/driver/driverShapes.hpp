#ifndef MACOS_DRIVER_DRIVER_SHAPES_HPP
#define MACOS_DRIVER_DRIVER_SHAPES_HPP
#include "driver.hpp"
namespace ld{

    class DriverShapesData{
    public :
        int m_duration{0};
        int m_device{0};
        std::string m_crop{};
    };

    class DriverShapes : public AtsDriver {
    public:
        DriverShapes() = default;
        ~DriverShapes() = default;
        bool execute(const crow::request &req, crow::response &res) override {return AtsDriver::execute(req, res);};

    private:
        DriverShapesData m_DriverShapesData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;
    };
}//end namespace ld
#endif //MACOS_DRIVER_DRIVER_SHAPES_HPP
