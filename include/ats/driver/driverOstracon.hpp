#ifndef MACOS_DRIVER_DRIVER_OSTRACON_HPP
#define MACOS_DRIVER_DRIVER_OSTRACON_HPP
#include "driver.hpp"
namespace ld{

    class DriverOstraconData{
    public :
        int m_handle{-1};
    };

    class DriverOstracon : public AtsDriver {
    public:
        DriverOstracon() = default;
        ~DriverOstracon() = default;
        bool execute(const crow::request &req, crow::response &res) override {return AtsDriver::execute(req, res);};

    private:
        DriverOstraconData m_DriverOstraconData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;
    };
}//end namespace ld

#endif //MACOS_DRIVER_DRIVER_OSTRACON_HPP
