#ifndef MACOS_DRIVER_DRIVER_CAPABILITIES_HPP
#define MACOS_DRIVER_DRIVER_CAPABILITIES_HPP
#include "driver.hpp"
namespace ld{
    class DriverCapabilities : public AtsDriver {
    public:
        DriverCapabilities() = default;
        ~DriverCapabilities() = default;
        bool execute(const crow::request &req, crow::response &res) override {return AtsDriver::execute(req, res);};

    private:

        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override {return true;}

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override {return true;}
    };
}
#endif //MACOS_DRIVER_DRIVER_CAPABILITIES_HPP
