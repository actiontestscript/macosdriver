#ifndef MACOS_DRIVER_DRIVER_FACTORY_HPP
#define MACOS_DRIVER_DRIVER_FACTORY_HPP
#include "driver.hpp"
#include "driverApplication.hpp"
#include "driverCapabilities.hpp"
#include "driverClose.hpp"
#include "driverCloseWindows.hpp"
#include "driverOstracon.hpp"
#include "driverRemoteDriver.hpp"
#include "driverShapes.hpp"


namespace ld{
    class AtsDriverFactory{
    public:
        AtsDriverFactory() = default;
        ~AtsDriverFactory() = default;

        static std::unique_ptr<AtsDriver> create(const std::string& driverType){
            if(driverType == "application"){
                return std::make_unique<DriverApplication>();
//                auto driver = std::make_unique<DriverApplication>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else if(driverType == "capabilities"){
                return std::make_unique<DriverCapabilities>();
//                auto driver = std::make_unique<DriverCapabilities>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else if(driverType == "close"){
                return std::make_unique<DriverClose>();
//                auto driver = std::make_unique<DriverClose>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else if(driverType == "closewindows"){
                return std::make_unique<DriverCloseWindows>();
//                auto driver = std::make_unique<DriverCloseWindows>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else if(driverType == "osrtacon"){
                return  std::make_unique<DriverOstracon>();
//                auto driver = std::make_unique<DriverOstracon>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else if(driverType == "remotedriver"){
                return std::make_unique<DriverRemoteDriver>();
//                auto driver = std::make_unique<DriverRemoteDriver>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else if(driverType == "shapes"){
                return std::make_unique<DriverShapes>();
//                auto driver = std::make_unique<DriverShapes>();
//                return std::unique_ptr<AtsDriver>(dynamic_cast<AtsDriver*>(driver.release()));
            }
            else
                return nullptr;
        }
    };
}//end namespace ld
#endif //MACOS_DRIVER_DRIVER_FACTORY_HPP