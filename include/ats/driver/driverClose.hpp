#ifndef MACOS_DRIVER_DRIVER_CLOSE_HPP
#define MACOS_DRIVER_DRIVER_CLOSE_HPP
#include "driver.hpp"
namespace ld{

    class DriverClose : public AtsDriver {
    public:
        DriverClose() = default;
        ~DriverClose() = default;
        bool execute(const crow::request &req, crow::response &res) override {return AtsDriver::execute(req, res);};

    private:

        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override {return true;};

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override {return true;};
    };
}//end namespace ld
#endif //MACOS_DRIVER_DRIVER_CLOSE_HPP
