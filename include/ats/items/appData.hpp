#ifndef MACOSDRIVER_APPDATA_HPP
#define MACOSDRIVER_APPDATA_HPP
#include "../../../apps/config.hpp"
#include "../../W3C/session/session.hpp"
#include <string>
#include <vector>
#define MACOS_ICON "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAACa0lEQVR4nOWUX0hTURzHZ+A9PWQS9BDlU+3uH1pud9y7u/WXZVIZlWSCBRoaCFqILwUVGDiYFP4bFQyCykKJNEgpw4b3qhA9RCQSEYL/hmO5qWOD2h/9xl0N3BDa1nqIDnyfzjmfzzm/80cm+x9b1l+hKhm+TKHlZmgtF1bouCF5IavJGLygYO8WWmsIKXQGxEJrufGMCRSMwXT45BkUGA7gbNVFVNZejknUGRHQOnZfs7UFdY1XYLHeQuede1GBspA3ZkZQyO7p7jDja38OJnvz0G07/lPA8GWZEej1Oz92bY/MPCWY6iGYe3MIKr0xRGu5xxkR8OajN14M9AUDX6zwzQ3As+RDdX2DT7pRKj2f/0dwBcOrdvMHvzld7ig4lkGH+OtGcR80Gg2VLj8rn9s/1tJhC62FS1lYXEZxabl0DkFay11Liy5nuKLi0+XOefdCHDyWu/cfoKyyJqJkeL+cZTenLFAyfM/z/lfB9eBShoRRVF9qhIrhoWRMFSnBpbqq9Ubn5PTsunAp458+w3K7PfboUitTZDj7/NSzXL/d3rYu3Ot1wzXRjputnTCXFHlrKjRVSYHhILsgkFaI5DtEghH7jpWrTRa4PYtxgiXXW0j9TdePYbJv2wpE8iQ5gZh9Tpq4NvUX1LC22eIFs6/jxkAkAYxQJb8XCNSJRIFvMDfcZmtZnXbOw+X2oLf/JR52nFpOHAeRGk1iB6QWAllNnNzcQPuVemNIzZoCatbkf2TJex+3epHUAbINyZVpeNNWCFQpBKoLIlmCSMIQN8b9nGEHORIFC+QdBCr9bxsTMgpjspy0Af98+wHzbt7+o/1isAAAAABJRU5ErkJggg=="
namespace ld{
    class appData {
    public:
        appData():m_name("MacOs Desktop");
        appData(const std::string& nm):m_name(nm){};
        appData(const std::string& nm, const std::string& pa, const std::vector<uint8_t> ic): appData(nm){
            m_path = pa;
            m_icon = ic;
            }
        ~appData() = default;
        [[nodiscard]] const amf::AmfObject &getAmfDesktopAppData() const { return amf_desktopAppData; };
        void initAmfDesktopAppData();
        void setAmfObjectDesktopAppData(amf::AmfObject &obj) const;

    private:
        std::string m_name{};
        std::string m_build{};
        std::string m_version{};
        std::string m_path{};
        std::vector<uint8_t> m_icon;

        amf::AmfObject amf_desktopAppData = amf::AmfObject("com.ats.executor.drivers.desktop.AppData", false,false);


    };
}
#endif //MACOSDRIVER_APPDATA_HPP
