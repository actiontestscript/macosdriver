#ifndef MACOSDRIVER_MOUSEWHEEL_HPP
#define MACOSDRIVER_MOUSEWHEEL_HPP
#include "mouse.hpp"
namespace ld{
    class MouseWheelData{
    public:
        int m_delta{-1};
    };

    class MouseWheel : public Mouse {
    public:
        MouseWheel() = default;
        ~MouseWheel() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Mouse::execute(req, res);};

    private:
        MouseWheelData m_MouseWheelData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;


    };
}//end namespace ld
#endif //MACOSDRIVER_MOUSEWHEEL_HPP
