#ifndef MACOSDRIVER_MOUSEFACTORY_HPP
#define MACOSDRIVER_MOUSEFACTORY_HPP
#include "mouse.hpp"
#include "mouseClick.hpp"
#include "mouseDoubleClick.hpp"
#include "mouseDown.hpp"
#include "mouseDrag.hpp"
#include "mouseMiddleClick.hpp"
#include "mouseMove.hpp"
#include "mouseRightClick.hpp"
#include "mouseWheel.hpp"

namespace ld{
    class MouseFactory {
    public:
        template <typename T>
        std::unique_ptr<T> create(const std::string &keyType){
            if(keyType == "move"){
                auto mouse = std::make_unique<MouseMove>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "click"){
                auto mouse = std::make_unique<MouseClick>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "doubleclick"){
                auto mouse = std::make_unique<MouseDoubleClick>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "down"){
                auto mouse = std::make_unique<MouseDown>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "drag"){
                auto mouse = std::make_unique<MouseDrag>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "middleclick"){
                auto mouse = std::make_unique<MouseMiddleClick>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "rightclick"){
                auto mouse = std::make_unique<MouseRightClick>();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else if(keyType == "wheel"){
                auto mouse = std::make_unique<MouseWheel >();
                return std::unique_ptr<Mouse>(dynamic_cast<Mouse*>(mouse.release()));
            }
            else
                return nullptr;
        }
    };
}
#endif //MACOSDRIVER_MOUSEFACTORY_HPP
