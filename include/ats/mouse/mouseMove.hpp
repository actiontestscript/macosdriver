#ifndef MACOSDRIVER_MOUSEMOVE_HPP
#define MACOSDRIVER_MOUSEMOVE_HPP
#include "mouse.hpp"
namespace ld{
    class MouseMoveData{
    public:
        int m_x{-1};
        int m_y{-1};
    };

    class MouseMove : public Mouse {
    public:
        MouseMove() = default;
        ~MouseMove() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Mouse::execute(req, res);};

    private:
        MouseMoveData m_MouseMoveData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;


    };
}//end namespace ld
#endif //MACOSDRIVER_MOUSEMOVE_HPP
