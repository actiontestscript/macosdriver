#ifndef MACOSDRIVER_MOUSEDOWN_HPP
#define MACOSDRIVER_MOUSEDOWN_HPP
#include "mouse.hpp"
namespace ld{
    class MouseDownData{
    public:
    };

    class MouseDown : public Mouse {
    public:
        MouseDown() = default;
        ~MouseDown() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Mouse::execute(req, res);};

    private:
//        MouseDownData m_MouseDownData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;


    };
}//end namespace ld
#endif //MACOSDRIVER_MOUSEDOWN_HPP
