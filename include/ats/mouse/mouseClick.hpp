#ifndef MACOSDRIVER_MOUSECLICK_HPP
#define MACOSDRIVER_MOUSECLICK_HPP
#include "mouse.hpp"
namespace ld{
    class MouseClickData{
    public:
    };

    class MouseClick : public Mouse {
    public:
        MouseClick() = default;
        ~MouseClick() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Mouse::execute(req, res);};

    private:
//        MouseClickData m_MouseClickData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;


    };
}//end namespace ld
#endif //MACOSDRIVER_MOUSECLICK_HPP
