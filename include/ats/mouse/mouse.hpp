#ifndef MACOSDRIVER_MOUSE_HPP
#define MACOSDRIVER_MOUSE_HPP
#include "../../../apps/config.hpp"
#include "../../W3C/session/session.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../../utils/utils.hpp"
#include "../../utils/httpRequest.hpp"
#include "../../../include/ats/recorder/desktopResponse.hpp"

using json = nlohmann::json;
namespace ld{
    class Mouse{
    public:
        Mouse() = default;
        virtual ~Mouse() = default;
        virtual bool execute(const crow::request& req, crow::response& res) = 0;
        [[maybe_unused]] CrowJsonResponse::ResponseType getResponseType(){return m_responseTypeJson;};
        [[maybe_unused]] void setResponseType(CrowJsonResponse::ResponseType responseType){m_responseTypeJson = responseType;};

    private:
        CrowJsonResponse::ResponseType m_responseTypeJson{CrowJsonResponse::ResponseType::JSON};

        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        virtual bool _parseJson(const crow::request& req) = 0;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        virtual bool _parsePostData(const crow::request &req) = 0 ;

    protected:

        char m_postDataDelimiter{'\n'};                                                                                 /*< Post data delimiter */
        void parseData(const crow::request &req);
    };

}//end namespace ld
#endif //MACOSDRIVER_MOUSE_HPP
