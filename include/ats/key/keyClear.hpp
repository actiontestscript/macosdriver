#ifndef MACOSDRIVER_KEYCLEAR_HPP
#define MACOSDRIVER_KEYCLEAR_HPP
#include "key.hpp"
namespace ld{
    class KeyClearData{
    public :
        std::string m_id{};
        std::string m_data{};
    };

    class KeyClear : public Key {
    public:
        KeyClear() = default;
        ~KeyClear() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Key::execute(req, res);};

    private:
        KeyClearData m_keyClearData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}//end namespace ld
#endif //MACOSDRIVER_KEYCLEAR_HPP
