#ifndef MACOS_DRIVER_KEY_ENTER_HPP
#define MACOS_DRIVER_KEY_ENTER_HPP
#include "key.hpp"
namespace ld{
    class KeyEnterData{
    public :
        std::string m_id{};
        std::string m_data{};
    };

    class KeyEnter : public Key {
    public:
        KeyEnter() = default;
        ~KeyEnter() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Key::execute(req, res);};

    private:
        KeyEnterData m_KeyEnterData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}//end namespace ld

#endif //MACOS_DRIVER_KEY_ENTER_HPP
