#ifndef MACOSDRIVER_KEYRELEASE_HPP
#define MACOSDRIVER_KEYRELEASE_HPP
#include "key.hpp"
namespace ld{
    class KeyReleaseData{
    public :
        int m_codePoint{-1};
    };

    class KeyRelease : public Key {
    public:
        KeyRelease() = default;
        ~KeyRelease() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Key::execute(req, res);};

    private:
        KeyReleaseData m_KeyReleaseData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}//end namespace ld

#endif //MACOSDRIVER_KEYRELEASE_HPP
