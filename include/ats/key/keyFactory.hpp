#ifndef MACOS_DRIVER_KEY_FACTORY_HPP
#define MACOS_DRIVER_KEY_FACTORY_HPP
#include "key.hpp"
#include "keyClear.hpp"
#include "keyDown.hpp"
#include "keyEnter.hpp"
#include "keyRelease.hpp"

namespace ld{
    class KeyFactory {
    public:
        template <typename T>
        std::unique_ptr<T> create(const std::string &keyType){
            if(keyType == "clear"){
                auto keys = std::make_unique<KeyClear>();
                return std::unique_ptr<Key>(dynamic_cast<Key*>(keys.release()));
            }
            else if(keyType == "enter"){
                auto keys = std::make_unique<KeyEnter>();
                return std::unique_ptr<Key>(dynamic_cast<Key*>(keys.release()));
            }
            else if(keyType == "down"){
                auto keys = std::make_unique<KeyDown>();
                return std::unique_ptr<Key>(dynamic_cast<Key*>(keys.release()));
            }
            else if(keyType == "release"){
                auto keys = std::make_unique<KeyRelease>();
                return std::unique_ptr<Key>(dynamic_cast<Key*>(keys.release()));
            }
            else
                return nullptr;
        }
    };
}//end namespace ld
#endif //MACOS_DRIVER_KEY_FACTORY_HPP
