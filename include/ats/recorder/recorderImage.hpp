#ifndef MACOSDRIVER_RECORDERIMAGE_HPP
#define MACOSDRIVER_RECORDERIMAGE_HPP
#include "recorder.hpp"


namespace ld{
    /*!
     * @class RecorderImageData
     * @brief The data of the recorder Image
     * @details The data of the recorder Image
     */
    class RecorderImageData {
    public:
        //double screenRect[4]{0, 0, 1, 1};                                                               /*> The screen rect */
        std::vector<double> screenRect{0, 0, 1, 1};                                                               /*> The screen rect */
        bool isReference{false};                                                                                        /*> The is reference */
    };

    /*!
     * @class RecorderImage
     * @brief The recorder Image
     * @details The recorder Image is used to record the image in the search area
     */
    class RecorderImage : public Recorder{
    private:
        RecorderImageData m_recorderImageData{};                                                                        /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;


        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderImage()= default;
        ~RecorderImage() override = default;

        /*!
         * @brief execute Execute the recorder Image
         * @details Execute the recorder Image. The recorder Image is used to record the image in the search area. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERIMAGE_HPP
