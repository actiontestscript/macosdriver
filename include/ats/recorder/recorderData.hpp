#ifndef MACOSDRIVER_RECORDERDATA_HPP
#define MACOSDRIVER_RECORDERDATA_HPP
#include "recorder.hpp"

namespace ld {
    /*!
     * @class RecorderData
     * @brief The recorder data class
     * @details The recorder data class is used to record the data of the elements in the search area
     */
    class RecorderDataData {
    public:
        std::string v1{};                                                                                               /*> The value 1 of data */
        std::string v2{};                                                                                               /*> The value 2 of data */
    };

    /*!
     * @class RecorderData
     * @brief The recorder data class
     * @details The recorder data class is used to record the data of the elements in the search area
     */
    class RecorderData : public Recorder {
    private:
        RecorderDataData m_recorderDataData{};                                                                        /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderData() = default;
        ~RecorderData() override = default;

        /*!
         * @brief execute Execute the recorder data
         * @details Execute the recorder data. The recorder data is used to record the data of the elements in the search area. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request &req, crow::response &res) override;
    };
}
#endif //MACOSDRIVER_RECORDERDATA_HPP
