#ifndef MACOSDRIVER_RECORDERSCREENSHOTMOBILE_HPP
#define MACOSDRIVER_RECORDERSCREENSHOTMOBILE_HPP
#include "recorder.hpp"
namespace ld{
    /*! @class RecorderScreenshotMobile
     * @brief The class that manage the recorder screenshot mobile
     * @details The class that manage the recorder screenshot mobile
     */
    class RecorderScreenshotDataMobile {
    public:
        int x{0};                                                                                                        /*> X */
        int y{0};                                                                                                        /*> Y */
        int width{1};                                                                                                    /*> Width */
        int height{1};                                                                                                   /*> Height */
        std::string url;                                                                                                 /*> Url */
    };

    class RecorderScreenshotMobile : public Recorder{
    private:
        RecorderScreenshotDataMobile m_recorderScreenshotDataMobile;                                                     /*> The recorder screenshot data */
        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderScreenshotMobile()= default;
        ~RecorderScreenshotMobile() override = default;
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERSCREENSHOTMOBILE_HPP
