#ifndef MACOSDRIVER_RECORDERELEMENT_HPP
#define MACOSDRIVER_RECORDERELEMENT_HPP
#include "recorder.hpp"
#include "testBound.hpp"
#include "visualElement.hpp"

namespace ld{
    /*!
     * @class RecorderElement
     * @brief The recorder element class
     * @details The recorder element class is used to record the elements in the search area
     */
    class RecorderElementData{
    public:
        TestBound elementBound{};                                                                                   /*> The element bound */
        long searchDuration{};                                                                                          /*> The search duration */
        int numElements{};                                                                                              /*> The number of elements */
        std::string selector{};                                                                                  /*> The search selector */
    };

    /*!
     * @class RecorderElement
     * @brief The recorder element class
     * @details The recorder element class is used to record the elements in the search area
     */
    class RecorderElement : public Recorder{
    private:
        RecorderElementData m_recorderElementData{};                                                                    /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;


        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderElement()= default;
        ~RecorderElement() override = default;

        /*!
         * @brief execute Execute the recorder Element
         * @details Execute the recorder element. The recorder element is used to record the elements in the search area. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERELEMENT_HPP
