#ifndef MACOSDRIVER_TESTBOUND_HPP
#define MACOSDRIVER_TESTBOUND_HPP
#include "amfInclude.hpp"
#include<vector>

namespace ld{
    /*!
     * @class TestBound
     * @brief The class TestBound
     * @details This class is used to store the configuration of the application.
     */
    class TestBound{
    private:

        amf::AmfObject amf_testBound = amf::AmfObject("com.ats.executor.TestBound", false, false);                                   /*>! amf object     */

    public:
        double x{0};                                                                                                                                               /*>! x value  */
        double y{0};                                                                                                                                               /*>! y value  */
        double width{1};                                                                                                                                           /*>! width value  */
        double height{1};                                                                                                                                          /*>! height value  */

        /*!
         * @brief TestBound
         * @details default constructor
         */
        TestBound()= default;
        TestBound(const double& x, const double& y, const double& width, const double& height):x(x), y(y), width(width), height(height){};

        /*!
         * @brief TestBound
         * @details constructor
         * @param const <std::string>& name
         * @param const <int& line>
         * @param const <std::string>& message
         */
        //explicit TestBound (std::vector<double> elementBound):x(elementBound[0]), y(elementBound[1]), width(elementBound[2]), height(elementBound[3]){};

        /*!
         * @brief getAmfTestBound
         * @details get amf object
         * @return const <amf::AmfObject&>
         */
        [[nodiscard]]  const amf::AmfObject& getAmfTestBound() const{ return amf_testBound; }

        /*!
         * @brief setAmfTestBound
         * @details set amf object
         * @param amf::AmfObject& obj
         */
        void setAmfTestBound(amf::AmfObject& obj) const ;

        /*!
         * @brief initAmfTestBound
         * @details init amf object
         */
        void initAmfTestBound() ;

        static bool isDefault(const TestBound& test);

        friend bool operator==(const TestBound& lhs, const TestBound& rhs) {
            return lhs.x == rhs.x && lhs.y == rhs.y && lhs.width == rhs.width && lhs.height == rhs.height;
        }
    };
}
#endif //MACOSDRIVER_TESTBOUND_HPP
