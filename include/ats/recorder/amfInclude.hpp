#ifndef MACOSDRIVER_AMFINCLUDE_HPP
#define MACOSDRIVER_AMFINCLUDE_HPP
#include "serializer.hpp"
#include "types/amfstring.hpp"
#include "types/amfinteger.hpp"
#include "types/amfobject.hpp"
#include "amf.hpp"
#include "types/amfarray.hpp"
#include "types/amfbool.hpp"
#include "types/amfbytearray.hpp"
#include "types/amfdouble.hpp"
#include "types/amfvector.hpp"
#include "types/amfnull.hpp"
#include "../../utils/utils.hpp"

using namespace amf;
namespace ld {
    /*!
     * @brief convertImagesToAmfArray
     * @param const <std::vector<std::vector<uint8_t>>& images
     * @return amf::AmfArray
     */
    [[maybe_unused]] amf::AmfArray convertImagesToAmfArray(const std::vector<std::vector<uint8_t>> &images);

    /*!
     * @brief convertAmfArrayToImages
     * @param const <amf::AmfArray>& array
     * @return std::vector<std::vector<uint8_t>>
     */
    [[maybe_unused]] std::vector<std::vector<uint8_t>> convertAmfArrayToImages(const amf::AmfArray &array);

    /*!
     * @brief serialiseAmfActions
     * @param const <amf::AmfObject>& obj
     * @return amf::v8
     */
    [[maybe_unused]] amf::v8 serialiseAmfActions(const amf::AmfObject& obj);

}

#endif //MACOSDRIVER_AMFINCLUDE_HPP
