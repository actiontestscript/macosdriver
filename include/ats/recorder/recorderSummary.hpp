#ifndef MACOSDRIVER_RECORDERSUMMARY_HPP
#define MACOSDRIVER_RECORDERSUMMARY_HPP
#include "recorder.hpp"
#include "testSummary.hpp"

namespace ld{
    /*! @class RecorderSummary
     * @brief The class that manage the recorder summary
     * @details The class that manage the recorder summary
     */
    class RecorderSummaryData {
    public:
        bool passed{false};                                                                                             /*> Passed or not */
        int actions{0};                                                                                                 /*> Number of actions */
        std::string suiteName{};                                                                                        /*> Suite name */
        std::string testName{};                                                                                         /*> Test name */
        std::string data{};                                                                                             /*> Data */
        std::string errorScript{};                                                                                      /*> Error script */
        int errorLine{0};                                                                                               /*> Error line */
        std::string errorMessage{};                                                                                     /*> Error message */
    };

    /*! @class RecorderSummary
     * @brief The class that manage the recorder summary
     * @details The class that manage the recorder summary
     */
    class RecorderSummary : public Recorder{
    private:
        RecorderSummaryData m_recorderSummaryData;                                                                       /*> The recorder summary data */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderSummary()= default;
        ~RecorderSummary() override = default;

        /*!
        * @brief execute Execute the recorder Summary
        * @details Execute the recorder Summary. this override the execute function of the recorder class
        * @param const <crow::request>& req The request
        * @param <crow::response>& res The response
        * @return True if the recorder start is executed, false otherwise
        */
        bool execute(const crow::request& req,  crow::response& res) override;

        void setErrorInfo(const json& value);
    };
}
#endif //MACOSDRIVER_RECORDERSUMMARY_HPP
