#ifndef MACOSDRIVER_RECORDERPOSITION_HPP
#define MACOSDRIVER_RECORDERPOSITION_HPP
#include "recorder.hpp"
namespace ld{
    /*!
     * @class RecorderPositionData
     * @brief The data of the recorder position
     * @details The data of the recorder position
     */
    class RecorderPositionData {
    public:
        std::string hpos{};                                                                                             /*> The horizontal position */
        std::string hposValue{};                                                                                        /*> The horizontal position value */
        std::string vpos{};                                                                                             /*> The vertical position */
        std::string vposValue{};                                                                                        /*> The vertical position value */
    };

    /*!
     * @class RecorderPosition
     * @brief The recorder position
     * @details The recorder position is used to record the position of the elements in the search area
     */
    class RecorderPosition : public Recorder{
    private:
        RecorderPositionData m_recorderPositionData{};                                                                  /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderPosition()= default;
        ~RecorderPosition() override = default;

        /*!
         * @brief execute Execute the recorder Position
         * @details Execute the recorder Position. The recorder Position is used to record the position of the elements in the search area. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERPOSITION_HPP
