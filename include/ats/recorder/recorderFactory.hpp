#ifndef MACOSDRIVER_RECORDERFACTORY_HPP
#define MACOSDRIVER_RECORDERFACTORY_HPP
#include "recorder.hpp"
#include "recorderStop.hpp"
#include "recorderScreenshot.hpp"
#include "recorderStart.hpp"
#include "recorderCreate.hpp"
#include "recorderImage.hpp"
#include "recorderValue.hpp"
#include "recorderData.hpp"
#include "recorderStatus.hpp"
#include "recorderElement.hpp"
#include "recorderPosition.hpp"
#include "recorderDownload.hpp"
#include "recorderImageMobile.hpp"
#include "recorderCreateMobile.hpp"
#include "recorderScreenshotMobile.hpp"
#include "recorderSummary.hpp"
#include "recorderTest.hpp"

namespace ld{
    /*!@class RecorderFactory
     * @brief The class RecorderFactory
     * @details This class is used to create a Recorder object
     */
    class RecorderFactory{
    public:
        /*!
         * @brief Create a Recorder object
         * @details Create a Recorder object
         * @param T The type of the Recorder object
         * @param type The type of the Recorder object
         * @return A unique pointer to the Recorder object
         */
        template <typename T>
        std::unique_ptr<T> create(const std::string& recordType) {
            if(recordType == "stop"){
                auto recorder = std::make_unique<RecorderStop>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "start"){
                auto recorder = std::make_unique<RecorderStart>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "screenshot"){
                auto recorder = std::make_unique<RecorderScreenshot>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "create"){
                auto recorder = std::make_unique<RecorderCreate>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "image"){
                auto recorder = std::make_unique<RecorderImage>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "value"){
                auto recorder = std::make_unique<RecorderValue>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "data"){
                auto recorder = std::make_unique<RecorderData>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "status"){
                auto recorder = std::make_unique<RecorderStatus>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "element"){
                auto recorder = std::make_unique<RecorderElement>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "position"){
                auto recorder = std::make_unique<RecorderPosition>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "download"){
                auto recorder = std::make_unique<RecorderDownload>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "imagemobile"){
                auto recorder = std::make_unique<RecorderImageMobile>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "createmobile"){
                auto recorder = std::make_unique<RecorderCreateMobile>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "screenshotmobile"){
                auto recorder = std::make_unique<RecorderScreenshotMobile>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "summary"){
                auto recorder = std::make_unique<RecorderSummary>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else if(recordType == "test"){
                auto recorder = std::make_unique<RecorderTest>();
                return std::unique_ptr<Recorder>(dynamic_cast<Recorder*>(recorder.release()));
            }
            else {
                return nullptr;
            }
        }
    };
}//namespace ld
#endif //MACOSDRIVER_RECORDERFACTORY_HPP
