#ifndef MACOSDRIVER_VISUALELEMENT_HPP
#define MACOSDRIVER_VISUALELEMENT_HPP
#include <string>
#include "testBound.hpp"


namespace ld{
    class VisualElement{
    private:
        amf::AmfObject amf_visualElement = amf::AmfObject("com.ats.recorder.VisualElement", false, false);

    public:
        bool isTestBoundApply{false};
        TestBound rectangle{};
        std::string criterias{};
        int foundElements{};
        long searchDuration{};
        std::string tag{};
        std::string hpos{};
        int hposValue{};
        std::string vpos{};
        int vposValue{};

        // Accesseur pour obtenir l'objet AmfObject.
        [[nodiscard]] const amf::AmfObject& getAmfVisualElement() const { return amf_visualElement;  }
        void setAmfVisualElement(amf::AmfObject& obj) const;
        void initAmfVisualElement();

        VisualElement()= default;
        VisualElement(const std::string& tag, const std::string& criterias,int foundElements,const TestBound& rectangle, long duration);
        void updatePosition(const std::string &hPos, const std::string &hPosValue, const std::string &vPos, const std::string &vPosValue);
        static bool isDefault(const VisualElement& test);
        friend bool operator==(const VisualElement& lhs, const VisualElement& rhs){
            return lhs.tag == rhs.tag && lhs.criterias == rhs.criterias && lhs.foundElements == rhs.foundElements && lhs.rectangle == rhs.rectangle
            && lhs.searchDuration == rhs.searchDuration && lhs.hpos == rhs.hpos && lhs.hposValue == rhs.hposValue && lhs.vpos == rhs.vpos
            && lhs.vposValue == rhs.vposValue;
        }

    };
}
#endif //MACOSDRIVER_VISUALELEMENT_HPP
