#ifndef MACOSDRIVER_RECORDERVALUE_HPP
#define MACOSDRIVER_RECORDERVALUE_HPP
#include "recorder.hpp"
namespace ld {
    /*!
     * @class RecorderValueData
     * @brief The data of the recorder start
     * @details The data of the recorder start
     */
    class RecorderValueData {
    public:
        std::string v{};                                                                                                /*>v  The value */
    };

    /*!
     * @class RecorderValue
     * @brief The recorder value
     * @details The recorder value
     */
    class RecorderValue : public Recorder {
    private:
        RecorderValueData m_recorderValueData{};                                                                        /*< The data of the recorder start */
        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderValue() = default;
        ~RecorderValue() override = default;

        /*!
         * @brief execute Execute the recorder value
         * @details Execute the recorder value. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request &req, crow::response &res) override;
    };
}
#endif //MACOSDRIVER_RECORDERVALUE_HPP
