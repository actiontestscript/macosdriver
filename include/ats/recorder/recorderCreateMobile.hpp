#ifndef MACOSDRIVER_RECORDERCREATEMOBILE_HPP
#define MACOSDRIVER_RECORDERCREATEMOBILE_HPP
#include "recorder.hpp"
namespace ld{
    /*!
     * @class RecorderCreateMobile
     * @brief The recorder create mobile class
     * @details The recorder create mobile class is used to record the creation of a mobile element
     */
    class RecorderCreateMobileData {
    public:
        std::string actionType{};                                                                                       /*> The action type */
        int line{0};                                                                                                    /*> The line */
        std::string script{};                                                                                           /*> The script */
        long timeLine{0};                                                                                               /*> The time line */
        std::string channelName{};                                                                                      /*> The channel name */
        TestBound channelDimension{};                                                                                   /*> The channel dimension */
        std::string url{};                                                                                              /*> The url */
        bool sync{false};                                                                                               /*> The sync */
        bool stop{false};                                                                                               /*> The stop */
    };

    /*!
     * @class RecorderCreateMobile
     * @brief The recorder create mobile class
     * @details The recorder create mobile class is used to record the creation of a mobile element
     */
    class RecorderCreateMobile : public Recorder{
    private:
        RecorderCreateMobileData m_recorderCreateMobileData{};                                                            /*< The data of the recorder start */

        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
         * @brief _jsonIsValid Check if the json is valid
         * @details Check if the json is valid
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the json is valid, false otherwise
         */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderCreateMobile()= default;
        ~RecorderCreateMobile() override = default;

        /*!
         * @brief execute Execute the recorder CreateMobile
         * @details Execute the recorder CreateMobile. The recorder CreateMobile is used to record the creation of a mobile element. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERCREATEMOBILE_HPP
