#ifndef MACOSDRIVER_RECORDERSCREENSHOT_HPP
#define MACOSDRIVER_RECORDERSCREENSHOT_HPP
#include "recorder.hpp"
namespace ld{
    /*! @class RecorderScreenshot
     * @brief The class that manage the recorder screenshot
     * @details The class that manage the recorder screenshot
     */
    class RecorderScreenshotData {
    public:
        int x{0};                                                                                                        /*> X */
        int y{0};                                                                                                        /*> Y */
        int width{1};                                                                                                    /*> Width */
        int height{1};                                                                                                   /*> Height */
    };

    class RecorderScreenshot : public Recorder{
    private:
        RecorderScreenshotData m_recorderScreenshotData;                                                                 /*> The recorder screenshot data */
        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderScreenshot()= default;
        ~RecorderScreenshot() override = default;

        /*!
        * @brief execute Execute the recorder Screenshot
        * @details Execute the recorder Screenshot. this override the execute function of the recorder class
        * @param const <crow::request>& req The request
        * @param <crow::response>& res The response
        * @return True if the recorder start is executed, false otherwise
        */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERSCREENSHOT_HPP
