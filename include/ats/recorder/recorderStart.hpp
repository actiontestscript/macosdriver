#ifndef MACOSDRIVER_RECORDERSTART_HPP
#define MACOSDRIVER_RECORDERSTART_HPP
#include "recorder.hpp"
#include "../../../include/ats/recorder/visualReport.hpp"

#include "../../utils/utils.hpp"


namespace ld{
    /*!
     * @class RecorderStart
     * @brief The data of the recorder start
     * @details The data of the recorder start
     */
    class RecorderStartData {
    public:
        std::string id{};                                                                                               /*< The id of the recorder start */
        std::string fullName{};                                                                                         /*< The full name of the recorder start */
        std::string description{};                                                                                      /*< The description of the recorder start */
        std::string author{};                                                                                           /*< The author of the recorder start */
        std::string groups{};                                                                                           /*< The groups of the recorder start */
        std::string preRequisites{};                                                                                    /*< The preRequisites of the recorder start */
        std::string externalId{};                                                                                       /*< The externalId of the recorder start */
        int videoQuality{};                                                                                             /*< The videoQuality of the recorder start */
        std::string started{};                                                                                          /*< The started of the recorder start */
    };

    /*!
     * @class RecorderStart
     * @brief The recorder start class herits from Recorder
     * @details The recorder start
     */
    class RecorderStart : public Recorder{
    private:
        RecorderStartData m_recorderStartData{};                                                                        /*< The data of the recorder start */
        std::string m_tmpPath{};                                                                                        /*< The path to the tmp folder */
        long m_spaceMin{102400};                                                                                       /*< The minimum space 100 Mo to record in Kb */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
         * @brief _jsonIsValid Check if the json is valid
         * @details Check if the json is valid
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the json is valid, false otherwise
         */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

        /*!
         * @brief _setTmpPath Set the tmp path
         * @details Set the tmp path to the tmp folder
         */
        void _setTmpPath();

        /*!
         * @brief _start Start the recording
         * @details Start the recording
         * @return True if the recording is started, false otherwise
         */
        bool _start();

    public:
        RecorderStart()= default;
        ~RecorderStart() override = default;

        /*!
         * @brief execute Execute the recorder start
         * @details Execute the recorder start to start the recording. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;

    };
}
#endif //MACOSDRIVER_RECORDERSTART_HPP
