#ifndef MACOSDRIVER_TESTSUMMARY_HPP
#define MACOSDRIVER_TESTSUMMARY_HPP
#include "testError.hpp"

namespace ld{
    /*!
     * @class TestSummary
     * @brief The class TestSummary
     * @details This class is used to store the report summary.
     */
    class TestSummary{
    private:
        AmfObject amf_testSummary= AmfObject("com.ats.recorder.TestSummary", false, false);         /*>! amf object     */

    public:
        std::string suiteName{};                                                                                                  /*>! suite name     */
        std::string testName{};                                                                                                   /*>! test name      */
        std::string summary{};                                                                                                    /*>! summary           */
        int status{0};                                                                                                            /*>! status         */
        int actions{1};                                                                                                           /*>! actions        */
        TestError scriptError{};                                                                                                  /*>! testError error   */

        /*!
         * @brief TestSummary
         * @details default constructor
         */
        TestSummary()= default;

        /*!
         * @brief TestSummary
         * @details constructor
         * @param const <bool&> passed
         * @param const <int&> actions
         * @param const <std::string&> suiteName
         * @param const <std::string&> testName
         * @param const <std::string&> data
         */
        TestSummary(const bool& passed,const int& actions, const std::string& suiteName, const std::string& testName, const std::string& summary);

        /*!
         * @brief TestSummary
         * @details constructor
         * @param const <bool&> passed
         * @param const <int&> actions
         * @param const <std::string&> suiteName
         * @param const <std::string&> testName
         * @param const <std::string&> data
         * @param const <std::string&> errorScript
         * @param const <int&> errorLine
         * @param const <std::string&> errorMessage
         */
        TestSummary(const bool& passed,const int& actions, const std::string& suiteName, const std::string& testName, const std::string& summary, const std::string& errorScript, const int& errorLine, const std::string& errorMessage);

        /*!
         * @brief getAmfTestSummary
         * @details get amf object
         * @return const <amf::AmfObject&>
         */
        [[nodiscard]] const amf::AmfObject& getAmfTestSummary() const { return amf_testSummary;}

        /*!
         * @brief setAmfTestSummary
         * @details set amf object
         * @param amf::AmfObject& obj
         */
        void setAmfTestSummary(amf::AmfObject& obj) const;

        /*!
         * @brief initAmfTestSummary
         * @details init amf object
         */
        void initAmfTestSummary();
    };
}
#endif //MACOSDRIVER_TESTSUMMARY_HPP
