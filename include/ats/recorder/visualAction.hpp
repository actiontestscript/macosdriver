#ifndef MACOSDRIVER_VISUALACTION_HPP
#define MACOSDRIVER_VISUALACTION_HPP
#include <string>
#include <list>
#include <vector>
#include "visualElement.hpp"
#include "testBound.hpp"

namespace ld{
    /*!
     * @class VisualAction
     * @brief The class VisualAction
     * @details This class is used to store the configuration of the application.
     */
    class VisualAction{
    private :
        amf::AmfObject amf_visualAction = amf::AmfObject("com.ats.recorder.VisualAction", false, false);                    /*>! amf object     */
        bool isVisualElement{false};                                                                                          /*>! is element     */
        bool isTestBound{false};                                                                                              /*>! is test bound  */
    public:
        /*!
         * @brief VisualAction
         * @details default constructor
         */
        VisualAction()= default;

        /*!
         * @brief VisualAction
         * @details constructor
         * @param bool stop
         * @param const <std::string&> type
         * @param const <int&> line
         * @param const <std::string&> script
         * @param const <long&> timeLine
         * @param const <std::string&> channelName
         * @param const <TestBound&> channelBound
         * @param const <std::string&> imageType
         */
        VisualAction(bool stop, const std::string& type, const int& line, std::string& script, const long &timeLine, std::string& channelName, const TestBound& channelBound, const std::string& imageType);
        VisualAction(const std::string& name);
        std::string START_SCRIPT = "StartScriptAction";                                                           /*>! start script   */

        int index{0};                                                                                                   /*>! index          */
        long timeLine{};                                                                                                /*>! time line      */

        int error{1};                                                                                                   /*>! error          */
        long duration{};                                                                                                /*>! duration       */

        std::string channelName{};                                                                                      /*>! channel name   */
        TestBound channelBound{};                                                                                       /*>! channel bound  */

        std::string type{};                                                                                             /*>! type           */
        int line{0};                                                                                                    /*>! line           */
        std::string value{};                                                                                            /*>! value          */
        std::string data{};                                                                                             /*>! data           */
        std::string script{};                                                                                           /*>! script         */

        VisualElement element{};                                                                                        /*>! element        */

        std::string imageType{"png"};                                                                                /*>! image type     */

        bool stop{false};                                                                                               /*>! stop           */

        /*
        int cpu{};
        int ram{};
        int netReceived{};
        int netSent{};
*/
        std::vector<uint8_t> record;                                                                                    /*>! record         */

//        std::vector<uint8_t> images;
        std::vector<std::vector<uint8_t>> images;                                                                       /*>! images         */
        int imageRef{0};                                                                                                /*>! image ref      */

        /*!
         * @brief getAmfVisualAction
         * @details get amf object
         * @return const <amf::AmfObject&>
         */
        [[nodiscard]] const amf::AmfObject& getAmfVisualAction() const { return amf_visualAction; }

        /*!
         * @brief setAmfVisualAction
         * @details set amf object
         * @param amf::AmfObject& obj
         */
        void initAmfVisualAction();

        /*!
         * @brief setAmfVisualAction
         * @details set amf object
         * @param amf::AmfObject& obj
         */

        void setAmfObjectVisualAction(amf::AmfObject& obj) const;

        void setIsVisualElement(bool b_isVisualElement){ this->isVisualElement = b_isVisualElement; };
        //void addImage(const TestBound& screenRect, bool isRef)
    };
}
#endif //MACOSDRIVER_VISUALACTION_HPP
