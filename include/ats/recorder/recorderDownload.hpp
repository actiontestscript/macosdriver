#ifndef MACOSDRIVER_RECORDERDOWNLOAD_HPP
#define MACOSDRIVER_RECORDERDOWNLOAD_HPP
#include "recorder.hpp"
namespace ld{
    class RecorderDownload : public Recorder{
    private:
        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};


    public:
        RecorderDownload()= default;
        ~RecorderDownload() override = default;

        /*!
         * @brief execute Execute the recorder download
         * @details Execute the recorder download
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder download is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERDOWNLOAD_HPP
