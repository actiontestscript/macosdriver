#ifndef MACOSDRIVER_RECORDERIMAGEMOBILE_HPP
#define MACOSDRIVER_RECORDERIMAGEMOBILE_HPP
#include "recorder.hpp"
namespace ld{
    /*!
     * @class RecorderImageMobile
     * @brief The recorder Image Mobile class
     * @details The recorder Image Mobile class is used to record the image of the mobile
     */
    class RecorderImageMobileData {
        public:
        double screenRect[4]{0, 0, 1, 1};                                                               /*> The screen rect */
        bool isReference{false};                                                                                        /*> The is reference */
        std::string url{};                                                                                              /*> The url */
    };

    /*!
     * @class RecorderImageMobile
     * @brief The recorder Image Mobile class
     * @details The recorder Image Mobile class is used to record the image of the mobile
     */
    class RecorderImageMobile : public Recorder{
    private:
        RecorderImageMobileData m_recorderImageMobileData{};                                                            /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;


        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderImageMobile()= default;
        ~RecorderImageMobile() override = default;

        /*!
         * @brief execute Execute the recorder Image Mobile
         * @details Execute the recorder Image Mobile. The recorder Image Mobile is used to record the image of the mobile. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request& req,  crow::response& res) override;
    };
}
#endif //MACOSDRIVER_RECORDERIMAGEMOBILE_HPP
