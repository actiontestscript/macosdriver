#ifndef MACOSDRIVER_RECORDERSTATUS_HPP
#define MACOSDRIVER_RECORDERSTATUS_HPP
#include "recorder.hpp"
namespace ld {
    /*!
     * @class RecorderStatusData
     * @brief The data of the recorder status
     * @details The data of the recorder status
     */
    class RecorderStatusData {
    public:
        int error{};                                                                                                    /*> The error of the recorder */
        long duration{};                                                                                                /*> The duration of the recorder */
    };

    /*!
     * @class RecorderStatus
     * @brief The recorder status
     * @details The recorder status
     */
    class RecorderStatus : public Recorder {
    private:
        RecorderStatusData m_recorderStatusData{};                                                                      /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request& req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
          * @brief _jsonIsValid Check if the json is valid
          * @details Check if the json is valid
          * @param const <crow::request>& req The request
          * @param <crow::response>& res The response
          * @return True if the json is valid, false otherwise
          */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};

    public:
        RecorderStatus() = default;
        ~RecorderStatus() override = default;

        /*!
         * @brief execute Execute the recorder status
         * @details Execute the recorder status. the status is the error and the duration of the recorder. this override the execute of the recorder
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request &req, crow::response &res) override;
    };
}
#endif //MACOSDRIVER_RECORDERSTATUS_HPP
