#ifndef MACOSDRIVER_RECORDERCREATE_HPP
#define MACOSDRIVER_RECORDERCREATE_HPP
#include "recorder.hpp"
#include "testBound.hpp"

namespace ld {
    /*! @class RecorderCreate
     * @brief The recorder create class
     * @details The recorder create class is used to record the creation of the elements in the search area
     */
    class RecorderCreateData {
    public:
        std::string actionType{};                                                                                       /*> The action type */
        int line{0};                                                                                                    /*> The line */
        std::string script{};                                                                                           /*> The script */
        long timeLine{0};                                                                                               /*> The time line */
        std::string channelName{};                                                                                      /*> The channel name */
        TestBound channelDimension{};                                                                                   /*> The channel dimension */
        bool sync{false};                                                                                               /*> The sync */
        bool stop{false};                                                                                               /*> The stop */
    };

    /*! @class RecorderCreate
     * @brief The recorder create class
     * @details The recorder create class is used to record the creation of the elements in the search area
     */
    class RecorderCreate : public Recorder {
    private:
        RecorderCreateData m_recorderCreateData{};                                                                        /*< The data of the recorder start */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        /*!
         * @brief _jsonIsValid Check if the json is valid
         * @details Check if the json is valid
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the json is valid, false otherwise
         */
        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};
    public:
        RecorderCreate() = default;
        ~RecorderCreate() override = default;

        /*!
         * @brief execute Execute the recorder Create
         * @details Execute the recorder Create. The recorder Create is used to record the creation of the elements in the search area. this override the execute function of the recorder class
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the recorder start is executed, false otherwise
         */
        bool execute(const crow::request &req, crow::response &res) override;
    };
}
#endif //MACOSDRIVER_RECORDERCREATE_HPP
