#ifndef MACOSDRIVER_RECORDERTEST_HPP
#define MACOSDRIVER_RECORDERTEST_HPP
#include "recorder.hpp"
#include "../../include/utils/imagePng.hpp"


namespace ld{
    class RecorderTest : public Recorder {
    private:
        bool _parseJson(const crow::request& req) override{return true;};
        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;

        bool _jsonIsValid(const crow::request &req, crow::response &res) override {return Recorder::_jsonIsValid(req, res);};
    public:
        RecorderTest() = default;
        ~RecorderTest() override = default;
        bool execute(const crow::request &req, crow::response &res) override;
        static amf::v8 serialiseAmfActions(const amf::AmfObject &obj);
    };
}
#endif //MACOSDRIVER_RECORDERTEST_HPP
