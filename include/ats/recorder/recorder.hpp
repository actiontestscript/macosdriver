#ifndef MACOSDRIVER_RECORDER_HPP
#define MACOSDRIVER_RECORDER_HPP

#include <fstream>
#include "../../../apps/config.hpp"
#include "../../W3C/session/session.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../../utils/utils.hpp"
#include "../../../include/ats/recorder/desktopResponse.hpp"
#include "../../include/utils/imagePng.hpp"
#include "../../graphicInterface/graphicInterfaceFactory.hpp"
#include "../../utils/httpRequest.hpp"


using json = nlohmann::json;

namespace ld{
    /*! @brief The class Recorder
     * @details This class is used recorder the actions of webbrowser. This class is abstract.
     */
    class Recorder{
    protected:
        Session* m_session{};                                                                                           /*< Session object */
        CrowJsonResponse m_crowJsonResponse;                                                                            /*< CrowJsonResponse object */
        std::string m_sessionId{};                                                                                      /*< Session id */
        bool m_recorderOld{false};                                                                                      /*< Recorder old */
        char m_postDataDelimiter{'\n'};                                                                                 /*< Post data delimiter */
        std::string m_browserSessionId{};                                                                               /*< Browser session id */
        std::string m_browserScreenShotUrl{};                                                                           /*< Browser screen shot url */
        bool m_browserHeadless{false};                                                                                  /*< Browser headless */

        /*!
         * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
         */
        virtual bool _parseJson(const crow::request& req) = 0;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        virtual bool _parsePostData(const crow::request &req) = 0 ;


        /*!
         * @brief _jsonIsValid Check if the json is valid
         * @details Check if the json is valid
         * @param const <crow::request>& req The request
         * @param <crow::response>& res The response
         * @return True if the json is valid, false otherwise
         */
        virtual bool _jsonIsValid(const crow::request &req, crow::response &res) = 0;

        /*!
         * @brief _jsonParseInt Parse the json to get an int
         * @details Parse the json to get an int
         * @param const <json>& value The json
         * @param const <std::string>& key The key
         * @param int& data The data
         */
        [[maybe_unused]]void _jsonParseInt(const json &value, const std::string &key, int &data);

        /*!
         * @brief _jsonParseDouble Parse the json to get a double
         * @details Parse the json to get a double
         * @param const <json>& value The json
         * @param const <std::string>& key The key
         * @param double& data The data
         */
        [[maybe_unused]] void _jsonParseDouble(const json &value, const std::string &key, double &data);

        /*!
         * @brief _jsonParseLong Parse the json to get a long
         * @details Parse the json to get a long
         * @param const <json>& value The json
         * @param const <std::string>& key The key
         * @param long& data The data
         */
        [[maybe_unused]]void _jsonParseLong(const json &value, const std::string &key, long &data);

        /*!
         * @brief _jsonParseString Parse the json to get a string
         * @details Parse the json to get a string
         * @param const <json>& value The json
         * @param const <std::string>& key The key
         * @param std::string& data The data
         */
        [[maybe_unused]]void _jsonParseString(const json &value, const std::string &key, std::string &data);

        /*!
         * @brief _jsonParseBool Parse the json to get a bool
         * @details Parse the json to get a bool
         * @param const <json>& value The json
         * @param const <std::string>& key The key
         * @param bool& data The data
         */
        [[maybe_unused]]void _jsonParseBool(const json &value, const std::string &key, bool &data);

        /*!
         * @brief _strParseInt Parse the string to get an int
         * @details Parse the string to get an int
         * @param std::string& strResearch The string
         * @param const char& delimiter The delimiter
         * @param const int& defaultValue The default value
         * @param int& data The data
         */
        [[maybe_unused]] void _strParseInt(std::string &strResearch, const char& delimiter, const int& defaultValue, int &data );

        /*!
         * @brief _strParseDouble Parse the string to get a double
         * @details Parse the string to get a double
         * @param std::string& strResearch The string
         * @param const char& delimiter The delimiter
         * @param const double& defaultValue The default value
         * @param double& data The data
         */
        [[maybe_unused]] void _strParseDouble(std::string &strResearch, const char& delimiter, const double& defaultValue, double &data);

        /*!
         * @brief _strParseLong Parse the string to get a long
         * @details Parse the string to get a long
         * @param std::string& strResearch The string
         * @param const char& delimiter The delimiter
         * @param const long& defaultValue The default value
         * @param long& data The data
         */
        [[maybe_unused]] void _strParseLong(std::string &strResearch, const char& delimiter, const long& defaultValue, long &data);

        /*!
         * @brief _strParseString Parse the string to get a string
         * @details Parse the string to get a string
         * @param std::string& strResearch The string
         * @param const char& delimiter The delimiter
         * @param const std::string& defaultValue The default value
         * @param std::string& data The data
         */
        [[maybe_unused]] void _strParseString(std::string &strResearch, const char& delimiter, const std::string& defaultValue, std::string &data);

        /*!
         * @brief _strParseBool Parse the string to get a bool
         * @details Parse the string to get a bool
         * @param std::string& strResearch The string
         * @param const char& delimiter The delimiter
         * @param const bool& defaultValue The default value
         * @param bool& data The data
         */
        [[maybe_unused]] void _strParseBool(std::string &strResearch, const char& delimiter, const bool& defaultValue, bool &data);


    public:
        const std::string API_SESSION = "api-session";

        /*!
         * @brief The constructor of the class Recorder
         * @details This constructor is used to initialize the session object.
         */
        Recorder(){m_session = Session::getInstance();}

        /*!
         * @brief The destructor of the class Recorder
         * @details This destructor is used to delete the session object.
         */
        virtual ~Recorder()= default;

        /*!
         * @brief The method execute
         * @details This method is used to execute the recorder. This method is abstract.
         * @param const <crow::request>&req The request object
         * @param <crow::response>&res The response object
         * @return <bool> true if the recorder is executed, false otherwise
         */
        virtual bool execute(const crow::request& req,  crow::response& res)=0;

        /*!
         * @brief setSessionId Set the session id
         * @details Set the session id
         * @param const <std::string>& sessionId The session id
         */
        void setSessionId(const std::string& sessionId){m_sessionId = sessionId;};

        /*!
         * @brief setRecorderOld Set the recorder old
         * @details Set the recorder old ats version
         * @param bool isOld True if the recorder is old, false otherwise
         */
        void setRecorderOld(bool isOld){m_recorderOld = isOld;};

        /*!
         * @brief flushFile Flush the file
         * @details Flush amf data into the file
         * @param const <std::string>& filename The filename
         * @param const <amf::v8>& data The data
         */
        static void flushFile(const std::string& filename, const amf::v8& data);

        /*!
         * @brief flushVisualReport Flush the visual report
         * @details Flush the visual report
         * @param const <std::string>& uuid The uuid
         */
        void flushVisualReport(const std::string& uuid);

        /*!
         * @brief flushVisualAction Flush the visual action
         * @details Flush the visual action
         * @param const <std::string>& uuid The uuid
         */
        void flushVisualAction(const std::string& uuid);

        /*!
         * @brief flushReportSummary Flush the report summary
         * @details Flush the report summary
         * @param const <std::string>& uuid The uuid
         */
        void flushReportSummary(const std::string& uuid);

        /*!
         * @brief flushReportSummary Flush the report summary
         * @details Flush the report summary
         * @param const <std::string>& uuid The uuid
         */
        void getResponse(crow::response &res) const;

        void setBrowserInfo(const json& value) ;
        void setBrowserInfoOld(std::string value) ;

        template <typename T>
        void addImage(T& objVisual, const TestBound& imgBound, const bool& isRef){
////            Session::RecorderInfo* recorder = m_session->getRecorder(uuid);
//            objVisual.imageType = "png";
//            ImagePng img;
//
//            //ImagePng::createEmptyPNGImage(img,  imgBound.width,  imgBound.height);
//            ImagePng::createEmptyPNGImage(img,  3,  3);
//            img.setAllPixelColor(255,0,255,255);
//            std::vector<uint8_t> imgData = img.pngImageToBytes(img);
////            recorder->currentAction.images = imgData;
//
            std::vector<uint8_t> imgData{} ;
/*
            if(m_browserSessionId == API_SESSION){
                imgData = Utils::base64_decode( m_imgApiBase64 );
            }
            else
                */
            if(!m_browserScreenShotUrl.empty()){
//            if(m_browserHeadless && !m_browserScreenShotUrl.empty()){
                imgData = Utils::base64_decode( HttpRequest::getScreenShot(m_browserScreenShotUrl) );
            }

/*
            auto ig = GraphicInterfaceFactory().createGraphicInterface<GraphicInterface>("x11");
            if(ig != nullptr){
                imgData = ig->screenShot(imgBound.x, imgBound.y, imgBound.width, imgBound.height);
            }
*/
            if(imgData.empty())
            {
                //img vide
                ImagePng img;
                //ImagePng::createEmptyPNGImage(img,  imgBound.width,  imgBound.height);
                ImagePng::createEmptyPNGImage(img,  3,  3);
//                img.setAllPixelColor(255,0,255,255);
                imgData = img.pngImageToBytes(img);
            }
            objVisual.imageType = "png";
//            objVisual.images.emplace_back(imgData);
//            else if (currentActionName == "visualAction") objVisual.images.emplace_back(imgData);

            objVisual.images.emplace_back(imgData);
        }

        void getWebDriverXY(const std::string &webDriverUrlScreenShot, double& offsetX, double& offsetY, const bool& isHeadless);
        void setOffsetWebDriver(const std::string &webDriverUrlScreenShot, Session::RecorderInfo& recorderInfo, const bool& isHeadless);
    };
}
#endif //MACOSDRIVER_RECORDER_HPP
