#ifndef MACOSDRIVER_TESTERROR_HPP
#define MACOSDRIVER_TESTERROR_HPP
#include <string>
#include "amfInclude.hpp"

namespace ld{
    /*!
     * @class TestError
     * @brief The class TestError
     * @details This class is used to store the configuration of the application.
     */
    class TestError{
    private:
        AmfObject amf_testError= AmfObject("com.ats.recorder.TestError", false, false);                /*>! amf object     */
    public:
        std::string script="";                                                                                                       /*>! script         */
        int line=0;                                                                                                                  /*>! script line    */
        std::string message="";                                                                                                      /*>! message        */

        /*!
         * @brief TestError
         * @details default constructor
         */
        TestError()= default;

        /*!
         * @brief TestError
         * @details constructor
         * @param const <std::string>& script
         * @param const <int& line>
         * @param const <std::string>& message
         */
        TestError(const std::string& script, const int& line, const std::string& message);

        /*!
         * @brief getAmfTestError
         * @details get amf object
         * @return const <amf::AmfObject&>
         */
        [[nodiscard]] const amf::AmfObject& getAmfTestError() const { return amf_testError;}

        /*!
         * @brief setAmfTestError
         * @details set amf object
         * @param amf::AmfObject& obj
         */
        void setAmfTestError(amf::AmfObject& obj) const;

        /*!
         * @brief initAmfTestError
         * @details init amf object
         */
        void initAmfTestError();
    };
}
#endif //MACOSDRIVER_TESTERROR_HPP
