#ifndef MACOSDRIVER_DESKTOPRESPONSE_HPP
#define MACOSDRIVER_DESKTOPRESPONSE_HPP
#include "amfInclude.hpp"
#include<vector>
#include "../../crowSettings/crowJsonResponse.hpp"

namespace ld {

    class AppData {
    public:
        [[nodiscard]] const amf::AmfObject &getAmfAppData() const { return amf_desktopAppData; }

        void initAmfDesktopWindowAppData();
        void setAmfObjectAppData(amf::AmfObject &obj) const;
        json serializeToJson();

    private:
        std::string m_version{};
        std::string m_buildVersion{};
        std::string m_name{};
        std::string m_Path{};
        std::vector<uint8_t> m_icon{};
        amf::AmfObject amf_desktopAppData = amf::AmfObject("com.ats.executor.drivers.desktop.AppData", false,
                                                          false);
    };
    /*!
     * @class DesktopWindow
     * @brief The class DesktopWindow
     * @details This class is used to store the DesktopWindow for old version of ATS
     */
    class DesktopWindow {
    public:
        int pid{};
        int handle{};
        AppData m_app{};
        [[nodiscard]] const amf::AmfObject &getAmfDesktopWindow() const { return amf_desktopWindow; }

        void initAmfDesktopWindow();
        json serializeToJson();
        void setAmfObjectDesktopWindow(amf::AmfObject &obj) const;
        [[nodiscard]] amf::AmfItemPtr WindowsToAmfItem() const;

    private:
        amf::AmfObject amf_desktopWindow = amf::AmfObject("com.ats.executor.drivers.desktop.DesktopWindow", false,false);
    };

    class DesktopData {
    private:
        amf::AmfObject amf_desktopData = amf::AmfObject("com.ats.executor.drivers.desktop.DesktopData", false, false);
        std::string m_data{};
        [[nodiscard]] std::string getData() const{return name + "\n" + value;};

    public:
        std::string name{};
        std::string value{":"};

        [[nodiscard]] const amf::AmfObject &getAmfDesktopData() const { return amf_desktopData; }
        void initAmfDesktopData();
        void setAmfObjectDesktopData(amf::AmfObject &obj) const;
        [[nodiscard]] amf::AmfItemPtr attributesToAmfItem() const;
        json serializeToJson();
    };

    class AtsElement {
    private:
        amf::AmfObject amf_atsElement = amf::AmfObject("com.ats.element.AtsElement", false, false);
    public:
        std::string id{};
        std::string tag{"*"};
        bool clickable{true};
        double x{0};
        double y{0};
        double width{-1};
        double height{-1};
        bool visible{false};
        bool password{false};
        int numChildren{0};
        std::vector<DesktopData> attributes{};
        std::vector<AtsElement> children{};

        [[nodiscard]] const amf::AmfObject &getAmfAtsElement() const { return amf_atsElement; }
        void initAmfAtsElement();
        void setAmfObjectAtsElement(amf::AmfObject &obj) const;
        [[nodiscard]] amf::AmfItemPtr elementsToAmfItem() const;
        static amf::AmfArray convertAttributesToArray(const std::vector<DesktopData> &attributes);
        static amf::AmfArray convertElementsToArray(const std::vector<AtsElement> &elements);
        json serializeToJson();
    };



    class DesktopResponse {
    public:
        int errorCode{0};
        std::string errorMessage{};
        std::vector<uint8_t> image{};
        std::vector<DesktopWindow> windows{};
        std::vector<DesktopData> attributes{};
        std::vector<AtsElement> elements{};

        [[nodiscard]] const amf::AmfObject& getAmfDesktopResponse() const { return amf_desktopResponse; }
        void initAmfDesktopResponse();
        void setAmfObjectDesktopResponse(amf::AmfObject& obj) const;
        static amf::AmfArray convertWindowsToArray(const std::vector<DesktopWindow>& window);
        static amf::AmfArray convertAttributesToArray(const std::vector<DesktopData>& attribute);
        static amf::AmfArray convertElementsToArray(const std::vector<AtsElement>& element);

        static void getResponse(crow::response &res,const CrowJsonResponse::ResponseType& responseType) ;
        static void getResponse(crow::response &res, DesktopResponse& desktopResponse,const CrowJsonResponse::ResponseType& responseType) ;
        json serializeToJson();
    private:
        amf::AmfObject amf_desktopResponse = amf::AmfObject("com.ats.executor.drivers.desktop.DesktopResponse", false, false);

    };
}
#endif //MACOSDRIVER_DESKTOPRESPONSE_HPP
