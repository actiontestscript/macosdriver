#ifndef MACOSDRIVER_SELECT_HPP
#define MACOSDRIVER_SELECT_HPP
#include "element.hpp"
namespace ld{
    class ElementSelectData{
    public :
        std::string m_id{};
        std::string m_type{};
        std::string m_value{};
        std::string m_regexp{};
    };

    class ElementSelect : public Element {
        public:
        ElementSelect() = default;
        ~ElementSelect() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Element::execute(req, res);};

    private:
        ElementSelectData m_elementSelectData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}
#endif //MACOSDRIVER_SELECT_HPP
