#ifndef MACOSDRIVER_CHILDS_HPP
#define MACOSDRIVER_CHILDS_HPP
#include "element.hpp"
namespace ld{
    class ElementChildsData{
    public :
        std::string m_id{};
        std::string m_tag{};
        std::vector<std::string> m_attributes{};
    };

    class ElementChilds : public Element {
        public:
        ElementChilds() = default;
        ~ElementChilds() = default;
        bool execute(const crow::request &req, crow::response &res) override ;
        //bool execute(const crow::request &req, crow::response &res) override {return Element::execute(req, res);};

    private:
        ElementChildsData m_elementChildsData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}
#endif //MACOSDRIVER_CHILDS_HPP
