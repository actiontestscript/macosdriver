#ifndef MACOSDRIVER_ELEMENTFACTORY_HPP
#define MACOSDRIVER_ELEMENTFACTORY_HPP
#include "element.hpp"
#include "attributes.hpp"
#include "childs.hpp"
#include "contextMenu.hpp"
#include "dialogBox.hpp"
#include "find.hpp"
#include "focus.hpp"
#include "fromPoint.hpp"
#include "listItems.hpp"
#include "loadTree.hpp"
#include "parents.hpp"
#include "root.hpp"
#include "select.hpp"
#include "script.hpp"
#include <memory>

namespace ld{

    class ElementFactory {
    public:
        template<typename T>
        std::unique_ptr <T> create(const std::string &elementType) {
            if (elementType == "childs") {
                auto elements = std::make_unique<ElementChilds>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "parents") {
                auto elements = std::make_unique<ElementParents>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "find") {
                auto elements = std::make_unique<ElementFind>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "attributes") {
                auto elements = std::make_unique<ElementAttributes>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "select") {
                auto elements = std::make_unique<ElementSelect>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "frompoint") {
                auto elements = std::make_unique<ElementFromPoint>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "script") {
                auto elements = std::make_unique<ElementScript>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "root") {
                auto elements = std::make_unique<ElementRoot>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "loadtree") {
                auto elements = std::make_unique<ElementLoadTree>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "listitems") {
                auto elements = std::make_unique<ElementListItems>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "dialogbox") {
                auto elements = std::make_unique<ElementDialogBox>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "focus") {
                auto elements = std::make_unique<ElementFocus>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }
            else if (elementType == "contextmenu") {
                auto elements = std::make_unique<ElementContextMenu>();
                return std::unique_ptr<Element>(dynamic_cast<Element *>(elements.release()));
            }

            else {
                return nullptr;
            }
        }
    };

}
#endif //MACOSDRIVER_ELEMENTFACTORY_HPP
