#ifndef MACOSDRIVER_DIALOGBOX_HPP
#define MACOSDRIVER_DIALOGBOX_HPP
#include "element.hpp"
namespace ld{
    class ElementDialogBoxData{
    public :
        std::string m_pId{};
    };

    class ElementDialogBox : public Element {
        public:
        ElementDialogBox() = default;
        ~ElementDialogBox() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Element::execute(req, res);};

    private:
        ElementDialogBoxData m_elementDialogBoxData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}
#endif //MACOSDRIVER_DIALOGBOX_HPP
