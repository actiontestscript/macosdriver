#ifndef MACOSDRIVER_FOCUS_HPP
#define MACOSDRIVER_FOCUS_HPP
#include "element.hpp"
namespace ld{
    class ElementFocusData{
    public :
        std::string m_id{};
    };

    class ElementFocus : public Element {
        public:
        ElementFocus() = default;
        ~ElementFocus() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Element::execute(req, res);};

    private:
        ElementFocusData m_elementFocusData{};
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}
#endif //MACOSDRIVER_FOCUS_HPP
