#ifndef MACOSDRIVER_LOADTREE_HPP
#define MACOSDRIVER_LOADTREE_HPP
#include "element.hpp"
namespace ld{
    class ElementLoadTreeData{
    public :
        int m_handle{};
        int m_pid{};
    };

    class ElementLoadTree : public Element {
        public:
        ElementLoadTree() = default;
        ~ElementLoadTree() = default;
        bool execute(const crow::request &req, crow::response &res) override {return Element::execute(req, res);};

    private:
        ElementLoadTreeData m_elementLoadTreeData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;
    };
}
#endif //MACOSDRIVER_LOADTREE_HPP
