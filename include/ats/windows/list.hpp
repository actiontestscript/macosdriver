#ifndef MACOSDRIVER_LIST_HPP
#define MACOSDRIVER_LIST_HPP
#include "windows.hpp"

namespace ld{
    class WindowsListData {
    public:
        std::vector<std::pair<std::string,std::string>> m_nameWebDriverProcessAndPids{};
        std::unordered_map<std::string, std::vector<std::string>> m_nameBrowserPids{};
        int m_pid{};
    };


    class WindowsList : public Windows {
    public:
        WindowsList();
        ~WindowsList() = default;
        bool execute(const crow::request &req, crow::response &res) override;

    private:
        WindowsListData m_windowsListData{};
        Session* m_session{};
        void getWebDriverSessionPid();
        void getBrowserPids();

        /*!
        * @brief _parseJson Parse the json
         * @details Parse the json to get the data of the recorder start
         * @param const <crow::request>& req The request
         * @return True if the json is parsed, false otherwise
 */
        bool _parseJson(const crow::request &req) override;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override;
    };

}

#endif //MACOSDRIVER_LIST_HPP
