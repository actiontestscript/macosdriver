#ifndef MACOSDRIVER_KEYS_HPP
#define MACOSDRIVER_KEYS_HPP
#include "windows.hpp"

namespace ld{
    class WindowsKeysData {
    public :
        int m_handle{};
        std::string m_keys{};
    };


    class WindowsKeys: public Windows {
    public:
        WindowsKeys();
        ~WindowsKeys() = default;
        bool execute(const crow::request &req, crow::response &res) override;
    private:
        WindowsKeysData m_windowsKeysData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override ;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;

    };

}

#endif //MACOSDRIVER_KEYS_HPP
