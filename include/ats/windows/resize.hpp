#ifndef MACOSDRIVER_RESIZE_HPP
#define MACOSDRIVER_RESIZE_HPP
#include "windows.hpp"

namespace ld{
    class WindowsResizeData{
    public :
        int m_handle{};
        int m_value1{};
        int m_value2{};
    };

    class WindowsResize : public Windows {
    public:
        WindowsResize();
        ~WindowsResize() = default;
        bool execute(const crow::request &req, crow::response &res) override;
    private:
        WindowsResizeData m_windowResizeData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override ;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;

    };

}

#endif //MACOSDRIVER_RESIZE_HPP
