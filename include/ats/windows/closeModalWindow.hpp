#ifndef MACOSDRIVER_CLOSEMODALWINDOW_HPP
#define MACOSDRIVER_CLOSEMODALWINDOW_HPP
#include "windows.hpp"

namespace ld{
    class WindowsCloseModalWindowData {
    public :
        int m_handle{};
    };

    class WindowsCloseModalWindow : public Windows {
    public:
        WindowsCloseModalWindow();
        ~WindowsCloseModalWindow() = default;
        bool execute(const crow::request &req, crow::response &res) override;
    private:
        WindowsCloseModalWindowData m_windowsCloseModalWindowData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override ;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;

    };

}

#endif //MACOSDRIVER_CLOSEMODALWINDOW_HPP
