#ifndef MACOSDRIVER_WINDOWS_HPP
#define MACOSDRIVER_WINDOWS_HPP
#include "../../../apps/config.hpp"
#include "../../W3C/session/session.hpp"
#include "../../crowSettings/crowJsonResponse.hpp"
#include "../../utils/utils.hpp"
#include "../../utils/httpRequest.hpp"
#include "../../../include/ats/recorder/desktopResponse.hpp"

using json = nlohmann::json;

namespace ld{

    class Windows{

    public:
        Windows()= default;
        virtual ~Windows()= default;
        virtual bool execute(const crow::request& req,  crow::response& res)=0;
        [[maybe_unused]] CrowJsonResponse::ResponseType getResponseType(){return m_responseTypeJson;};

        [[maybe_unused]] void setResponseType(CrowJsonResponse::ResponseType responseType){m_responseTypeJson = responseType;};

    private :
        CrowJsonResponse::ResponseType m_responseTypeJson{CrowJsonResponse::ResponseType::JSON};
        /*!
          * @brief _parseJson Parse the json
          * @details Parse the json to get the data of the recorder start
          * @param const <crow::request>& req The request
          * @return True if the json is parsed, false otherwise
          */
        virtual bool _parseJson(const crow::request& req) = 0;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        virtual bool _parsePostData(const crow::request &req) = 0 ;
    protected:

        char m_postDataDelimiter{'\n'};                                                                                 /*< Post data delimiter */
        void parseData(const crow::request &req);
//        static void getResponse(crow::response &res) ;
//        static void getResponse(crow::response &res, DesktopResponse& desktopResponse) ;
//        static int getNbrData(std::string data, const std::string& delimiter);


    };


}
#endif //MACOSDRIVER_WINDOWS_HPP
