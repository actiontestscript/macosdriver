#ifndef MACOSDRIVER_MOVE_HPP
#define MACOSDRIVER_MOVE_HPP
#include "windows.hpp"

namespace ld{
    class WindowsMoveData{
    public :
        int m_handle{};
        int m_value1{};
        int m_value2{};
    };

    class WindowsMove : public Windows {
    public:
        WindowsMove();
        ~WindowsMove() = default;
        bool execute(const crow::request &req, crow::response &res) override;
    private:
        WindowsMoveData m_windowMoveData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override ;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;

    };

}

#endif //MACOSDRIVER_MOVE_HPP
