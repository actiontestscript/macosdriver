#ifndef MACOSDRIVER_WINDOWTITLE_HPP
#define MACOSDRIVER_WINDOWTITLE_HPP
#include "windows.hpp"

namespace ld{
    class WindowsTitleData{
    public :
        std::string m_title{};
        int m_pid{};
    };

    class WindowsTitle : public Windows {
    public:
        WindowsTitle();
        ~WindowsTitle() = default;
        bool execute(const crow::request &req, crow::response &res) override;
    private:
        WindowsTitleData m_windowTitleData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override ;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;

    };

}

#endif //MACOSDRIVER_WINDOWTITLE_HPP
