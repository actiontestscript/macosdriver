#ifndef MACOSDRIVER_SWITCH_HPP
#define MACOSDRIVER_SWITCH_HPP
#include "windows.hpp"

namespace ld{
    class WindowsSwitchData{
    public :
        int m_pid{};
        int m_index{};
        int m_handle{};
    };

    class WindowsSwitch : public Windows {
    public:
        WindowsSwitch();
        ~WindowsSwitch() = default;
        bool execute(const crow::request &req, crow::response &res) override;
    private:
        WindowsSwitchData m_windowSwitchData;
        /*!
        * @brief _parseJson Parse the json
        * @details Parse the json to get the data of the recorder start
        * @param const <crow::request>& req The request
        * @return True if the json is parsed, false otherwise
        */
        bool _parseJson(const crow::request &req) override ;

        /*!
         * @brief _parsePostData Parse the post data old method
         * @details Parse the post data old method
         * @param const <crow::request>& req The request
         * @return True if the post data is parsed, false otherwise
         */
        bool _parsePostData(const crow::request &req) override ;

    };

}

#endif //MACOSDRIVER_SWITCH_HPP
