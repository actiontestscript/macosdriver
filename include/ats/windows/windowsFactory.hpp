#ifndef MACOSDRIVER_WINDOWSFACTORY_HPP
#define MACOSDRIVER_WINDOWSFACTORY_HPP

#include "windows.hpp"
#include "title.hpp"
#include "handle.hpp"
#include "list.hpp"
#include "move.hpp"
#include "resize.hpp"
#include "toFront.hpp"
#include "switch.hpp"
#include "close.hpp"
#include "url.hpp"
#include "keys.hpp"
#include "state.hpp"
#include "closeWindow.hpp"
#include "closeModalWindow.hpp"


namespace ld{
    class WindowsFactory{
    public:
        template <typename T>
        std::unique_ptr<T> create(const std::string& windowsType) {
            if(windowsType == "title"){
                auto windows = std::make_unique<WindowsTitle>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "handle"){
                auto windows = std::make_unique<WindowsHandle>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "list"){
                auto windows = std::make_unique<WindowsList>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "move"){
                auto windows = std::make_unique<WindowsMove>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "resize"){
                auto windows = std::make_unique<WindowsResize>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "tofront"){
                auto windows = std::make_unique<WindowsToFront>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "switch"){
                auto windows = std::make_unique<WindowsSwitch>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "close"){
                auto windows = std::make_unique<WindowsClose>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "url"){
                auto windows = std::make_unique<WindowsUrl>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "keys"){
                auto windows = std::make_unique<WindowsKeys>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "state"){
                auto windows = std::make_unique<WindowsState>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "closewindow"){
                auto windows = std::make_unique<WindowsClose>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else if(windowsType == "closemodalwindows"){
                auto windows = std::make_unique<WindowsCloseModalWindow>();
                return std::unique_ptr<Windows>(dynamic_cast<Windows*>(windows.release()));
            }
            else {
                return nullptr;
            }
        }
    };

}
#endif //MACOSDRIVER_WINDOWSFACTORY_HPP
