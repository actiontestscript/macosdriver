#include "../../include/utils/xmlSettings.hpp"

namespace ld{

    bool XmlSettings::isExist() {
        return findAtsProperties();
    }

    bool
    XmlSettings::loadParameters() {
        if(m_fileFullPath.empty()) findAtsProperties();

        pugi::xml_document doc;
        if(!doc.load_file(m_fileFullPath.c_str())) {
            LD_LOG_DEBUG << "Error loading file " << m_fileFullPath << " xml bad format" << std::endl;
            return false;
        }
        std::set<std::string> ipsMap;

        m_remoteHostname = doc.child("execute").child("SystemDriver").child("remoteHostname").child_value();

        for (pugi::xml_node allowedIp : doc.child("execute").child("SystemDriver").children("allowedIp")) {
            ipsMap.insert( allowedIp.child_value() );
        }

        for (pugi::xml_node allowedIpRange : doc.child("execute").child("SystemDriver").children("allowedIpRange")) {
            std::string startIp = allowedIpRange.child("start").child_value();
            std::string endIp = allowedIpRange.child("end").child_value();
            if(Network::isValidIpv4(startIp) && Network::isValidIpv4(endIp)){
                int range = getRange(startIp,endIp);
                int iStartIp = getLastOctet(startIp);
                std::string ipNetwork = getIpNetwork(startIp);
                for(int j=0;j<=range;j++){
                    ipsMap.insert(ipNetwork + std::to_string(iStartIp+ j) );
                }
            }
        }

        int i=0;
        for (auto& ip : ipsMap) {
            if(i>0) m_allowedIps += ",";
            m_allowedIps += ip;
            i++;
        }

        if (!m_allowedIps.empty()){
            std::vector<std::string> listIpsLocal = Network::getListIpsLocal();
            //int i=0;
            for (const auto &ip : listIpsLocal) {
              //  if(i>0) m_localIpsBinding += ",";
                if(ip.compare(0,3, "127") != 0) {
                    m_localIpsBinding += ",";
                    m_localIpsBinding += ip;
                }
                //i++;
            }
        }

        ld::Config::logLevelAll = "INFO";

        pugi::xml_node debugNode = doc.child("execute").child("SystemDriver").child("Debug");
        if(debugNode){
            if(debugNode.child("LogType")){
                std::string logTypeStr = debugNode.child_value("LogType");
                std::transform(logTypeStr.begin(), logTypeStr.end(), logTypeStr.begin(), ::toupper);
                if(logTypeStr == "FILE" || logTypeStr == "CONSOLE") {
                    ld::Config::logType = logTypeStr;
                }
            }
            if(debugNode.child_value("LogLevelAll")){
                std::string logLevelAllStr = debugNode.child_value("LogLevelAll");
                std::transform(logLevelAllStr.begin(), logLevelAllStr.end(), logLevelAllStr.begin(), ::toupper);
                if(logLevelAllStr=="ALL" || logLevelAllStr=="DEBUG" || logLevelAllStr=="INFO" || logLevelAllStr=="WARNING" || logLevelAllStr=="ERROR" || logLevelAllStr=="CRITICAL" || logLevelAllStr=="SEVERE") {
                    ld::Config::logLevelAll = logLevelAllStr;
                }
            };

            if(debugNode.child_value("LogLevelWebDriver")){
                std::string logLevelWebDriverStr = debugNode.child_value("LogLevelWebDriver");
                std::transform(logLevelWebDriverStr.begin(), logLevelWebDriverStr.end(), logLevelWebDriverStr.begin(), ::toupper);
                if(logLevelWebDriverStr=="ALL" || logLevelWebDriverStr=="DEBUG" || logLevelWebDriverStr=="INFO" || logLevelWebDriverStr=="WARNING" || logLevelWebDriverStr=="ERROR" || logLevelWebDriverStr=="CRITICAL" || logLevelWebDriverStr=="SEVERE") {
                    ld::Config::logWebDriverLevel = logLevelWebDriverStr;
                }
            }

            if(debugNode.child_value("LogLevelCrowCpp")){
                std::string logLevelCrowCppStr = debugNode.child_value("LogLevelCrowCpp");
                std::transform(logLevelCrowCppStr.begin(), logLevelCrowCppStr.end(), logLevelCrowCppStr.begin(), ::toupper);
                if(logLevelCrowCppStr=="DEBUG" || logLevelCrowCppStr=="INFO" || logLevelCrowCppStr=="WARNING" || logLevelCrowCppStr=="ERROR" || logLevelCrowCppStr=="CRITICAL") {
                    ld::Config::logCrowCppLevel = logLevelCrowCppStr;
                }
            }
        }

        //BrowserPath
        pugi::xml_node browsersNode = doc.child("execute").child("browsers");
        if (!browsersNode) { return true; }

        for (pugi::xml_node browser = browsersNode.child("browser"); browser; browser = browser.next_sibling("browser")) {
            std::string browserName = browser.child_value("name");
            std::string browserPath = browser.child_value("path");
            if(!browserName.empty() && !browserPath.empty()){
                ld::Session::addBrowserAtsPropertiesPath(browserName,browserPath);
            }
        }

        return true;
    }

    int XmlSettings::getLastOctet(const std::string &ip) {
        std::regex pattern("\\.(\\d+)$");
        std::smatch match;
        if(std::regex_search(ip,match,pattern))
            return std::stoi(match[1]);
        return -1;
    }

    int XmlSettings::getRange(const std::string &startIp,const std::string &endIp) {
        int start = getLastOctet(startIp);
        int end = getLastOctet(endIp);
        return abs(end - start);
    }

    std::string XmlSettings::getIpNetwork(const std::string &ip) {
        std::regex pattern(R"(^((?:\d+\.){3})\d+$)");
        std::smatch match;
        if(std::regex_search(ip,match,pattern))
            return match[1];
        return "";
    }

    /*
    void XmlSettings::setPath() {
        std::string applicationPath = ld::Config::currentPath;
        size_t pos{};
        do{
            pos = applicationPath.find('/');
            if(pos != std::string::npos) {
                m_fileFullPath += applicationPath.substr(0, pos);
                m_fileFullPath += DIRSEPARATOR;
            }
            applicationPath = applicationPath.substr(pos+1);
        }while (pos != std::string::npos);
        m_fileFullPath += m_fileName;
    }
    */

    bool XmlSettings::findAtsProperties() {
        std::string applicationPath = ld::Config::currentPath;

        //check in current app directory
        if(Utils::fileExist(applicationPath + DIRSEPARATOR + m_fileName)){
            m_fileFullPath = applicationPath + DIRSEPARATOR + m_fileName;
            return true;
        }

        //check in directory up
        size_t pos{};
        do{
            pos = applicationPath.find('/');
            if(pos != std::string::npos) {
                m_fileFullPath += applicationPath.substr(0, pos);
                m_fileFullPath += DIRSEPARATOR;
            }
            applicationPath = applicationPath.substr(pos+1);
        }while (pos != std::string::npos);

        if(Utils::fileExist(m_fileFullPath + m_fileName)){
            m_fileFullPath += m_fileName;
            return true;
        }

        //check in home directory
        std::string homePath = getenv("HOME");
        if(Utils::fileExist(homePath + DIRSEPARATOR + m_fileName)){
            m_fileFullPath = homePath + DIRSEPARATOR + m_fileName;
            return true;
        }
        return false;
    }


    std::string XmlSettings::m_fileName {".atsProperties"};
    std::string XmlSettings::m_fileFullPath{};
}
