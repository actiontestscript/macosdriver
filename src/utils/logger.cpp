
#include "../../include/utils/logger.hpp"
namespace ld {
//initialize static members
    std::string Logger::filename = "ld_log";
    bool Logger::loggingToFile = false;
    LogLevel Logger::currentLogLevel = LogLevel::NONE;
    std::streambuf* Logger::originalCoutBuffer = std::cout.rdbuf();
    std::streambuf* Logger::originalCerrBuffer = std::cerr.rdbuf();

    // Static Definition
    const std::string Logger::ATS_AGENT_INFO = "ats-agent-info";
    const std::string Logger::ATS_DRIVER_VERSION = "ats-driver-version";
    const std::string Logger::ATS_DRIVER_PORT = "ats-driver-port";
    const std::string Logger::ATS_AGENT_PORT = "ats-agent-port";
    const std::string Logger::ATS_AGENT_OUTPUT = "ats-agent-output";
    const std::string Logger::ATS_AGENT_WARNING = "ats-agent-warning";
    const std::string Logger::ATS_AGENT_ERROR = "ats-agent-error";
    const std::string Logger::ATS_AGENT_CONNECTED = "ats-agent-connected";
    Logger::LogCallback Logger::logCallback = nullptr;
    Logger::MsgStartupCallback Logger::msgStartupCallback = nullptr;

}