#include "../../include/utils/atsProfile.hpp"
namespace ld{

    void AtsProfile::printProfile(const crow::request& req, crow::response &res) {
        getJsonHeaderResponse(res);
        if(!checkProfile(req, res)){ return; }
        json value;
        value["userDataPath"] = m_userDataPath;
        json j;
        j["value"] = value;
        res.code = 200;
        res.write(j.dump());
    }

    bool AtsProfile::checkProfile(const crow::request& req, crow::response& res){
        //check if json is valid
        if(!isValidJson(req.body)){
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return false;
        }
        const json& bodyInit = json::parse(req.body);
        //check if json contain 'value' key
        if(bodyInit.find("value") == bodyInit.end()){
            _setMsgErrorCode("ats_error_profile_value_not_found","","");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return false;
        }
        const json& body = bodyInit["value"];

        //check if json contain 'userProfile' key
        if(!_getJsonUSerProfile(body)){
            _setMsgErrorCode("ats_error_profile_userProfile_not_found","","");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return false;
        }

        //check if json contain 'browserName' key
        if(!_getJsonBrowserName(body)){
            _setMsgErrorCode("ats_error_profile_browserName_not_found","","");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return false;
        }

        if(!setHomePath()){
            _setMsgErrorCode("ats_error_profile_homePath_not_found","","");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return false;
        }


        // 1 check if userProfile is Path or name
        std::string userProfile = getUserProfile();
        size_t pos = userProfile.find(DIRSEPARATOR);
        if(pos != std::string::npos) {
            setUserProfileAbsolutePath(true);
            setProfileDirectoryIsCreated(userProfile);
            if(!getProfileDirectoryIsWritable()){
                _setMsgErrorCode("ats_error_profile_userProfile_directory_not_writable","","");
                res.code = m_errorCode;
                res.write(_jsonMsgError().dump());
                return false;
            }

            if(getBrowserName() == "firefox"){
                //create the path et make the profile
                createProfileFirefox(res);
            }
            else setUserDataPath(getUserProfile());
            return true;
        }
        else {
            setUserDataPath(getHomePath() + DIRSEPARATOR + getAtsFolder() + DIRSEPARATOR + getBrowserName() + DIRSEPARATOR + getUserProfile());
            setProfileDirectoryIsCreated(getUserDataPath());
            if(!getProfileDirectoryIsWritable()){
                _setMsgErrorCode("ats_error_profile_userProfile_directory_not_writable","","");
                res.code = m_errorCode;
                res.write(_jsonMsgError().dump());
                return false;
            }
/*            if(!Utils::directoryExist(getUserDataPath())){
                //create the path
                std::string result{};
                std::string command = "mkdir -p " + getUserDataPath();
                if(!Utils::exec(command.c_str(),result)){
                    _setMsgErrorCode("ats_error_profile_userProfile_directory_not_created","","");
                    res.code = m_errorCode;
                    res.write(_jsonMsgError().dump());
                    return false;
                }
            }
            if(!Utils::directoryIsWritable(getUserDataPath())){
                _setMsgErrorCode("ats_error_profile_userProfile_directory_not_writable","","");
                res.code = m_errorCode;
                res.write(_jsonMsgError().dump());
                return false;
            }
*/
            if(!Utils::directoryAsFilesOrDirectory(getUserDataPath())){
                if(getBrowserName() == "firefox") {
                    createProfileFirefox(res);
                    setUserDataPath(getHomePath() + DIRSEPARATOR + getAtsFolder() + DIRSEPARATOR + getBrowserName() + DIRSEPARATOR + getUserProfile());
                }
            }
        }

        return true;
    }

    bool AtsProfile::_getJsonUSerProfile(const json& body) {
        if(body.find("userProfile") != body.end()){
            if(body["userProfile"].is_string()){
                setUserProfile(body["userProfile"]);
                return true;
            }
        }
        return false;
    }

    bool AtsProfile::_getJsonBrowserName(const json &body) {
        if(body.find("browserName") != body.end()){
            if(body["browserName"].is_string()){
                setBrowserName(body["browserName"]);
                return true;
            }
        }
        return false;
    }

    bool AtsProfile::createProfileFirefox(crow::response &res) {
        if(!Utils::applicationExist("firefox")) {
            _setMsgErrorCode("ats_error_browser_not_found","","");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return false;
        }

        createListProfilesFirefox();
        std::string userProfile = getUserProfile();

        if(getUserProfileIsAbsolutePath()){
            setUserDataPath(userProfile);
            std::string patternAuto{};
            patternAuto.append("^(");
            patternAuto.append(getProfileAutoName());
            patternAuto.append(")([\\d]+)$");
            std::regex pattern(patternAuto);
            int max{0};
            //check if path exist on profile firefox
            bool exist{false};
            for(auto& profile : m_listProfilesFirefox){
                if(profile.path == userProfile) {
                    setUserDataPath(profile.path);
                    setUserProfile(profile.name);
                    exist = true;
                    break;
                }
                if(std::regex_search(profile.name, pattern)){
                    std::smatch matches;
                    if(std::regex_search(profile.name, matches, pattern)){
                        int num = std::stoi(matches[2]);
                        if(num > max) max = num;
                    }
                }
            }
            //create directory
            if(!getProfileDirectoryIsCreated() && getProfileDirectoryIsWritable())
                if (!createProfileDirectory(getUserDataPath())) return false;

            if(!exist){
            //create a autoName for profile
                std::string autoName{};
                autoName.append(getProfileAutoName());
                autoName.append(std::to_string(max+1));
                setUserProfile(autoName);
//                setUserDataPath(getUserProfile());
                std::string result{};
                std::string command = "firefox --no-remote -CreateProfile \"" + getUserProfile() + " " + getUserDataPath() + "\"";
                return Utils::exec(command.c_str(),result);
            }
            else if(!getProfileDirectoryIsCreated() ){
                std::string result{};
                std::string command = "firefox --no-remote -CreateProfile \"" + getUserProfile() + " " + getUserDataPath() + "\"";
                return Utils::exec(command.c_str(),result);
            }
        }
        else{
            //create directory
            if(!getProfileDirectoryIsCreated() && getProfileDirectoryIsWritable())
                if (!createProfileDirectory(getUserDataPath())) return false;
            std::string result{};
            std::string command = "firefox --no-remote -CreateProfile \"" + getUserProfile() + " " + getUserDataPath() + "\"";
            return Utils::exec(command.c_str(),result);

        }
        return true;
    }

    bool AtsProfile::createListProfilesFirefox() {
        setPathFirefoxProfile(getHomePath() + DIRSEPARATOR + getPathFirefoxProfile());
        std::ifstream profileIni(getPathFirefoxProfile());
        if(!profileIni.is_open()) return false;
        std::string line;
        std::regex patternEndLine("[\\n]");
        ProfileFirefox profile;
        while (std::getline(profileIni, line)) {
            line = std::regex_replace(line, patternEndLine, "");
            if(line.empty() || line[0] == ';') continue;
            if(line.find("[Profile") != std::string::npos) {
                if(!profile.name.empty()){
                    m_listProfilesFirefox.push_back(profile);
                    profile = ProfileFirefox();
                }
            }
            else{
                size_t posEqual = line.find('=');
                std::string key = line.substr(0,posEqual);
                std::string value = line.substr(posEqual+1);
                if(key == "Name") profile.name = value;
                else if(key == "IsRelative") profile.isRelative = !(value == "0");
                else if(key == "Default") profile.isDefault = !(value == "0");
                else if(key == "Path") profile.path = value;
            }
        }
        if(!profile.name.empty()) m_listProfilesFirefox.push_back(profile);
        profileIni.close();

        return true;
    }

    void AtsProfile::setProfileDirectoryIsCreated(const std::string &directoryPath) {
        std::string userProfile = directoryPath;

        if(!Utils::directoryExist(userProfile)){
            m_profileDirectoryIsCreated = false;
            //check if parent directory exist and is writable
            std::regex pattern("^(.*)/[^/]*$");
            std::smatch matches;
            bool checkOk= false;
            while (!checkOk) {
                if (std::regex_search(userProfile, matches, pattern)) {
                    userProfile = matches[1];
                    if (Utils::directoryExist(userProfile)) {
                        //if directory exist check if readable
                        if (!AtsProfile::directoryIsWritable(userProfile)) {
                            setProfileDirectoryIsWritable(false);
                            checkOk = true;
                        }
                        else{
                            setProfileDirectoryIsWritable(true);
                            checkOk = true;
                        }
                    }
                }
                else checkOk = true;
            }
        }
        else if(!AtsProfile::directoryIsWritable(userProfile)){
            m_profileDirectoryIsCreated = true;
            setProfileDirectoryIsWritable(false);
        }
        else{
            m_profileDirectoryIsCreated = true;
            setProfileDirectoryIsWritable(true);
        }
    }

    bool AtsProfile::createProfileDirectory(const std::string &directoryPath) {
        //create the path
        std::string result{};
        std::string command = "mkdir -p " + directoryPath;
        return Utils::exec(command.c_str(),result);
    }

    bool AtsProfile::directoryIsWritable(const std::string &directoryPath){
        std::string path = directoryPath;
        const std::regex pattern("^(.*)/[^/]*$");
        std::smatch matches;
        bool checkOk= false;
        if (!Utils::directoryIsWritable(directoryPath)) {
            return false;
        }
        while (!checkOk) {
            if (std::regex_search(path, matches, pattern)) {
                path = matches[1];
                if (!Utils::directoryIsWritable(directoryPath)) {
                    return false;
                }

            }
            else checkOk = true;
        }
    return true;
    }

    bool AtsProfile::setHomePath(){
        std::string homePath{};
        struct passwd *pw = getpwuid(getuid());
        if(pw != nullptr) m_homePath = pw->pw_dir;
        else return false;

        std::string result{};
        std::string command = "which firefox";
        if(Utils::exec(command.c_str(),result)){
            std::regex pattern("^/snap/.*");
                //installation snap
            if(std::regex_search(result, pattern)){
                homePath = pw->pw_dir;
                homePath += DIRSEPARATOR;
                homePath += + "snap/firefox/common";
                homePath +=  DIRSEPARATOR;
                homePath += m_pathFirefoxProfile;
            }
            else{
                homePath = pw->pw_dir;
                homePath += DIRSEPARATOR;
                homePath += m_pathFirefoxProfile;
            }
            return true;
        }
    return false;
    }

}//namespace ld
