#include "../../include/utils/imagePng.hpp"
namespace ld {
    ImagePng::ImagePng():data(),width(0),height(0) {}

    std::vector<unsigned char> ImagePng::pngImageToBytes(const ImagePng& img) {
        std::vector<unsigned char> output;
        png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
        if (!png_ptr) return output;

        png_infop info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr) {
            png_destroy_write_struct(&png_ptr, nullptr);
            return output;
        }
        if (setjmp(png_jmpbuf(png_ptr))) {
            png_destroy_write_struct(&png_ptr, &info_ptr);
            return output;
        }

        std::vector<png_bytep> row_pointers(img.height);
        for (int y = 0; y < img.height; y++) {
            auto* non_const_data = const_cast<png_byte*>(img.data.data());
            row_pointers[y] = reinterpret_cast<png_bytep>(non_const_data + y * img.width * 4);
        }

        png_set_rows(png_ptr, info_ptr, row_pointers.data());
        png_set_IHDR(png_ptr, info_ptr, img.width, img.height, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

        png_set_write_fn(png_ptr, &output, [](png_structp png_ptr, png_bytep data, png_size_t length) {
            auto *output = reinterpret_cast<std::vector<unsigned char> *>(png_get_io_ptr(png_ptr));
            output->insert(output->end(), data, data + length);
        }, nullptr);

        png_write_info(png_ptr, info_ptr);
        png_write_image(png_ptr, row_pointers.data());
        png_write_end(png_ptr, nullptr);
        png_destroy_write_struct(&png_ptr, &info_ptr);

        return output;
    }

    std::vector<unsigned char> ImagePng::pngImageToBytes(ImagePng& img,const int& w, const int& h){
        if(data.empty()) createEmptyPNGImage(img, w, h);
        return pngImageToBytes(img);
    }

    void ImagePng::setPixelColor(const int& x,const int& y, const int& red, const int& green, const int& blue,const int& alpha) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            data[(y * width + x) * 4 + 0] = red;
            data[(y * width + x) * 4 + 1] = green;
            data[(y * width + x) * 4 + 2] = blue;
            data[(y * width + x) * 4 + 3] = alpha;
        }
    }

    void ImagePng::setAllPixelColor(const int &red, const int &green, const int &blue, const int &alpha) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                setPixelColor(x, y, red, green, blue, alpha);
            }
        }
    }

    void ImagePng::setColorType(int colorType) {
        if(colorType == PNG_COLOR_TYPE_GRAY || colorType == PNG_COLOR_TYPE_GRAY_ALPHA || colorType == PNG_COLOR_TYPE_PALETTE ||
           colorType == PNG_COLOR_TYPE_RGB || colorType == PNG_COLOR_TYPE_RGB_ALPHA)
        m_colorType = colorType;
        else
        m_colorType = PNG_COLOR_TYPE_RGB;
    }
}
