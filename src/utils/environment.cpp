#include "../../include/utils/environment.hpp"

namespace ld {
    bool Environment::getLinuxDistribution() {
        std::ifstream osReleaseFile("/etc/os-release");
        std::string line;
        std::regex nameRegex("^ID=\"?(\\w+)\"?");
        if (osReleaseFile.is_open()) {
            while (std::getline(osReleaseFile, line)) {
                std::smatch matches;
                if (std::regex_search(line, matches, nameRegex) && matches.size() > 1) {
                    m_distributionName = matches[1].str();
                    std::transform(m_distributionName.begin(), m_distributionName.end(), m_distributionName.begin(),tolower);
                    break;
                }
            }
            osReleaseFile.close();
        } else {
            if(ld::Config::logType != "NONE")
                LD_LOG_ERROR << "Environment -> Unable to open /etc/os-release" << std::endl;
            return false;
        }
        return true;
    }

    bool Environment::checkEnvironmentVariable() {
        m_graphicalEnvironment = false;
        if (getLinuxDistribution()) {
            if (m_distributionName == "unknown") return false;

//            if(m_distributionName == "ubuntu" || m_distributionName == "opensuse" || m_distributionName == "debian" || m_distributionName == "fedora"
            /*
            || m_distributionName == "centos"
            || m_distributionName == "arch" || m_distributionName == "manjaro" || m_distributionName == "gentoo" || m_distributionName == "alpine"
            || m_distributionName == "void" || m_distributionName == "linuxmint" || m_distributionName == "elementary"
            || m_distributionName == "solus" || m_distributionName == "deepin" || m_distributionName == "kali"
            || m_distributionName == "raspbian" || m_distributionName == "neon" || m_distributionName == "zorin" || m_distributionName == "pop"
            || m_distributionName == "antergos" || m_distributionName == "parrot" || m_distributionName == "trisquel"
            || m_distributionName == "ubuntu-gnome" || m_distributionName == "ubuntu-mate" || m_distributionName == "ubuntu-budgie"
            || m_distributionName == "ubuntu-kylin" || m_distributionName == "ubuntu-studio" || m_distributionName == "ubuntu-server"
            || m_distributionName == "ubuntu-core" || m_distributionName == "ubuntu-touch"
            */
//                    ){
            const char *displayEnv = std::getenv("DISPLAY");
            if (displayEnv != nullptr && displayEnv[0] != '\0') {
                const char *xdgSessionType = std::getenv("XDG_SESSION_TYPE");
                if (xdgSessionType != nullptr && xdgSessionType[0] != '\0') {
                    if (std::string(xdgSessionType) == "x11") {
                        m_sessionType = "x11";
                        m_graphicalEnvironment = true;
                        return true;
                    } else if (std::string(xdgSessionType) == "wayland") {
                        m_sessionType = "wayland";
                        m_graphicalEnvironment = true;
                        return true;
                    } else if (std::string(xdgSessionType) == "mir") {
                        m_sessionType = "mir";
                        m_graphicalEnvironment = true;
                        return true;
                    }
                }
            }
                //          }
            else return false;
        }
        return false;
    }

    Environment::Environment() {
        m_sessionType = "Aqua";
        m_graphicalEnvironment = true;
        m_isDockerContainers = getDockerContainers();
    }

    bool Environment::getDockerContainers(){
        const char* dockerEnv = std::getenv("DOCKER");
        return (dockerEnv != nullptr && dockerEnv[0] != '\0');
    }

}