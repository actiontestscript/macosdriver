#include "../../include/utils/atsProfile.hpp"
#include "../../include/utils/passwd.hpp"

namespace ld {
    void ld::Passwd::_parseJson(const crow::request &req) {
        const json &jsonBody = json::parse(req.body);
        if (jsonBody.find("value") != jsonBody.end()) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "login", m_login);
            CrowJsonResponse::jsonParse(value, "password", m_password);
        }
    }

    void Passwd::printCapabilities(crow::response &res) {
        CrowJsonResponse::getJsonHeaderResponse(res);
        json value;
        value["login"] = m_login;
        value["password"] = m_password;
        value["passwordHash"] = m_passwordHash;
        value["check"] = m_check;
        json j;
        j["value"] = value;
        res.code = 200;
        res.write(j.dump());
    }

    Passwd::Passwd(const crow::request& req) {
        if (CrowJsonResponse::isJson(req.body)) _parseJson(req);
        checkLoginPassword();
    }

    ld::PasswdData::PasswdData() {
        m_passwdData.push_back({"eric", "udtqcsshnd"});
    }

    bool Passwd::checkLoginPassword() {
        std::hash<std::string> hasher;
        size_t hash;
        if (!m_passwdData.m_passwdData.empty()) {
            for (auto &lp : m_passwdData.m_passwdData) {
                hash= hasher(lp[1]);
                std::stringstream ss;
                ss << hash;
                if (lp[0] == m_login && ss.str() == m_password) {
                    m_check = true;
                    break;
                }
            }
        }
        return false;
    }
}// namespace ld
