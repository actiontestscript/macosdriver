#include "../../include/utils/utils.hpp"

namespace ld{

    template <typename T>
     T Utils::getSysCtl(const std::string& name){
        T value;
        size_t  bufferSizes = sizeof(value);
        sysctlbyname(name.c_str(), &value, &bufferSizes, nullptr, 0);
        return value;
    }
    template <>
    std::string Utils::getSysCtl(const std::string& name){
        char buffer[256];
        size_t  bufferSizes = sizeof(buffer);
        sysctlbyname(name.c_str(), &buffer, &bufferSizes, nullptr, 0);
        return {buffer, bufferSizes};
    }

}