
#include "../../include/utils/webDriverProxy.hpp"

namespace ld {
    void ld::WebDriverProxy::Start(int portIn, int portOut,const std::string& bindAddr) {
        crow::SimpleApp app;

        CROW_ROUTE(app, "/")
                .methods("GET"_method, "POST"_method, "DELETE"_method)
                        ([](const crow::request& req) {
                            crow::response res;
                            res.code = 201;
                            res.write("macos driver is started");
                            return res;
                        });

        CROW_ROUTE(app, "/<path>")
                .methods("GET"_method, "POST"_method, "DELETE"_method)
                        ([&](const crow::request& req, const std::string& path) {
                            crow::response response;
                            httplib::Client cli("localhost", portOut);
                            const std::string urlParam = "/" + path;
                            std::string upgrade = req.get_header_value("Upgrade");
                            std::string hostOrigin = req.get_header_value("Host");

                            if(!upgrade.empty() && upgrade == "h2c"){
                                response.code = 505;
                                response.write("HTTP Version Not Supported");
                                return response;
                            }

                            httplib::Headers headers;
                            for(const auto& header : req.headers){
                                if(header.first == "Host") continue;
                                headers.emplace(header.first, header.second);
                            }
                            std::string localAddr = "127.0.0.1:"+std::to_string(portIn);
                            headers.emplace("Host",localAddr);

//userAgent selenium/4.17.0 (java mac)
                            if (req.method == "POST"_method) {
                                auto body = req.body;
//                                LD_LOG_DEBUG << "wdp POST : " << methode << std::endl;
                                LD_LOG_DEBUG << "body : " << body << std::endl;
                                std::string contentType = req.get_header_value("Content-Type");
                                LD_LOG_DEBUG << "contentType : " << contentType << std::endl;

                                auto res = cli.Post( urlParam, headers, body,contentType);
                                if (res == nullptr) {
                                    CrowJsonResponse::getJsonNullResponse(response);
                                    return response;
                                }
                                LD_LOG_DEBUG << "res->status : " << res->status << std::endl;
                                LD_LOG_DEBUG << "res->body : " << res->body << std::endl;

                                response.code = res->status;
                                response.write(res->body);

                                for(const auto& header : req.headers){
                                    if(header.first == "Host") continue;
                                    headers.emplace(header.first, header.second);
                                }
                                std::string localAddr = "127.0.0.1:"+std::to_string(portIn);
                                headers.emplace("Host",localAddr);
                                return response;

                            } else if (req.method == "GET"_method) {
                                LD_LOG_DEBUG << "wdp GET : " << std::endl;
                                LD_LOG_DEBUG << "urlParam : " << urlParam << std::endl;
                                auto res = cli.Get( urlParam , headers);
                                if (res == nullptr) {
                                    CrowJsonResponse::getJsonNullResponse(response);
                                    return response;
                                }
                                response.code = res->status;
                                response.write(res->body);
                                LD_LOG_DEBUG << "res->status : " << res->status << std::endl;
                                LD_LOG_DEBUG << "res->body : " << res->body << std::endl;
                                return response;

                            } else if (req.method == "DELETE"_method) {
                                auto res = cli.Delete( urlParam, headers);
                                if (res == nullptr) {
                                    CrowJsonResponse::getJsonNullResponse(response);
                                    return response;
                                }
                                response.code = res->status;
                                response.write(res->body);
                                return response;
                            }

                            CrowJsonResponse::getJsonNullResponse(response);
                            return response;
                        });

        app.port(portIn).bindaddr(bindAddr).multithreaded().run();
    }
}
//nghttpx --frontend='0.0.0.0,9706;no-tls' -b127.0.0.1,9705

//~/git/proxyhttp2/external/nghttp2/src/nghttpx  --log-l--frontend='0.0.0.0,9706;no-tls' -b127.0.0.1,9721  --host-rewrite --log-level=INFO --single-process