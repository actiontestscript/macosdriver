#include "../../include/utils/network.hpp"

namespace ld{
    int Network::getAvailablePort(const int &portStart,const int &PortEnd, const std::string &ip) {
        int i = portStart;
        int res = 0;
        while (res == 0 && i <= PortEnd) {
            struct sockaddr_in server{};
            memset(&server, 0, sizeof(server));
            int sock;
            //create socket
            sock = socket(AF_INET, SOCK_STREAM, 0);

            //configure settings in address struct
            server.sin_family = AF_INET;
            server.sin_port = htons(i);
            server.sin_addr.s_addr = inet_addr(ip.c_str()); //inet_addr converts string to long format ipV4 address

            //test if port is open
            res = connect(sock, (sockaddr * ) & server, sizeof(server));

            #if defined(__linux__) || defined(__APPLE__)
                close(sock);
            #elif defined(_WIN32)
                closesocket(sock);
            #endif
            bool debug = false;
            if(res == -1 ) {
                int err = errno;
                if(debug){
                    switch (err) {
                        case EADDRINUSE: LD_LOG_DEBUG << "EADDRINUSE  socket already in use" << std::endl; break;
                        case EAFNOSUPPORT: LD_LOG_DEBUG << "EAFNOSUPPORT  address family not supported" << std::endl; break;
                        case EALREADY: LD_LOG_DEBUG << "EALREADY  socket already connected" << std::endl; break;
                        case EBADF: LD_LOG_DEBUG << "EBADF  invalid socket" << std::endl; break;
                        case ECONNREFUSED: LD_LOG_DEBUG << "ECONNREFUSED  connection refused" << std::endl; break;
                        case EFAULT: LD_LOG_DEBUG << "EFAULT  invalid address" << std::endl; break;
                        case EINPROGRESS: LD_LOG_DEBUG << "EINPROGRESS  socket is non-blocking and the connection cannot be completed immediately" << std::endl; break;
                        case EINTR: LD_LOG_DEBUG << "EINTR  system call was interrupted" << std::endl; break;
                        case EINVAL: LD_LOG_DEBUG << "EINVAL  invalid argument passed" << std::endl; break;
                        case EISCONN: LD_LOG_DEBUG << "EISCONN  socket is already connected" << std::endl; break;
                        case ENETUNREACH: LD_LOG_DEBUG << "ENETUNREACH  network is unreachable" << std::endl; break;
                        case ENOTSOCK: LD_LOG_DEBUG << "ENOTSOCK  descriptor is a file, not a socket" << std::endl; break;
                        case ETIMEDOUT: LD_LOG_DEBUG << "ETIMEDOUT  timeout while attempting connection" << std::endl; break;
                        case EACCES: LD_LOG_DEBUG << "EACCES  permission to create a socket of the specified type and/or protocol is denied" << std::endl; break;
                        case EPERM: LD_LOG_DEBUG << "EPERM  the user tried to connect to a broadcast address without having the socket broadcast flag enabled or the connection request failed because of a local firewall rule" << std::endl; break;
                        case EADDRNOTAVAIL: LD_LOG_DEBUG << "EADDRNOTAVAIL  address not available" << std::endl; break;
                        case EAGAIN: LD_LOG_DEBUG << "EAGAIN  no more free local ports or insufficient entries in the routing cache" << std::endl; break;
                        case ECONNRESET: LD_LOG_DEBUG << "ECONNRESET  connection reset by peer" << std::endl; break;
                        case EHOSTUNREACH: LD_LOG_DEBUG << "EHOSTUNREACH  no route to host" << std::endl; break;
                        case ENOBUFS: LD_LOG_DEBUG << "ENOBUFS  not enough free memory" << std::endl; break;
                        case ENOMEM: LD_LOG_DEBUG << "ENOMEM  not enough free memory" << std::endl; break;
                        case EPROTONOSUPPORT: LD_LOG_DEBUG << "EPROTONOSUPPORT  protocol not supported" << std::endl; break;
                        case EPROTOTYPE: LD_LOG_DEBUG << "EPROTOTYPE  protocol wrong type for socket" << std::endl; break;
                        case EOPNOTSUPP: LD_LOG_DEBUG << "EOPNOTSUPP  operation not supported on transport endpoint" << std::endl; break;
                        case EDESTADDRREQ: LD_LOG_DEBUG << "EDESTADDRREQ  destination address required" << std::endl; break;
                        case EMSGSIZE: LD_LOG_DEBUG << "EMSGSIZE  message too long" << std::endl; break;
                        case ENETDOWN: LD_LOG_DEBUG << "ENETDOWN  network is down" << std::endl; break;
                    }
                }
                return i;
            }

            i++;
        }
        return -1;
    }

    std::vector<std::string> Network::getListIpsLocal(const bool& withSubnetMask) {
        std::vector<std::string> ips;
        struct ifaddrs *interfaces = nullptr;
        struct ifaddrs *addr = nullptr;

        if (getifaddrs(&interfaces) == -1) {
            LD_LOG_ERROR << "network->getListIpsLocal->getifaddrs error" << std::endl;
            return ips;
        }

        for (addr = interfaces; addr != nullptr; addr = addr->ifa_next) {
            if (addr->ifa_addr && addr->ifa_addr->sa_family == AF_INET) { // Filter IPv4
                char ip[INET_ADDRSTRLEN];
                void* tmpAddrPtr = &((struct sockaddr_in *)addr->ifa_addr)->sin_addr;
                inet_ntop(AF_INET, tmpAddrPtr, ip, INET_ADDRSTRLEN);

                if (withSubnetMask) {
                    auto *netmask = (struct sockaddr_in *)addr->ifa_netmask;
                    char netmaskIp[INET_ADDRSTRLEN];
                    inet_ntop(AF_INET, &netmask->sin_addr, netmaskIp, INET_ADDRSTRLEN);

                    std::string ipWithMask = std::string(ip) + "/" + std::string(netmaskIp);
                    ips.emplace_back(ipWithMask);
                } else {
                    ips.emplace_back(ip);
                }
            }
        }
        freeifaddrs(interfaces);
        return ips;
    }

    bool Network::isValidIpv4(const std::string &ipAddress) {
        std::regex pattern("^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
        return std::regex_match(ipAddress, pattern);
    }

    bool Network::getIpMask(const std::string &interface, std::string &mask) {
        std::string result{};
        std::regex pattern("[\\n]");
        std::string cmd = "ip addr show "+ interface +" | awk '/inet / {print $2}' ";
        if(Utils::exec(cmd.c_str(), result)){
            result = std::regex_replace(result, pattern, "");
            size_t p = result.find('/');
            if(p != std::string::npos){
                std::string s_cidr = result.substr(p+1);
                int cidr = std::stoi(s_cidr);
                uint32_t ui32_mask = (1 << (32 - cidr)) - 1;
                ui32_mask = ~ui32_mask;
                auto *bytes = (uint8_t *) &ui32_mask;
                mask = std::to_string(bytes[3]) + "." + std::to_string(bytes[2]) + "." + std::to_string(bytes[1]) + "." + std::to_string(bytes[0]);
                return true;
            }
        }
        return false;
    }

    std::string Network::getIpNetwork(const std::string &ip, const std::string &mask) {
        struct in_addr ipAddr{}, maskAddr{}, ipNetworkAddr{};
        inet_aton(ip.c_str(), &ipAddr);
        inet_aton(mask.c_str(), &maskAddr);
        uint32_t ip_net = ipAddr.s_addr & maskAddr.s_addr;
        ipNetworkAddr.s_addr = ip_net;
        //uint32_t ip_net = ntohl(ipAddr.s_addr) && ntohl(maskAddr.s_addr);
        //ipNetworkAddr.s_addr = htonl(ip_net);
        return {std::string(inet_ntoa(ipNetworkAddr))};
    }

    std::string Network::getIpHost(const std::string &ip, const std::string &mask) {
        struct in_addr ipAddr{}, maskAddr{}, ipHostAddr{};
        inet_aton(ip.c_str(), &ipAddr);
        inet_aton(mask.c_str(), &maskAddr);
        //       uint32_t invMask = ~ntohl(maskAddr.s_addr);
        //        uint32_t ip_net = ntohl(ipAddr.s_addr) && invMask;
        uint32_t invMask = ~(maskAddr.s_addr);
        //uint32_t ip_net = ntohl(ipAddr.s_addr) & invMask;
        uint32_t ip_net = (ipAddr.s_addr) & invMask;

        //ipHostAddr.s_addr = htonl(ip_net);
        ipHostAddr.s_addr = ip_net;
        return {std::string(inet_ntoa(ipHostAddr))};
    }

    std::string Network::getDefaultGatewayIp() {
        std::string result{};
        std::regex pattern("[\\n]");
        std::string cmd = "ip route show | awk '/default/ {print $3}'";
//        std::string cmd = "route -n |awk '/^0.0.0.0/ {print $2}'";
        if(Utils::exec(cmd.c_str(), result)){
            result = std::regex_replace(result, pattern, "");
        }
        return result;
    }


}