#include <cmath>
#include "../../../include/W3C/capabilities/cap_os.hpp"
#include "../../../include/W3C/capabilities/capabilities.hpp"
#include "../../../include/W3C/session/session.hpp"

namespace ld {
    Capabilities::Type Os::type = Capabilities::Type::Os;
    Os::Os() {
        utsname uts{};
        if (uname(&uts) >= 0) {
            m_data.machineName = uts.nodename;
            m_data.version = uts.version;
            m_data.processorArch = uts.machine;
            m_data.platformName = uts.sysname;
            m_data.platformVersion = uts.release;
        }
        m_data.driverVersion = std::to_string(MACOSDRIVER_VERSION_MAJOR) + "." + std::to_string(MACOSDRIVER_VERSION_MINOR) + "." + std::to_string(MACOSDRIVER_VERSION_PATCH) ;
        m_data.script = 30000;
        m_data.pageLoad = 300000;
        m_data.implicit = 0.0f;
        m_data.currentUrl = -1;
        m_data.buildNumber = m_data.platformVersion;
        setProcessorInfo();
        setDiskInfo();
        setScreenInfo();
    }

    bool Os::executeCapabilities(const crow::request& req, crow::response& res){
        CrowJsonResponse jRes;
        CrowJsonResponse::MsgError msgError;
        if(!_setOsBase(msgError)){
            jRes.setMsgErrorCode(msgError.error,"",msgError.stacktrace);
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

        return true;
    }

    void Os::printCapabilities(json &j){
        json capabilities;
        capabilities["browserName"] = m_data.platformName;
        capabilities["platformName"] = m_data.platformName;
        capabilities["platformVersion"] = m_data.version;
        capabilities["architecture"] = m_data.processorArch;
        capabilities["machine"] = m_data.machineName;
        json timeouts;
        timeouts["implicit"] = m_data.implicit;
        timeouts["pageLoad"] = m_data.pageLoad;
        if(m_data.script == 0) timeouts["script"] = nullptr;
        else timeouts["script"] = m_data.script;
        capabilities["timeouts"] = timeouts;
        j["capabilities"] = capabilities;
   }

    bool Os::getTimeouts(const std::string& sessionId,json& j){
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        json timeouts;
        timeouts["implicit"] = caps->m_data.implicit;
        timeouts["pageLoad"] = caps->m_data.pageLoad;
        if(caps->m_data.script == 0) timeouts["script"] = nullptr;
        else timeouts["script"] = caps->m_data.script;
        j["value"] = timeouts;
        return true;
    }
    bool Os::setTimeouts(const std::string& sessionId,const crow::request& req) {
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        const std::string& body = req.body;
        const json& src=json::parse(body);
        if(src.find("implicit") != src.end() && src["implicit"].is_number()){
            caps->m_data.implicit = src["implicit"];
        }
        if(src.find("pageLoad") != src.end() && src["pageLoad"].is_number()){
            caps->m_data.pageLoad = src["pageLoad"];
        }
        if(src.find("script") != src.end() && (src["script"].is_number() || src["script"].is_null() )){
            if (src["script"].is_null()) { caps->m_data.script = 0; }
            else { caps->m_data.script = src["script"]; }
        }
        return true;
    }


    bool Os::_setOsBase(CrowJsonResponse::MsgError& msgError) {
        msgError.clear();
        utsname uts{};
        if (uname(&uts) >= 0) {
            m_data.machineName = uts.nodename;
            m_data.version = uts.version;
            m_data.processorArch = uts.machine;
            m_data.platformName = uts.sysname;
            m_data.platformVersion = uts.release;
            return true;
        }
        else{
            msgError.error="unknown_error";
            msgError.stacktrace="Os::_setOsBase Error in uname";
            return false;
        }
    }

    bool Os::navigateTo(const std::string &sessionId, const crow::request &req) {
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        std::string body = req.body;
        json src=json::parse(body);
        if(src.find("url") != src.end() && src["url"].is_string()){
            std::string url = src["url"];
            std::regex urlRegex("^(http|https|ftp)://.*");
            if(std::regex_match(url, urlRegex)){
                caps->m_data.urls.push_back(url);
                caps->m_data.currentUrl = static_cast<int>(caps->m_data.urls.size() -1 ) ;
                return true;
            }
        }

        return false;
    }

    bool Os::getCurrentUrl(const std::string &sessionId, json& j) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        json urls;
        if(caps->m_data.currentUrl == -1 ||caps->m_data.urls[caps->m_data.currentUrl].empty() ) urls["value"] = nullptr;
        else urls["value"] = caps->m_data.urls[caps->m_data.currentUrl];
        j = urls;
        return true;
    }

    bool Os::navigateBack(const std::string &sessionId) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        if(caps->m_data.currentUrl > 0){
            caps->m_data.currentUrl--;
            return true;
        }
        return true;
    }

    bool Os::navigateForward(const std::string &sessionId) {
        CrowJsonResponse jRes;
        Session* sessions = Session::getInstance();
        SessionData::SessionInfo* sessionInfo = sessions->getSessionInfo(sessionId);
        if(sessionInfo == nullptr){ return false; }
        auto* caps = dynamic_cast<Os*>(sessionInfo->capabilities.get());
        if(caps == nullptr){ return false; }
        if(caps->m_data.currentUrl < static_cast<int>(caps->m_data.urls.size() -1 ) ){
            caps->m_data.currentUrl++;
            return true;
        }
        return true;
    }

    void Os::setProcessorInfo() {
        m_data.processorCoreNumber = std::to_string(getSysCtl<int>("hw.ncpu"));
        m_data.processorSocket = getSysCtl<std::string>("machdep.cpu.brand_string");

        //model name
        m_data.processorName  = "Family " + std::to_string( getSysCtl<int>("machdep.cpu.family"));
        m_data.processorName += " Model " + std::to_string( getSysCtl<int>("machdep.cpu.model"));
        m_data.processorName += " Stepping " + std::to_string( getSysCtl<int>("machdep.cpu.stepping"));

        //cpu Mhz
        long cpuSpeed = getSysCtl<long>("hw.cpufrequency");
        m_data.processorSpeed = std::to_string((cpuSpeed / 1000000));

    }

    void Os::setDiskInfo() {
        if(!Session::getInstance()->getIsDockerContainer()) {
            //disk Space
            std::string result{};
            std::string cmdFullSize = "df -m " + m_data.systemDriveMountPoint + " | awk 'NR==2 {print $2}'";
            std::string cmdFreeSize = "df -m " + m_data.systemDriveMountPoint + " | awk 'NR==2 {print $4}'";
//            LD_LOG_DEBUG << "cmdFullSize : " << cmdFullSize << std::endl;
//            LD_LOG_DEBUG << "cmdFreeSize : " << cmdFreeSize << std::endl;
            std::regex pattern("[\\t\\n]");
            if (Utils::exec(cmdFullSize.c_str(), result)) {
                result = std::regex_replace(result, pattern, "");
                m_data.systemDriveTotalSize = result + " Mo";
            }

            result = "";
            if (Utils::exec(cmdFreeSize.c_str(), result)) {
                result = std::regex_replace(result, pattern, "");
                m_data.systemDriveFreeSpace = result + " Mo";
            }
        }
        else {
            m_data.systemDriveMountPoint = "/docker containers/";
            m_data.systemDriveFreeSpace = "1000 Mo";
            m_data.systemDriveTotalSize = "2000 Mo";
            LD_LOG_DEBUG << "systemDriveMountPoint : " << m_data.systemDriveMountPoint << std::endl;
            LD_LOG_DEBUG << "systemDriveFreeSpace : " << m_data.systemDriveFreeSpace << std::endl;
            LD_LOG_DEBUG << "systemDriveTotalSize : " << m_data.systemDriveTotalSize << std::endl;


        }

    }

    void Os::setScreenInfo() {
        std::string cmd = "system_profiler SPDisplaysDataType | grep Resolution";
        std::string result = "";
        std::regex pattern("Resolution: (\\d+) x (\\d+)");
        std::smatch matches;
        if(Utils::exec(cmd.c_str(), result)){
            if (std::regex_search(result, matches, pattern) && matches.size() >= 3) {
                m_data.screenWidth = matches[1].str();
                m_data.screenHeight = matches[2].str();
            }
        }else
        {
            m_data.screenWidth = 1;
            m_data.screenHeight = 1;
        }
        m_data.graphicsInterface = OsData::GraphicsInterface::Aqua;
        m_data.headlessMode = true;
        m_data.withGraphicsInterface = true;
        m_data.interactive = true;
    }
}// namespace ld
