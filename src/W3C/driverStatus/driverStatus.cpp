#include "../../include/W3C/driverStatus/driverStatus.hpp"

namespace ld{
    void DriverStatus::printCapabilities(crow::response &res) {
        m_session = Session::getInstance();

        getJsonHeaderResponse(res);
        json os;
        os["arch"] = m_osData.getData().processorName;
        os["version"] = m_osData.getData().version;
//        os["name"] = m_osData.getData().platformName;
        os["name"] = "macos";
        os["machineName"] = m_osData.getData().machineName;
        json build;
        build["version"] = m_osData.getData().driverVersion;

        json wsAgent;
        wsAgent["usedBy"] = Session::getWsAgentUsedBy();
        wsAgent["port"] = Session::getWsAgentPort();

        json value;
        value["message"] = "ATS MacosDriver ready for new session";
        value["ready"] = !Session::getWsAgentIsConnected();
        value["remoteAgent"] = wsAgent;
        value["os"] = os;
        value["build"] = build;
        value["graphics"]= m_session->getEnvironment().getGraphicalEnvironment();

        //session webdriver
        json webdriver;
        if(m_session->m_sessions.empty()) webdriver=nullptr;
        else{
            json webDriverSession = json::array();
            for(auto & session : m_session->m_sessions){
                if(!session.isEnded && session.isWebDriver){
                    json jsession;

                    jsession["browserName"] = session.browserName;
                    jsession["sessionId"] = session.id;
                    jsession["webdriverName"] = session.webdriverProcessName;
                    jsession["webdriverUrl"] = "http://"+ m_remoteAddress +":"+session.webdriverPort;
                    //jsession["webdriverUrl"] = session.webdriverUrl;
                    jsession["webdriverPort"] = session.webdriverPort;
                    jsession["webdriverPath"] = session.webdriverPath;
                    webDriverSession.push_back(jsession);
                    //webdriver[session.browserName] = jsession;
                }
            }
            webdriver = webDriverSession;
        }
        value["webdriver"] = webdriver;
        json root;
        root["value"] = value;
        res.write(root.dump());
        res.code = 200;
    }

    void DriverStatus::setRemoteAddress(const crow::request &req) {
        for(auto &header : req.headers){
            if(header.first == "Host"){
                std::string host = header.second;
                size_t pos = host.find(':');
                if(pos != std::string::npos)
                    m_remoteAddress = host.substr(0, pos);

            }
        }
    }

}