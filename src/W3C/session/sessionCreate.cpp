#include "../../include/W3C/session/sessionCreate.hpp"
namespace ld{


    SessionCreate::SessionCreate() {
        m_session = Session::getInstance();
    }

    void SessionCreate::getBrowserName(crow::request& req,  std::string &browserName){
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("capabilities") != jsonBody.end()){
            if(jsonBody["capabilities"].find("firstMatch") != jsonBody["capabilities"].end()){
                json firstMatch = jsonBody["capabilities"]["firstMatch"];
                if (firstMatch.is_array()) {
                    for (auto& element : firstMatch) {
                        if(element.find("browserName") != element.end()){
                            browserName = element["browserName"];
                            return;
                        }
                    }
                }
                else if(firstMatch.find("browserName") != firstMatch.end()){
                    browserName = firstMatch["browserName"];
                    return;
                }
            }
            if(jsonBody["capabilities"].find("alwaysMatch") != jsonBody["capabilities"].end()){
                json alwaysMatch = jsonBody["capabilities"]["alwaysMatch"];
                if(alwaysMatch.is_array()){
                    for (auto& element : alwaysMatch) {
                        if(element.find("browserName") != element.end()){
                            browserName = element["browserName"];
                            return;
                        }
                    }
                }
                else if(alwaysMatch.find("browserName") != alwaysMatch.end()){
                    browserName = alwaysMatch["browserName"];
                    return;
                }
            }

        }
        else browserName = "";
    }

    void SessionCreate::_getCapName(std::string& name){
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
        if(name =="chrome" || name =="firefox" || name =="safari" || name =="edge" || name =="ie" || name =="opera"){
            name = "BrowserWeb";
        }
        else if(name =="android" || name =="ios" || name =="windows" || name =="linux" || name =="mac"){
            name = "Os";
        }
        else{
            name = "";
        }
    }

    void SessionCreate::create(crow::request& req,  crow::response& res){
        getJsonHeaderResponse(res);
        MsgError msgError;
        if(m_session->getIsMaxSessionReached()){
            _setMsgErrorCode("session_not_created", "Session limit reached", "The maximum number of sessions is reached");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return;
        }


        //Common Verifications for all Json
        const std::string& body = req.body;
        if(!_isValidJson(body)){
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return;
        }




        //check BrowserName name for capabilities
        std::string name;
        getBrowserName(req, name);
        _getCapName(name);
        if(name.empty()){
            _setMsgErrorCode("invalid_argument", "", "BrowserName unknown");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return;
        }

        //factory load in function of name de json BrowserName found
        auto caps = CapabilitiesFactory().create<Capabilities>(name);
        if(!caps->executeCapabilities(req, res)){ return;} ;


        json jsonMsgReturn;     //json message to return
        SessionData::SessionInfo sessionInfo;
        sessionInfo.id = Guid::generate(static_cast<int>(Guid::GuidVersion::V4));
        sessionInfo.startTime = std::time(nullptr);
        sessionInfo.nameCapabilities = name;

        //sessionInfo.capabilities.reset(caps.release());
        sessionInfo.capabilities = std::move(caps);
        sessionInfo.capabilities->printCapabilities(jsonMsgReturn);
        jsonMsgReturn["sessionId"]= sessionInfo.id;

        m_session->m_sessions.push_back(std::move(sessionInfo));


        std::string msgReturn = jsonMsgReturn.dump();
        res.code = 200;
        res.write(msgReturn);

    }

    bool SessionCreate::isOsDriverLaunch(const crow::request &req, std::string& webDriverName ) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end()) {
            const json& jsonValue = jsonBody["value"];
            if(jsonValue.find("capabilities") != jsonValue.end()) {
                // capabilities -> os
                if (jsonValue["capabilities"].find("os") != jsonValue["capabilities"].end()) {
                }
            }

            // remoteDirver
            if(jsonValue.find("remoteDriver") != jsonValue.end()){
                if(jsonValue["remoteDriver"].empty() || !jsonValue["remoteDriver"].is_string()) return false;
                webDriverName = jsonValue["remoteDriver"];
                m_browserName = webDriverName;
            }
            else return false;

            // applicationPath
            //check .atsProperties Application path
            m_applicationPath = Session::getBrowserAtsPropertiesPath(m_browserName) ;
            if(!m_applicationPath.empty()){
                return true;
            }

            //check json applicationPath
            if(jsonValue.find("applicationPath") != jsonValue.end()){
                if(jsonValue["applicationPath"].empty() || jsonValue["applicationPath"].is_null()){
                    m_applicationPath = "";
                    return true;
                }
                else if(!jsonValue["applicationPath"].is_string()) return false;
                else {
                    m_applicationPath = jsonValue["applicationPath"];
                    Session::addBrowserAtsPropertiesPath(m_browserName, m_applicationPath);
                    return true;
                }
            }
            else {
                m_applicationPath = "";
                return true;
            }
        }
        return false;
    }

    void SessionCreate::createOsDriver(const crow::request& req,  crow::response& res, const std::string& webDriverName ) {
        getJsonHeaderResponse(res);
        MsgError msgError;
        if (m_session->getIsMaxSessionReached()) {
            _setMsgErrorCode("session_not_created", "Session limit reached",
                             "The maximum number of sessions is reached");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return;
        }


        auto caps = CapabilitiesFactory().create<Capabilities>("Os");
        auto driver = DriversFactory().create<Driver>(webDriverName);
        if (driver == nullptr) {
            _setMsgErrorCode("ats_error_remoteDriver_incorrect", "", "");
            res.code = m_errorCode;
            res.write(_jsonMsgError().dump());
            return;
        }
        //url retour:
        for (auto &header: req.headers) {
            if (header.first == "Host") {
                driver->setDriverRemoteHeaderUrl(header.second);
            }
        }

        driver->setRemoteIp(req.remote_ip_address);
        driver->isResponseExecuted = true;
        if (!m_applicationPath.empty()) {
            driver->setJsonOptionsApplicationPath(true);
            driver->setAlternativeApplicationPath(m_applicationPath);
        }

        if (!driver->execute(res)) return;
        //check if driver is already launched in session
        if (!m_session->getProcessIsFirstStart(driver->getDriverName())){
            if (driver->isInProgress()) {
                //m_session->m_sessions[driver->getSessionId()].isWebDriver = true;
                if (!m_session->m_sessions.empty()) {
                    for (auto &session: m_session->m_sessions) {
                        if (session.nameCapabilities == "webDriver: " + webDriverName && !session.isEnded) {
                            driver->setSessionId(session.id);
                        }
                    }
                }
                driver->printCapabilities(res);
                return;
            }
        }
        else{
            m_session->setProcessIsFirstStart(driver->getDriverName(), false);
        }

        SessionData::SessionInfo sessionInfo;
        sessionInfo.id = Guid::generate(static_cast<int>(Guid::GuidVersion::V4));
        driver->setSessionId(sessionInfo.id);
        sessionInfo.isWebDriver = true;
        sessionInfo.webdriverPort = std::to_string(driver->getDriverPort());
        sessionInfo.webdriverPath = driver->getDriverPath();
        sessionInfo.webdriverProcessName = driver->getDriverProcessName();
        sessionInfo.webdriverName = driver->getDriverName();
        sessionInfo.webdriverUrl = driver->getDriverUrl();
        sessionInfo.browserName = driver->getBrowserName();
        sessionInfo.webDriverOffsetX = driver->getWebDriverOffsetX();
        sessionInfo.webDriverOffsetY = driver->getWebDriverOffsetY();
        sessionInfo.webDriverHeadlessOffsetX = driver->getWebDriverHeadlessOffsetX();
        sessionInfo.webDriverHeadlessOffsetY = driver->getWebDriverHeadlessOffsetY();
        sessionInfo.startTime = std::time(nullptr);
        sessionInfo.nameCapabilities = "webDriver: " + webDriverName;
        sessionInfo.applicationName = driver->getBrowserName();
        sessionInfo.applicationPath = driver->getBrowserPath();
        sessionInfo.applicationVersion = driver->getBrowserVersion(true);

        //sessionInfo.capabilities.reset(caps.release());
        sessionInfo.capabilities = std::move(caps);
        m_session->m_sessions.push_back(std::move(sessionInfo));
        driver->printCapabilities(res);

    }



} //end namespace ld

