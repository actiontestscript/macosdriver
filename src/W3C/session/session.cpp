#include "../../include/W3C/session/session.hpp"

namespace ld{
    std::atomic<ld::Session*> ld::Session::m_instance{nullptr};
    std::mutex ld::Session::m_mutex{};
    //std::map<std::string, ld::Session::ProcessInfo> m_processes{};

    void Session::addSession(SessionData::SessionInfo&& sessionInfo) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_sessions.push_back(std::move(sessionInfo));
    }

    bool Session::deleteSession(const std::string &id) {
        std::lock_guard<std::mutex> lock(m_mutex);
        for(auto it = m_sessions.begin(); it != m_sessions.end(); ++it){
            if(it->id == id){
                if(!it->isInfinite)
                    it->isEnded = true;
                return true;
            }
        }
        return false;
    }

    SessionData::SessionInfo* Session::getSessionInfo(const std::string& uuid){
        std::lock_guard<std::mutex> lock(m_mutex);
        for(auto& session : m_sessions){
            if(session.id == uuid){
                return &session;
            }
        }
        return nullptr;
    }

    bool Session::getIsMaxSessionReached() const{
        int count = 0;
        for(auto& session : m_sessions){
            if(!session.isEnded){count++;}
        }
        return count >= m_maxSession;
    }

    bool Session::isActiveSession(const std::string &uuid) {
        return !isEndedSession(uuid);
    }

    bool Session::isEndedSession(const std::string &uuid) {
        std::lock_guard<std::mutex> lock(m_mutex);
        for(auto& session : m_sessions){
            if(session.id == uuid){
                return session.isEnded;
            }
        }
        return false;
    }

    const std::vector<std::string> &Session::getBindIpAddresses() const {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_bindIpAddresses;
    }

    void Session::setBindIpAddresses(const std::vector<std::string> &bindIpAddresses) {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_bindIpAddresses = bindIpAddresses;
    }

    std::string Session::getProcessPort(const std::string& driverName) {
        std::string port{};
        if (!m_sessions.empty()) {
            auto it = m_processes.find(driverName);
            if (it != m_processes.end()) {
                port = m_processes[driverName].port;
            }
        }
        return port;
    }

    Session::RecorderInfo *Session::getRecorder(const std::string &uuidRecorder) {
        std::lock_guard<std::mutex> lock(m_mutex);
        for(auto &recorder : m_recorders){
            if(recorder.id == uuidRecorder){
                return &recorder;
            }
        }
        return nullptr;
    }

    void Session::setRecorder(const std::string &uuidRecorder, const Session::RecorderInfo &recorderInfo) {
        std::lock_guard<std::mutex> lock(m_mutex);
        bool isFound = false;
        for(auto &recorder : m_recorders){
            if(recorder.id == uuidRecorder){
                recorder = recorderInfo;
                isFound = true;
                break;
            }
        }
        if(!isFound) m_recorders.push_back(recorderInfo);
    }

    bool Session::deleteRecorder(const std::string &uuidRecorder) {
        std::lock_guard<std::mutex> lock(m_mutex);
        for(auto it = m_recorders.begin(); it != m_recorders.end(); ++it){
            if(it->id == uuidRecorder){
                m_recorders.erase(it);
                return true;
            }
        }
        return false;
    }
}
