#include "../../../include/W3C/session/sessionGet.hpp"
namespace ld{;
    void SessionGet::printCapabilities(crow::response &res) {
        getJsonHeaderResponse(res);
        json capabilities;
        capabilities["MachineName"] = m_osData.getData().machineName;
        capabilities["DriverVersion"] = m_osData.getData().driverVersion;
        capabilities["C++Version"] = std::to_string(__cplusplus);
        capabilities["ScreenWidth"] = m_osData.getData().screenWidth;
        capabilities["ScreenHeight"] = m_osData.getData().screenHeight;
        capabilities["VirtualWidth"] = m_osData.getData().virtualWidth;
        capabilities["VirtualHeight"] = m_osData.getData().virtualHeight;
        capabilities["Version"] = m_osData.getData().version;
        capabilities["DriveLetter"] = m_osData.getData().systemDriveMountPoint;
        capabilities["DiskTotalSize"] = m_osData.getData().systemDriveTotalSize;
        capabilities["DiskFreeSpace"] = m_osData.getData().systemDriveFreeSpace;
        capabilities["BuildNumber"] = m_osData.getData().buildNumber;
//        capabilities["Name"] = m_osData.getData().platformName;
        capabilities["Name"] = "macos";
        capabilities["CountryCode"] = m_osData.getData().countryCode;
        capabilities["CpuSocket"] = m_osData.getData().processorSocket;
        capabilities["CpuName"] = m_osData.getData().processorName;
        capabilities["CpuArchitecture"] = m_osData.getData().processorArch;
        capabilities["CpuMaxClockSpeed"] = m_osData.getData().processorSpeed;
        capabilities["CpuCores"] = m_osData.getData().processorCoreNumber;
        capabilities["Interactive"] = m_osData.getData().interactive;





        json value;
        value["sessionId"] = sessionId;
        value["capabilities"] = capabilities;
        json root;
        root["value"] = value;
        res.write(root.dump());
        res.code = 200;

    }




} // namespace ld
