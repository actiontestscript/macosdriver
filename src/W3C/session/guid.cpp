//
// Created by eric on 14/02/23.
//
#include "../../include/W3C/session/guid.hpp"
namespace ld{

    std::string Guid::_generateV4() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<uint8_t> dis(0, 255);
        uint8_t bytes[16];
        for (unsigned char & byte : bytes) {
            byte = dis(gen);
        }

        // Configure les bits de version 4 (0100)
        bytes[6] = (bytes[6] & 0x0F) | 0x40;
        bytes[8] = (bytes[8] & 0x3F) | 0x80;

        std::stringstream ss;
        ss << std::hex << std::setfill('0');
        for (int i = 0; i < 16; i++) {
            if (i == 3 || i == 5 || i == 7 || i == 9) {
                ss << '-';
            }
            ss << std::setw(2) << static_cast<int>(bytes[i]);
        }
        /*
        for (unsigned char byte : bytes) {
            ss << std::setw(2) << static_cast<int>(byte);
        }
         */
        return ss.str();
    }

    std::string Guid::_generateV1() {
        timespec ts{};
                    clock_gettime(CLOCK_REALTIME, &ts);
        uint64_t ns_since_1582 = ((uint64_t) ts.tv_sec * 1000000000ULL + (uint64_t)ts.tv_nsec) / 100ULL;
        uint32_t time_low = ns_since_1582 & 0xFFFFFFFF;                                 /*!< 32 bits of time_low */
        uint16_t time_mid = (ns_since_1582 >> 32) & 0xFFFF;                              /*!< 16 bits of time_mid */
        uint16_t time_hi_and_version = ((ns_since_1582 >> 48) & 0x0FFF) | 0x1000;       /*!< 12 bits of time_hi_and_version */

        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> dis(0, 0xFFFF);

        //generate node aleatory number
        uint8_t node[6];
        for(unsigned char & i : node){
            i = dis(gen);
        }

        //generate clock_seq aleatory number
        uint16_t clock_seq = dis(gen);
        uint8_t clock_seq_low = clock_seq & 0xFF;
        uint8_t clock_seq_hi_and_reserved = (clock_seq >> 8) & 0x80;

        //make uuid 16 bytes
        uint8_t uuid[16];
        std::memcpy(uuid, &time_low, sizeof(time_low));
        std::memcpy(uuid + sizeof(time_low), &time_mid, sizeof(time_mid));
        std::memcpy(uuid + sizeof(time_low) + sizeof(time_mid), &time_hi_and_version, sizeof(time_hi_and_version));
        std::memcpy(uuid + sizeof(time_low) + sizeof(time_mid) + sizeof(time_hi_and_version), &clock_seq_hi_and_reserved, sizeof(clock_seq_hi_and_reserved));
        std::memcpy(uuid + sizeof(time_low) + sizeof(time_mid) + sizeof(time_hi_and_version) + sizeof(clock_seq_hi_and_reserved), &clock_seq_low, sizeof(clock_seq_low));
        std::memcpy(uuid + sizeof(time_low) + sizeof(time_mid) + sizeof(time_hi_and_version) + sizeof(clock_seq_hi_and_reserved) + sizeof(clock_seq_low), &node, sizeof(node));

        std::stringstream ss;
        ss << std::hex << std::setfill('0');

        for (int i = 0; i < 16; i++) {
            ss << std::setw(2) << static_cast<int>(uuid[i]);

            if (i == 3 || i == 5 || i == 7 || i == 9) {ss << '-';}

        }
        return ss.str();
    }

    std::string Guid::generate(int version) {
        switch (version) {
            case static_cast<int>(Guid::GuidVersion::V1):
                return _generateV1();
            case static_cast<int>(Guid::GuidVersion::V4):
                return _generateV4();
            default:
                return _generateV4();
        }
    }

}

