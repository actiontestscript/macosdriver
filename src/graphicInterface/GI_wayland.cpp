#include "../../include/graphicInterface/GI_wayland.hpp"
namespace ld{


    void GI_Wayland::getScreenSize() {
        struct wl_display *display = wl_display_connect(nullptr);
        if (display == nullptr) {
            // Handle error
            return ;
        }

        // Get the registry
        struct wl_registry *registry = wl_display_get_registry(display);
        if (registry == nullptr) {
            // Handle error
            return ;
        }

        // Bind the registry
        wl_registry_add_listener(registry, &registry_listener, nullptr);
        wl_display_dispatch(display);
        wl_display_roundtrip(display);

        // Get the output
        auto *output = static_cast<wl_output *>(wl_registry_bind(registry, m_outputId, &wl_output_interface,1));
        if (output == nullptr) {
            // Handle error
            return ;
        }

        // Get the output information
        wl_output_add_listener(output, &output_listener, nullptr);
        wl_display_dispatch(display);
        wl_display_roundtrip(display);

        // Print the output resolution
        printf("Output resolution: %dx%d\n", m_screenWidth, m_screenHeight);

        // Cleanup
        wl_output_destroy(output);
        wl_registry_destroy(registry);
        wl_display_disconnect(display);

    }
}