#include "../../../include/ats/element/childs.hpp"

namespace ld {

    bool ElementChilds::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }


    bool ElementChilds::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementChildsData.m_id);
            CrowJsonResponse::jsonParse(value, "tag", m_elementChildsData.m_tag);
            CrowJsonResponse::jsonParse(value, "attributes", m_elementChildsData.m_attributes);
            return true;
        }
        return false;
    }

    bool ElementChilds::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementChildsData.m_id);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementChildsData.m_tag);
        while(!data.empty()){
            std::string attribute;
            if(data.find('\t') != std::string::npos)
                CrowJsonResponse::strParse(data, '\t', std::string(""), attribute);
            else {
                attribute = data;
                data = "";
//                data.clear();
            }
            m_elementChildsData.m_attributes.push_back(attribute);
        }
        return true;
    }
}
