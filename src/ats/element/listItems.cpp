#include "../../../include/ats/element/listItems.hpp"

namespace ld {

    bool ElementListItems::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementListItemsData.m_id);
            return true;
        }
        return false;
    }

    bool ElementListItems::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementListItemsData.m_id);
        return true;
    }
}
