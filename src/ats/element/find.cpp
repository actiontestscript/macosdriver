#include "../../../include/ats/element/find.hpp"

namespace ld {

    bool ElementFind::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_elementFindData.m_handle);
            CrowJsonResponse::jsonParse(value, "tag", m_elementFindData.m_tag);
            CrowJsonResponse::jsonParse(value, "attributes", m_elementFindData.m_attributes);
            return true;
        }
        return false;
    }

    bool ElementFind::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_elementFindData.m_handle);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementFindData.m_tag);
        while(!data.empty()){
            std::string attribute;
            CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), attribute);
            m_elementFindData.m_attributes.push_back(attribute);
        }
        return true;
    }
}
