#include "../../../include/ats/element/contextMenu.hpp"

namespace ld {

    bool ElementContextMenu::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementContextMenuData.m_pid);
            return true;
        }
        return false;
    }

    bool ElementContextMenu::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_elementContextMenuData.m_pid);

        return true;
    }
}
