#include "../../../include/ats/element/parents.hpp"

namespace ld {

    bool ElementParents::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_elementParentsData.m_id);

            return true;
        }
        return false;
    }

    bool ElementParents::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_elementParentsData.m_id);
        return true;
    }
}
