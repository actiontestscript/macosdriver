#include "../../../include/ats/recorder/testSummary.hpp"
namespace ld {
    TestSummary::TestSummary(const bool &passed, const int &actions, const std::string &suiteName,
                                     const std::string &testName, const std::string &summary) {
        if (passed) this->status = 1;
        else this->status = 0;
        if (suiteName.empty()) this->suiteName = "noSuiteDefined";
        else this->suiteName = suiteName;
        if (testName.empty()) this->testName = "noTestDefined";
        else this->testName = testName;
        this->actions = actions;
        this->summary = summary;
        this->summary.erase(std::remove(this->summary.begin(), this->summary.end(), '\n'), this->summary.end());
        this->summary.erase(std::remove(this->summary.begin(), this->summary.end(), '\r'), this->summary.end());
    }

    TestSummary::TestSummary(const bool &passed, const int &actions, const std::string &suiteName,
                                     const std::string &testName, const std::string &summary,
                                     const std::string &errorScript,
                                     const int &errorLine, const std::string &errorMessage):TestSummary(passed, actions, suiteName, testName, summary) {

        this->scriptError = TestError(errorScript, errorLine, errorMessage);


    }

    void TestSummary::setAmfTestSummary(AmfObject &obj) const {
        obj.addSealedProperty("error", scriptError.getAmfTestError());
        obj.addSealedProperty("suiteName", amf::AmfString(suiteName));
        obj.addSealedProperty("testName", amf::AmfString(testName));
        obj.addSealedProperty("status", amf::AmfInteger(status));
        obj.addSealedProperty("actions", amf::AmfInteger(actions));
        obj.addSealedProperty("summary", amf::AmfString(summary));

    }

    void TestSummary::initAmfTestSummary() {
        scriptError.initAmfTestError();
        setAmfTestSummary(amf_testSummary);
    }
}
