#include "../../../include/ats/recorder/amfInclude.hpp"

namespace ld{
    [[maybe_unused]] amf::AmfArray convertImagesToAmfArray(const std::vector<std::vector<uint8_t>>& images) {
        amf::AmfArray amfImages;
//        int i=0;
        for (const auto& image : images) {
            amf::AmfByteArray byteArray(image, true);
            amfImages.push_back(byteArray);
            //if (i == 0) amfImages.push_back(byteArray);
            //i++;
            //amfImages.emplace_back(amf::AmfByteArray(image));
        }
        return amfImages;
    }
    [[maybe_unused]] amf::AmfArray convertAtsElementToAmfArray(const std::vector<std::vector<uint8_t>>& images) {
        amf::AmfArray amfImages;
        for (const auto& byteArray : images) {
            amfImages.push_back(amf::AmfByteArray(byteArray));
        }
        return amfImages;
    }



    amf::v8 serialiseAmfActions(const amf::AmfObject& obj){
        Serializer serializer;
        serializer << obj;
        return serializer.data();
    }


}