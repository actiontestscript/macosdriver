#include "../../../include/ats/recorder/recorderStop.hpp"
namespace ld {
    bool RecorderStop::execute(const crow::request &req, crow::response &res) {
        //flush data
        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            if (recorder->currentActionName == "visualReport")
                flushVisualReport(recorder->id);
            else if (recorder->currentActionName == "visualAction")
                flushVisualAction(recorder->id);
            else if (recorder->currentActionName == "reportSummary")
                flushReportSummary(recorder->id);
        }

        getResponse(res);

        return true;
    }

    bool RecorderStop::_parseJson(const crow::request &req) { return true; }

    bool RecorderStop::_parsePostData(const crow::request &req) { return true; }
}// namespace ld

