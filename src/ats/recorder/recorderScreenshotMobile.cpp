#include "../../../include/ats/recorder/recorderScreenshotMobile.hpp"
namespace ld {
    bool RecorderScreenshotMobile::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        getResponse(res);
        return true;
    }

    bool RecorderScreenshotMobile::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            _jsonParseInt(value, "x", m_recorderScreenshotDataMobile.x);
            _jsonParseInt(value, "y", m_recorderScreenshotDataMobile.y);
            _jsonParseInt(value, "w", m_recorderScreenshotDataMobile.width);
            _jsonParseInt(value, "h", m_recorderScreenshotDataMobile.height);
            _jsonParseString(value, "url", m_recorderScreenshotDataMobile.url);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderScreenshotMobile::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseInt(data, m_postDataDelimiter, 0, m_recorderScreenshotDataMobile.x);
        _strParseInt(data, m_postDataDelimiter, 1, m_recorderScreenshotDataMobile.y);
        _strParseInt(data, m_postDataDelimiter, 2, m_recorderScreenshotDataMobile.width);
        _strParseInt(data, m_postDataDelimiter, 3, m_recorderScreenshotDataMobile.height);
        _strParseString(data,m_postDataDelimiter,"",m_recorderScreenshotDataMobile.url);
        setBrowserInfoOld(data);
        return true;

    }

}// namespace ld