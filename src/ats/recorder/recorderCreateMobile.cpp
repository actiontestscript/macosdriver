#include "../../../include/ats/recorder/recorderCreateMobile.hpp"
namespace ld {
    bool RecorderCreateMobile::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        getResponse(res);

        return true;
    }

    bool RecorderCreateMobile::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            json value = jsonBody["value"];
            _jsonParseString(value, "actionType", m_recorderCreateMobileData.actionType);
            _jsonParseInt(value, "line", m_recorderCreateMobileData.line);
            _jsonParseString(value, "script", m_recorderCreateMobileData.script);
            _jsonParseLong(value, "timeLine", m_recorderCreateMobileData.timeLine);
            _jsonParseString(value, "channelName", m_recorderCreateMobileData.channelName);

            if(value.find("channelDimension") != value.end() && value["channelDimension"].is_array()) {
                const json& channelDimension = value["channelDimension"];
                if(channelDimension.size() == 4){
                    for(int i = 0; i < 4; i++){
                        if(channelDimension[i].is_number()){
                            if(i == 0 ) m_recorderCreateMobileData.channelDimension.x = channelDimension[i];
                            else if(i == 1 ) m_recorderCreateMobileData.channelDimension.y = channelDimension[i];
                            else if(i == 2 ) m_recorderCreateMobileData.channelDimension.width = channelDimension[i];
                            else if(i == 3 ) m_recorderCreateMobileData.channelDimension.height = channelDimension[i];
                        }
                        else if(channelDimension[i].is_string()){
                            const std::string& channelDimensionValue = channelDimension[i];
                            if(ld::Utils::isDouble(channelDimensionValue)){
                                if(i == 0 ) m_recorderCreateMobileData.channelDimension.x = std::stod(channelDimensionValue);
                                else if(i == 1 ) m_recorderCreateMobileData.channelDimension.y = std::stod(channelDimensionValue);
                                else if(i == 2 ) m_recorderCreateMobileData.channelDimension.width = std::stod(channelDimensionValue);
                                else if(i == 3 ) m_recorderCreateMobileData.channelDimension.height = std::stod(channelDimensionValue);
                            }
                        }
                    }
                }
            }
            _jsonParseString(value, "url", m_recorderCreateMobileData.url);
            _jsonParseBool(value, "sync", m_recorderCreateMobileData.sync);
            _jsonParseBool(value, "stop", m_recorderCreateMobileData.stop);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderCreateMobile::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateMobileData.actionType);
        _strParseInt(data, m_postDataDelimiter, 0, m_recorderCreateMobileData.line);
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateMobileData.script);
        _strParseLong(data, m_postDataDelimiter, 0, m_recorderCreateMobileData.timeLine);
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateMobileData.channelName);
        _strParseDouble(data, m_postDataDelimiter, 0, m_recorderCreateMobileData.channelDimension.x);
        _strParseDouble(data, m_postDataDelimiter, 0, m_recorderCreateMobileData.channelDimension.y);
        _strParseDouble(data, m_postDataDelimiter, 1, m_recorderCreateMobileData.channelDimension.width);
        _strParseDouble(data, m_postDataDelimiter, 1, m_recorderCreateMobileData.channelDimension.height);
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateMobileData.url);
        _strParseBool(data, m_postDataDelimiter, false, m_recorderCreateMobileData.sync);
        _strParseBool(data, m_postDataDelimiter, false, m_recorderCreateMobileData.stop);
        setBrowserInfoOld(data);
        return true;
    }
}// namespace ld

