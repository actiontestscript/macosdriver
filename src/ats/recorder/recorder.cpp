#include "../../../include/ats/recorder/recorder.hpp"
namespace ld{
    bool Recorder::_jsonIsValid(const crow::request &req, crow::response &res){
        const std::string& body = req.body;
        if(!m_crowJsonResponse.isValidJson(body)){
            LD_LOG_INFO << m_crowJsonResponse.jsonMsgError().dump()<< std::endl;
            res.code = m_crowJsonResponse.m_errorCode;
            res.write(m_crowJsonResponse.jsonMsgError().dump());
            return false;
        }
        return true;
    }

    void Recorder::_jsonParseInt(const json &value, const std::string &key, int &data){
        if(value.find(key) != value.end() ) {
            if (value[key].is_number()) data = value[key];
            else if (value[key].is_string()) {
                const std::string& dataStr = value[key];
                if (ld::Utils::isInt(dataStr)) data = std::stoi(dataStr);
            }
        }
    }

    void Recorder::_jsonParseLong(const json &value, const std::string &key, long &data) {
        if(value.find(key) != value.end() ) {
            if (value[key].is_number()) data = value[key];
            else if (value[key].is_string()) {
                const std::string& dataStr = value[key];
                if (ld::Utils::isLong(dataStr)) data = std::stol(dataStr);
            }
        }
    }

    [[maybe_unused]] void Recorder::_jsonParseDouble(const json &value, const std::string &key, double &data){
        if(value.find(key) != value.end() ) {
            if (value[key].is_number()) data = value[key];
            else if (value[key].is_string()) {
                const std::string& dataStr = value[key];
                if (ld::Utils::isDouble(dataStr)) data = std::stod(dataStr);
            }
        }
    }

    void Recorder::_jsonParseString(const json &value, const std::string &key, std::string &data){
        if(value.find(key) != value.end() && value[key].is_string()) data = value[key];
    }

    void Recorder::_jsonParseBool(const json &value, const std::string &key, bool &data) {
        if (value.find(key) != value.end() && value[key].is_boolean()) data = value[key];
        else if (value.find(key) != value.end() && value[key].is_string()) {
            const std::string& keyValue = value[key];
            if (keyValue == "true") data = true;
            else if (keyValue == "false") data = false;
        }
    }

    void Recorder::flushFile(const std::string& filename, const amf::v8& data) {
        std::ofstream file;
        file.open(filename, std::ios::out | std::ios::app);
        if(file.is_open()){
            file.write((char *) data.data(), data.size());
        }
        file.close();
    }

    void Recorder::flushVisualReport(const std::string& uuid) {
        Session::RecorderInfo* recorder = m_session->getRecorder(uuid);
        recorder->currentReport.initAmfVisualReport();
        flushFile(recorder->amfFullPathFileName, serialiseAmfActions(recorder->currentReport.getAmfVisualReport()));
    }

    void Recorder::flushVisualAction(const std::string &uuid) {
        Session::RecorderInfo* recorder = m_session->getRecorder(uuid);
        recorder->currentAction.initAmfVisualAction();
        flushFile(recorder->amfFullPathFileName, serialiseAmfActions(recorder->currentAction.getAmfVisualAction()));

    }

    void Recorder::flushReportSummary(const std::string &uuid) {
        Session::RecorderInfo* recorder = m_session->getRecorder(uuid);
        recorder->currentSummary.initAmfTestSummary();
        flushFile(recorder->amfFullPathFileName, serialiseAmfActions(recorder->currentSummary.getAmfTestSummary()));

    }

    void Recorder::getResponse(crow::response &res) const{
        if(!m_recorderOld) {
            ld::CrowJsonResponse::getJsonNullResponse(res);
        }
        else{
            DesktopResponse desktopResponse;
            desktopResponse.initAmfDesktopResponse();
            amf::v8 resAmf = serialiseAmfActions(desktopResponse.getAmfDesktopResponse());
            res.set_header("Content-Type", "application/x-amf");
            std::string resAmfStr(reinterpret_cast<const char*>(resAmf.data()), resAmf.size());
            res.write(resAmfStr);
        }
    }

    void Recorder::_strParseInt(std::string &strResearch, const char& delimiter, const int& defaultValue, int &data ){
        size_t pos{};
        pos = strResearch.find(delimiter);
        std::string dataStr = strResearch.substr(0, pos);
            if(ld::Utils::isInt(dataStr)) data = std::stoi(dataStr);
            else data = defaultValue;
            strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseLong(std::string &strResearch, const char& delimiter, const long& defaultValue, long &data ){
        size_t pos{};
        pos = strResearch.find(delimiter);
        std::string dataStr = strResearch.substr(0, pos);
        if(ld::Utils::isLong(dataStr)) data = std::stol(dataStr);
        else data = defaultValue;
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseDouble(std::string &strResearch, const char& delimiter, const double& defaultValue, double &data ){
        size_t pos{};
        pos = strResearch.find(delimiter);
        std::string dataStr = strResearch.substr(0, pos);
        if(ld::Utils::isDouble(dataStr)) data = std::stod(dataStr);
        else data = defaultValue;
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseString(std::string &strResearch, const char& delimiter, const std::string& defaultValue, std::string &data ){
        size_t pos{};
        pos = strResearch.find(delimiter);
        std::string dataStr = strResearch.substr(0, pos);
        if(!dataStr.empty()) data = dataStr;
        else data = defaultValue;
        strResearch.erase(0, pos + 1);
    }

    void Recorder::_strParseBool(std::string &strResearch, const char& delimiter, const bool& defaultValue, bool &data ){
        size_t pos{};
        pos = strResearch.find(delimiter);
        std::string dataStr = strResearch.substr(0, pos);
        if(dataStr == "true") data = true;
        else if(dataStr == "false") data = false;
        else data = defaultValue;
        strResearch.erase(0, pos + 1);
    }

    void Recorder::setBrowserInfo(const json& value){
        if(value.find("driverInfo")  != value.end()) {
            const json& driverInfo = value["driverInfo"];
            CrowJsonResponse::jsonParse(driverInfo, "sessionId", m_browserSessionId);
            CrowJsonResponse::jsonParse(driverInfo, "screenshotUrl", m_browserScreenShotUrl);
            CrowJsonResponse::jsonParse(driverInfo, "headless", m_browserHeadless);
        }
    }

    void Recorder::setBrowserInfoOld(std::string value){
        CrowJsonResponse::strParse(value, m_postDataDelimiter, std::string(""), m_browserSessionId);
        CrowJsonResponse::strParse(value, m_postDataDelimiter, std::string(""), m_browserScreenShotUrl);
        CrowJsonResponse::strParse(value, m_postDataDelimiter, false, m_browserHeadless);
    }

    void Recorder::getWebDriverXY(const std::string &webDriverUrlScreenShot, double& offsetX, double& offsetY, const bool& isHeadless){
        if(webDriverUrlScreenShot.empty()){ return; }
        std::string hostname{};
        std::string port{};
        std::string params{};
        std::regex pattern(R"(^http[s]?:\/\/([\w\.-]+):(\d+)(/.*))");
        std::smatch match;
        if(std::regex_search(webDriverUrlScreenShot, match, pattern)){
            hostname = match[1];
            port = match[2];
            params = match[3];
        }
        if (!port.empty()){
            for(const auto& session : m_session->m_sessions ){
                if(!session.isEnded && session.webdriverPort == port){
                    if(!isHeadless){
                        offsetX = session.webDriverOffsetX;
                        offsetY = session.webDriverOffsetY;
                        return;
                    }
                    else{
                        offsetX = session.webDriverHeadlessOffsetX;
                        offsetY = session.webDriverHeadlessOffsetY;
                        return;
                    }
                }
            }
        }
    }

    void Recorder::setOffsetWebDriver(const std::string &webDriverUrlScreenShot, Session::RecorderInfo& recorderInfo, const bool& isHeadless) {
        //LD_LOG_INFO << "isHeadless : "<<isHeadless<< std::endl;
        //get offsetDriver
        if(!m_browserScreenShotUrl.empty()) {
            double offsetWebDriverX {0};
            double offsetWebDriverY {0};
            getWebDriverXY(webDriverUrlScreenShot, offsetWebDriverX, offsetWebDriverY, isHeadless);

            TestBound testBound;

            HttpRequest::getWebDriverGetWindowRect(m_browserScreenShotUrl, testBound.x, testBound.y, testBound.width,
                                                   testBound.height);
            double widthInner{0};
            double heightInner{0};
            Utils::getPngSize(HttpRequest::getScreenShot(m_browserScreenShotUrl), widthInner, heightInner);
            recorderInfo.offsetPositionX = static_cast<int>(((testBound.width - widthInner) / 2) + offsetWebDriverX);
            recorderInfo.offsetPositionY = static_cast<int>(((testBound.height - heightInner) / 2) + offsetWebDriverY);
        }
        else {
            recorderInfo.offsetPositionX = 0;
            recorderInfo.offsetPositionY = 0;
        }
    }

}
