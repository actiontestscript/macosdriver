#include "../../../include/ats/recorder/visualReport.hpp"

ld::VisualReport::VisualReport(const std::string &id, const std::string &package, const std::string &description,
                               const std::string &author, const std::string &groups, const std::string &prereq,
                               const std::string &externalId, int quality, const std::string &started) {
//    this->type = "startVisualReport";
//    this->line = -1;
    this->script = package;
    this->quality = quality;
    this->started = started;
    this->cpuSpeed = m_osInfo.m_data.processorSpeedGhz;
    this->cpuCount = stoi(m_osInfo.m_data.processorCoreNumber);
    this->totalMemory = stol(m_osInfo.m_data.systemDriveTotalSize);
    this->osInfo = "MacOs";
    if(!id.empty()) this->id = id;
    if(!author.empty()) this->author = author;
    if(!description.empty()) this->description = description;
    if(!groups.empty()) this->groups = groups;
    if(!prereq.empty()) this->prerequisite = prereq;
    if(!externalId.empty()) this->externalId = externalId;

}

void ld::VisualReport::initAmfVisualReport() {
    setAmfVisualReport(amf_visualReport);
}

void ld::VisualReport::setAmfVisualReport(AmfObject &obj) const {
    obj.addSealedProperty("author", amf::AmfString(author));
    obj.addSealedProperty("description", amf::AmfString(description));
    obj.addSealedProperty("started", amf::AmfString(started));
    obj.addSealedProperty("groups", amf::AmfString(groups));
    obj.addSealedProperty("id", amf::AmfString(id));
//    obj.addSealedProperty("name", amf::AmfString(name));
    obj.addSealedProperty("prerequisite", amf::AmfString(prerequisite));
    obj.addSealedProperty("quality", amf::AmfInteger(quality));
    obj.addSealedProperty("cpuSpeed", amf::AmfDouble(cpuSpeed));
    obj.addSealedProperty("totalMemory", amf::AmfDouble(totalMemory));
    obj.addSealedProperty("cpuCount", amf::AmfInteger(cpuCount));
    obj.addSealedProperty("osInfo", amf::AmfString(osInfo));
    obj.addSealedProperty("externalId", amf::AmfString(externalId));
    obj.addSealedProperty("script", amf::AmfString(script));
    //setAmfObjectVisualAction(obj);
}

