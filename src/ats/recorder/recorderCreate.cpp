#include "../../../include/ats/recorder/recorderCreate.hpp"
namespace ld {
    bool RecorderCreate::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder == nullptr) {
            LD_LOG_ERROR << "RecorderCreate::execute: recorder not found" << std::endl ;
            m_crowJsonResponse.setMsgErrorCode("ats_recorder_not_found", "", m_sessionId);
            res.code = m_crowJsonResponse.m_errorCode;
            res.write(m_crowJsonResponse.jsonMsgError().dump());
            return false;
        }

        if(recorder->currentActionName == "visualReport"){
            /*
            if(m_recorderCreateData.channelDimension.width > 1.0 && m_recorderCreateData.channelDimension.height > 1.0) {
                m_recorderCreateData.channelDimension.width = 1;
                m_recorderCreateData.channelDimension.height = 1;
                addImage(recorder->currentReport, m_recorderCreateData.channelDimension, false);
            }
            */
            flushVisualReport(recorder->id);
            /* START_SCRIPT */
//            recorder->currentAction = VisualAction(m_recorderCreateData.stop, m_recorderCreateData.actionType, m_recorderCreateData.line, m_recorderCreateData.script, m_recorderCreateData.timeLine, m_recorderCreateData.channelName, m_recorderCreateData.channelDimension, recorder->imageType);
//                Session::RecorderInfo* vReport = m_session->getRecorder(recorder->id);
            recorder->currentAction = VisualAction(recorder->currentReport.script);
/*
            if(m_recorderCreateData.channelDimension.width > 1.0 && m_recorderCreateData.channelDimension.height > 1.0) {
                addImage(recorder->currentAction, m_recorderCreateData.channelDimension, false);
            }
*/
            addImage(recorder->currentAction, m_recorderCreateData.channelDimension, false);
            flushVisualAction(recorder->id);
//            recorder->currentIndex++;
            recorder->currentActionName = "visualAction";
        }
        else {
/*
            if(m_recorderCreateData.channelDimension.width > 1.0 && m_recorderCreateData.channelDimension.height > 1.0) {
                m_recorderCreateData.channelDimension.width = 1;
                m_recorderCreateData.channelDimension.height = 1;
                addImage(recorder->currentAction , m_recorderCreateData.channelDimension, false);
            }
*/
            if(API_SESSION == m_browserSessionId){
                recorder->currentAction.imageType = "api";
            }
            else{
                addImage(recorder->currentAction , m_recorderCreateData.channelDimension, false);
            }

            flushVisualAction(recorder->id);
        }
        recorder->currentIndex++;


        recorder->currentAction = VisualAction(m_recorderCreateData.stop, m_recorderCreateData.actionType, m_recorderCreateData.line, m_recorderCreateData.script, m_recorderCreateData.timeLine, m_recorderCreateData.channelName, m_recorderCreateData.channelDimension, recorder->imageType);
        recorder->currentAction.index = recorder->currentIndex;

        //channel bound  addImage
        //if(m_recorderCreateData.channelDimension[2] > 1.0 && m_recorderCreateData.channelDimension)

        getResponse(res);

        return true;
    }

    bool RecorderCreate::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "actionType", m_recorderCreateData.actionType);
            CrowJsonResponse::jsonParse(value, "line", m_recorderCreateData.line);
            CrowJsonResponse::jsonParse(value, "script", m_recorderCreateData.script);
            CrowJsonResponse::jsonParse(value, "timeLine", m_recorderCreateData.timeLine);
            CrowJsonResponse::jsonParse(value, "channelName", m_recorderCreateData.channelName);
            CrowJsonResponse::jsonParse(value, "channelDimensionX", m_recorderCreateData.channelDimension.x);
            CrowJsonResponse::jsonParse(value, "channelDimensionY", m_recorderCreateData.channelDimension.y);
            CrowJsonResponse::jsonParse(value, "channelDimensionWidth", m_recorderCreateData.channelDimension.width);
            CrowJsonResponse::jsonParse(value, "channelDimensionHeight", m_recorderCreateData.channelDimension.height);

            /*
            _jsonParseString(value, "actionType", m_recorderCreateData.actionType);
            _jsonParseInt(value, "line", m_recorderCreateData.line);
            _jsonParseString(value, "script", m_recorderCreateData.script);
            _jsonParseLong(value, "timeLine", m_recorderCreateData.timeLine);
            _jsonParseString(value, "channelName", m_recorderCreateData.channelName);
            _jsonParseDouble(value, "channelDimensionX", m_recorderCreateData.channelDimension.x);
            _jsonParseDouble(value, "channelDimensionY", m_recorderCreateData.channelDimension.y);
            _jsonParseDouble(value, "channelDimensionWidth", m_recorderCreateData.channelDimension.width);
            _jsonParseDouble(value, "channelDimensionHeight", m_recorderCreateData.channelDimension.height);
             */
/*
            if(value.find("channelDimension") != value.end() && value["channelDimension"].is_array()) {
                const json& channelDimension = value["channelDimension"];
                if(channelDimension.size() == 4){
                    for(int i = 0; i < 4; i++){
                        if(channelDimension[i].is_number()){
                            if(i == 0 ) m_recorderCreateData.channelDimension.x = channelDimension[i];
                            else if(i == 1 ) m_recorderCreateData.channelDimension.y = channelDimension[i];
                            else if(i == 2 ) m_recorderCreateData.channelDimension.width = channelDimension[i];
                            else if(i == 3 ) m_recorderCreateData.channelDimension.height = channelDimension[i];
                        }
                        else if(channelDimension[i].is_string()){
                            const std::string& channelDimensionValue = channelDimension[i];
                            if(ld::Utils::isDouble(channelDimensionValue)){
                                if(i == 0 ) m_recorderCreateData.channelDimension.x = std::stod(channelDimensionValue);
                                else if(i == 1 ) m_recorderCreateData.channelDimension.y = std::stod(channelDimensionValue);
                                else if(i == 2 ) m_recorderCreateData.channelDimension.width = std::stod(channelDimensionValue);
                                else if(i == 3 ) m_recorderCreateData.channelDimension.height = std::stod(channelDimensionValue);
                            }
                        }
                    }
                }
            }
            */
            CrowJsonResponse::jsonParse(value, "sync", m_recorderCreateData.sync);
            CrowJsonResponse::jsonParse(value, "stop", m_recorderCreateData.stop);
//            _jsonParseBool(value, "sync", m_recorderCreateData.sync);
//            _jsonParseBool(value, "stop", m_recorderCreateData.stop);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }
    bool RecorderCreate::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateData.actionType);
        _strParseInt(data, m_postDataDelimiter, 0, m_recorderCreateData.line);
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateData.script);
        _strParseLong(data, m_postDataDelimiter, 0, m_recorderCreateData.timeLine);
        _strParseString(data, m_postDataDelimiter, "", m_recorderCreateData.channelName);
        _strParseDouble(data, m_postDataDelimiter, 0, m_recorderCreateData.channelDimension.x);
        _strParseDouble(data, m_postDataDelimiter, 0, m_recorderCreateData.channelDimension.y);
        _strParseDouble(data, m_postDataDelimiter, 1, m_recorderCreateData.channelDimension.width);
        _strParseDouble(data, m_postDataDelimiter, 1, m_recorderCreateData.channelDimension.height);
        _strParseBool(data, m_postDataDelimiter, false, m_recorderCreateData.sync);
        _strParseBool(data, m_postDataDelimiter, false, m_recorderCreateData.stop);
        setBrowserInfoOld(data);
        return true;
    }
}// namespace ld

