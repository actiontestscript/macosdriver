#include "../../../include/ats/recorder/testError.hpp"

ld::TestError::TestError(const std::string &script, const int &line, const std::string &message) {
    if(!script.empty()) this->script = script;
    if(!message.empty()) this->message = message;
    this->line = line ;
}

void ld::TestError::setAmfTestError(AmfObject &obj) const {
    obj.addSealedProperty("script", amf::AmfString(script));
    obj.addSealedProperty("line", amf::AmfInteger(line));
    obj.addSealedProperty("message", amf::AmfString(message));
}

void ld::TestError::initAmfTestError() {
    setAmfTestError(amf_testError);
}

