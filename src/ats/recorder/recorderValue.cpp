#include "../../../include/ats/recorder/recorderValue.hpp"
namespace ld{
    bool RecorderValue::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            recorder->currentAction.value = m_recorderValueData.v;
        }

        getResponse(res);
        return true;
    }

    bool RecorderValue::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "v", m_recorderValueData.v);
//            _jsonParseString(value, "v", m_recorderValueData.v);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderValue::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderValueData.v);
        //_strParseString(data, m_postDataDelimiter,"", m_recorderValueData.v);
        setBrowserInfoOld(data);
        return true;
    }
}//namespace ld

