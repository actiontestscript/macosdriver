#include "../../../include/ats/recorder/testBound.hpp"
namespace ld {

    void TestBound::setAmfTestBound(amf::AmfObject &obj) const  {
        obj.addSealedProperty("height", amf::AmfDouble(height));
        obj.addSealedProperty("width", amf::AmfDouble(width));
        obj.addSealedProperty("x", amf::AmfDouble(x));
        obj.addSealedProperty("y", amf::AmfDouble(y));
    }

    void TestBound::initAmfTestBound()  {
        setAmfTestBound(amf_testBound);
    }

    bool TestBound::isDefault(const TestBound &test) {
            TestBound defaultTestBound;
            return defaultTestBound == test;
    }

}
