#include "../../../include/ats/recorder/recorderImageMobile.hpp"
namespace ld {
    bool RecorderImageMobile::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
//            recorder->currentAction.images.emplace_back(0x00);
//            recorder->currentAction.imageRef = m_recorderImageData.isReference;
        }

        getResponse(res);

        return true;
    }

    bool RecorderImageMobile::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            if(value.find("screenRect") != value.end() && value["screenRect"].is_array()) {
                const json& screenRect = value["screenRect"];
                if(screenRect.size() == 4){
                    for(int i = 0; i < 4; i++){
                        if(screenRect[i].is_number()){
                            m_recorderImageMobileData.screenRect[i] = screenRect[i];
                        }
                        else if(screenRect[i].is_string()){
                            std::string screenRectValue = screenRect[i];
                            if(ld::Utils::isDouble(screenRectValue)){
                                m_recorderImageMobileData.screenRect[i] = std::stod(screenRectValue);
                            }
                        }
                    }
                }
            }
            _jsonParseBool(value, "isRef", m_recorderImageMobileData.isReference);
            _jsonParseString(value, "url", m_recorderImageMobileData.url);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderImageMobile::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseDouble(data,m_postDataDelimiter,0,m_recorderImageMobileData.screenRect[0]);
        _strParseDouble(data,m_postDataDelimiter,0,m_recorderImageMobileData.screenRect[1]);
        _strParseDouble(data,m_postDataDelimiter,1,m_recorderImageMobileData.screenRect[2]);
        _strParseDouble(data,m_postDataDelimiter,1,m_recorderImageMobileData.screenRect[3]);
        _strParseBool(data,m_postDataDelimiter,false,m_recorderImageMobileData.isReference);
        _strParseString(data,m_postDataDelimiter,"",m_recorderImageMobileData.url);
        setBrowserInfoOld(data);
        return true;
    }
}// namespace ld

