#include "../../../include/ats/recorder/recorderImage.hpp"
namespace ld {
    bool RecorderImage::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            TestBound imgBound {m_recorderImageData.screenRect[0], m_recorderImageData.screenRect[1], m_recorderImageData.screenRect[2], m_recorderImageData.screenRect[3]};
            //addImage(m_sessionId,imgBound, m_recorderImageData.isReference);
            addImage(recorder->currentAction,imgBound, m_recorderImageData.isReference);
//            recorder->currentAction.imageType = "png";
//            ImagePng img;
//            //m_recorderImageData.screenRect[2] = 1;
//            //m_recorderImageData.screenRect[3] = 1;
//            img.createEmptyPNGImage(img,  m_recorderImageData.screenRect[2],  m_recorderImageData.screenRect[3]);
//            //img.setAllPixelColor(255,0,255,255);
//            std::vector<uint8_t> imgData = img.pngImageToBytes(img);
////            recorder->currentAction.images = imgData;
//
//            recorder->currentAction.images.emplace_back(imgData);
//            recorder->currentAction.images.emplace_back(0x00);
//            recorder->currentAction.imageRef = m_recorderImageData.isReference;
        }

        getResponse(res);

        return true;
    }

    bool RecorderImage::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            if(value.find("screenRect") != value.end() && value["screenRect"].is_array()) {
                CrowJsonResponse::jsonParse(value,"screenRect" , m_recorderImageData.screenRect);
/*                const json& screenRect = value["screenRect"];
                //CrowJsonResponse::jsonParse(screenRect,"screenRect" , m_recorderImageData.screenRect);
                if(screenRect.size() == 4){
                    for(int i = 0; i < 4; i++){
                        if(screenRect[i].is_number()){
                            m_recorderImageData.screenRect[i] = screenRect[i];
                        }
                        else if(screenRect[i].is_string()){
                            std::string screenRectValue = screenRect[i];
                            if(ld::Utils::isDouble(screenRectValue)){
                                m_recorderImageData.screenRect[i] = std::stod(screenRectValue);
                            }
                        }
                    }
                }
                */
            }
            CrowJsonResponse::jsonParse(value, "isRef", m_recorderImageData.isReference);
//            _jsonParseBool(value, "isRef", m_recorderImageData.isReference);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderImage::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseDouble(data,m_postDataDelimiter,0,m_recorderImageData.screenRect[0]);
        _strParseDouble(data,m_postDataDelimiter,0,m_recorderImageData.screenRect[1]);
        _strParseDouble(data,m_postDataDelimiter,1,m_recorderImageData.screenRect[2]);
        _strParseDouble(data,m_postDataDelimiter,1,m_recorderImageData.screenRect[3]);
        _strParseBool(data,m_postDataDelimiter,false,m_recorderImageData.isReference);
        setBrowserInfoOld(data);
        return true;
    }
}   // namespace ld
