#include "../../../include/ats/recorder/recorderStart.hpp"


namespace ld {
    bool RecorderStart::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                LD_LOG_DEBUG << m_crowJsonResponse.jsonMsgError().dump() << std::endl;
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        //make tmpPath
        _setTmpPath();

        //check if directory atsRecorder exists
        if (ld::Utils::directoryExist(m_tmpPath)) {
            //check if directory is writtable
            if (!ld::Utils::directoryIsWritable(m_tmpPath)) {
                LD_LOG_DEBUG << "directory not writable: " << m_tmpPath << std::endl;
                m_crowJsonResponse.setMsgErrorCode("ats_directory_not_writable", "", m_tmpPath);
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            } else {
                std::string result{};
                std::string command = "rm -rf " + m_tmpPath + "/*";
                ld::Utils::exec(command.c_str(), result);
            }
        } else {
            //create directory
            std::string result{};
            std::string command = "mkdir -p " + m_tmpPath;
            if (!ld::Utils::exec(command.c_str(), result)) {
                LD_LOG_DEBUG << "directory not created: " << m_tmpPath << std::endl;
                m_crowJsonResponse.setMsgErrorCode("ats_directory_not_created", "", m_tmpPath);
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }

        //check space on drive tmp
        std::string freeSpace = ld::Utils::getDriveSize(m_tmpPath, Utils::FileSizeUnit::KB, Utils::FileSizeType::Available);
        if (!ld::Utils::isLong(freeSpace)) {
            std::string stackTrace = "freeSpace: " + freeSpace + " Mo ";
            LD_LOG_DEBUG << "ats_not_enough_space "<< stackTrace << std::endl;
            m_crowJsonResponse.setMsgErrorCode("ats_not_enough_space", "", stackTrace);
            res.code = m_crowJsonResponse.m_errorCode;
            res.write(m_crowJsonResponse.jsonMsgError().dump());
            return false;
        }
        const long& freeSpaceLong = std::stol(freeSpace);
        if ( m_spaceMin > freeSpaceLong  ) {
            std::string stackTrace = "freeSpace: " + std::to_string(freeSpaceLong / 1024) + " Mo ";
            LD_LOG_DEBUG << "ats_not_enough_space" << stackTrace << std::endl;
            m_crowJsonResponse.setMsgErrorCode("ats_not_enough_space", "", stackTrace);
            res.code = m_crowJsonResponse.m_errorCode;
            res.write(m_crowJsonResponse.jsonMsgError().dump());
            return false;
        }

        Session::RecorderInfo recorderInfo;
        recorderInfo.id = m_sessionId;
        recorderInfo.currentIndex = -1;
        recorderInfo.amfFullPathFileName = m_tmpPath + DIRSEPARATOR + m_sessionId + ".tmp";
        recorderInfo.currentActionName = "visualReport";
        VisualReport visualReport(m_recorderStartData.id, m_recorderStartData.fullName, m_recorderStartData.description, m_recorderStartData.author, m_recorderStartData.groups, m_recorderStartData.preRequisites, m_recorderStartData.externalId, m_recorderStartData.videoQuality, m_recorderStartData.started);
        recorderInfo.currentReport = visualReport;

        m_browserHeadless = (!m_session->getEnvironment().getGraphicalEnvironment() || m_browserHeadless);
        setOffsetWebDriver(m_browserScreenShotUrl, recorderInfo, m_browserHeadless);

        m_session->setRecorder(m_sessionId, recorderInfo);
        m_session->setRecorderSessionId(m_sessionId);



        getResponse(res);

        return true;
    }
    bool RecorderStart::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.id);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.fullName);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.description);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.author);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.groups);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.preRequisites);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.externalId);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 3, m_recorderStartData.videoQuality);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_recorderStartData.started);
        setBrowserInfoOld(data);
        return true;
    }

    bool RecorderStart::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if (jsonBody.find("value") != jsonBody.end()) {
            const json& value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "id", m_recorderStartData.id);
            CrowJsonResponse::jsonParse(value, "fullName", m_recorderStartData.fullName);
            CrowJsonResponse::jsonParse(value, "description", m_recorderStartData.description);
            CrowJsonResponse::jsonParse(value, "author", m_recorderStartData.author);
            CrowJsonResponse::jsonParse(value, "groups", m_recorderStartData.groups);
            CrowJsonResponse::jsonParse(value, "preRequisites", m_recorderStartData.preRequisites);
            CrowJsonResponse::jsonParse(value, "externalId", m_recorderStartData.externalId);
            CrowJsonResponse::jsonParse(value, "videoQuality", m_recorderStartData.videoQuality);
            CrowJsonResponse::jsonParse(value, "started", m_recorderStartData.started);
            setBrowserInfo(value);
        } else return false;
        return true;
    }

    void RecorderStart::_setTmpPath() {
        m_tmpPath = ld::Config::tmpPath;
        if (m_tmpPath[(m_tmpPath.size() - 1)] != DIRSEPARATOR) m_tmpPath += DIRSEPARATOR;
        m_tmpPath += "ats_recorder";
    }

    bool RecorderStart::_start() {

        return false;
    }

}// namespace ld