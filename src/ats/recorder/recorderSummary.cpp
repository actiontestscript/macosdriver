#include "../../../include/ats/recorder/recorderSummary.hpp"

namespace ld {
    bool RecorderSummary::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);



        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            //flush visual report or current action
            if(recorder->currentActionName == "visualReport") flushVisualReport(recorder->id);
            else flushVisualAction(recorder->id);

            //create summary
            recorder->currentActionName="reportSummary";
            if (!m_recorderSummaryData.passed && !m_recorderSummaryData.errorScript.empty()) {
                recorder->currentSummary = TestSummary(m_recorderSummaryData.passed,m_recorderSummaryData.actions, m_recorderSummaryData.suiteName, m_recorderSummaryData.testName, m_recorderSummaryData.data, m_recorderSummaryData.errorScript, m_recorderSummaryData.errorLine, m_recorderSummaryData.errorMessage);
            }
            else {
                recorder->currentSummary = TestSummary(m_recorderSummaryData.passed,m_recorderSummaryData.actions, m_recorderSummaryData.suiteName, m_recorderSummaryData.testName, m_recorderSummaryData.data);
            }
        }


        getResponse(res);
        return true;
    }

    bool RecorderSummary::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            _jsonParseBool(value, "passed", m_recorderSummaryData.passed);
            _jsonParseInt(value, "actions", m_recorderSummaryData.actions);
            _jsonParseString(value, "suiteName", m_recorderSummaryData.suiteName);
            _jsonParseString(value, "testName", m_recorderSummaryData.testName);
            _jsonParseString(value, "data", m_recorderSummaryData.data);

            setErrorInfo(value);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderSummary::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseBool(data, m_postDataDelimiter, false, m_recorderSummaryData.passed);
        _strParseInt(data, m_postDataDelimiter, 0 , m_recorderSummaryData.actions);
        _strParseString(data, m_postDataDelimiter, "", m_recorderSummaryData.suiteName);
        _strParseString(data, m_postDataDelimiter, "", m_recorderSummaryData.testName);
        _strParseString(data, m_postDataDelimiter, "", m_recorderSummaryData.data);
        _strParseString(data, m_postDataDelimiter, "", m_recorderSummaryData.errorScript);
        _strParseInt(data, m_postDataDelimiter, 0 , m_recorderSummaryData.errorLine);
        _strParseString(data, m_postDataDelimiter, "", m_recorderSummaryData.errorMessage);
        setBrowserInfoOld(data);
        return true;
    }

    void RecorderSummary::setErrorInfo(const json& value){
        if(value.find("error")  != value.end()) {
            const json& error = value["error"];
            CrowJsonResponse::jsonParse(error, "script", m_recorderSummaryData.errorScript);
            CrowJsonResponse::jsonParse(error, "line", m_recorderSummaryData.errorLine);
            CrowJsonResponse::jsonParse(error, "message", m_recorderSummaryData.errorMessage);
        }
    }

}// namespace ld

