#include "../../../include/ats/recorder/visualElement.hpp"

namespace ld{


    VisualElement::VisualElement(const std::string& tag, const std::string& criterias,int foundElements,const TestBound& rectangle, long duration) {
        this->foundElements = foundElements;
        this->rectangle = rectangle;
        this->searchDuration = duration;
        if(!tag.empty()) this->tag = tag;
        if(!criterias.empty()) this->criterias = criterias;

    }

    void VisualElement::updatePosition(const std::string &hPos, const std::string &hPosValue, const std::string &vPos, const std::string &vPosValue) {

        if(!hPosValue.empty()){
            this->hpos = hPos;
            if(ld::Utils::isInt(hPosValue)) this->hposValue = std::stoi(hPosValue);
        }
        if(!vPosValue.empty()){
            this->vpos = vPos;
            if(ld::Utils::isInt(vPosValue)) this->vposValue = std::stoi(vPosValue);
        }

    }



    void VisualElement::setAmfVisualElement(AmfObject &obj) const {
        //isVisualElement = true;
        //obj.addSealedProperty("bound", amf::AmfObject(bound.amf_bound));
        //if(!isTestBoundApply) obj.addSealedProperty("bound", bound.getAmfTestBound());
        obj.addSealedProperty("rectangle", rectangle.getAmfTestBound());
//        obj.addSealedProperty("bound", bound.getAmfTestBound());
        //obj.addSealedProperty("bound", amf::AmfNull());
        obj.addSealedProperty("criterias", amf::AmfString(criterias));
        obj.addSealedProperty("foundElements", amf::AmfInteger(foundElements));
        obj.addSealedProperty("searchDuration", amf::AmfDouble(searchDuration));
        obj.addSealedProperty("tag", amf::AmfString(tag));
        obj.addSealedProperty("hpos", amf::AmfString(hpos));
        obj.addSealedProperty("hposValue", amf::AmfInteger(hposValue));
        obj.addSealedProperty("vpos", amf::AmfString(vpos));
        obj.addSealedProperty("vposValue", amf::AmfInteger(vposValue));
    }

    void VisualElement::initAmfVisualElement() {
        //if (isTestBoundApply) bound.setAmfTestBound(amf_visualElement);
        //else bound.initAmfTestBound();
        rectangle.initAmfTestBound();
        //bound.setAmfTestBound(amf_visualElement);
        setAmfVisualElement(amf_visualElement);
//        amf_visualElement.addSealedProperty("bound", bound.getAmfTestBound());
    }

    bool VisualElement::isDefault(const VisualElement &test) {
        VisualElement defaultVisualElement;
        return defaultVisualElement == test;
    }

}