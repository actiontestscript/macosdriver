#include "../../../include/ats/recorder/recorderElement.hpp"
namespace ld{
    bool RecorderElement::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        std::string tag="*";
        size_t pos;
        std::string criterias = m_recorderElementData.selector;
        std::vector<std::string> criteriasDatas;
        do{
            pos = criterias.find(',');
            if(pos != std::string::npos){
                criteriasDatas.push_back(criterias.substr(0, pos));
                criterias = criterias.substr(pos+1);
            }
        }
        while(pos != std::string::npos);
        if(!criteriasDatas.empty()) {
            tag = criteriasDatas[0];
            if(criteriasDatas.size() == 1) {
                m_recorderElementData.selector += "(no-criterias)";
            }
        }
        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr) {
            m_recorderElementData.elementBound.x -= recorder->offsetPositionX;
            m_recorderElementData.elementBound.y -= recorder->offsetPositionY;
            recorder->currentAction.setIsVisualElement(true);
            VisualElement visualElement(tag, m_recorderElementData.selector, m_recorderElementData.numElements, m_recorderElementData.elementBound, m_recorderElementData.searchDuration);
            recorder->currentAction.element = visualElement;
/*
            //offset recorder
            if(m_session->getCurrentDriverName(m_sessionId) == "chrome"){ m_session->setRecorderOffsetPositionXY(m_sessionId, 2, 110); }
            else if(m_session->getCurrentDriverName(m_sessionId) == "firefox"){ m_session->setRecorderOffsetPositionXY(m_sessionId, 2, 110); }
            else if(m_session->getCurrentDriverName(m_sessionId) == "msedge"){ m_session->setRecorderOffsetPositionXY(m_sessionId, 2, 110); }
            else if(m_session->getCurrentDriverName(m_sessionId) == "brave"){ m_session->setRecorderOffsetPositionXY(m_sessionId, 2, 110); }
            else if(m_session->getCurrentDriverName(m_sessionId) == "opera"){ m_session->setRecorderOffsetPositionXY(m_sessionId, 2, 110); }
*/
            //offset position element for webDriver screenshot correction
        }


        getResponse(res);

        return true;
    }

    bool RecorderElement::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            if(value.find("elementBound") != value.end() && value["elementBound"].is_array()) {
                std::vector<double> elementBound{0, 0, 0, 0};
                CrowJsonResponse::jsonParse(value, "elementBound", elementBound);
                m_recorderElementData.elementBound.x = elementBound[0];
                m_recorderElementData.elementBound.y = elementBound[1];
                m_recorderElementData.elementBound.width = elementBound[2];
                m_recorderElementData.elementBound.height = elementBound[3];
            }

//                const json& elementBound = value["elementBound"];
//                if (elementBound.size() == 4) {
//                    for (int i = 0; i < 4; i++) {
//                        CrowJsonResponse::jsonParse(elementBound[i], m_recorderElementData.elementBound[i]);
/*
                        if (elementBound[i].is_number()) {
                            if(i == 0 ) m_recorderElementData.elementBound.x = elementBound[i];
                            else if(i == 1 ) m_recorderElementData.elementBound.y = elementBound[i];
                            else if(i == 2 ) m_recorderElementData.elementBound.width = elementBound[i];
                            else if(i == 3 ) m_recorderElementData.elementBound.height = elementBound[i];
                        } else if (elementBound[i].is_string()) {
                            const std::string& elementBoundValue = elementBound[i];
                            if (ld::Utils::isDouble(elementBoundValue)) {
                                if(i == 0 ) m_recorderElementData.elementBound.x = std::stod(elementBoundValue);
                                else if(i == 1 ) m_recorderElementData.elementBound.y = std::stod(elementBoundValue);
                                else if(i == 2 ) m_recorderElementData.elementBound.width = std::stod(elementBoundValue);
                                else if(i == 3 ) m_recorderElementData.elementBound.height = std::stod(elementBoundValue);
                            }
                        }
                        */
            CrowJsonResponse::jsonParse(value, "searchDuration", m_recorderElementData.searchDuration);
            CrowJsonResponse::jsonParse(value, "numElements", m_recorderElementData.numElements);
            CrowJsonResponse::jsonParse(value, "selector", m_recorderElementData.selector);
//            _jsonParseLong(value, "searchDuration", m_recorderElementData.searchDuration);
//            _jsonParseInt(value, "numElements", m_recorderElementData.numElements);
//            _jsonParseString(value, "searchCriterias", m_recorderElementData.searchCriterias);
            setBrowserInfo(value);
        }
        else return false;
        return true;
    }

    bool RecorderElement::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseDouble(data, m_postDataDelimiter, 0, m_recorderElementData.elementBound.x);
        _strParseDouble(data, m_postDataDelimiter, 0, m_recorderElementData.elementBound.y);
        _strParseDouble(data, m_postDataDelimiter, 1, m_recorderElementData.elementBound.width);
        _strParseDouble(data, m_postDataDelimiter, 1, m_recorderElementData.elementBound.height);
        _strParseLong(data, m_postDataDelimiter, 0L, m_recorderElementData.searchDuration);
        _strParseInt(data, m_postDataDelimiter, 0, m_recorderElementData.numElements);
        _strParseString(data, m_postDataDelimiter, "", m_recorderElementData.selector);
        setBrowserInfoOld(data);
        return true;
    }
}// namespace ld