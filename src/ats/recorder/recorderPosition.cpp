#include "../../../include/ats/recorder/recorderPosition.hpp"

namespace ld {
    bool RecorderPosition::execute(const crow::request &req, crow::response &res) {
        if(!m_recorderOld) {
            //check if json is valid
            if (!_jsonIsValid(req, res)) return false;

            //parse json
            if (!_parseJson(req)) {
                m_crowJsonResponse.setMsgErrorCode("invalid_argument", "", "JSON key value is not valid or missing");
                res.code = m_crowJsonResponse.m_errorCode;
                res.write(m_crowJsonResponse.jsonMsgError().dump());
                return false;
            }
        }
        else _parsePostData(req);

        Session::RecorderInfo* recorder = m_session->getRecorder(m_sessionId);
        if(recorder != nullptr){
            recorder->currentAction.setIsVisualElement(true);
            recorder->currentAction.element.updatePosition(m_recorderPositionData.hpos, m_recorderPositionData.hposValue, m_recorderPositionData.vpos, m_recorderPositionData.vposValue);
        }

        getResponse(res);

        return true;
    }

    bool RecorderPosition::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json& value = jsonBody["value"];
            _jsonParseString(value, "hpos", m_recorderPositionData.hpos);
            _jsonParseString(value, "hposValue", m_recorderPositionData.hposValue);
            _jsonParseString(value, "vpos", m_recorderPositionData.vpos);
            _jsonParseString(value, "vposValue", m_recorderPositionData.vposValue);
            setBrowserInfo(value);
        }
//        if(m_recorderPositionData.hpos.empty()){m_recorderPositionData.hpos = "0";}
//        if(m_recorderPositionData.vpos.empty()){m_recorderPositionData.vpos = "0";}
        else return false;
        return true;
    }

    bool RecorderPosition::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        _strParseString(data, m_postDataDelimiter, "", m_recorderPositionData.hpos);
        _strParseString(data, m_postDataDelimiter, "", m_recorderPositionData.hposValue);
        _strParseString(data, m_postDataDelimiter, "", m_recorderPositionData.vpos);
        setBrowserInfoOld(data);
//        if(m_recorderPositionData.hpos.empty()){m_recorderPositionData.hpos = "0";}
//        if(m_recorderPositionData.vpos.empty()){m_recorderPositionData.vpos = "0";}
        return true;
    }
}// namespace ld
