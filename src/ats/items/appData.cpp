#include "../../../include/ats/items/appData.hpp"
namespace ld{
    void appData::initAmfDesktopAppData() { setAmfObjectDesktopAppData(amf_desktopAppData); }

    void appData::setAmfObjectDesktopAppData(amf::AmfObject &obj) const {
        obj.addSealedProperty("version", amf::AmfString(m_version));
        obj.addSealedProperty("build", amf::AmfString(m_build));
        obj.addSealedProperty("path", amf::AmfString(m_path));
        obj.addSealedProperty("name", amf::AmfString(m_name));
        obj.addSealedProperty("icon", amf::AmfByteArray(m_icon));
    }


}
