#include "../../../include/ats/mouse/mouseWheel.hpp"
namespace ld {
    bool MouseWheel::_parseJson(const crow::request &req) {
        const json &jsonBody = json::parse(req.body);
        if (jsonBody.find("value") != jsonBody.end()) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "delta", m_MouseWheelData.m_delta);
            return true;
        }
        return false;
    }

    bool MouseWheel::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_MouseWheelData.m_delta);

        return true;
    }
}//end namespace ld