#include "../../../include/ats/mouse/mouse.hpp"
namespace ld{
    bool Mouse::execute(const crow::request &req, crow::response &res ) {
        parseData(req);
        CrowJsonResponse::getJsonNullResponse(res);
        return true;
    }

    void Mouse::parseData(const crow::request &req) {
        const std::string& body = req.body;
        if(CrowJsonResponse::isJson(body)){
            if(!_parseJson(req))
                if(_parsePostData(req)) setResponseType(CrowJsonResponse::ResponseType::AMF);

        }
        else {
            _parsePostData(req);
            setResponseType(CrowJsonResponse::ResponseType::AMF);
        }
    }

}//end namespace ld
