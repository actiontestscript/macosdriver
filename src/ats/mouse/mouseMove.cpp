#include "../../../include/ats/mouse/mouseMove.hpp"
namespace ld {
    bool MouseMove::_parseJson(const crow::request &req) {
        const json &jsonBody = json::parse(req.body);
        if (jsonBody.find("value") != jsonBody.end()) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "x", m_MouseMoveData.m_x);
            CrowJsonResponse::jsonParse(value, "y", m_MouseMoveData.m_y);
            return true;
        }
        return false;
    }

    bool MouseMove::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_MouseMoveData.m_x);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_MouseMoveData.m_y);
        return true;
    }
}//end namespace ld