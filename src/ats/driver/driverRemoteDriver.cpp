#include "../../../include/ats/driver/driverRemoteDriver.hpp"
namespace ld {

    bool DriverRemoteDriver::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "appName", m_DriverRemoteDriverData.m_appName);
            CrowJsonResponse::jsonParse(value, "appPath", m_DriverRemoteDriverData.m_appPath);
            return true;
        }
        return false;
    }

    bool DriverRemoteDriver::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_DriverRemoteDriverData.m_appName);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_DriverRemoteDriverData.m_appPath);
        return true;
    }
}
