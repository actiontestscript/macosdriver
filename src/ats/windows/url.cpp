#include "../../../include/ats/windows/url.hpp"
namespace ld{
    WindowsUrl::WindowsUrl() = default;

    bool WindowsUrl::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }


    bool WindowsUrl::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowUrlData.m_handle);
            CrowJsonResponse::jsonParse(value, "fname", m_windowUrlData.m_fName);
            return true;
        }
        return false;
    }

    bool WindowsUrl::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowUrlData.m_handle);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_windowUrlData.m_fName);
        return true;
    }
}