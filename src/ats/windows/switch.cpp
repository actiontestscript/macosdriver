#include "../../../include/ats/windows/switch.hpp"
namespace ld{
    WindowsSwitch::WindowsSwitch() = default;

    bool WindowsSwitch::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }


    bool WindowsSwitch::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "pid", m_windowSwitchData.m_pid);
            CrowJsonResponse::jsonParse(value, "index", m_windowSwitchData.m_index);
            CrowJsonResponse::jsonParse(value, "handle", m_windowSwitchData.m_handle);
            return true;
        }
        return false;
    }

    bool WindowsSwitch::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        int nbrData = Utils::getIteration(data, std::string()+ m_postDataDelimiter);
        if(nbrData >1 ) {
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowSwitchData.m_pid);
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowSwitchData.m_index);
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowSwitchData.m_handle);
        }
        else{
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowSwitchData.m_handle);
        }
        return true;
    }
}