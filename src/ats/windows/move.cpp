#include "../../../include/ats/windows/move.hpp"
namespace ld{
    WindowsMove::WindowsMove() = default;

    bool WindowsMove::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }

    bool WindowsMove::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowMoveData.m_handle);
            CrowJsonResponse::jsonParse(value, "value1", m_windowMoveData.m_value1);
            CrowJsonResponse::jsonParse(value, "value2", m_windowMoveData.m_value2);
            return true;
        }
        return false;
    }

    bool WindowsMove::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowMoveData.m_handle);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowMoveData.m_value1);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowMoveData.m_value2);
        return true;
    }
}