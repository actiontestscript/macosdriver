#include "../../../include/ats/windows/close.hpp"
namespace ld{
    WindowsClose::WindowsClose() = default;

    bool WindowsClose::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        //DesktopResponse::getResponse(res);
        return true;
    }


    bool WindowsClose::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowCloseData.m_handle);
            return true;
        }
        return false;
    }

    bool WindowsClose::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowCloseData.m_handle);
        return true;
    }


}