#include "../../../include/ats/windows/keys.hpp"
namespace ld{
    WindowsKeys::WindowsKeys() = default;

    bool WindowsKeys::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }

    bool WindowsKeys::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowsKeysData.m_handle);
            CrowJsonResponse::jsonParse(value, "keys", m_windowsKeysData.m_keys);

            return true;
        }
        return false;
    }

    bool WindowsKeys::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsKeysData.m_handle);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_windowsKeysData.m_keys);
        return true;
    }
}