#include "../../../include/ats/windows/title.hpp"
namespace ld{
    WindowsTitle::WindowsTitle() = default;

    bool WindowsTitle::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }

    bool WindowsTitle::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "title", m_windowTitleData.m_title);
            CrowJsonResponse::jsonParse(value, "pid", m_windowTitleData.m_pid);
            return true;
        }
        return false;
    }

    bool WindowsTitle::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, std::string(""), m_windowTitleData.m_title);
        CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowTitleData.m_pid);
        return true;
    }
}