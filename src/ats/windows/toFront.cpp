#include "../../../include/ats/windows/toFront.hpp"
namespace ld{
    WindowsToFront::WindowsToFront() = default;

    bool WindowsToFront::execute(const crow::request &req, crow::response &res) {
        parseData(req);
        DesktopResponse::getResponse(res,getResponseType());
        return true;
    }


    bool WindowsToFront::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "handle", m_windowsToFrontData.m_handle);
            CrowJsonResponse::jsonParse(value, "pid", m_windowsToFrontData.m_pid);
            return true;
        }
        return false;
    }


    bool WindowsToFront::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        int nbrElements = Utils::getIteration(data, std::string()+ m_postDataDelimiter);
        /*
        size_t pos = std::string::npos;
        do {
            pos = data.find(m_postDataDelimiter);f
            if (pos != std::string::npos) {
                data.erase(0, pos + 1);
            }
            nbrElements++;
        }
        while (pos != std::string::npos) ;
        data = req.body;
         */
        if(nbrElements > 1) {
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsToFrontData.m_handle);
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsToFrontData.m_pid);
        }
        else{
            CrowJsonResponse::strParse(data, m_postDataDelimiter, 0, m_windowsToFrontData.m_pid);
        }
        return true;
    }
}