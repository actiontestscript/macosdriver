#include "../../../include/ats/key/keyDown.hpp"
namespace ld{
    bool KeyDown::_parseJson(const crow::request &req) {
        const json& jsonBody = json::parse(req.body);
        if(jsonBody.find("value") != jsonBody.end() ) {
            const json &value = jsonBody["value"];
            CrowJsonResponse::jsonParse(value, "codePoint", m_KeyDownData.m_codePoint);
            return true;
        }
        return false;
    }

    bool KeyDown::_parsePostData(const crow::request &req) {
        std::string data = req.body;
        CrowJsonResponse::strParse(data, m_postDataDelimiter, -1, m_KeyDownData.m_codePoint);

        return true;
    }

} //end namespace ld
