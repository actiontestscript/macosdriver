#include "../../include/drivers/driverGecko.hpp"

namespace ld{
    Gecko::Gecko(){
        m_session = Session::getInstance();
        m_driverName = "gecko";
        m_driverProcessName = "geckodriver";
        m_browserName = "firefox";
        m_browserPath = "/Applications/Firefox.app/Contents/MacOS";
        m_browserBundleIdentifier = "org.mozilla.firefox";
        m_defaultPort = 9710;   //default driver 4444
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="firefox/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAADAFBMVEVHcEzWRwovK04AAADZbScAAACbEwIhEgYGAAAGEyQJBQDrhSXgdR8AAADFTg4yCgFtGwTscB7MUhsBAADzqhfws0ELBQAnYpoqZqJAhbmqIgQNBALkoCx4iYLAJAS/KwXtgBvWhxOyUDaVSQm1Ug4kL1RzFQLvtCkROXfnrR8dCwHijCXpojbYZCDynTIJAgC4IATCJAOYEQPUVQ7ipl7RNgfxgx3whh+tWAxrRAapQyUSAgBGlcRUtNmA0Og1bqAFJVwJLGQZRYHy1E8kW5kkXZXqz06FbGmsVTTIbi7kgwg+cJfpkg3SdCnSSQvTWhPVYBbZZxbdbRiVDwLNSg7PVBLGRxAQPHzSVhHZYQ/UWwzfZA31xUfYSwfRRAjlhA8YRYTH9PzPTwy/8vrAMwUhc7r73EvfcQjdagfAPQwjZaUSNXG28fvJPAfidgvvnxjeVwoIJ2IngcfkfgobVJXhWwyg6/jrlxbXUwzN9vzY+f4aZrDNRQhEqOOV5POkGATKUhMXTIzjbA7MYgvbYwfjTQgwj9Jfwes7ltWrXiGoHQSxJQa/bC8BMIDfdB+C3fK3LQX1vjGQCwJSuOocXKACHFuhIQUCCzboiwwdbLXQ9v00i8uW6/kCKXR22/QsZJ0BI26n8P1o1PKr6fU1eb73vilUlL9XqM9qwuRiLTb0syJVseCXSizvuEXwqzH600C5NgyKCQKqJATncxXumA4bLVJMsejuoiQCAyI0hsUML2lEpN3nkz+x7vrXdSnPZBx/jZVsd3svgMGK5vZUWnKGSiwBFVDqml0IPo24QhLfi0t1ZmR50enMpYJ6JBvghDPFVyj3uiB1zeuI0uUkUouennwtWqhZx/Ccg2RIjMVQfrb54WTz0Vn86mprWz74zjLWmSCdYyQ1HRxRLkiZgDwvbamixsHproSho5SmvrHor1StfErD3922l3wtUH2jYVGQtb4iQXDNpUmCqbGkurBFXHCBhGuJkYHu6Iwoca/ayGBjiqD67nDFtWG7w6oAFADX5/G4AAABAHRSTlMA/v0pBRz+AxEJRRslUf6ZvPcONP4SbhZndvJkN/6DX9r469n3N9ZRgfuDUWpKiXjnRaVmr72t7tNY8YzDRPvm8dj1nFDW9zXF6Iay2dT///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8pR0J9xwAAA3NJREFUOMtjYEAC4i4SkpKStuIMWIG4huuyy4uTEysX29lIcGJIc2o43exfmFgFApVly+0lkCVFuRlUHROWXKpLrqycNWtWZWLOmc9fvN1Z4Ar0dFQdsq/dXPHh7JtEIEjOWdy//PcPWQ8hqLyQoCBTYcK1W+/D3u5NBoK9ZXUrli+TvXtVSQaiQLe11b/n/KkLYT2Xy/bm5Oytu7Ridcetf39v3/AUBSuQj9scHRkZEBZ64/CcsoULy+rOtK9evUT2z507a5zBCow3F1RUVESEBpx88fJZ3Zw5h/vnLbpee+XX3bWf3EBGsCgUxMb6+SVFBGTGFpQenDdv3iKg/Oyf39JiYtLUgQq0m4DycTUhoQEXjh8/u6y9vX3yttpDWalfY9Ji0nSACsxKGRnjampCIgJPPT/wdPfkydt2LmlrKyn5uDYm7b4eOwOLeQGjX1wI0IC+PQd37q6t7Zh9KCtrSuO772vXpU305WHgUQA6wC/JP2D/vj0H2to6OmZnTSlpbEzIvh0DVLBGl4FFkBEkH8YW/DhhfhEIZJU0JiSE70pbP3H9qpXaDNylfiF+Ef7Tpm06MnNmKhjMTAgPDy9cH7P0/oN7Mgw8jLk1If6hmyak9MyvBoL5WdVTwsOzs7dOjFl68YEmNwO3aW5uiL9/Sm5c0L7TR073nH91tSQ8e1fhk+1LV4mU+wC9KZeSAvSCX2xrUmBfX19Q4P7XV8ILC7euyr+YX17sBVRgmNKd2eAfWVBaMe0kXwPf0ewTJ+ZOnXovP19kxowMHlBQG9R3Z4YGBEZHN5w7t5KpFwjmLpiqWJ6/pTi9AZxo5OrrN4UBVQQFdTItmLvg2PTp05mObi+fEZ+uogRJMFLBwRsgKoBKju3Y8agzPX57cXF680oTSIqRZ5sUnBkW6h8IUhLYnJ6XkV6clxff8lATmqQYjII3As1oiAgICAyMjIyMimrJyCuP3yKiA0u1HAKTNgZPCAsJ9ff3j4qKSIrKyIuPj1e0RGQKYYHgScFdmSGhUZGR0dHRTU1NGRkWXBwIBaxc/GxA0J2ZVFFQGh3d0tK8xkqaGSl7cf7ntVbrqu/qnpCSW1OTFNXcqSwmzMGOnPNYucT41aSkQAr4VJQFtHiZUeQZGNg5WYV5pfW1+Pn1xaR5uf6jSUPVcHCwAgEHByc7QhoAg/FiQ9HG/TAAAAAASUVORK5CYII=";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
        /*
        m_webDriverOffsetX = -2;
        m_webDriverOffsetY = -14;
        m_webDriverHeadlessOffsetX = 2;
        m_webDriverHeadlessOffsetY = -14;
*/
    }

    std::string Gecko::getBrowserVersion(const bool& fullVersion) {
        std::string version ="not found";
        std::string result{};
        std::string applicationPath = ld::Session::getBrowserAtsPropertiesPath(m_browserName);

        std::string cmd ;

        if(!applicationPath.empty()){
            ld::Utils::escapePath(applicationPath);
            cmd = applicationPath + " --version";
        }
        else {
            ld::Utils::escapePath(m_browserPath);
            cmd = m_browserPath + " --version";
        }
        if (ld::Utils::exec(cmd.c_str(), result)) {
            std::regex pattern;
            if(fullVersion) pattern = R"((\d+(\.\d+)+))";
            else pattern = R"(Mozilla Firefox\s+(\d+)\..*)";       /*> pattern use to find major version */
            std::smatch matches;
                if(std::regex_search(result, matches, pattern)){
                    m_browserVersion = matches[1].str();
                    version = m_browserVersion;
                }
        }
        return version;
    }

    std::string Gecko::getWebDriverVersion(const bool& fullVersion) {
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;
        std::string driverPath = currentPath + m_driverProcessName;

        if(!ld::Utils::fileExist(driverPath)){return "not found"; }

        std::string cmd = driverPath + " --version";
        if(ld::Utils::exec(cmd.c_str(), result)){

            std::regex pattern;
            pattern = R"(^geckodriver\s+(\d+\.\d+\.\d+).*)";

            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                return matches[1].str();
            }
        }
        return "not found";
    }

    bool Gecko::downloadWebdriver() {

        std::string latest_version = getBrowserVersion(false);
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;
        std::string extension =".tgz";

        std::string geckoVersionToDownload = getGeckoDriverVersionDownload(latest_version);
        LD_LOG_DEBUG << "browser version : " << latest_version << std::endl;
        LD_LOG_DEBUG << "geckoVersionToDownload: " << geckoVersionToDownload << std::endl;
        LD_LOG_DEBUG << "currentPath: " << currentPath << std::endl;
        std::string urlDownload = m_downloadUrl + geckoVersionToDownload + ".tgz";

        std::string cmdDownload = "curl -q -o " + currentPath + m_driverProcessName + extension +" " + urlDownload ; // -q quiet mode, -O output
        cmdDownload += " > /dev/null 2>&1";
        LD_LOG_DEBUG << "cmdDownload: " << cmdDownload << std::endl;
        //        std::string cmdDownload = "wget " + urlDownload + " -q -O LATEST.zip"; // -q quiet mode, -O output//
        std::thread t1(ld::Utils::exec, cmdDownload.c_str(), std::ref(result));
        t1.join();

        result="";
        cmdDownload = "du -k "+ currentPath + m_driverProcessName + extension +" | awk '{print $1}'";
        LD_LOG_DEBUG << "cmdDownload check taille > 0 : " << cmdDownload << std::endl;
        std::regex pattern("\\n");
        ld::Utils::exec(cmdDownload.c_str(), result);
        result = std::regex_replace(result, pattern, "");
        if(result == "0"){
            if(ld::Utils::fileExist(currentPath + m_driverProcessName)) return true;
            LD_LOG_DEBUG << "Download failed driver gecko" << std::endl;
            return false;
        }

        //unzip
        //std::string cmdUnzip = "unzip -qo "+ currentPath + m_driverProcessName +".zip "+m_driverProcessName;
        std::string cmdUnTar = "tar -xzf "+ currentPath + m_driverProcessName + extension + " -C " + currentPath;
        //std::string cmdUnzip = "unzip -qo LATEST.zip geckodriver";
        LD_LOG_DEBUG << "cmdUnTar: " << cmdUnTar << std::endl;
        std::thread t2(ld::Utils::exec, cmdUnTar.c_str(), std::ref(result));
        t2.join();

        //delete zip file
        std::string cmdDelete = "rm -f "+ currentPath + m_driverProcessName+ extension;
        LD_LOG_DEBUG << "cmdDelete: " << cmdDelete << std::endl;
        std::thread t3(ld::Utils::exec, cmdDelete.c_str(), std::ref(result));
        t3.join();

        if(isBrowserWebDriverVersionMatch(latest_version, getWebDriverVersion(false))) return true;

        LD_LOG_WARNING << "different version on gecko and geckoWebDriver";

        return false;
    }



    bool Gecko::setLocalListIpsBind(const std::vector<std::string>& localIps) {
            //if(localIps.empty()) return false;
            if(!Session::getIsLocal()) {
                m_localListIpsBind = "0.0.0.0";
                return true;
            }
            m_localListIpsBind="";
            return false;
    }

    void Gecko::makeDriverArgslineCmd() {
        Gecko::BrowserInfo browserInfo = getBrowserInfo(m_browserVersion);
        if(browserInfo.binary) m_driverArgslineCmd = " --binary " + m_browserPath;
        m_driverArgslineCmd += " --port " + std::to_string(m_port);
        if(!m_session->getIsDockerContainer()){
            if(!m_whiteListIps.empty() && !Session::getIsLocal()) {
                if(browserInfo.hosts) m_driverArgslineCmd += " --host " + m_localListIpsBind;
                if(browserInfo.allowHosts) m_driverArgslineCmd += " --allow-hosts " + m_whiteListIps;
            }
        }
        if(browserInfo.marionnettePort) m_driverArgslineCmd +=" --marionette-port 2828"  ;
        if(ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")){
            std::string level;
            if(ld::Config::logWebDriverLevel =="ALL") level = "trace";
            else if(ld::Config::logWebDriverLevel =="DEBUG") level = "debug";
            else if(ld::Config::logWebDriverLevel =="INFO") level = "info";
//            else if(ld::Config::logWebDriverLevel =="WARNING") level = "warn";        //no Listening information
            else if(ld::Config::logWebDriverLevel =="ERROR") level = "error";
//            else if(ld::Config::logWebDriverLevel =="CRITICAL") level = "fatal";      //no Listening information
            else level = "info";

            if(browserInfo.log) m_driverArgslineCmd += " --log " + level;
        }
        else
           if(browserInfo.log) m_driverArgslineCmd +=" --log info"  ;

        //m_driverArgslineCmd +=" --log-no-truncate"  ;
    }

    std::string Gecko::getGeckoDriverVersionDownload(const string &browserVersion) {
        std::string version = "LATEST";
        if(browserVersion.empty()) return version;
        if(!Utils::isInt(browserVersion)) return version;
        for(auto& it : m_mapBrowserVersion){
            int ver = std::stoi(browserVersion);
            if( ver >= it.first.first && ver <= it.first.second){
                version = it.second.fileDownload;
                break;
            }
        }
        return version;
    }

    bool Gecko::isBrowserWebDriverVersionMatch(const string &browserVersion, const string &webDriverVersion) {
        if(browserVersion.empty() || webDriverVersion.empty()) return false;
        std::string geckoDriverVersion = getGeckoDriverVersionDownload(browserVersion);
        if(getGeckoDriverVersionDownload(browserVersion) == webDriverVersion) return true;
        return false;
    }

    Gecko::BrowserInfo Gecko::getBrowserInfo(const string &browserVersion) {
        std::string version = browserVersion;
        if(version.empty()) version="999";
        if(!Utils::isInt(version)) version="999";
        for(auto& it : m_mapBrowserVersion){
            int ver = std::stoi(version);
            if( ver >= it.first.first && ver <= it.first.second){
                return it.second;
            }
        }
        return m_mapBrowserVersion.rbegin()->second;
    }

    std::map<std::pair<int, int>, Gecko::BrowserInfo> Gecko::m_mapBrowserVersion = {
            {{52, 52},{52, 52,"0.17.0",false, true, false, true, true, false,false,true}},
            {{53, 54},{53, 54,"0.18.0",false, true, false, true, true, false,false,true}},
            {{55, 56},{55, 56,"0.20.1",false, true, false, true, true, false,false,true}},
            {{57, 59},{57, 59,"0.25.0",false, true, false, true, true, false,false,true}},
            {{60, 77},{60, 77,"0.29.1",false, true, false, true, true, false,false,true}},
            {{78, 90},{78, 90,"0.30.0",false, true, false, true, true, false,false,true}},
            {{91, 101},{91, 101,"0.31.0"}},
            {{102, 118},{102, 118,"0.33.0"}},
            {{119, 999},{119, 999,"0.33.0"}}
    };//https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html


}

