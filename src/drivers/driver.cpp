#include "../../include/drivers/driver.hpp"

namespace ld {
    Driver::Driver(){
        if(Session::getCpuArchitecture() == "arm64") m_downloadUrl+= MAC_OS_PATH_ARM64 ;
        else m_downloadUrl+= MAC_OS_PATH_X86_64 ;
    };

    bool Driver::checkTools(crow::response &res) {
        //check unzip
/*
        std::string result{};
        std::string cmd = "whereis unzip";
        ld::Utils::exec(cmd.c_str(), result);
        std::regex pattern(R"(^unzip:\s+(.*))");
        std::smatch matches;
        if (std::regex_search(result, matches, pattern)) {
            if (matches[1].str().empty()) {
                LD_LOG_DEBUG << "unzip not found" << std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_unzip_not_found", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }
        */
        std::string result{};
        std::string cmd = "whereis curl";
        ld::Utils::exec(cmd.c_str(), result);
        std::regex pattern_curl(R"(^curl:\s+(.*))");       /*> pattern use to find major version */
        std::smatch matches_curl;
        if (std::regex_search(result, matches_curl, pattern_curl)) {
            if (matches_curl[1].str().empty()) {
                LD_LOG_ERROR << "curl not found" << std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_curl_not_found", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }

        return true;
    }

    bool Driver::isInstalled() {
        return ld::Utils::fileExist(m_browserPath);
  }

    bool Driver::isInProgress() {
        if (m_session->m_processes.empty()) return false;
        auto it = m_session->m_processes.find(m_driverName);
        if (it != m_session->m_processes.end()) {

            //check if port is in use
            LD_LOG_DEBUG << "Driver -> IsInProgress -> m_driverName: " << m_driverName << " port: " << m_session->m_processes[m_driverName].port << std::endl;
            LD_LOG_DEBUG << "Driver -> IsInProgress -> port: " << m_session->m_processes[m_driverName].port << std::endl;

            if (Network::isPortInUse(std::stoi(m_session->m_processes[m_driverName].port))) {
                return true;
            }
        }
        return false;
    }

    bool Driver::execute(crow::response &res) {
        std::string applicationPath = Session::getBrowserAtsPropertiesPath(m_browserName);
        if(!applicationPath.empty()) m_browserPath = applicationPath;

        makePath(type::BROWSER);

        //check if the browser is installed
        if (!isInstalled()) {
            LD_LOG_ERROR << "Browser not found" << std::endl;
            CrowJsonResponse jRes;
            jRes.setMsgErrorCode("ats_error_browser_not_found", "", "");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

        //check if tools present
        if (!checkTools(res)) return false;

        std::string browserVersion{};
        browserVersion = getBrowserVersion(false);
        std::string webDriverVersion{};
        webDriverVersion = getWebDriverVersion(false);
        bool sessionExist{false};

        //check if the driver is in progress
        if (isInProgress()) {
            //url response
            makeUrl();
            //update session for url
            if(!m_session->m_sessions.empty()){
                std::string port{};
                auto it = m_session->m_processes.find(m_driverName);

                if (it != m_session->m_processes.end()) {
                    port = m_session->m_processes[m_driverName].port;
                }
                for(auto& session: m_session->m_sessions){
                    if(session.webdriverPort == port && session.isWebDriver && !session.isEnded){
                        m_sessionId = session.id;
                        session.webdriverUrl = m_url;
                    }
                }
            }

            if(isBrowserWebDriverVersionMatch(browserVersion, webDriverVersion)) {
                if (isResponseExecuted) return true;
                printCapabilities(res);
                return true;
            }
            sessionExist=true;
        }

        if (!isBrowserWebDriverVersionMatch(browserVersion, webDriverVersion)) {
            if (sessionExist){
                stopDriver();
            }

            if (!downloadWebdriver()) {
                LD_LOG_ERROR << "Error download driver"<< std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_driver_download", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }

        makePath(type::DRIVER);

        //check if the driver is already running
        //if(Network::getAvailablePort(m_port,m_port,"127.0.0.1") == -1){
        if (Network::isPortInUse(m_port)) {
            //check if a port is available in the range
            m_port = Network::getAvailablePort(m_port, m_PortRangeEnd);
            if (m_port == -1) {
                LD_LOG_ERROR << "No port available in the range" << std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_port_range", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }

        //set the local list of ips
        setLocalListIpsBind(m_localListIps);

        //set the white list of ips remote access
        setWhiteListIps(m_localListIps);

        //define the url to connect to the driver
        makeUrl();

        //start the driver
        if (startDriver()) {
            if(isResponseExecuted) return true;
            printCapabilities(res);
            return true;
        } else {
            CrowJsonResponse jRes;
            LD_LOG_ERROR << "Error start driver" << std::endl;
            jRes.setMsgErrorCode("ats_error_driver_start", "", "");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

    }

    void Driver::deleteDriver(crow::response &res, std::string& port) {
        if (isInProgress()) {
            std::string pid{};
            std::string cmd = "lsof -i:" + port + " | awk 'NR==2 {print $2}'";
            //std::string cmd = "ss -tnlp | grep " + port;
//            LD_LOG_INFO<<m_driverName<< " : " << cmd << std::endl;
            std::regex pattern("([0-9]*)");
            std::smatch pidMatches;
            std::regex newLine("\n");

            if (ld::Utils::exec(cmd.c_str(), pid)) {
                pid = std::regex_replace(pid, newLine, "");
                //pid = std::regex_replace(pid, pattern, "");
                if(std::regex_search(pid, pidMatches, pattern)) {
                    pid = pidMatches[1].str();
                    cmd = "kill -9 " + pid + " > /dev/null 2>&1";
//                LD_LOG_INFO<<m_driverName<< " : " << cmd << std::endl;

                    ld::Utils::exec(cmd.c_str(), pid);
                    std::unique_lock<std::mutex> lock(ld::Session::m_mutex);
                    m_session->m_processes.erase(m_driverName);
                    lock.unlock();
                    bool isJoinable = true;
                    while (isJoinable) {
                        isJoinable = m_session->m_threads[port].joinable();
                    }
                    lock.lock();
                    m_session->m_threads.erase(port);
                    lock.unlock();
                }
            }
            else{
                LD_LOG_DEBUG<<m_driverName<< " : " << "No process found for port " << port<< std::endl;
            }
        }
        if(isResponseExecuted) return;
        json j;
        j["value"] = nullptr;
        res.code = 200;
        res.write(j.dump());
    }

    bool Driver::downloadWebdriver() {

        std::string latest_version = getBrowserVersion(false);
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        std::string extension=".tgz";
        currentPath += DIRSEPARATOR;

        std::string urlDownload = m_downloadUrl + latest_version +extension ;
        std::string cmdDownload = "curl -q -o " + currentPath + m_driverProcessName + extension +" " + urlDownload ; // -q quiet mode, -O output
        cmdDownload += " > /dev/null 2>&1";
        LD_LOG_DEBUG << "Download driver : " << cmdDownload << std::endl;
        std::thread t1(ld::Utils::exec, cmdDownload.c_str(), std::ref(result));
        t1.join();

        result="";
        cmdDownload = "du -k "+ currentPath + m_driverProcessName + extension +" | awk '{print $1}'";
        LD_LOG_DEBUG << "Check download : " << cmdDownload << std::endl;
        std::regex pattern("\\n");
        ld::Utils::exec(cmdDownload.c_str(), result);
        result = std::regex_replace(result, pattern, "");
        if(result == "0"){
            if(ld::Utils::fileExist(currentPath + m_driverProcessName)) return true;
            return false;
        }

        //unzip Driver
        //std::string cmdUnzip = "unzip -qo "+ currentPath + m_driverProcessName +".zip "+m_driverProcessName;
        //std::string cmdUnzip = "unzip -qo "+ currentPath + m_driverProcessName + extension +" "+m_driverProcessName;
       // std::string cmdUnTar = "tar -xzf "+ currentPath + m_driverProcessName + extension +" "+m_driverProcessName;
        std::string cmdUnTar = "tar -xzf "+ currentPath + m_driverProcessName + extension +" -C "+ currentPath;
        LD_LOG_DEBUG << "unTar driver : " << cmdUnTar << std::endl;
        std::thread t2(ld::Utils::exec, cmdUnTar.c_str(), std::ref(result));
        t2.join();


        //delete zip file
        std::string cmdDelete = "rm -f "+ currentPath + m_driverProcessName+ extension;
        LD_LOG_DEBUG << "Delete archive file : " << cmdDelete << std::endl;
        std::thread t3(ld::Utils::exec, cmdDelete.c_str(), std::ref(result));
        t3.join();

        //add execution attribute
        std::string cmdChmod = "chmod +x "+ currentPath + m_driverProcessName;
        LD_LOG_DEBUG << "Add execution attribute : " << cmdChmod << std::endl;
        std::thread t5(ld::Utils::exec, cmdChmod.c_str(), std::ref(result));
        t5.join();


        if(isBrowserWebDriverVersionMatch(latest_version, getWebDriverVersion(false))) return true;

        LD_LOG_WARNING << "different version of webDriver and browser" << std::endl;

        return false;
    }



    std::string Driver::getLocalListIpsBind() { return m_localListIpsBind; }

    bool Driver::setLocalListIpsBind(const std::vector<std::string>& localIps) {
        if(localIps.empty()) return false;
        int i = 0;
        for (auto &ip : localIps) {
            if(i>0) m_localListIpsBind += ",";
            m_localListIpsBind += ip;
            i++;
        }
        return true;
    }

    std::string Driver::getWhiteListIps() { return m_localListIpsBind; }

    bool Driver::setWhiteListIps(const std::vector<std::string>& localIps) {
        if(Session::getIsLocal()) {
            m_whiteListIps="";
            return true;
        }

        std::string localhost = "127.0.0.1";
        std::string globalIp = Session::getGlobalIps();
        std::set<string> allowedIpsSet;
        for(auto& ip: Utils::split(Session::getAllowedIps(), ",")){
            if(!ip.empty() && ip != localhost ) allowedIpsSet.insert(ip);
        }
        if(globalIp != "" && globalIp != localhost){
            allowedIpsSet.insert(Session::getGlobalIps());
        }

        if(!allowedIpsSet.empty()){
            std::vector<std::string> interfaceIpLocal = Network::getListIpsLocal();
            if(!interfaceIpLocal.empty()) {
                for (auto &ip: interfaceIpLocal) {
                    if (!ip.empty() && ip != localhost) allowedIpsSet.insert(ip);
                }
            }

            for(auto& ip: allowedIpsSet){
                if(!ip.empty()) {
                    if (!m_whiteListIps.empty()) m_whiteListIps += ",";
                    m_whiteListIps += ip;
                }
            }
            if (!m_whiteListIps.empty()) m_whiteListIps += ",";
            m_whiteListIps+=localhost;

        }
        else{
            m_whiteListIps = "";
        }

/*
        std::string ips = Session::getAllowedIps();
        if (localIps.empty() && m_session->getAllowedIps().empty()) return false;
        m_whiteListIps = m_session->getAllowedIps();
        return true;
*/
    return true;
    }

    void Driver::makeDriverArgslineCmd() {
        //m_driverArgslineCmd = "--silent";
        m_driverArgslineCmd += " --port=" + std::to_string(m_port);
        //m_driverArgslineCmd += " --bind-address=" + m_localListIpsBind;
//        LD_LOG_DEBUG << "makeDriverArgslineCmd --------------------------------: " << m_whiteListIps << std::endl;
        if(!m_whiteListIps.empty()) {
            m_driverArgslineCmd +=" --whitelisted-ips=" + m_whiteListIps ;
            m_driverArgslineCmd +=" --allowed-ips=" + m_whiteListIps ;
            m_driverArgslineCmd +=" --allowed-origins=" + m_whiteListIps ;

//            LD_LOG_DEBUG << "makedriver -> non vide whitelisted-ips : " << m_whiteListIps << std::endl;
        }

        if(ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")){
            std::string atsLogPath = ld::Config::tmpPath ;
            atsLogPath += DIRSEPARATOR;
            atsLogPath += "ats_log";
            atsLogPath += DIRSEPARATOR;
            atsLogPath += m_driverName + ".log";
            //m_driverArgslineCmd += " --log-path=" + atsLogPath;
            m_driverArgslineCmd += " --log-level=ALL" ;
        }
    }

    void Driver::makeUrl() {
//        m_url = "http://#HOST#:"+ std::to_string(m_port);
//        return;

        if(!m_remoteHeaderHostUrl.empty()) {
            size_t pos = m_remoteHeaderHostUrl.find(':');
            std::string hostname{};
            if(pos != std::string::npos) hostname = m_remoteHeaderHostUrl.substr(0, pos);
            else hostname = m_remoteHeaderHostUrl;
            std::string port = m_session->getProcessPort(m_driverName);
            if(!port.empty()) m_port = std::stoi(port);
            if(hostname == "localhost") hostname="127.0.0.1";
            m_url = "http://" + hostname + ":" + std::to_string(m_port);
        }
        else {
            std::vector<std::string> localIpsFull = Network::getListIpsLocal(true);
            std::string defaultGateway = Network::getDefaultGatewayIp();
            std::string defaultIpUrl{};

            for (const auto &ipFull: localIpsFull) {
                size_t p = ipFull.find('/');
                std::string ip = ipFull.substr(0, p);
                std::string mask = ipFull.substr(p + 1);
                std::string networkIp = Network::getIpNetwork(ip, mask);
                std::string networkRemoteIp = Network::getIpNetwork(m_remoteIp, mask);
                if (networkIp == networkRemoteIp) {
                    m_url = "http://" + ip + ":" + std::to_string(m_port);
                    return;
                }
                std::string networkDefaultGateway = Network::getIpNetwork(defaultGateway, mask);
                if (networkIp == networkDefaultGateway) {
                    defaultIpUrl = ip;
                }
            }

            if (!defaultIpUrl.empty()) {
                m_url = "http://" + defaultIpUrl + ":" + std::to_string(m_port);
                return;
            }

            if (!m_session->getRemoteHostname().empty()) {
                m_url = "http://" + m_session->getRemoteHostname() + ":" + std::to_string(m_port);
            } else {
                m_url = "http://localhost:" + std::to_string(m_port);
            }
        }
    }

    void Driver::printCapabilities(crow::response &res) {

        json details;
        //if first invocation use m_url
//        if (m_session->m_processes[m_driverName].url.empty())
            details["driverUrl"] = m_url;
//        else
//            details["driverUrl"] = m_session->m_processes[m_driverName].url;
        details["driverName"] = m_driverName;
        details["driverVersion"] = getWebDriverVersion(true);
        details["applicationVersion"] = getBrowserVersion(true);
        details["applicationPath"] = m_browserPath;
        json capabilities;
        capabilities["capabilities"] = details;
        json sessionId;
        sessionId["sessionId"] = getSessionId();
        json value;
        value["sessionId"] = sessionId["sessionId"];
        value["capabilities"] = capabilities["capabilities"];
        json j;
        j["value"] = value;
        res.code = 200;
        res.write(j.dump());
    }

    bool Driver::startDriver() {
        std::atomic<bool> isRunning{false};
        auto *response_ptr = new std::string();
        makeDriverArgslineCmd();
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;
        std::string cmd = currentPath + m_driverProcessName;
//        LD_LOG_INFO << "driver process name avant arg : " << m_driverProcessName << std::endl;
//        LD_LOG_INFO << "arg -------------------> " << m_driverArgslineCmd << std::endl;
        cmd += " " + m_driverArgslineCmd;

        std::function<void()> exec_func = [cmd, response_ptr, &isRunning, this]() {

//            if(m_driverName == "gecko" && ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")) {
            if( ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")) {
                std::string atsLogPath = ld::Config::tmpPath ;
                atsLogPath += DIRSEPARATOR;
                atsLogPath += "ats_log";
                atsLogPath += DIRSEPARATOR;
                atsLogPath += m_driverName + ".log";

                execDriver(cmd.c_str(), *response_ptr, m_driverName, m_port, m_url,isRunning,true, atsLogPath);
            }
            else {
                execDriver(cmd.c_str(), *response_ptr, m_driverName, m_port, m_url, isRunning);
            }

            delete response_ptr;
        };
        std::thread t1(exec_func);
        t1.detach();
        m_session->m_threads[std::to_string(m_port)] = std::move(t1);
        int retry=0;
        int maxTry=100;
        while((retry < maxTry) && !isRunning.load() ) {
            retry++;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        if ( !isRunning.load() ) {
            LD_LOG_ERROR << "Driver::startDriver: driver "<< m_driverName << " not started" << std::endl;
            return false;
        }
        if(m_driverName =="safari") {
            std::string pid{};
            std::string cmd = "lsof -i:" + std::to_string(m_port) + " | awk 'NR==2 {print $2}'";
            std::regex pattern("([0-9]*)");
            std::regex newLine("\n");
            std::smatch pidMatches;
            if (ld::Utils::exec(cmd.c_str(), pid)) {
                pid = std::regex_replace(pid, newLine, "");
                if (std::regex_search(pid, pidMatches, pattern)) {
                    return true;
                }
            }
            LD_LOG_ERROR << "Driver::startDriver: driver " << m_driverName << " not started" << std::endl;
            return false;
        }

        return true;
    }

    bool Driver::stopDriver() {
        if(!m_session->m_sessions.empty()){
            auto it = m_session->m_processes.find(m_driverName);
            std::string port{};
            port = m_session->getProcessPort(m_driverName);
            if (!port.empty()) {
                m_port = std::stoi(port);
                for(auto& session: m_session->m_sessions) {
                    if (session.webdriverPort == port && session.isWebDriver && !session.isEnded) {
                        m_sessionId = session.id;
                    }
                }
            }
            /*
            if (it != m_session->m_processes.end()) {
                port= m_session->m_processes[m_driverName].port;
                m_port = std::stoi(port);

                //search in sessions if there is a session with the same port
                for(auto& session: m_session->m_sessions){
                    if(session.webdriverPort == port && session.isWebDriver && !session.isEnded){
                        m_sessionId = session.id;
                    }
                }
            }
             */
            bool backupResponseExecuted = isResponseExecuted;
            //kill process
            isResponseExecuted=true;
            crow::response response{};
            deleteDriver(response, port);

            //delete session
            m_session->deleteSession(m_sessionId);
            isResponseExecuted=backupResponseExecuted;
            }

        return true;
    }

    void Driver::makePath(const type& t) {
        std::string response{};
        std::string cmd{};
        if(t == type::BROWSER) {
            if(m_jsonOptionsApplicationPath && !m_alternativeApplicationPath.empty()) {
                if (access(m_alternativeApplicationPath.c_str(), F_OK) != -1) {
                    m_browserPath = m_alternativeApplicationPath;
                    std::string path = m_browserPath;
                    size_t p{};
                    do {
                        p = path.find('/');
                        if (p == std::string::npos) m_browserName = path.substr(0, p);
                        path = path.substr(p + 1);
                    } while (p != std::string::npos);
                return;
                }
            }
            else cmd += "mdfind \"kMDItemCFBundleIdentifier =='" + m_browserBundleIdentifier + "'\" | head -n 1";
        }
        else
            cmd+="pwd";     //the driver is not installed in the system, it is in the same folder of the executable

        if (ld::Utils::exec(cmd.c_str(), response)) {
            //cleaning the response
            size_t p = response.find('\n');
            if (p != std::string::npos) {
                response = response.substr(0, p);
            }
        }
        if (t == type::BROWSER)
            m_browserPath = response+ "/Contents/MacOS/" + m_browserName;
        else
            m_driverPath = response + "/" + m_driverProcessName;
    }

    std::string Driver::getBrowserVersion(const bool& fullVersion) {
        std::string version ="not found";
        std::string result{};
//        std::string cmd = m_browserPath +"/Contents/MacOS/" + m_browserName;
        std::string cmd = m_browserPath ;
        ld::Utils::escapePath(cmd);
        cmd += " --version";
        if (ld::Utils::exec(cmd.c_str(), result)) {
            std::regex pattern(R"(\b(\d+)(\.\d+)+\b)");

            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                if(fullVersion) version = matches[0].str();
                else version = matches[1].str();
            }
        }
        return version;
    }

    std::string Driver::getWebDriverVersion(const bool& fullVersion) {
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;
        std::string driverPath = currentPath + m_driverProcessName;

        if(!ld::Utils::fileExist(driverPath)){return "not found"; }

        std::string cmd = driverPath + " --version";
        if(ld::Utils::exec(cmd.c_str(), result)){
            std::regex pattern(R"(\b(\d+)(\.\d+)+\b)");

            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                if(fullVersion) return matches[0].str();
                else return  matches[1].str();
            }

            /*
            std::regex pattern;
            if(fullVersion) pattern = R"(^ChromeDriver\s+(\d+\.\d+\.\d+\.\d+).*)";
            else pattern = R"(^ChromeDriver\s+(\d+)\..*)";
            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                return matches[1].str();
            }
            */
        }
        return "not found";
    }

    void Driver::setSessionId(const std::string &sessionId) { m_sessionId = sessionId; }

    std::string Driver::getSessionId() { return m_sessionId; }

    int Driver::getDriverPort() {return m_port; }

    std::string Driver::getDriverName() { return m_driverName; }

    std::string Driver::getDriverProcessName() { return m_driverProcessName; }

    std::string Driver::getDriverPath() { return m_driverPath; }

    std::string Driver::getDriverUrl() { return m_url; }

    std::string Driver::getBrowserName() { return m_browserName; }

    std::string Driver::getBrowserPath() { return m_browserPath; }

    std::string Driver::getRemoteIp() { return m_remoteIp; }

    double Driver::getWebDriverOffsetX() { return m_webDriverOffsetX; }

    double Driver::getWebDriverOffsetY() { return m_webDriverOffsetY; }

    double Driver::getWebDriverHeadlessOffsetX() { return m_webDriverHeadlessOffsetX; }

    double Driver::getWebDriverHeadlessOffsetY() { return m_webDriverHeadlessOffsetY; }

    std::string Driver::getIconBrowserBase64() { return m_iconBrowserBase64; }

    void Driver::setRemoteIp(const std::string& remoteIp) { m_remoteIp = remoteIp; }

    void Driver::setDriverRemoteHeaderUrl(const std::string &headerHostUrl) { m_remoteHeaderHostUrl = headerHostUrl; }

    bool Driver::getJsonOptionsApplicationPath(){return m_jsonOptionsApplicationPath;}

    void Driver::setJsonOptionsApplicationPath(const bool& jsonOptionsApplicationPath){m_jsonOptionsApplicationPath = jsonOptionsApplicationPath;}

    std::string Driver::getAlternativeApplicationPath(){ return m_alternativeApplicationPath; }

    void Driver::setAlternativeApplicationPath(const std::string& alternativeApplicationPath) { m_alternativeApplicationPath = alternativeApplicationPath; }

    bool Driver::isBrowserWebDriverVersionMatch(const std::string& browserVersion,const std::string& webDriverVersion){return browserVersion == webDriverVersion;}

} // namespace ld