#include "../../include/drivers/driverEdge.hpp"

namespace ld{
    Edge::Edge(){
        m_session = Session::getInstance();
        m_driverName = "msedge";
        m_driverProcessName = "msedgedriver";
        m_browserName = "Microsoft Edge";
        m_browserPath = "/Applications/Microsoft Edge.app/Contents/MacOS";
        m_browserBundleIdentifier = "com.microsoft.edgemac";
        m_defaultPort = 9710;   //default driver 9515
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="msedge/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAACXBIWXMAAA7EAAAOxAGVKw4bAAADAFBMVEVHcExDlrtz3kInV54jW6cwgdUxhNshVZ85dHIlSIwpRYwqR5Jguz4nSI8uftooRYdmxUQxg9s1g7xQseIwV3VWwPAzfbAgSoogTpRz3kJTpFJz3kMvgNIsetpy2kQsRZAxhNgiQ4Zv10src89YwuQseNpZuZVoznto0oNkzZdn0Isteclavq0jT5YnTJVQs+QkS5Jw2lVfyL5asE5VpDhlwzkkT5lkzZ4sec0wgdtx2kciUZp03kF030Jx209eulpv2Vpgwnlr1HYnS5Jiy6stedFWv/MoUJ8hSoopPn503kEoR4tWv/InSYwrUJsqcsgiSowpSJEjUZ0pSJEvfdEnZ6YmZLQvfNEoabhhxX4tTZ8mY7Esds4wSqI2hbomVaJex8QhXKJWv/E/kKFjyo4rdMErdcFcw7lFm69Wv+9ew5wufNpGocsxarc9kLotdrhWv/JLpro8jsFt1mpVu9xLqNNXwO0ud7pYwecxfbsseNRXwOs+k8ZWvOBRtNkgUJYwe7pUtLwkYaBu12NZweMlaLpTuNlbxNQ3h79x200tL3Fw2lNUu+law9gzhtpXwOxXwOlZwtxQrsNMq8w7jsAwfsc0gr8sedolWqkobcQhU5onZ74naL4reNoqctEreNord9kyhtsziNswg9szh9sfS4wyhdsfTpMvgNsfUZl030EgTI8gTZEfSYofT5Vx21BYweUgSosxhNsfUJcuf9ogUppu2GRx3Exv2Vxw2lYtfNpy3EofS41ZwuFZw91r1XRs1m5n0Yphy7BizKtkzp1fybhp04Jo0oZt1mpjzaVgyrRz3UNw2VkziNpw2lNz3UZYwekvfcgsetozh9pr1XJq03xq1Hhp035XwO9m0JFn0I5fyLxmz5Rt12dlz5ZjzaFz3Edex8Fu2GFu2F9XwOpdx8dcxsxdxslXwOxcxdFcxc5bxNUzhtVaw9pbxNMvfs5axNcxg9Eyhdhlzplz3UQgUpxhy6dfyL8wgM0sd8Rex8QvfMUteMAxg9UziNkue8J8Nx/hAAAAoXRSTlMAFsEa/f7+/QTBMo0zgPhJLrf9Nwn9/f78zQ2oZ6DZZ74PWP3J/iVI/FhoRzvYnz/j96VCGyTM9ELsdOew7PRQhJHnrrhOrJH1KYBf5HZbOvSa34f9Rv39/Pdv/P1o/bvzEvL5/aJg/fzs/eP9bf39tv39if0jzf7z/f16/P79/f1DH/CGLuh5/Mofm1+inZO2vUeKvM/V9ZlR/br8ssmLyHV8gOEAAAJvSURBVDjLY2AgATD29E9QUZnY294ogk1av0D0xYUNe/d9ePfqeYKRDoZuN9EbT4DSb1+/fPb04e2jh1zUUOSFfW7Atf/6fvTUocMnzTWR5ZNQtR8+ef/IvZtacPnSTHTtRz7fuXVQQx0qz1GGrP3U4W8g7QePH7vqJAVRUH1hQ0lOkZBQXF5hOkj7fZD2Y9ev7T9QDPFARW1N+f+LP378uPgnPwOu/dqVA3ebwUa01FX+/vjm37+z/z79ff9TLAuo/ThI+4ltj3YogVzQ1nDxzdlFQLBkycJPrD/FUqDatz0+s7MbqGBSx2+4/EJBmbTUZLD2u492nDm3vhOoYPKfj2eh0qy5ICtDI8Had+w8t363HJA/5eInqHxIPMRXjmDtO9dv331eEsid9uMfWHoZK3sURIE9RPv2B19OSwO5Uz9eBssvi94aC1FgDNH+4MuqVSBfKHwCS28KX/nVHaLAAaR99/nTq1bJcQC5fQvB8osjVn4NAvEZBAzB2oHytgIgvgxIetPixSxbt271Awnw2EG0r2IyBRvYxAbUDgTsK7dutWYGiZhYSTNJ2njxwCJbASy/Aqhg5UozRSyJUZsNKL1iBcvmlSs3b9blwqJCECS/JWzd5s2b162T0IMpEZHnhrKYLYHyW2LWrAOC5cvXrDFw5uYNDuQMEOeHGWHBt2XLll0sa5YvB8mvWb16z56lGzeK+yIsceXbtWtXVf2aNctB0qv3LAXJ8yI7wz8bqEJZYs/q1VDtGz28UR2aKLvr0iVl9o1LQQCondMTwy+tstNVVefMEl+7dq04Jz/WzKs4c/7cBfNmz5DvIiXLAwAvq4ynMfNhtAAAAABJRU5ErkJggg==";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
        /*
        m_webDriverOffsetX = -2;
        m_webDriverOffsetY = 28;
        m_webDriverHeadlessOffsetX = -2;
        m_webDriverHeadlessOffsetY = -58;
         */
    }
}//end ld

