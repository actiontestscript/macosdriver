#include "../../include/drivers/driverOpera.hpp"

namespace ld{
    Opera::Opera(){
        m_session = Session::getInstance();
        m_driverName = "opera";
        m_driverProcessName = "operadriver";
        m_browserName = "opera";
        m_browserPath = "/Applications/Opera.app/Contents/MacOS";
        m_browserBundleIdentifier = "com.operasoftware.Opera";
        m_defaultPort = 9710;       //default driver 9515
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="opera/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAB1FBMVEVHcEzaOi24MSblPS/pPjDpPjDLNirpPjDpPjDpPjCbKSDhPC6NJR2tLiOGIxvfOy6VJx7AMyflPS+vLiTeOy3jPC6jKyG/LiLpPjDdOi3jPC7pPjC/LiK/LiKGIxuXKB+JJByVJx6xLyTJNSnINSmvLiTXOSzWOSy+MifINSm3LCHALiK6LSHALiK/LiKGIBjALiJwGxTALiKqKR6MIhm+LiK8LSKkKB2/LiKBHxe7LSHBLiLALiK/LiKBHxdzGxSbJRzALiKtKh+SIxrBLiLALiJuGhSBHxfALiKuKh/ALiLBLiKmKB6/LiKRIxrALiK/LiJ3HRXBLiLBLiLAMye2MCWeJhy/LiLBLiLALiK6LSHALiK/LiKzKyDALiLBLyLALiLBLiK4LCF8Hha/LiLALiKmKB6xKyDBLiLALiJ/HxeWJBufJhzAMyfALiK/LiK/LiK/LiLpPjC/LiLoPS/UNinALiLnPS/mPS/DLyPFMCTdOSzCLyPfOizkPC7WNynWNinIMSXGMCTgOi3HMSTTNSjEMCPaOCvlPC7OMyfiOy3JMiXNMybMMybeOizYNyrmPC/ZOCrVNinBLiLbOCvQNCfRNSjWNyrhOy3LMibXNyrjOy4bdhGFAAAAcnRSTlMAawxUzqhr3bb3BFQEDARYBBVYDFRUDPCpWFjP5f0ICAgIN1paN1paNxVPymG54R+wDJtpDntcPvsWdI5v9RAOMstsEZi8AwhuQsSkI/oMtuMBd5Y2FR3+dWhHwfQpWoG6kzEE+NlGO4ZxJTUbN/7O7NyaJjA9AAAB5ElEQVQ4y5VTZ0MTURBMNMkl0rv03mwoYkFBxa6ADbA3LCggqDvHXUJC6N1eKX+WSUjI3YMv3JedvZm3+94Wh2M3X3ldrdNZM1S6M5te4PWJmH5dfN7Mfdv5HE1kbDEAGMtrpmh7FTrLLRJexdv+wYG+3qeBPyLuPVY+wyUyHSp+8Trq3b+JWV1c9Qk+hfxS6MGjuH/tCkZEXIkYjG9OoStx4tIRBJkl7iaJyAiKbllyHsCcKRK7aTLv/+M3rttufRDjItrma9MYYAKtTTbBIYzqIplR7KVgEkft7z53GtMi3ggsY/38QItSmTb8EvFFql7BAN+AU4qgEV9IdBNV0v4EGhTBCUySqCHy0I4D5xXBRYyScMYEE8AxRXB8S1BF+x1oVgRnMEOilqiaNghcUARn8ZVEHVEJn7kAnFQEhyP98pXHCzWFy3b+ah7GYoVy5Ed7deeuTXADAZa6IIpT2aywgcc2QXskg5a+6WQzxD88sfIdnSG/SE7c3c9uBPDcIriNecvAOHILOdFG3rOtH/fwlyOXlTjQQ0XQKHoVcx8WL3JoM6w5c5llZebN+w/E714a/3VxpyiFydZEn18e/vzx09xsWLSk7auVms/VM9eXFrh6ack7r2dJdZXHU1lRtquN3wA4fZTdJgG0KwAAAABJRU5ErkJggg==";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
        /*
        m_webDriverOffsetX = -2;
        m_webDriverOffsetY = 28;
        m_webDriverHeadlessOffsetX = 2;
        m_webDriverHeadlessOffsetY = 40;
*/
    }


    std::string Opera::getBrowserVersion(const bool& fullVersion) {
        std::string version ="not found";
        std::string result{};
        std::string cmd = m_browserPath ;
        ld::Utils::escapePath(cmd);
        cmd += " --version";

        if (ld::Utils::exec(cmd.c_str(), result)) {
            std::regex pattern;
            if(fullVersion) pattern = R"(^(\d+\.\d+\.\d+\.\d+).*)";
            else pattern = R"(^(\d+)\..*)";       /*> pattern use to find major version */
            std::smatch matches;
                if(std::regex_search(result, matches, pattern)){
                    version = matches[1].str();
                    int v = std::stoi(version);
                    v += 14;
                    version = std::to_string(v);
                }
        }
        return version;
    }

    std::string Opera::getWebDriverVersion(const bool& fullVersion) {
        std::string result{};
        std::string currentPath = ld::Config::currentPath;
        currentPath += DIRSEPARATOR;
        std::string driverPath = currentPath + m_driverProcessName;
        if(!ld::Utils::fileExist(driverPath)){return "not found"; }

        std::string cmd = driverPath + " --version";
        if(ld::Utils::exec(cmd.c_str(), result)){
            std::regex pattern;
            if(fullVersion) pattern = R"(^OperaDriver\s+(\d+\.\d+\.\d+\.\d+).*)";
            else pattern = R"(^OperaDriver\s+(\d+)\..*)";       /*> pattern use to find major version */
            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                return matches[1].str();
            }
        }
        return "not found";
    }

    bool Opera::execute(crow::response &res) {
        std::string applicationPath = Session::getBrowserAtsPropertiesPath(m_browserName);
        if(!applicationPath.empty()) m_browserPath = applicationPath;

        makePath(type::BROWSER);

        //check if the browser is installed
        if (!isInstalled()) {
            LD_LOG_ERROR << "Browser not found" << std::endl;
            CrowJsonResponse jRes;
            jRes.setMsgErrorCode("ats_error_browser_not_found", "", "");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

        //check if tools present
        if (!checkTools(res)) return false;

        std::string browserVersion{};
        browserVersion = getBrowserVersion(false);
        std::string webDriverVersion{};
        webDriverVersion = getWebDriverVersion(false);
        bool sessionExist{false};

        //check if the driver is in progress
        if (isInProgress()) {
            //url response
            makeUrl();
            //update session for url
            if(!m_session->m_sessions.empty()){
                std::string port{};
                auto it = m_session->m_processes.find(m_driverName);

                if (it != m_session->m_processes.end()) {
                    port = m_session->m_processes[m_driverName].port;
                }
                for(auto& session: m_session->m_sessions){
                    if(session.webdriverPort == port && session.isWebDriver && !session.isEnded){
                        m_sessionId = session.id;
                        session.webdriverUrl = m_url;
                    }
                }
            }

            if(isBrowserWebDriverVersionMatch(browserVersion, webDriverVersion)) {
                if (isResponseExecuted) return true;
                printCapabilities(res);
                return true;
            }
            sessionExist=true;
        }

        if (!isBrowserWebDriverVersionMatch(browserVersion, webDriverVersion)) {
            if (sessionExist){
                stopDriver();
            }

            if (!downloadWebdriver()) {
                LD_LOG_ERROR << "Error download driver"<< std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_driver_download", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }

        makePath(type::DRIVER);

        //check if the driver is already running
        //if(Network::getAvailablePort(m_port,m_port,"127.0.0.1") == -1){
        if (Network::isPortInUse(m_port)) {
            //check if a port is available in the range
            m_port = Network::getAvailablePort(m_port, m_PortRangeEnd);
            if (m_port == -1) {
                LD_LOG_ERROR << "No port available in the range" << std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_port_range", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }

//        m_port=9706;
        //set the local list of ips
        setLocalListIpsBind(m_localListIps);

        //set the white list of ips remote access
        setWhiteListIps(m_localListIps);

        //define the url to connect to the driver
        makeUrl();

        //start the driver
        if (startDriver()) {
            if(isResponseExecuted) return true;
            printCapabilities(res);
            return true;
        } else {
            CrowJsonResponse jRes;
            LD_LOG_ERROR << "Error start driver" << std::endl;
            jRes.setMsgErrorCode("ats_error_driver_start", "", "");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

    }

    void Opera::makeDriverArgslineCmd() {
        std::string logLevel = " --silent";
        m_driverArgslineCmd = "--port="+std::to_string(m_port);
        m_driverArgslineCmd += " --bind-address=" + m_localListIpsBind;
//        LD_LOG_DEBUG << "makeDriverArgslineCmd --------------------------------: " << m_whiteListIps << std::endl;
        if(!m_whiteListIps.empty()) {
            m_driverArgslineCmd +=" --whitelisted-ips=" + m_whiteListIps ;
            m_driverArgslineCmd +=" --allowed-ips=" + m_whiteListIps ;
            m_driverArgslineCmd +=" --allowed-origins=" + m_whiteListIps ;

//            LD_LOG_DEBUG << "makedriver -> non vide whitelisted-ips : " << m_whiteListIps << std::endl;
        }

        if(ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")){
            std::string atsLogPath = ld::Config::tmpPath ;
            atsLogPath += DIRSEPARATOR;
            atsLogPath += "ats_log";
            atsLogPath += DIRSEPARATOR;
            atsLogPath += m_driverName + ".log";
            //m_driverArgslineCmd += " --log-path=" + atsLogPath;
            logLevel = " --log-level=ALL" ;
        }
        m_driverArgslineCmd += logLevel;
    }
}//namespace ld

