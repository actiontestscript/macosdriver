#include "../../include/drivers/driverBrave.hpp"

namespace ld{
    Brave::Brave(){
        m_session = Session::getInstance();
        m_driverName = "brave";
        m_driverProcessName = "bravedriver";
        m_browserName = "Brave Browser";
        m_browserPath = "/Applications/Brave Browser.app/Contents/MacOS";
        m_browserBundleIdentifier = "com.brave.Browser";
        m_defaultPort = 9710;       //default driver 9515
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="brave/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAACXBIWXMAAA7EAAAOxAGVKw4bAAACvlBMVEVHcEzXYCoXCgSgRx9PIw8EAQD3cTT2bjDfZCwAAABNIg8UCQQCAQCeRx/YYSrdYystFAhRJBC1USMiDwYGAgEXCgShSB/3fUXbYiv2bjD1bjBOIw+vTiIVCQRfKhICAQCpTCHkZi1YJxF9OBgyFgkmEQfgZCyfRx8JAwHjZi0MBQJiLBO4UiRtMRVRJBBiKxN6NxgEAQBXJxH4fUX0hVP3hlKhSB+nSyG+aUIgDgYpEgjVdUnTXynVYCpXJxHNckjGbUTygk/3fkegRx/3eD62ZUDMXCiYRB74f0hLIQ7mfEuXUjT3dz3PXSn2g0/3fEP3djv4gEnuglH3fETof0/3f0fQdEnkfk/oaC7deEn3dTrfe07paS73eUD3eUD3bzH////4fUX2bjD1bjD+/v7+8OrqaS73cDL68Oz89vP3eD7+8+7je0vfZzD4jVz7vaH2f0n8zrn46+X+/Pv0bTDmg1bjgVTjgVX79fL5nHLiiWD79fP7ybLxqIbvbTHw1Mf89fHlcj79+fj58OvmglXlkGn82Mj2bzHlcj32bjHx1sn5mm79+vjxpYLkkWrway/iiWHwbDH7x6/puqTwbC/jl3TifE381sXkZy/36OH46+b5wqjifU74gUvkmnn3djz4hVHpuKLkZy7++vj3djv+9/T89/T4vaP35+H++vnkqo/47Ob6roz3bzLlrJL++ff3e0P8zbj6poD6t5n80r/3ekL5o3z96uH149v97OP818b93c76r43+8u382cn6rovhlnPjqY7+8uzfeEj9+vnhl3Tfdkb259/5pH3jqI368e3+7ufii2P6tZb+/PzijGX+7eb9+/ryrY3939H++Pb+7+nipYrtlWzxgk/0wqv5lWf4jFrtl3DvmHHio4f4jFvgiWL94NLxgU7yrIz5lmn0w638z7rvmXH++fbgiWEZB+SmAAAAX3RSTlMA4SSxYwj8/ukSYSMHr+LiA2SgAgUlsv7h/f1ilCRECJPrQ4kGBeqwAuoCOKJDZUKHBTb+WVuIiREBARTX2S07E1zMfuMQznzPKzIB5M9exfbNOcM3yjwu4DH3Ld/o5/4IBd8AAAH7SURBVDjLY2CgIjART4QCY1Ns8iEB8XAQFYQu6xoWHRePBGLD7S2Q5R0Cl2TFo4Cs+R7WSApC47EAJyQFMfHxW7fv25+3bWN2duGJvD27d26Ij49AyEeyxcdvSUABu9bGs3nBFQTHx28CChanQGWPABlr4uP9vaHyjunxc5cmHD+ae+xkUUJC0cHDuQd2JCxeEJ/uB1XgEh8/HaTxUH7B44SESwX5e0G8tvh4H6gCt/j4BojZDwsTEp5dhrAz2+N9oQrc4+Nbm8CCT68mJKTeBDN7+uLjnaEKPNnjk6sqQKJ3QcRzEFE9ITme3Q6qwNImPj6prDkhIeXGg3vXXj46l5CQUZ4UH29rBfOmGTDcklqA+lLvx8ffTgUySoHy8ebwcDAChWw3yOQXT25dAdGVIBFtuAJhCSB3Ekii+M71iyC6BijAL4AIawUgvxcofup8ScmZDCCjFigggxRZ+kD+xIWr1+WcvXB6c86qWZPrgAK6SApUNYECUxYtXw8yfuXsGf1ArqIKcooRAjkqad4ycBBOBXkhXgclySlJgcSS56xISJg5LRnElpRDUaBhwA42o6O+q7ETxGJX1kJNtYYsHOB0lpYGptT0RDCSvaw6Oyw1souJYssY8tIQQ+I5BFlx5C1uXqAh7MxMuHMfDxcjHwsr3gzKyUlqlgYA/ZjmeQe+USgAAAAASUVORK5CYII=";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
/*
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 26;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = -50;
        */
    }
}

