#include "../../include/drivers/driverChrome.hpp"

namespace ld{
    Chrome::Chrome(){
        m_session = Session::getInstance();
        m_driverName = "chrome";
        m_driverProcessName = "chromedriver";
        m_browserName = "Google Chrome";
        m_browserPath = "/Applications/Google Chrome.app/Contents/MacOS";
        m_browserBundleIdentifier = "com.google.Chrome";
        m_defaultPort = 9710;       //defaut driver 9515
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="chrome/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAADAFBMVEVHcEwAAACxMywAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADcUUPPMi3UKym/GR3RIyQuMg0oKwwrcTEAAACyhRqMaBQtIAgAAAAobzIAAAAAAADnXU3hVUbcSDzaPzfpZFLjXk3fPzjdODI+mjyMdAPXHCEvdS3SRxooZCZmfDTZtARBoj9Cpj81gTc7kD0lXilEqkHlvQYyejPdqxnGmRYzhTvxxgcfVCU7jzrUqBDSmx+5kRFBn0AKHQ2ygB3puw8JGwyXbBohXSq2gx7MkyEvfzn80glNuEnkNjP60AvlPjhMuElMtkn70QrhLC3jMjD0xRRKskffIifsXE3nQjvoRz5alc9lndTZHyNKtUc4gMJKsEjqUUVDhsbiLy5PjsvhKSpIq0fwwBZGo0ZHqEfqZFLpTUPoSUDkOTU1f8HgJCg/g8Uqdrf5zA76zw1Ls0lEqUIicK9IrkbvZ1blOzbiRyNGsUJLi8rgRiXotxXO1d/Q3+1vpdlpodbxxBDhSSnkshrG1ODzxwgvervuuxv2yRE7kj7grB1BmkPlsiHSmSHsWUvtYlLrVUjjRjznVUfqU0f620TdLSvI2ejjT0HbJCXgJilBnkH1yA4yfL11qtvquhaDzYH3zwjX5PBGiMjR3Od6r94sc633yg5BoUJVks04jj3hYGItebpfmdF6qdXbpR42dKnJ1+NJisnb6NzNGx+Or8nruB3P2uV1xXIdZKAcY59LqUNFoEbuvhfwwws/l0G5tyExhDvlUEPhPTbfNjH2zATgNTH92jf51zTcQxzSHSBbmD/hWFjy7dTiyMi747nhQ0OmRivsvRBMir5BhLuLtdzv6tH421HpzGSNtt3hWFfhvVwhbazi2srY2+H111Dp9ujHJCJsw2nXxRRpqWCJVy1Sn0DtzA1vqDQhbqyHtnOzz+fha2/Y1YVmtmbFPkDAFxvIGh5yXyy1JR+YJhrASEtZcjDp0tOzzueTZVKUtivXoCDdpyDrxRM2ijxYrEFunju0lSYAAACxAADUwKqdAAABAHRSTlMAJBQqCRkEAhQNPnVWlVqWNDqtEa17SC3BMjrws6Sk8LPx8X4c8RioHPF+u6i29Hjyu4z0tvTyfsHB9IzzRrvzSpiYx/T0/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wkU0R8XygAAAsdJREFUOMtjYKAiYOJWkE8FAmkZbiZs0rL7+oN7iouLe4L7p/TJoSth51Hsi+ntDgaC7t6YtND9SjzsKPK8B/aHpqelxUyYEBOTlh4a4nVoEi+SCnb+SR4ZXiEhoaHp6aGhISFeJR7+cVP54SrY+ab6xfl7ZJR4gUFJBlDaL2FaBB9MhZrmsWkJQCX+HmDgD5bOCopQ/Q+RZ9MpOxuRNS0hwc/P78yJa1Pq6+srKipKS0uPqrOBFWiUuR+/GxEUFJS1/FRVVdWtm9cbGipn70yqm9GkAnaBcpm7+8MNQBUvNl+ctXDhrKubrwAlcz3d3A5qgVzBoh3o7v7o/YaI5ZvDw2trampqaxfcdgODuYYsQAWcBpHu7u7v3rz9vCpsZTgQrAxbdQmiwNOME6iAo8AHqGD7h9fV6+KXhgHB0vjyeUfACnybOOAK3D99/JI9f115fHx8+fzY6omoCryBCgK/Po+Kyo4FguzsqMkQBZ67IArywUY8mNc8MzMzKioqM3Pm5MNgBXkQBTZFYAXudy7k5Kxobl6xIuf8SYgNi+3BCiygRsypXnSjEAieLlp7GSyfV2cJUsBptCQa7Ar3041rtm3dum3N2pcQFyw+Zw3yJotARyJUxZz1jRs3Nq5/9hgsnxtQKQAKKHZW3emJgRAV7ve2bHm1ZA/IfJC8CSs4wjkF9aYDzYC41D0w5clckP2LA2YbC3JCoptVvy05MTrfB2xK9Pb7vkDtrQEt7aKskOhmYGEUatuRAlbik1/0LS8PKJ20qV2EkQWW5DiZhZbtTgYqiY4u+v2zNSAgqaVhtQgzJyJRCjObmy7r2JGckpK8J2lny6bO1VaizMLIyZqT0VZi76+23R1/Ojvbf3zvshNj5ETNGCys4g5OEq57/3Z1uTg7iomzsrCj5S02TlZGSSkuIJCSZGTlZMOSO9lZODnAgBNZNwAB7h95t57bzwAAAABJRU5ErkJggg==";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
        /*
        m_webDriverOffsetX = 2;
        m_webDriverOffsetY = 6;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = -50;
         */
    }
}//end ld

