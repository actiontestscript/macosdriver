#include "../../include/drivers/driverSafari.hpp"

namespace ld{
    Safari::Safari(){
        m_session = Session::getInstance();
        m_driverName = "safari";
        m_driverProcessName = "safaridriver";
        m_driverPath = "/usr/bin/";
        m_browserName = "Safari";
        m_browserPath = "/Applications/Safari.app/Contents/MacOS";
        m_browserBundleIdentifier = "com.apple.Safari";
        m_defaultPort = 9710;       //default driver 9515
        m_port = m_defaultPort;
        m_localListIps = Network::getListIpsLocal();
        m_downloadUrl +="safari/";
        m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAADFklEQVR4nN1VW0tUURSWIiHwtd8SYVAP6uAllUyjIMqw6CIUVARZYBClzng3IkqdVBRCfUid8kETlcKazrGyzs69VUgISyG6WMLofPHtM4cc58iIb7Vhwebstb611rcuJyHhvz4TExOJ40IeNIVsN4QUhlA/bZGC30whC6izKfBX1lSeYakpUygMv55B88AcavoWUNu3oO/8xjfTUmr8vdq/YeCHwFZTKB+Ne8dmcaZ5EWleIKUCONYU1sJ7mg8461/UOtqRUF4AW+I6cMDv9H9BmtcGrB9e1qClvSta0quArnchZFYDqV6gqmdBOzEsWR6XFirWBeZx+G4YhU1heHzA9d4VpFbYWVBy6oDL3Sv6nlH2G0UlJhoez0cykbmu4CwW+WTKjIqRtwSXkRKhx01OXHuDwaw9yL75VWfbNzYLw1LTroW3u0VpXrNrIyBed2BPeQj3TpZBJSfhanGrzpIBFfsXnSzyY7m3ZAc7g8qdEyFc6rQpSFkjR0qn8DZnN2Z2JaJ7X4b+RgpZJzYDMQwh21yKKyfZfjpCH5BVHQ2cWh5Gw+k6LO3drsFHk3cg49Yv+22Vnn9wjg5EjANDqB/s86ONYZQ8io3+9gAw/uS5Bh/emYRzF/qj3mlDW84JsVwd8PF4cxg3AtEOCu8DSyFgenoaLwIBdOQdigmANrRlkKalvrk4kB8cilan7fEB1icbfGhoCO0Ds0hbA046qedQZFrScqtBOwvEQrFgLJw2GLXBR569xPmWRdfCsyHYGHSii2ypVjcHBU6bsuWofMoPSDWDnmGFzEp7RcRIpJXZ2sUP7DY1rMkDMQ6CweA2UyjJQePQpFcCT43PuNLxfd1BIziHkQHRJjD2kdFPrbthuRUZQX1gHkWNIWTX2DRd7FyJalvWhxQyS64TrhVnVRhC5riC/6VKeZ1l5/GGdepdEX6dZUcnrBMjX7PsyhLiHa5cQ8gKGnC3cPy5mmPWtdfmnLQ4m3RD63pV0XNZk9U/HM4Jha044vxwhJJxaVnv2IWX+dwt7G0OI8W+yzZ2C3U2Bf7PnD+zmgq7n0UOOwAAAABJRU5ErkJggg==";
      //  m_iconBrowserBase64 = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAACXBIWXMAAA7EAAAOxAGVKw4bAAACvlBMVEVHcEzXYCoXCgSgRx9PIw8EAQD3cTT2bjDfZCwAAABNIg8UCQQCAQCeRx/YYSrdYystFAhRJBC1USMiDwYGAgEXCgShSB/3fUXbYiv2bjD1bjBOIw+vTiIVCQRfKhICAQCpTCHkZi1YJxF9OBgyFgkmEQfgZCyfRx8JAwHjZi0MBQJiLBO4UiRtMRVRJBBiKxN6NxgEAQBXJxH4fUX0hVP3hlKhSB+nSyG+aUIgDgYpEgjVdUnTXynVYCpXJxHNckjGbUTygk/3fkegRx/3eD62ZUDMXCiYRB74f0hLIQ7mfEuXUjT3dz3PXSn2g0/3fEP3djv4gEnuglH3fETof0/3f0fQdEnkfk/oaC7deEn3dTrfe07paS73eUD3eUD3bzH////4fUX2bjD1bjD+/v7+8OrqaS73cDL68Oz89vP3eD7+8+7je0vfZzD4jVz7vaH2f0n8zrn46+X+/Pv0bTDmg1bjgVTjgVX79fL5nHLiiWD79fP7ybLxqIbvbTHw1Mf89fHlcj79+fj58OvmglXlkGn82Mj2bzHlcj32bjHx1sn5mm79+vjxpYLkkWrway/iiWHwbDH7x6/puqTwbC/jl3TifE381sXkZy/36OH46+b5wqjifU74gUvkmnn3djz4hVHpuKLkZy7++vj3djv+9/T89/T4vaP35+H++vnkqo/47Ob6roz3bzLlrJL++ff3e0P8zbj6poD6t5n80r/3ekL5o3z96uH149v97OP818b93c76r43+8u382cn6rovhlnPjqY7+8uzfeEj9+vnhl3Tfdkb259/5pH3jqI368e3+7ufii2P6tZb+/PzijGX+7eb9+/ryrY3939H++Pb+7+nipYrtlWzxgk/0wqv5lWf4jFrtl3DvmHHio4f4jFvgiWL94NLxgU7yrIz5lmn0w638z7rvmXH++fbgiWEZB+SmAAAAX3RSTlMA4SSxYwj8/ukSYSMHr+LiA2SgAgUlsv7h/f1ilCRECJPrQ4kGBeqwAuoCOKJDZUKHBTb+WVuIiREBARTX2S07E1zMfuMQznzPKzIB5M9exfbNOcM3yjwu4DH3Ld/o5/4IBd8AAAH7SURBVDjLY2CgIjART4QCY1Ns8iEB8XAQFYQu6xoWHRePBGLD7S2Q5R0Cl2TFo4Cs+R7WSApC47EAJyQFMfHxW7fv25+3bWN2duGJvD27d26Ij49AyEeyxcdvSUABu9bGs3nBFQTHx28CChanQGWPABlr4uP9vaHyjunxc5cmHD+ae+xkUUJC0cHDuQd2JCxeEJ/uB1XgEh8/HaTxUH7B44SESwX5e0G8tvh4H6gCt/j4BojZDwsTEp5dhrAz2+N9oQrc4+Nbm8CCT68mJKTeBDN7+uLjnaEKPNnjk6sqQKJ3QcRzEFE9ITme3Q6qwNImPj6prDkhIeXGg3vXXj46l5CQUZ4UH29rBfOmGTDcklqA+lLvx8ffTgUySoHy8ebwcDAChWw3yOQXT25dAdGVIBFtuAJhCSB3Ekii+M71iyC6BijAL4AIawUgvxcofup8ScmZDCCjFigggxRZ+kD+xIWr1+WcvXB6c86qWZPrgAK6SApUNYECUxYtXw8yfuXsGf1ArqIKcooRAjkqad4ycBBOBXkhXgclySlJgcSS56xISJg5LRnElpRDUaBhwA42o6O+q7ETxGJX1kJNtYYsHOB0lpYGptT0RDCSvaw6Oyw1souJYssY8tIQQ+I5BFlx5C1uXqAh7MxMuHMfDxcjHwsr3gzKyUlqlgYA/ZjmeQe+USgAAAAASUVORK5CYII=";
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 0;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = 0;
/*
        m_webDriverOffsetX = 0;
        m_webDriverOffsetY = 26;
        m_webDriverHeadlessOffsetX = 0;
        m_webDriverHeadlessOffsetY = -50;
        */
    }

    bool Safari::execute(crow::response &res) {
        makePath(type::BROWSER);

        //check if the browser is installed
        if (!isInstalled()) {
            LD_LOG_ERROR << "Browser not found" << std::endl;
            CrowJsonResponse jRes;
            jRes.setMsgErrorCode("ats_error_browser_not_found", "", "");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }

        //check if tools present
        if (!checkTools(res)) return false;

        std::string browserVersion{};
        browserVersion = getBrowserVersion(false);
        std::string webDriverVersion{};
        webDriverVersion = browserVersion;
        bool sessionExist{false};

        //check if the driver is in progress
        if (isInProgress()) {
            //url response
            makeUrl();
            //update session for url
            if(!m_session->m_sessions.empty()){
                std::string port{};
                auto it = m_session->m_processes.find(m_driverName);

                if (it != m_session->m_processes.end()) {
                    port = m_session->m_processes[m_driverName].port;
                }
                for(auto& session: m_session->m_sessions){
                    if(session.webdriverPort == port && session.isWebDriver && !session.isEnded){
                        m_sessionId = session.id;
                        session.webdriverUrl = m_url;
                    }
                }
            }

            if(browserVersion == webDriverVersion) {
                if (isResponseExecuted) return true;
                printCapabilities(res);
                return true;
            }
            sessionExist=true;
        }

        makePath(type::DRIVER);

        //check if the driver is already running
        //if(Network::getAvailablePort(m_port,m_port,"127.0.0.1") == -1){
        if (Network::isPortInUse(m_port)) {
            //check if a port is available in the range
            m_port = Network::getAvailablePort(m_port, m_PortRangeEnd);
            if (m_port == -1) {
                LD_LOG_ERROR << "No port available in the range" << std::endl;
                CrowJsonResponse jRes;
                jRes.setMsgErrorCode("ats_error_port_range", "", "");
                res.code = jRes.m_errorCode;
                res.write(jRes.jsonMsgError().dump());
                return false;
            }
        }

        //set the local list of ips
        setLocalListIpsBind(m_localListIps);

        //set the white list of ips remote access
        setWhiteListIps(m_localListIps);

        //define the url to connect to the driver
        makeUrl();

        //start the driver
        if (startDriver()) {
            if(isResponseExecuted) return true;
            printCapabilities(res);
            return true;
        } else {
            CrowJsonResponse jRes;
            LD_LOG_ERROR << "Error start driver" << std::endl;
            jRes.setMsgErrorCode("ats_error_driver_start", "", "");
            res.code = jRes.m_errorCode;
            res.write(jRes.jsonMsgError().dump());
            return false;
        }
    }

    bool Safari::startDriver() {
        std::atomic<bool> isRunning{false};
        auto *response_ptr = new std::string();
        std::string cmd = m_driverPath ;
        if(Session::getIsLocal()) {
        cmd += " --port=" + std::to_string(m_port);
//        LD_LOG_DEBUG << "safari IsLocal"<< std::endl;
        }
        else {
            //get port
            int proxyHttp2PortIn = m_port+1;
            if (Network::isPortInUse(proxyHttp2PortIn)) {
                //check if a port is available in the range
                proxyHttp2PortIn = Network::getAvailablePort(proxyHttp2PortIn, m_PortRangeEnd);
                if (proxyHttp2PortIn == -1) {
                    LD_LOG_ERROR << "No port available for proxyHttp2 in the range" << std::endl;
                    return false;
                }
            }
            cmd += " --port=" + std::to_string(proxyHttp2PortIn);
            //check if the proxyHttp2 is installed
            if(!proxyHttp2IsInstalled()) {
                LD_LOG_ERROR << "ProxyHttp2 not found." << std::endl;
                LD_LOG_ERROR << "for remote access to safari please install nghttp2" << std::endl;
                LD_LOG_ERROR << "brew install nghttp2" << std::endl;
                return false;
            }
            std::string argmumentProxyHttp2 = " --frontend='0.0.0.0,"+std::to_string(m_port);
            argmumentProxyHttp2 +=";no-tls' -b127.0.0.1," + std::to_string(proxyHttp2PortIn);
            argmumentProxyHttp2 +=" --host-rewrite --single-process ";
            argmumentProxyHttp2 +=" --log-level=INFO 2&>1 > /dev/null";
            std::function<void()> exec_func_proxyHttp2 = [this, argmumentProxyHttp2]() {
                std::string cmdProxyHttp2 = m_proxyHttp2Path  + argmumentProxyHttp2;
                LD_LOG_DEBUG << "cmdProxyHttp2 : " << cmdProxyHttp2 << std::endl;
                std::string result{};
                ld::Utils::exec(cmdProxyHttp2.c_str(), result);
            };
            std::thread tProxyHttp2(exec_func_proxyHttp2);
            tProxyHttp2.detach();
            m_session->m_threads[std::to_string(proxyHttp2PortIn)] = std::move(tProxyHttp2);

        }



        std::function<void()> exec_func = [cmd, response_ptr, &isRunning, this]() {

//            if(m_driverName == "gecko" && ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")) {
            if( ld::Config::logType == "FILE" && !(ld::Config::logWebDriverLevel.empty() || ld::Config::logWebDriverLevel == "NONE")) {
                std::string atsLogPath = ld::Config::tmpPath ;
                atsLogPath += DIRSEPARATOR;
                atsLogPath += "ats_log";
                atsLogPath += DIRSEPARATOR;
                atsLogPath += m_driverName + ".log";

                execDriver(cmd.c_str(), *response_ptr, m_driverName, m_port, m_url,isRunning,true, atsLogPath);
            }
            else {
                execDriver(cmd.c_str(), *response_ptr, m_driverName, m_port, m_url, isRunning);
            }

            delete response_ptr;
        };
        std::thread t1(exec_func);
        t1.detach();
        m_session->m_threads[std::to_string(m_port)] = std::move(t1);
        int retry=0;
        int maxTry=100;
        while((retry < maxTry) && !isRunning.load() ) {
            retry++;
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
        if ( !isRunning.load() ) {
            LD_LOG_ERROR << "Driver::startDriver: driver "<< m_driverName << " not started" << std::endl;
            return false;
        }
        else {
            LD_LOG_ALWAYS << "Safari was started successfully" << std::endl;
            return true;
        }

        //m_session->m_threads.emplace_back(std::move(t1));
        return true;
    }

    std::string Safari::getBrowserVersion(const bool& fullVersion) {
        std::string version ="not found";
        std::string result{};
        std::string pathInfoPlist;
        size_t pos = m_browserPath.rfind("MacOS");
        if(pos != std::string::npos){
            pathInfoPlist = m_browserPath.substr(0,pos);
        }
        else {
            pathInfoPlist = m_browserPath;
        }


//        std::string cmd = m_browserPath +"/Contents/MacOS/" + m_browserName;
        std::string cmd = "defaults read " + pathInfoPlist  + "Info.plist CFBundleShortVersionString";
//        ld::Utils::escapePath(cmd);
        if (ld::Utils::exec(cmd.c_str(), result)) {
            std::regex pattern(R"(\b(\d+)(\.\d+)+\b)");

            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                if(fullVersion) version = matches[0].str();
                else version = matches[1].str();
            }
        }
        return version;
    }

    std::string Safari::getWebDriverVersion(const bool& fullVersion) {
        std::string result{};
        std::string driverPath = "/usr/bin/" + m_driverProcessName;

        if(!ld::Utils::fileExist(driverPath)){return "not found"; }

        std::string cmd = driverPath + " --version";
        if(ld::Utils::exec(cmd.c_str(), result)){
            std::regex pattern(R"(\b(\d+)(\.\d+)+\b)");

            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                if(fullVersion) return matches[0].str();
                else return  matches[1].str();
            }

            /*
            std::regex pattern;
            if(fullVersion) pattern = R"(^ChromeDriver\s+(\d+\.\d+\.\d+\.\d+).*)";
            else pattern = R"(^ChromeDriver\s+(\d+)\..*)";
            std::smatch matches;
            if(std::regex_search(result, matches, pattern)){
                return matches[1].str();
            }
            */
        }
        return "not found";
    }

    void Safari::makePath(const type& t) {
        std::string response{};
        std::string cmd{};
        if(t == type::BROWSER) {
            if(m_jsonOptionsApplicationPath && !m_alternativeApplicationPath.empty()) {
                if (access(m_alternativeApplicationPath.c_str(), F_OK) != -1) {
                    m_browserPath = m_alternativeApplicationPath;
                    std::string path = m_browserPath;
                    size_t p{};
                    do {
                        p = path.find('/');
                        if (p == std::string::npos) m_browserName = path.substr(0, p);
                        path = path.substr(p + 1);
                    } while (p != std::string::npos);
                    return;
                }
            }
            else cmd += "mdfind \"kMDItemCFBundleIdentifier =='" + m_browserBundleIdentifier + "'\" | head -n 1";
        }
        else
            cmd+="pwd";     //the driver is not installed in the system, it is in the same folder of the executable

        if (ld::Utils::exec(cmd.c_str(), response)) {
            //cleaning the response
            size_t p = response.find('\n');
            if (p != std::string::npos) {
                response = response.substr(0, p);
            }
        }
        if (t == type::BROWSER)
            m_browserPath = response+ "/Contents/MacOS/" ;
        else
            m_driverPath  +=  m_driverProcessName;
    }

    bool Safari::proxyHttp2IsInstalled() {
        std::string cmd = "whereis " + m_proxyHttp2Name + " | grep -o '/[^ ]*' | head -n 1";
        std::string response{};
        if (ld::Utils::exec(cmd.c_str(), response)) {
            //cleaning the response
            if(response.empty()) return false;
            size_t p = response.find('\n');
            if (p != std::string::npos) {
                response = response.substr(0, p);
            }
            m_proxyHttp2Path = response ;
            LD_LOG_DEBUG << "proxyHttp2 path : " << m_proxyHttp2Path << std::endl;
            return true;
        }
        return false;
    }

    bool Safari::proxyHttp2IsRunning() {
        if(!proxyHttp2IsInstalled()) return false;
        std::string cmd = "ps -ef | grep " + m_proxyHttp2Name + " | grep -v grep | awk '{print $2}'";
        std::string response{};
        if (ld::Utils::exec(cmd.c_str(), response)) {
            //cleaning the response
            if(!response.empty()) return true;
        }
        return false;
    }
}

