//
// Created by eric on 07/02/23.
//
#include "../../include/lineCmdOption/options.hpp"
namespace ld {
    /*!
    *\brief Constructor
    * Construcor of OptionsData
    */
    OptionsData::OptionsData() {
        string key, value, help, regexValidate;
        int errorNumber = 0;
        bool visibility = true, isCmdLineSet = false;

        //port
        key = value = help = regexValidate = "";
        key = "port";
        value = "9700";
        help = "--port=PORT \t\tport to listen on. port between (1024-65535)";
        regexValidate = "^(?:102[4-9]|10[3-9][0-9]|1[4-9][0-9][0-9]|[2-9][0-9]{3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$";
        errorNumber = 2;
        visibility = true;
        OptionsStruct::strOptions p{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, p));

        //bind-address
        key = value = help = regexValidate = "";
        key = "bind-address";
        value = "127.0.0.1";
        help = "--bind-address=LIST \tcomma-separated allowList ip address of the server";
        regexValidate = "^((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]))((,(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(,(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])))*$";
        errorNumber = 3;
        visibility = false;
        OptionsStruct::strOptions allowed_ips{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, allowed_ips));

        //version
        key = value = help = regexValidate = "";
        key = "version";
        value = std::to_string(MACOSDRIVER_VERSION_MAJOR) + "." + std::to_string(MACOSDRIVER_VERSION_MINOR);
        help = "--version\t\tprint the version number and exit";
        regexValidate = "^[0-9]*(\\.)[0-9]*$";
        errorNumber = 1;
        visibility = true;
        OptionsStruct::strOptions version{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, version));

        //docker all webdriver
        key = value = help = regexValidate = "";
        key = "allwebdriver";
        value = "false";
        help = "--allWebDriver=true\tdownload all Web Driver";
        regexValidate = "^true|false$";
        errorNumber = 6;
        visibility = true;
        OptionsStruct::strOptions allWebDriver{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, allWebDriver));

        //loglevel
        key = value = help = regexValidate = "";
        key = "loglevel";
        value = "silent";
        help = "--loglevel=silent\t\tpossible value: all,debug, info, error, warning, silent \n";
        regexValidate = "^all|debug|info|error|warning|silent$";
        errorNumber = 7;
        visibility = false;
        OptionsStruct::strOptions loglevel{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, loglevel));


        key = value = help = regexValidate = "";
        key = "local";
        value = "false";
        help = "--local\t\tExecute l'agent webServer en local";
        regexValidate = "^true|false$";
        errorNumber = 8;
        visibility = true;
        OptionsStruct::strOptions local{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, local));

        //global-ips
        key = value = help = regexValidate = "";
        key = "globalip";
        value = "127.0.0.1";
        help = "--globalIp=IPv4\tpublic Ip access on remote server";
        regexValidate = "^((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]))((,(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(,(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])))*$";
        errorNumber = 9;
        visibility = true;
        OptionsStruct::strOptions globalIp{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, globalIp));

        //allowedips
        key = value = help = regexValidate = "";
        key = "allowedips";
        value = "127.0.0.1";
        help = "--allowedIps=LIST \tcomma-separated allowed list of remote IP address which are allowed to connect to system driver";
        regexValidate = "^((?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)|(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9]))((,(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))|(,(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])))*$";
        errorNumber = 9;
        visibility = true;
        OptionsStruct::strOptions allowedIps{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, allowedIps));

        //silent
        key = value = help = regexValidate = "";
        key = "silent";
        value = "false";
        help = "--silent\t\tlog nothing (equivalent to --logLevel=silent)";
        regexValidate = "^true|false$";
        errorNumber = 11;
        visibility = true;
        OptionsStruct::strOptions silent{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, silent));

        //help
        key = value = help = regexValidate = "";
        key = "help";
        value = "false";
        help = "--help\t\tprint this help and exit";
        regexValidate = "^true|false$";
        errorNumber = 12;
        visibility = true;
        OptionsStruct::strOptions optHelp{key, value, help, regexValidate, errorNumber, visibility, isCmdLineSet};
        m_listOptions.insert(pair<string, OptionsStruct::strOptions>(key, optHelp));

    }

    bool OptionsData::setOptionsIsCmdSet(const string &key, const bool &value) {
        if (m_listOptions.find(key) != m_listOptions.end()) {
            m_listOptions[key].isCmdLineSet = value;
            return true;
        }
        return false;
    }
}