//
// Created by eric on 07/02/23.
//
#include "../../include/lineCmdOption/optionLineCmd.hpp"
namespace ld {
    OptionLineCmd::OptionLineCmd(int argc, char **argv) {
        m_argc = argc;
        m_options = (m_argc > 1);
        m_optionsError = false;
        std::copy(argv, argv + argc, std::back_inserter(m_argv));
    }

    OptionLineCmd::OptionLineCmd(int argc, char **argv, map<string, OptionsStruct::strOptions> listOptions,
                                 Errors errors) : OptionLineCmd(argc, argv) {
        m_errors = errors;
        m_listArgv = listOptions;
        m_numError = 0;
        m_optionsError = readOptions();
    }


    void OptionLineCmd::setOptions(const string &key, const string &value, const string &detailHelp,
                                   const string &regValidate) {
        OptionsStruct::strOptions aOptions{key, value, detailHelp, regValidate};
        if (m_listArgv.find(key) != m_listArgv.end())
            m_listArgv[key] = aOptions;
        else
            m_listArgv.insert(std::pair<string, OptionsStruct::strOptions>(key, aOptions));
    }

    bool OptionLineCmd::setOptionsValue(const string &key, const string &value) {
        if (m_listArgv.find(key) != m_listArgv.end()) {
            if(!empty(value)) m_listArgv[key].value = value;
            return true;
        }
        return false;
    }

    bool OptionLineCmd::setOptionsIsCmdSet(const string &key, const bool &value) {
        if (m_listArgv.find(key) != m_listArgv.end()) {
            m_listArgv[key].isCmdLineSet = value;
            return true;
        }
        return false;
    }

    bool OptionLineCmd::getOptionIsCmdSet(const string &key) {
        return (m_listArgv.find(key) != m_listArgv.end()) ? m_listArgv[key].isCmdLineSet : false;
    }

    string OptionLineCmd::getOptionValue(const string &key) {
        return (m_listArgv.find(key) != m_listArgv.end()) ? m_listArgv[key].value : "";
    }

    bool OptionLineCmd::readOptions() {

        string options{};
        bool argPar = false;
        if (m_argc == 2 && (m_argv[1] == "-h" || m_argv[1] == "--h" || m_argv[1] == "/h" || m_argv[1] == "-help" ||
                            m_argv[1] == "--help" ||m_argv[1] == "/help" || m_argv[1] == "-?" || m_argv[1] == "/?")) {
            LD_LOG_ALWAYS << "Usage : ./macosdriver [OPTIONS]\nOptions"<<std::endl;
            for (auto &a: m_listArgv) {
                if (a.second.visibility)
                    LD_LOG_ALWAYS<<"  "<< a.second.detailsHelp << std::endl;
            }
            LD_LOG_ALWAYS<< std::endl;
            m_numError = 0;
            return true;
        } else if (m_argc == 2 && (Utils::toLower(m_argv[1]) == "--version")) {
            LD_LOG_ALWAYS << "MacosDriver " << MACOSDRIVER_VERSION_MAJOR << "." << MACOSDRIVER_VERSION_MINOR << "." << MACOSDRIVER_VERSION_PATCH << std::endl;
            m_numError = 0;
            return true;
        } else if (m_argc > 1) {
            for (int i = 1; i < m_argc; i++) {
                size_t p;
                p = m_argv[i].find('=');
                if (p != string::npos) {
                    options = m_argv[i].substr(2, p - 2);
                    options = Utils::toLower(options);
                    if(options == "port") setPortArgument(true);
                    if (!setOptionsValue(options, m_argv[i].substr(p + 1))) {
                        m_numError = 1;
                        return true;
                    }
                } else {
                    options = m_argv[i].substr(2, p - 2);
                    options = Utils::toLower(options);
                    if(options == "local" || options == "silent" || options == "allwebdriver") {
                        setOptionsValue(options, "true");
                        continue;
                    }

                    m_numError = 1;
                    return true;
                }
            }

            return __checkInput();
        }

        return false;
    }


    bool OptionLineCmd::__checkInput() {
        for (auto it = m_listArgv.begin(); it != m_listArgv.end(); ++it) {
//    for (map <string, OptionsStruct::strOptions>::iterator it = m_listArgv.begin(); it != m_listArgv.end(); ++it) {
            std::regex pattern(it->second.regValidate);
            if (!std::regex_match(it->second.value, pattern)) {
                m_errors.displayError(it->second.errorNumber);
                return true;
            }
        }

        return false;
    }
}

