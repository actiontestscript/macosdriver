//
// Created by eric on 20/02/23.
//
#include "../../include/crowSettings/crowJsonResponse.hpp"

namespace ld{
    CrowJsonResponseDataErrorCode::CrowJsonResponseDataErrorCode(){
        m_errorMap = {
                {"element_click_intercepted", {400,"element click intercepted" ,"The Element Click command could not be completed because the element receiving the events is obscuring the element that was requested clicked."}},
                {"element_not_interactable", {400,"element not interactable" ,"A command could not be completed because the element is not pointer- or keyboard interactable."}},
                {"insecure_certificate", {400,"insecure certificate" ,"Navigate caused the user agent to hit a certificate warning, which is usually the result of an expired or invalid TLS certificate."}},
                {"invalid_argument", {400,"invalid argument" ,"The arguments passed to a command are either invalid or malformed."}},
                {"invalid_cookie_domain", {400,"invalid cookie domain" ,"An illegal attempt was made to set a cookie under a different domain than the current page."}},
                {"invalid_element_state", {400,"invalid element state" ,"A command could not be completed because the element is in an invalid state, e.g. attempting to click an element that is no longer attached to the document."}},
                {"invalid_selector", {400,"invalid selector" ,"Argument was an invalid selector (e.g. XPath/CSS)." }},
                {"invalid_session_id", {404,"invalid session id" ,"Occurs if the given session id is not in the list of active sessions, meaning the session either does not exist or that it’s not active."}},
                {"javascript_error", {500,"javascript error" ,"An error occurred while executing JavaScript supplied by the user."}},
                {"move_target_out_of_bounds", {500,"move target out of bounds" ,"The target for mouse interaction is not in the browser’s viewport and cannot be brought into that viewport."}},
                {"no_such_alert", {404,"no such alert" ,"An attempt was made to operate on a modal dialog when one was not open."}},
                {"no_such_cookie", {404,"no such cookie" ,"No cookie matching the given path name was found amongst the associated cookies of the current browsing context’s active document."}},
                {"no_such_element", {404,"no such element" ,"An element could not be located on the page using the given search parameters."}},
                {"no_such_frame", {404,"no such frame" ,"A request to switch to a frame could not be satisfied because the frame could not be found."}},
                {"no_such_window", {404,"no such window" ,"A request to switch to a different window could not be satisfied because the window could not be found."}},
                {"no_such_shadow_root", {404,"no such shadow root" ,"The shadow root of the element could not be found."}},
                {"script_timeout_error", {500,"script timeout" ,"A script did not complete before its timeout expired."}},
                {"session_not_created", {500,"session not created" ,"A new session could not be created."}},
                {"stale_element_reference", {404,"stale element reference" ,"An element command failed because the referenced element is no longer attached to the DOM."}},
                {"detached_shadow_root",{404,"Detached shadow root" ,"The shadow root of the element is detached."}},
                {"timeout", {500,"timeout" ,"An operation did not complete before its timeout expired."}},
                {"unable_to_set_cookie", {500,"unable to set cookie" ,"A request to set a cookie’s value could not be satisfied."}},
                {"unable_to_capture_screen", {500,"unable to capture screen" ,"A screen capture was made impossible."}},
                {"unexpected_alert_open", {500,"unexpected alert open" ,"A modal dialog was open, blocking this operation."}},
                {"unknown_command", {404,"unknown command" ,"The requested command matched a known URL but did not match an method for that URL."}},
                {"unknown_error", {500,"unknown error" ,"An unknown server-side error occurred while processing the command."}},
                {"unknown_method", {405,"unknown method" ,"The requested command matched a known URL but did not match an method for that URL."}},
                {"unsupported_operation", {500,"unsupported operation" ,"Indicates that a command that should have executed properly cannot be supported for some reason."}},

                //ats error
                {"ats_error_browser_not_found", {404,"browser not found","web browser not found or not installed"}},
                {"ats_error_driver_download", {404,"error download web driver","web driver download error compatible with your browser"}},
                {"ats_error_driver_not_found", {404,"web driver not found","web driver not found for your browser"}},
                {"ats_error_unzip_not_found",{404,"unzip not installed","unzip not found or not installed"}},
                {"ats_error_wget_not_found",{404,"wget not installed","wget not found or not installed"}},
                {"ats_error_curl_not_found",{404,"curl not installed","curl not found or not installed"}},
                {"ats_error_driver_start",{404,"error drivers start","unknown error driver start"}},
                {"ats_snap_firefox",{404,"firefox snap install","the greckodriver dont work with firefox snap installation."}},
                {"ats_error_remoteDriver_incorrect",{404,"remoteDriver name incorrect","RemoteDriver name is incorrect"}},
                {"ats_error_port_range",{404,"error port range","port range unavailable"}},
                {"ats_error_profile_userProfile_not_found",{404,"userProfile not found in Json",""}},
                {"ats_error_profile_browserName_not_found",{404,"browserName not found in Json",""}},
                {"ats_error_profile_userProfile_directory_not_exist",{404,"userProfile directory not exists",""}},
                {"ats_error_profile_userProfile_directory_not_writable",{404,"userProfile directory is not writable",""}},
                {"ats_error_profile_userProfile_directory_not_created",{404,"userProfile directory is not created",""}},
                {"ats_error_profile_homePath_not_found",{404,"system homePath not found",""}},
                {"ats_error_profile_firefox_directory_not_found",{404,"firefox profile directory not found ",""}},
                {"ats_not_enough_space",{507,"not enough space available ",""}},
                {"ats_directory_not_writable",{403,"directory is not writable ",""}},
                {"ats_directory_not_created",{403,"directory is not created ",""}}

        };
    }

    CrowJsonResponseDataErrorCode::ErrorCode CrowJsonResponse::getErrorCode (const std::string& errorCode){
        return m_lisErrorCodes.getErrorMap()[errorCode];
    }

    json CrowJsonResponse::_jsonMsgError(){
        json j;
        json valueData;
        json errorValue;
        errorValue["error"] = m_errorCode;
        errorValue["message"] = m_errorMessage;
        errorValue["stacktrace"] = m_stackTrace;
        valueData["error"] = errorValue;
        j["value"] = valueData;
        return j;
    }

    void CrowJsonResponse::_setMsgErrorCode(const std::string& errorStrCode,const std::string& errorMessage, const std::string& stackTrace){
        auto errorCode = getErrorCode(errorStrCode);
        m_errorCode = errorCode.httpCode;
        m_errorMessage = errorCode.jsonErrorCode;
        m_stackTrace = stackTrace;
    }

    bool CrowJsonResponse::_isValidJson(const std::string& body)  {
        try{
            json j;
            if (json::accept(body)) {
                j = json::parse(body);
                return true;
            } else {
                return false;
            }
        }
        catch (json::parse_error& e){
            // output exception information
            std::string stacktrace = "message: " + std::string(e.what()) + "\n" + "exception id: " + std::to_string(e.id) + "\n" + "byte position of error: " + std::to_string(e.byte) + "\n" ;
            _setMsgErrorCode("invalid_argument","",stacktrace);
            return false;
        }
    }

    bool CrowJsonResponse::isJson(const std::string &body) {
        try{
            json j;
            return (json::accept(body));
        }
        catch (json::parse_error& e){
            return false;
        }
    }

}
