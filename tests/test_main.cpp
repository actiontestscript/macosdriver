#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_all.hpp>
#include "utils/test_utils.cpp"

int main(int argc, char* argv[])
{
    Catch::Session session;

    int returnCode = session.applyCommandLine(argc, argv);
    if(returnCode != 0)
        return returnCode;

    int numFailed = session.run();

    return numFailed;
}



