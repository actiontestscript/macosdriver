#define CATCH_CONFIG_MAIN
#include <catch2/catch_test_macros.hpp>
#include "../../include/utils/utils.hpp"

// Test cases for Utils class
#include "test_applicationExist.cpp"
#include "test_exec.cpp"
#include "test_fileExist.cpp"
#include "test_directoryExist.cpp"